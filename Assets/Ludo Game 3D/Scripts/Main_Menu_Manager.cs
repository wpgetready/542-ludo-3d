﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class Main_Menu_Manager : MonoBehaviour 
{
	public GameObject localSetupScreen;
	public GameObject creditsScreen;
	public GameObject settingsScreen;
	public Button soundButton;
	public Sprite soundOn,soundOff;
	private bool isSoundOn = true;
	public Toggle highGraphicsToggle;
	public RawImage splashScreen;
	public float splashFadeOutSpeed = 10.0f;
	private bool startRemovingSplash = false;
	public AudioSource background;
	void Start()
	{
		if (!Game_Manager._gminstance.hasSplashShown) {
			splashScreen.gameObject.SetActive (true);
			Invoke ("RemoveSplash", 2.0f);
		}
		else
		splashScreen.gameObject.SetActive (false);
		localSetupScreen.SetActive (false);
		creditsScreen.SetActive (false);
		settingsScreen.SetActive (false);
		highGraphicsToggle.onValueChanged.AddListener ((value)=> HGToggleListener(value));
		if (!Game_Manager._gminstance.isSoundOn)
			ToggleSound ();
	}
	void Update()
	{
		if (startRemovingSplash && splashScreen.color.a >= 0)
			splashScreen.color -= new Color (0, 0, 0, splashFadeOutSpeed * Time.deltaTime);
		if(startRemovingSplash && splashScreen.color.a <= 0)
			splashScreen.gameObject.SetActive (false);
			
	}
	public void NewGame()
	{
		localSetupScreen.SetActive (true);
	}
	public void PACPU()
	{
		Game_Manager._gminstance.SetPlayers (1);
		StartLocalGame ();
	}
	public void Credits()
	{
		creditsScreen.SetActive (!creditsScreen.activeSelf);
	}
	public void Settings()
	{
		settingsScreen.SetActive (!settingsScreen.activeSelf);	
	}

	public void Exit()
	{
		Application.Quit ();
	}

	private void StartLocalGame()
	{
		Game_Manager._gminstance.ChangeArena ();
		SceneManager.LoadScene (1);
	}

	public void BackToMainScreen()
	{
		localSetupScreen.SetActive (false);
	}

	public void Local2P()
	{
		Game_Manager._gminstance.SetPlayers (2);
		StartLocalGame ();
	}
	public void Local3P()
	{
		Game_Manager._gminstance.SetPlayers (3);
		StartLocalGame ();
	}
	public void Local4P()
	{
		Game_Manager._gminstance.SetPlayers (4);
		StartLocalGame ();
	}
	public void ToggleSound()
	{
		isSoundOn = !isSoundOn;
		if (isSoundOn) {
			soundButton.image.sprite = soundOn;	
			Game_Manager._gminstance.isSoundOn = true;
			background.Play ();
		} else if (!isSoundOn) {
			soundButton.image.sprite = soundOff;
			Game_Manager._gminstance.isSoundOn = false;
			background.Stop ();
		}
	}

	void HGToggleListener(bool flag)
	{
		if (flag) {
			QualitySettings.SetQualityLevel (2);
			Debug.Log ("Quality Settings Changed to High "+QualitySettings.GetQualityLevel());
		} else if (!flag) {
			QualitySettings.SetQualityLevel (0);
			Debug.Log ("Quality Settings Changed to Low "+QualitySettings.GetQualityLevel());
		}
	}

	void RemoveSplash()
	{
	//	splashScreen.gameObject.SetActive (false);
		startRemovingSplash = true;
		Game_Manager._gminstance.hasSplashShown = true;
	}
}