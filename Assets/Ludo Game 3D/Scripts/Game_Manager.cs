﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game_Manager : MonoBehaviour {

	/*THE SOLE PURPOSE OF THIS SCRIPT IS TO TELL THE MAIN GAME HOW MANY PLAYERS ARE PLAYING*/
	[SerializeField]int noOfPlayers=2;
	private static Game_Manager gmInstance;
	public static Game_Manager _gminstance{get{ return gmInstance;}} 
	public int arenaIndex=0;
	public bool isSoundOn = true;
	public bool hasSplashShown = false;
	void Start()
	{
		if(gmInstance!= null && gmInstance!= this){
			Destroy (this.gameObject);
		}
		else
		{
			gmInstance = this;
		}
		DontDestroyOnLoad (this.gameObject);
		arenaIndex = Random.Range (0, 2);
	}

	void Update()
	{
		if (arenaIndex > 1)
			arenaIndex = 0;
	}
	public void SetPlayers(int i)
	{
		noOfPlayers = i;
		Debug.Log ("Game Started for " + i + " players.");
	}

	public int GetPlayers()
	{
		return noOfPlayers;
	}

	public void ChangeArena()
	{
		arenaIndex += 1;
	}
}
