﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Color_Manager_Script : MonoBehaviour 
{
	public string myName = "";																//The name of the color (required to give notifications on turns)
	public bool amIPlayin = false;															//Why turn my gotiya on if I am not playin
	public GameObject[] meriGotiya = new GameObject[4];										//We must cache all our Gotiya
	public Dice_Script dc;																	//Dice script is needed to roll the dice , to stop it, and to take diceInput
	public Turn_Manager tm;																	//Needed to change turns
	public int diceInput = 0;
	public bool canIPlay;
	public Raycast_Manager rm;
	public Transform[] myPath;
	public Transform[] safePoints;
	public int noOfPlaying = 0;
	public bool CPU = false;
	public int noOfGotiyaWin;
	public GameObject OnWinObj;
	public bool haveWon=false;
	public bool haveLost= false;
	public AudioSource mukkeKiAwaaz;
	public Camera greenLane, redLane, yellowLane, blueLane;
	public Camera myMainCamera;
	void Start()
	{
		OnWinObj.SetActive (false);
		/*Lets Activate the button and pieces for players who are actually playing*/
		#region NOOFPLAYERS
		if (Game_Manager._gminstance.GetPlayers () == 1) {
			if (myName == "Green")
				amIPlayin = true;
			else if (myName == "Blue") {
				amIPlayin = true;
				CPU = true;
			}
			else if (myName == "Yellow")
				amIPlayin = false;
			if (myName == "Red")
				amIPlayin = false;
		}
		else if (Game_Manager._gminstance.GetPlayers () == 2) {
			if (myName == "Green")
				amIPlayin = true;
			else if (myName == "Blue")
				amIPlayin = true;
			else if (myName == "Yellow")
				amIPlayin = false;
			if (myName == "Red")
				amIPlayin = false;
		} else if (Game_Manager._gminstance.GetPlayers () == 3) {
			if (myName == "Green")
				amIPlayin = true;
			else if (myName == "Blue")
				amIPlayin = true;
			else if (myName == "Yellow")
				amIPlayin = true;
			if (myName == "Red")
				amIPlayin = false;
		} else if (Game_Manager._gminstance.GetPlayers () == 4) {
			if (myName == "Green")
				amIPlayin = true;
			else if (myName == "Blue")
				amIPlayin = true;
			else if (myName == "Yellow")
				amIPlayin = true;
			if (myName == "Red")
				amIPlayin = true;
		}
		#endregion

		foreach (GameObject go in meriGotiya)
			go.SetActive (amIPlayin);
		CalculatePlayables ();
	}

	void Update()
	{
		canIPlay = meriGotiya [0].GetComponent<Goti_Status> ().canBePlayed || meriGotiya [1].GetComponent<Goti_Status> ().canBePlayed
		|| meriGotiya [2].GetComponent<Goti_Status> ().canBePlayed || meriGotiya [3].GetComponent<Goti_Status> ().canBePlayed;

	}
		
	public void StopTheDice()									//Called by TurnManager
	{
		diceInput = dc.dInp;
		Invoke ("AfterDiceAction", 0.5f);
	}

	/*THIS METHOD CONTAINS SCRIPT TO BE EXECUTED AFTER ROLLING OF DICE HAS OCCURED*/
	void AfterDiceAction()
	{
		if (!haveLost) {
			if (!canIPlay) {												//If none of my goti can move , lets pass the turn
				Debug.Log (myName + " Could't do anything");
				tm.PerformNextTurn ();
			} else {
				Debug.Log ("Waiting for " + myName + "'s turn");
				CalculatePlayables ();

				if (!CPU)
					rm.gameObject.SetActive (true);
				else {
					rm.gameObject.SetActive (false);
					GetComponent<Enemy_AI> ().MovePiece ();
				}
				if (noOfPlaying == 1) {
					Goti_Status theGoti = new Goti_Status ();
					foreach (GameObject go in meriGotiya) {
						if (go.GetComponent<Goti_Status> ().canBePlayed)
							theGoti = go.GetComponent<Goti_Status> ();
					}
					theGoti.PerformMovement ();
				}
				foreach (GameObject go in meriGotiya) {
					if (go.GetComponent<Goti_Status> ().canBePlayed) {
						go.GetComponent<Goti_Status> ().ShowICanPlay ();
					}
				}
			}
		}
	}

	public void StopShowingAll()
	{
		foreach (GameObject go in meriGotiya)
			go.GetComponent<Goti_Status> ().StopShowing ();
	}

	public void CalculatePlayables()
	{
		noOfPlaying = 0;
		foreach (GameObject go in meriGotiya) {
			if (go.GetComponent<Goti_Status> ().canBePlayed)
				noOfPlaying += 1;
		}
		noOfGotiyaWin = 0;
		foreach (GameObject go in meriGotiya) {
			if (go.GetComponent<Goti_Status> ().myState == Goti_Status.typeOfState.won)
				noOfGotiyaWin += 1;
			}
		CalculateIfIWin ();
	}

	public void CalculateIfIWin () 
	{
		if (amIPlayin) 
		{
			if (noOfGotiyaWin == 4)
				IWIN ();
		}
	}

	public void IWIN()
	{
		OnWinObj.SetActive (true);
		haveWon = true;
		tm.wonPos += 1;
		if (tm.wonPos == 1)
			tm.first = myName;	
		else if (tm.wonPos == 2)
			tm.second = myName;
		else if (tm.wonPos == 3)
			tm.third = myName;
		OnWinObj.transform.GetChild (tm.wonPos - 1).gameObject.SetActive (true);
	}

	public void ILOST()
	{
		haveLost = true;
		if (tm.wonPos == 1)
			tm.second = myName;	
		else if (tm.wonPos == 2)
			tm.third = myName;
		else if (tm.wonPos == 3)
			tm.fourth = myName;
	}

	public void DisableAllCameras()
	{
		greenLane.enabled = false;
		blueLane.enabled = false;
		redLane.enabled = false;
		yellowLane.enabled = false;
		Debug.Log ("Disabled all cameras");
	}

	public void EnableMainCamera(bool flag)
	{
		myMainCamera.enabled = flag;
	}
}
