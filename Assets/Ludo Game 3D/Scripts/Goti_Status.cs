﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goti_Status : MonoBehaviour 
{
	public enum typeOfState{
		open,
		close,
		restricted,
		won
	};
	public typeOfState myState;
	public bool canBePlayed=false;
	public Color_Manager_Script myManger;
	public bool actionPhase = false;								//Only turns On for raycasting when player taps a goti so it can move
	bool yoICanPlay = false;										//Only turns On if goti can be played 

	public Vector3 finalPos;
	[SerializeField] int curIndexPos=-1;
	private bool startRunning = false;
	public float runSpeed = 10.0f;
	public float hieght = 0.65f;
	private bool calledOnce = false;
	private Vector3 myStartPos;
	private float initY;
	public int restrictionOutput;
	private int pathL;
	public Transform myWinPos;
	private bool hasCollided = false;
	public bool isSafe = false;
	public Animator anime;
	public List <Vector3> pathToFollow;
	private int listCounter = 0;
	private Vector3 initCPos;
	private Vector3 pos;
	public float animaOffsetX = 0;
	public float animaOffsetY = 0,animaOffsetZ = 0;
	public float walkAnimaX=0,walkAnimaY=0;
	public float attackOffX=0, attackOffZ=0;
	public Enemy_AI eai;
	public int worldPosToBeReached;
	public bool canCutSomeOne=false, becomingSafe=false, 
	winning=false, canChase = false, mightGetAttacked = false,
	doingNothing=false,isChaser=false;
	public int chaseGap=0;
	private bool performedNext = false;
	public int myWorldPosition = 70;
	public Transform safePoint;
	private bool attacking = false,gettingHit = false;
	public AudioSource asWalking;
	public GameObject powObj;
	public AudioSource openingAS;
	private Camera myTargetCam;

	void Start()
	{
		myState = typeOfState.close;
		pathL = myManger.myPath.Length-1;
		myStartPos = transform.position;
		pos = Vector3.zero;
		initCPos = transform.GetChild(0).localPosition;
		if (myManger.CPU)
			eai = myManger.GetComponent<Enemy_AI> ();
		myWorldPosition = 200;
		myTargetCam = Camera.main;
	}

	void Update()
	{
		restrictionOutput = pathL - curIndexPos;

		#region YOICANPLAY_CONCEPT
		if(myState!= typeOfState.won){
		if (yoICanPlay) {
			gameObject.layer = 0;
				transform.GetChild(0).localPosition = new Vector3(initCPos.x+animaOffsetX,initCPos.y+animaOffsetY,initCPos.z+animaOffsetZ);
			anime.Play("Select_Me",-1);															//<--------------ANIMATION

		} else if (!yoICanPlay) 
			{
				gameObject.layer = 2;
				if(!startRunning && !attacking)
				{
					anime.Play("Idle",-1);														//<--------------ANIMATION
					transform.GetChild(0).localPosition = initCPos;
				}
				else if(!startRunning && attacking)
				{
					anime.Play("Punch",-1);														//<--------------ANIMATION
					Vector3 pos = new Vector3 (initCPos.x+attackOffX,initCPos.y, initCPos.z+attackOffZ);
					transform.GetChild(0).localPosition = pos;
				}
				else if(!startRunning && gettingHit)
				{
					anime.Play("Hit",-1);
				}
			}
		}
		#endregion

		#region CAN_BE_PLAYED_LOGIC_IN_STATES
		if (myState == typeOfState.close) {
			if (myManger.diceInput == 6)
				canBePlayed = true;
			else
				canBePlayed = false;
		} 
		else if (myState == typeOfState.open)
			canBePlayed = true;
		else if(myState == typeOfState.restricted)
			canBePlayed = false;
		else if(myState == typeOfState.won){
			canBePlayed = false;
			anime.Play("Victory",-1);
		}
		#endregion

		#region RESTRICTED_STATE_LOGIC
		if(myState!=typeOfState.won){
		if(myManger.diceInput > restrictionOutput)
			myState = typeOfState.restricted;
		else if(myManger.diceInput<= restrictionOutput)
		{
			if(myState != typeOfState.close)
			myState = typeOfState.open;
		}
		}
		#endregion

		if (startRunning && pathToFollow.Count >0) {
			myManger.StopShowingAll ();													//Lets stop the animation in which pieces wave their hands in order to be chosen
			anime.Play ("Walking", -1);													//<--------------ANIMATION
	
			pos = new Vector3 (pathToFollow[listCounter].x, pathToFollow[listCounter].y + hieght, pathToFollow[listCounter].z);
			calledOnce = false;
//			Debug.Log ("listCounter = " + listCounter + " moving towards " + pos);
			transform.GetChild(0).localPosition = new Vector3(initCPos.x+walkAnimaX,initCPos.y+walkAnimaY,initCPos.z);
			transform.position = Vector3.MoveTowards (transform.position, pos, runSpeed * Time.deltaTime);
			if (transform.position == pos) {
	//			Debug.Log ("Reached " + listCounter + " pos now increasing the value by 1");
				listCounter++;
			}
		}

		if (transform.position == new Vector3 (finalPos.x, finalPos.y + hieght, finalPos.z)) {
			if (!calledOnce) {
				if (finalPos == myManger.myPath [myManger.myPath.Length - 1].position)
					GotiWin ();
				else
					AfterMovement ();
				calledOnce = true;
			}
		}
	}

	public void ShowICanPlay()
	{
		yoICanPlay = true;
		actionPhase = true;
	}

	public void StopShowing()									//Called by manager in StopShowing All, StopShowingAll is called when Goti starts running
	{
		yoICanPlay = false;
		actionPhase = false;
		transform.GetChild(0).localPosition = initCPos;
	}

	public void PerformMovement ()
	{
		Debug.Log (myManger.myName + "'s goti moving...: "+name);
		worldPosToBeReached = myWorldPosition+myManger.diceInput;
		int diff = worldPosToBeReached - 52;
		if (diff > 0)
			worldPosToBeReached = diff;
		performedNext = false;
		hasCollided = false;
		if (myState == typeOfState.close) {
			openingAS.Play ();
			curIndexPos = 0;
			pathToFollow.Add(myManger.myPath [curIndexPos].position);
			finalPos = myManger.myPath [curIndexPos].position;
			startRunning = true;
			myState = typeOfState.open;
		} 
		else if (myState == typeOfState.open) 
		{
			int z = curIndexPos + myManger.diceInput;
			finalPos = myManger.myPath [z].position;
			for(int i=0; i<myManger.diceInput; i++)
			{
				curIndexPos += 1;
				pathToFollow.Add (Vector3.zero);
				pathToFollow[i] =  myManger.myPath [curIndexPos].position;
			}
			startRunning = true;
		}
	}

	void AfterMovement()
	{
		if(!attacking){
			anime.Play ("Idle", -1);									//<========================ANIMATION
			transform.GetChild(0).localPosition = initCPos;
		}
		else{
			anime.Play ("Punch", -1);									//<========================ANIMATION
			Vector3 pos = new Vector3 (initCPos.x+attackOffX,initCPos.y, initCPos.z+attackOffZ);
			transform.GetChild(0).localPosition = pos;
		}
		pathToFollow.Clear();
		listCounter = 0;
		foreach (Transform tr in myManger.safePoints) {
			Vector2 myXY = new Vector2 (transform.position.x, transform.position.z);
			Vector2 saXY = new Vector2 (tr.position.x, tr.position.z);
			if (myXY == saXY) {
				isSafe = true;
				break;
			} else
				isSafe = false;
		}
		if (myManger.diceInput != 6) {
			startRunning = false;
			Invoke ("NextTurn", 0.5f);
		}
		else 
		{
			startRunning = false;
			Invoke ("RepeatTurn", 0.5f);
		}
		Debug.Log (myManger.name + " played");
		myManger.CalculatePlayables ();
	}

	void GotiWin ()
	{
		transform.position = myWinPos.position;
		transform.eulerAngles = new Vector3 (transform.eulerAngles.x, myWinPos.eulerAngles.y, transform.eulerAngles.z);
		startRunning = false;
		myState = typeOfState.won;
		myManger.CalculatePlayables ();
		if (myManger.noOfGotiyaWin != 4)
			myManger.tm.RepeatTurn ();
		else
			myManger.tm.PerformNextTurn();
	}

	public IEnumerator CutGoti(float f)
	{
		anime.Play ("Hit", -1);
		gettingHit = true;
		yield return new WaitForSeconds (f);
		gettingHit = false;
		transform.position = myStartPos;
		myState = typeOfState.close;
	}

	void OnTriggerEnter(Collider col)
	{
		if (col.tag == "Goti") {
			Debug.Log (name + " collided with " + col.name);
			Goti_Status otherGS = col.GetComponent<Goti_Status> ();
			//THAT MEANS WE ARE HAVING THE CURRENT TURN AND WE DONT CUT PIECES IN OUR WAY TO FINALPOS
			if (myManger.tm.currentTurn == myManger) 
			{
				int otherWP = otherGS.myWorldPosition;
				if(otherWP == worldPosToBeReached)
				{
					if (otherGS.myManger != myManger && !otherGS.isSafe) {
						Debug.Log (name + "with pos "+ worldPosToBeReached+ " attacked " + col.name+ " with pos "+otherWP);
						hasCollided = true;
						if ((worldPosToBeReached >= 45 && worldPosToBeReached <= 52) || (worldPosToBeReached >= 1 && worldPosToBeReached <= 5)) {
							myManger.greenLane.enabled = true;
							myTargetCam = myManger.greenLane;
						} else if (worldPosToBeReached >= 6 && worldPosToBeReached <= 18) {
							myManger.redLane.enabled = true;
							myTargetCam = myManger.redLane;
						} else if (worldPosToBeReached >= 19 && worldPosToBeReached <= 31) {
							myManger.blueLane.enabled = true;
							myTargetCam = myManger.blueLane;
						} else if (worldPosToBeReached >= 32 && worldPosToBeReached <= 44) {
							myManger.yellowLane.enabled = true;
							myTargetCam = myManger.greenLane;
						}
						myManger.EnableMainCamera (false);
						Vector3 pos = new Vector3 (initCPos.x+attackOffX,initCPos.y, initCPos.z+attackOffZ);
						transform.GetChild(0).localPosition = pos;
						anime.Play ("Punch", -1);					//<======================ANIMATION
						attacking = true;
						myManger.mukkeKiAwaaz.PlayDelayed(0.5f);

						StartCoroutine(otherGS.CutGoti(1));

						Invoke ("AfterAttack", 1f);
					}
					else
						Debug.Log ("Could'nt attack " + otherGS.name + " because either it was safe or was in the same team as us");
				}
				else
					Debug.Log ("Could'nt attack " + otherGS.name + " because it was on different position than us"+(worldPosToBeReached)+" and "+(otherWP));
			}
		} 
		else if (col.tag == "PathCube") 
		{
			transform.eulerAngles = new Vector3 (transform.eulerAngles.x, col.transform.eulerAngles.y, transform.eulerAngles.z);
			myWorldPosition = col.GetComponent<WorldPos> ().worldPos;
			Debug.Log ("Collided with path cube, changed world pos to : " + myWorldPosition);
			if (startRunning) {
				if(Game_Manager._gminstance.isSoundOn)
				asWalking.Play ();
			}
		}
	}
		
	void AfterAttack()
	{
		Vector3 spawnPos = new Vector3 (transform.position.x, transform.position.y+2.0f, transform.position.z);
		GameObject go = GameObject.Instantiate (powObj, spawnPos, Quaternion.identity)as GameObject;
		go.GetComponent<Pow_Script> ().targetCam = myTargetCam;
		Invoke ("ResetAllCameras", 1.0f);
		startRunning = false;
		attacking = false;
		transform.position = new Vector3 (finalPos.x, finalPos.y + hieght, finalPos.z);
		myManger.tm.RepeatTurn ();
	}

	void NextTurn()
	{
		if (!performedNext) {
			performedNext = true;
			if (!hasCollided)
				myManger.tm.PerformNextTurn ();
		/*	if (hasCollided)
				RepeatTurn ();*/
		}
	}

	void RepeatTurn()
	{
		if(!hasCollided)
			myManger.tm.RepeatTurn ();
	}

	public void FlagLogics()
	{
		#region FLAGSFOR_OPEN
		/*FLAGS FOR OPEN STATE*/
		worldPosToBeReached = myWorldPosition+myManger.diceInput;
		int diff = worldPosToBeReached - 52;
		if (diff > 0)
			worldPosToBeReached = diff;
		for(int i=0; i<=3; i++){
			if(myState == typeOfState.open)
			{
				/////////////*CanCut Flag*/
				foreach(Goti_Status gs in eai.enemyPieces)
				{
					if(!gs.isSafe)
					{
						int enemyCoord = gs.myWorldPosition;

						if(enemyCoord == worldPosToBeReached){
							canCutSomeOne = true;
							break;
						}
						else
							canCutSomeOne = false;
					}
				}

				/////////////*BecomingSafe Flags*/
				foreach (Transform sp in myManger.safePoints) {
					if(myWorldPosition>=21 && myWorldPosition<26)
					{
						becomingSafe = false;
					}
					else
					{
						int safeCoord = sp.GetComponent<WorldPos>().worldPos;

						if (safeCoord == worldPosToBeReached){
							safePoint = sp;
							becomingSafe = true;
							break;
						}
						else
							becomingSafe = false;
					}
				}

				/////////////*Can chase someone flags*/
				foreach(Goti_Status gs in eai.enemyPieces)
				{
					int enemyCoord = gs.myWorldPosition;

					if(enemyCoord > worldPosToBeReached){
						if(enemyCoord-worldPosToBeReached<6 && enemyCoord-worldPosToBeReached>=0){
							canChase = true;
							chaseGap = enemyCoord-worldPosToBeReached;
							break;
						}
					}
					else
						canChase = false;
				}
				////////////*Winning Flags*/
				Vector2 winCoord = new Vector2(myManger.myPath[myManger.myPath.Length-1].position.x,
					myManger.myPath[myManger.myPath.Length-1].position.z);
				Vector2 targetCoord = new Vector2(myManger.myPath[curIndexPos+myManger.diceInput].
					position.x,myManger.myPath[curIndexPos+myManger.diceInput].position.z);
				if(targetCoord == winCoord)
				{
					winning = true;
					break;
				}
				else
					winning = false;
				//////////////*isChaserOrNot*/
				foreach(Goti_Status gs in eai.enemyPieces)
				{
					int enemyCoord = gs.myWorldPosition;

					if(enemyCoord <= worldPosToBeReached){
						if(worldPosToBeReached-enemyCoord<=5 && worldPosToBeReached-enemyCoord>=0 && isSafe){
							isChaser = true;
							break;
						}
						else
							isChaser = false;
					}
					else
						isChaser = false;
				}
				//////////////*MIGHT GET ATTACKED*/
				foreach(Goti_Status gs in eai.enemyPieces)
				{
					int enemyCoord = gs.myWorldPosition;
					if(worldPosToBeReached> enemyCoord && !becomingSafe)
					{
						mightGetAttacked = true;
						break;
					}
					else if(worldPosToBeReached< enemyCoord)
						mightGetAttacked = false;
					
				}
				//////////////*DOING NOTHING FLAGs*/
				doingNothing = ((!canCutSomeOne)&&(!becomingSafe)&&(!canChase)&&(!winning));
			}
		}
		#endregion
	}

	void ResetAllCameras()
	{
		myManger.EnableMainCamera (true);
		myTargetCam = Camera.main;
		Debug.Log ("Enabled main camera");
		myManger.DisableAllCameras ();
	}
}
