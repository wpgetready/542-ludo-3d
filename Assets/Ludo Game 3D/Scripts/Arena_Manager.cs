﻿using UnityEngine;

public class Arena_Manager : MonoBehaviour {

	public GameObject[] arenas;

	void Start () {
		foreach (GameObject go in arenas) {
			go.SetActive (false);
		}	
		arenas [Game_Manager._gminstance.arenaIndex].SetActive (true);
	}
}
