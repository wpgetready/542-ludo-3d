﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using Facebook.Unity;
using System.Collections.Generic;
using Facebook.MiniJSON;

public class myFBHolder : MonoBehaviour 
{
	/*THE UI IS DIVIDED INTO THAT TO BE SHOWN BEFORE LOGGING IN AND AFTER LOGGING IN*/
	public GameObject UIFBLoggedIn;						
	public GameObject UIFBNotLoggedIn;

	public Text UIFBName;								//Logger's name will be shown on this text

	public Image profilePic;							//Assign this to any image created in UI

	public Dictionary<string,string> profile = null;	//Dictionary is required , as its returned by method on line 96
	public string userName="none";

	public string FBUrl;

	void Awake ()
	{
		FB.Init (SetInit, OnHideUnity);					//FB.Init is required to use FB SDK
		UIFBLoggedIn.SetActive(false);
	}

	private void SetInit()
	{
		Debug.Log ("FB init done");

		if (FB.IsLoggedIn) {
			Debug.Log ("FB logged in on awake");
		} 
		else 
		{
			Debug.Log ("FB not logged in on awake");		
		}
	}

	private void OnHideUnity(bool isGameShown)
	{
		if (!isGameShown) 
		{
			Time.timeScale = 0;								// Pause the game if its minimized
		} 
		else 
		{
			Time.timeScale = 1;								// If its not, then resume it
		}
	}

	public void FBLogin()
	{
		//var perms = new List<string>(){"public_profile", "email", "user_friends"};
		var perms = new List<string>(){"email"};					//The list of permissions we need from facebook, keep this list as small as possible
		FB.LogInWithReadPermissions (perms, AuthCallback);			//Almost every FB function has a authorised callBack, which you have to create (using correct parameters)
	} 

	void AuthCallback(ILoginResult result)							// The parameter for Loging function Authorised callback is ILoginResult
	{
		if (FB.IsLoggedIn) {
			Debug.Log ("Login Completed");
			DealWithFBMenus (true);
		} else {
			Debug.Log ("Login Failed");
			DealWithFBMenus (false);
		}
	}

	void DealWithFBMenus(bool isLoggedIn)							// Lets correct our UI
	{
		if (isLoggedIn) {
			UIFBLoggedIn.SetActive (true);
			UIFBNotLoggedIn.SetActive (false);

			//FB.API (GetPictureURL ("me", 128, 128), HttpMethod.GET, DealWithProfilePic);
			FB.API("me/picture?type=square&height=128&width=128", HttpMethod.GET, DealWithProfilePic);	// THere we go, now we got user DP nd user name
			FB.API ("/me?fields=id,first_name", HttpMethod.GET, DealWithUserName);
		} 
		else {
			UIFBLoggedIn.SetActive (false);
			UIFBNotLoggedIn.SetActive (true);
		}
	}

	#region RequiredMethods
	public static string GetPictureURL(string facebookID, int? width = null, int? height = null, string type = null)
	{
		string url = string.Format("/{0}/picture", facebookID);
		string query = width != null ? "&width=" + width.ToString() : "";
		query += height != null ? "&height=" + height.ToString() : "";
		query += type != null ? "&type=" + type : "";
		if (query != "") url += ("?g" + query);
		return url;
	}
	public static Dictionary<string, string> DeserializeJSONProfile(string response)
	{
		var responseObject = Json.Deserialize(response) as Dictionary<string, object>;
		object nameH;
		var profile = new Dictionary<string, string>();
		if (responseObject.TryGetValue("first_name", out nameH))
		{
			profile["first_name"] = (string)nameH;
		}
		return profile;
	}
	#endregion

	void DealWithProfilePic(IGraphResult result)
	{
		if (result.Error != null) 
		{
			Debug.Log ("Problem Getting Profile Pic");
			FB.API (GetPictureURL ("me", 128, 128), HttpMethod.GET, DealWithProfilePic);
			return;
		}
		profilePic.sprite = Sprite.Create(result.Texture , new Rect(0,0,128,128), Vector2.zero);
		profilePic.color = Color.white;
	}

	void DealWithUserName(IGraphResult result)
	{
		if (result.Error != null) 
		{
			Debug.Log ("Problem Getting Profile Pic");
			FB.API ("/me?fields=id,first_name", HttpMethod.GET, DealWithUserName);
			return;
		}
		profile = DeserializeJSONProfile (result.RawResult);
		userName = profile ["first_name"];
		UIFBName.text = userName;
		//Debug.Log (userName);
	}

	public void Share()
	{
		FB.FeedShare (
			string.Empty,																								//To ID (If we leave it empty, it means we want to post on our wall nd not on someone else's wall)
			new System.Uri("https://play.google.com/store/apps/details?id=com.HuriyaSoft.LudoMania"),					//The Link			(Enter your game's link here)
			"Ludo Mania",																								//Title Name 		(Enter your Game name here)
			"WOW!",																										//Link Caption		(e.g: "I am playing this game, join me")
			"Hey, check this ludo game out!",	 																		//Link Description 
			new System.Uri("http://letiarts.com/letiarts2014/wp-content/uploads/2014/04/icon_game.png"),				//Picture URI		(Enter your game's icon's URL)
			string.Empty,																								//mediaSource
			ShareCallBack
		);
	}
	void ShareCallBack(IShareResult result)
	{
		Debug.Log ("Share On FB : " + result);																			//prints Success if Share succeedes
	}

	public void InviteFriends()
	{
		FB.Mobile.AppInvite (
			new System.Uri("https://fb.me/127381907787356"),
		//	new System.Uri("https://play.google.com/store/apps/details?id=com.HuriyaSoft.LudoMania"),
			new System.Uri("http://letiarts.com/letiarts2014/wp-content/uploads/2014/04/icon_game.png"),
			InviteCallBack
			);
	}
	void InviteCallBack(IAppInviteResult result)
	{
		Debug.Log ("Invite Success: " + result);
	}

	public void Challenge()
	{
		FB.AppRequest (
			"Come and play agianst me, lets see if you can beat me!",											//The message to be sent in challenge
			null,
			new List<object> (){ "app_users" },
			null,
			null,
			null,
			"Ludo Mania",
			DealWithChallenge
		);
	}

	void DealWithChallenge(IAppRequestResult result)
	{
		Debug.Log (result.RawResult);
	}

	public void OpenFBPage()
	{
		Application.OpenURL (FBUrl);
	}

}

