﻿using UnityEngine;

public class Pow_Script : MonoBehaviour 
{
	public float destroyTime;
	public Camera targetCam;
	void Start() 
	{
		Destroy (this.gameObject,destroyTime);
	}

	void Update()
	{
		transform.LookAt (targetCam.transform);
	}

}
