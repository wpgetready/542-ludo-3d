﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Turn_Manager : MonoBehaviour 
{
	public List<Color_Manager_Script> listOfPlayers;
	public Text notificationText;
	public Color_Manager_Script currentTurn,nextTurn;
	[SerializeField]private int indexTurn;
	public Raycast_Manager rm;
	public GameObject board;
	public GameObject diceRollButt;
	private bool startRolling = false;														//Needed so we can reset the ring and call funcns when player stops rolling the dice
	public Dice_Script dc;
	private bool calledOnce = false;
	public bool isPaused = false;
	public GameObject pauseUI;
	public int wonPos = 0;
	public bool isCPUGame = false;
	public GameObject gameCompletionObj,ListObj;
	public int noOfPlayers = 0;
	public string first,second,third,fourth;
	public AudioSource asDice,victoryAS;
	public Camera myMain, victoryCam;
	public GameObject victoryObj;
	void Start () {
		noOfPlayers = Game_Manager._gminstance.GetPlayers ();
		if (noOfPlayers == 1)
			isCPUGame = true;
		else
			isCPUGame = false;
		diceRollButt.gameObject.SetActive (true);

		Color_Manager_Script[] allP = GameObject.FindObjectsOfType<Color_Manager_Script> ();
		foreach (Color_Manager_Script cms in allP) {
			if (cms.amIPlayin)
				listOfPlayers.Add (cms);
		}
		currentTurn = listOfPlayers [0];
		notificationText.gameObject.SetActive (false);
		SetCurrentTurnTo (listOfPlayers [0],0);
		gameCompletionObj.SetActive (false);
	}

	void Update () 
	{
		if (notificationText.gameObject.activeSelf)
			notificationText.color -= new Color (0, 0, 0, 0.01f);

		#region NEXT_TURN_LOGIC
		if (currentTurn != null) 
		{
			if (indexTurn == listOfPlayers.Count - 1)
				nextTurn = listOfPlayers [0];
			else
				nextTurn = listOfPlayers [indexTurn + 1];
		}
		#endregion

		if (startRolling)
			dc.RollDice ();

		#region DICE_COLOR
		if (currentTurn.myName == "Green")
			dc.diceR.material.color = Color.green;
		else if (currentTurn.myName == "Red")
			dc.diceR.material.color= Color.red;
		else if (currentTurn.myName == "Blue")
			dc.diceR.material.color= Color.blue;
		else if (currentTurn.myName == "Yellow")
			dc.diceR.material.color = Color.yellow;
		#endregion

		#region AI_LOGIC
		if(currentTurn.CPU && !calledOnce)
		{
			currentTurn.GetComponent<Enemy_AI>().rollDice=true;
			calledOnce = true;
		}
		#endregion

		#region Pause_Functions
		pauseUI.SetActive(isPaused);
		if(isPaused)
			Time.timeScale = 0;
		else
			Time.timeScale = 1;
		#endregion
	}

	public void SetCurrentTurnTo(Color_Manager_Script cms,int i)
	{
		Debug.Log ("Setted turn to : " + cms.myName);
		indexTurn = i;
		dc.EnableArrow ();
		currentTurn = cms;
		if (currentTurn.haveWon) {
			if (indexTurn == listOfPlayers.Count - 1)
				indexTurn = 0;
			else
				indexTurn += 1;
			
			Debug.Log ("But since it has won, we changed turn to: " + listOfPlayers[indexTurn]);
			SetCurrentTurnTo (listOfPlayers [indexTurn],indexTurn);
		} 
		else 
		{
			Debug.Log ("Since it has not won, its " + cms.myName+ "'s turn.");
			GiveNotification ("" + currentTurn.myName + "'s Turn!");
		}
	}

	public void GiveNotification(string s)
	{
		notificationText.gameObject.SetActive (true);
		if (currentTurn.myName == "Green")
			notificationText.color = Color.green;
		else if (currentTurn.myName == "Red")
			notificationText.color = Color.red;
		else if (currentTurn.myName == "Blue")
			notificationText.color = Color.blue;
		else if (currentTurn.myName == "Yellow")
			notificationText.color = Color.yellow;
		notificationText.text = s;

	}

	public void PerformNextTurn()
	{
		diceRollButt.SetActive (true);
		rm.gameObject.SetActive (false);
		if (indexTurn == listOfPlayers.Count - 1)
			SetCurrentTurnTo (nextTurn, 0);
		else
			SetCurrentTurnTo (nextTurn, indexTurn + 1);

		if (isCPUGame) {
			if (wonPos == 1)
				ShowGameCompletion ();
		} 
		else 
		{
			if (wonPos == noOfPlayers-1)
				ShowGameCompletion ();
		}
	}

	public void RepeatTurn()
	{
		calledOnce = false;
		if(currentTurn.CPU)
			diceRollButt.SetActive (false);
		else
			diceRollButt.SetActive (true);
		rm.gameObject.SetActive (false);
		SetCurrentTurnTo (currentTurn, indexTurn);
	}
	public void DeactivateButton()
	{
		diceRollButt.SetActive (false);
	}

	public void StopDice()
	{
		diceRollButt.SetActive(false);
		asDice.Stop ();
		startRolling = false;
		dc.StopDice ();
		currentTurn.StopTheDice ();
		if (!currentTurn.CPU)
			calledOnce = false;
	}

	public void RollTheDice()
	{
		if(Game_Manager._gminstance.isSoundOn)
			asDice.Play ();
		startRolling = true;
		diceRollButt.SetActive(false);
		Debug.Log (currentTurn.myName + " is rolling the dice");
		float rand = Random.Range (0.8f, 1.4f);
		Invoke ("StopDice", rand);
	}

	public void AIRoll()
	{
		if(Game_Manager._gminstance.isSoundOn)
			asDice.Play ();
		startRolling = true;
		diceRollButt.SetActive(false);
		Debug.Log (currentTurn.myName + " is rolling the dice");
	}

	public void PauseGame()
	{
		isPaused = !isPaused;
	//	int rand = Random.Range (0, 5);
	//	if(rand == 0)
			AdMob_Manager.Instance.ShowImageAd ();
	}

	public void BTM()
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene (0);
		AdMob_Manager.Instance.ShowImageAd ();
	}

	public void Replay()
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene (1);	
	}

	void ShowGameCompletion()
	{
		victoryAS.Play ();
		gameCompletionObj.SetActive (true);
		ListObj.SetActive (true);
		myMain.enabled = false;
		victoryCam.enabled = true;
		if (first == "Red") {
			victoryObj.transform.GetChild (0).gameObject.SetActive (true);
			Animator anime = victoryObj.transform.GetChild (0).GetChild (0).GetComponent<Animator> ();
			anime.Play ("Victory", -1);
		} else if (first == "Blue") {
			victoryObj.transform.GetChild (1).gameObject.SetActive (true);
			Animator anime = victoryObj.transform.GetChild (0).GetChild (1).GetComponent<Animator> ();
			anime.Play ("Victory", -1);
		} else if (first == "Yellow") {
			victoryObj.transform.GetChild (2).gameObject.SetActive (true);
			Animator anime = victoryObj.transform.GetChild (2).GetChild (0).GetComponent<Animator> ();
			anime.Play ("Victory", -1);
		} else if (first == "Green") {
			victoryObj.transform.GetChild (3).gameObject.SetActive (true);
			Animator anime = victoryObj.transform.GetChild (3).GetChild (0).GetComponent<Animator> ();
			anime.Play ("Victory", -1);
		}
		foreach (Color_Manager_Script cms in listOfPlayers) {
			if (!cms.haveWon)
				cms.ILOST ();
		}
		if (wonPos == 1) {
			ListObj.transform.GetChild (0).gameObject.SetActive (true);
			ListObj.transform.GetChild (0).GetComponent<Text>().text = "Winner: "+first;
			ListObj.transform.GetChild (1).gameObject.SetActive (true);
			ListObj.transform.GetChild (1).GetComponent<Text>().text = "Loser: "+second;
		}
		else if (wonPos == 2) {
			ListObj.transform.GetChild (0).gameObject.SetActive (true);
			ListObj.transform.GetChild (0).GetComponent<Text>().text = "Winner: "+first;
			ListObj.transform.GetChild (1).gameObject.SetActive (true);
			ListObj.transform.GetChild (1).GetComponent<Text>().text = "Second: "+second;
			ListObj.transform.GetChild (2).gameObject.SetActive (true);
			ListObj.transform.GetChild (2).GetComponent<Text>().text = "Loser: "+third;
		}
		else if (wonPos == 3) {
			ListObj.transform.GetChild (0).gameObject.SetActive (true);
			ListObj.transform.GetChild (0).GetComponent<Text>().text = "Winner: "+first;
			ListObj.transform.GetChild (1).gameObject.SetActive (true);
			ListObj.transform.GetChild (1).GetComponent<Text>().text = "Second: "+second;
			ListObj.transform.GetChild (2).gameObject.SetActive (true);
			ListObj.transform.GetChild (2).GetComponent<Text>().text = "Third: "+third;
			ListObj.transform.GetChild (3).gameObject.SetActive (true);
			ListObj.transform.GetChild (3).GetComponent<Text>().text = "Loser: "+fourth;
		}
	}
}
