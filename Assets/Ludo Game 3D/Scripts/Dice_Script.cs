﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Dice_Script : MonoBehaviour 
{
	Transform thisTransform;
	public float diceRollSpeed = 10.0f;
	public int dInp=0;
	public Turn_Manager tm;
	public Renderer diceR;
	public Image diceArrow;
	private bool startArrowAnim = false;
	private Vector3 initArrow;
	public float arrowMovementRange=10, arrowMovementSpeed=10;

	void Start()
	{
		thisTransform = transform;
		diceArrow.gameObject.SetActive (true);
		initArrow = diceArrow.transform.position;
	}

	void Update()
	{
		if (Input.GetKey (KeyCode.R))
			RollDice ();
		if (Input.GetKeyUp (KeyCode.R))
			StopDice ();

		if (startArrowAnim) 
		{
			float yOff = Mathf.PingPong (Time.time * arrowMovementSpeed, arrowMovementRange);
			diceArrow.transform.position = new Vector3 (initArrow.x, initArrow.y + yOff, initArrow.z);
		}
	}

	public void RollDice ()
	{
		float direction = Random.Range (0, 2);
		if (direction == 0)
			direction = -1;
		else if (direction == 1)
			direction = 1;
		thisTransform.Rotate (1*diceRollSpeed*direction, 1*diceRollSpeed*direction, 1*diceRollSpeed*direction);
		DisableArrow ();
	}

	public void StopDice()
	{
		tm.DeactivateButton ();
		int rand = Random.Range (1, 7);
		dInp = rand;
		if (rand == 2)
			thisTransform.localEulerAngles = new Vector3 (0, 0, 0);
		else if (rand == 6)
			thisTransform.localEulerAngles = new Vector3 (0, 0, -90);
		else if (rand == 5)
			thisTransform.localEulerAngles = new Vector3 (90, 0, 0);
		else if (rand == 3)
			thisTransform.localEulerAngles = new Vector3 (-90, 0, 0);
		else if (rand == 4)
			thisTransform.localEulerAngles = new Vector3 (0, 0, 90);
		else if (rand == 1)
			thisTransform.localEulerAngles = new Vector3 (-180, 0, 0);
	}

	public void EnableArrow()
	{
		diceArrow.gameObject.SetActive (true);
		startArrowAnim = true;
	}

	public void DisableArrow()
	{
		diceArrow.gameObject.SetActive (false);
		startArrowAnim = false;
	}
		
}
