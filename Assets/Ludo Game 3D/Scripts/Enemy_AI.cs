﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_AI : MonoBehaviour 
{
	public bool rollDice = false;
	public Turn_Manager tm;
	public Color_Manager_Script cms;
	public Goti_Status[] myPieces;
	public Goti_Status[] enemyPieces;
	public List <Goti_Status> playAblePieces;
	public List <Goti_Status> closedPieces;
	public List <Goti_Status> wonPieces;
	public Transform[] vicinityPath;

	void Start()
	{
		cms = GetComponent<Color_Manager_Script> ();
	}
	void Update () 
	{
		if (rollDice) {
			tm.AIRoll ();
			float rand = Random.Range (0.3f, 2.1f);
			Invoke ("StopRolling", rand);
			rollDice = false;
		}
	}

	void StopRolling()
	{
		tm.StopDice ();
		foreach (Goti_Status gs in myPieces) {
			if(gs.myState != Goti_Status.typeOfState.won)
				gs.FlagLogics ();
		}
	}

	public void MovePiece()									//Called by CMS
	{
		if (!cms.haveLost) {
			/*WE WILL MOVE CPU PIECES BASED ON DICE INPUT*/
			int noClosed = 0;
			closedPieces.Clear ();
			Debug.Log ("CPU Got " + cms.diceInput);
			int noOpened = 0;
			playAblePieces.Clear ();
			int noWon = 0;
			wonPieces.Clear ();
			/*LETS CALCULATE HOW MANY ARE OPEN*/
			foreach (Goti_Status gs in myPieces) {
				if (gs.myState == Goti_Status.typeOfState.open) {
					playAblePieces.Add (gs);
					playAblePieces [noOpened] = gs;
					noOpened += 1;
				}
			}
			/*HOW MANY HAVE WON*/
			foreach (Goti_Status gs in myPieces) {
				if (gs.myState == Goti_Status.typeOfState.won) {
					wonPieces.Add (gs);
					wonPieces [noWon] = gs;
					noWon += 1;
				}
			}
			/*HOW MANY ARE CLOSED*/
			foreach (Goti_Status gs in myPieces) {
				if (gs.myState == Goti_Status.typeOfState.close) {
					closedPieces.Add (myPieces [0]);
					closedPieces [noClosed] = gs;
					noClosed += 1;
				}
			}
			if (cms.diceInput == 6) { 									//IF DICE INPUT IS 6
				/*Lets Check If all or 3 are closed, if all or 3 are closed, we will open piece*/
				/*There is a 90% chance that CPU might open a piece if player's pieces are in house's vicinity*/
				/*There is a 30% chance that CPU might also open a piece without any reason*/

				if (noClosed + noWon == 4 && noClosed != 0) {
					closedPieces [0].PerformMovement ();
					Debug.Log ("Since All were closed, CPU opened a piece");
				} else if (noClosed + noWon == 3 && noClosed != 0) {
					closedPieces [0].PerformMovement ();
					Debug.Log ("Since 3 were closed, CPU opened a piece");
				} else if (noClosed <= 2 && noClosed != 0) {
					int rand = Random.Range (1, 11);
					/*LETS CHECK IF ANYONE IS IN OUR VICINITY SO WE CAN CUT IT*/
					if (isAnyOneInVicinity ()) {
						int rand2 = Random.Range (0, closedPieces.Count);
						int rand3 = Random.Range (1, 11);
						if (rand3 <= 9) {
							closedPieces [rand2].PerformMovement ();
							Debug.Log ("CPU opened a piece coz there is a chance to attack player");
						} else if (rand3 > 9)
							TurnNotOn6 ();
					} else if (!isAnyOneInVicinity ()) {
						if (rand <= 3)/*30% chance that CPU opens w/0 any reason*/
						closedPieces [0].PerformMovement ();
						else
							TurnNotOn6 ();
					}
				} else
					TurnNotOn6 ();
			} else { 														//IF DICE INPUT IS NOT 6
				/*Lets check if more than one are opened i.e. if we have a choice*/
				if (noOpened == 1) {
					Debug.Log ("CPU had no choice so had to move the only piece");
				}
				if (noOpened > 1) {
					TurnNotOn6 ();
				}
			}
		}
	}

	bool isAnyOneInVicinity()
	{
		foreach (Goti_Status gs in enemyPieces) {
			Vector2 enemyCoords = new Vector2(gs.transform.position.x,gs.transform.position.z);
			foreach (Transform tr in vicinityPath) {
				Vector2 vicinityCoords = new Vector2 (tr.position.x, tr.position.z);
				if (vicinityCoords == enemyCoords)
					return true;
				else
					return false;
			}
		}
		return false;
	}

	void TurnNotOn6()
	{
		/*TURN LOGIC WORKS IN FOLLOWING WAY:*/
		/*In OPEN AND UNSAFE: Top priority is given to cut player piece, then to becoming safe, then to not gettingCut, then to chasing , 
		then to winning and finally if noOne is doing anything, we move the first playAble piece*/
		/*In OPEN AND SAFE: Top priority is given to cut player piece, then to becoming safe, if all are safe, then CPU chooses the piece to move following a
		complicated set of instructions, these are written down*/
			/*IF ALL ARE SAFE, THEN CPU FOLLOWS THESE INSTRUCTIONS:
			1. LETS SEE OUT OF THE SAFE PIECES, WHICH ONE IS A CHASER, i.e. can chase player pieces (if player pieces are on the same block or 5 steps behind, then the
				piece is a chaser, we will try not to move the chaser, as it is capable of cutting player pieces and can be attacked by player if moved)
			2. IF ALL ARE CHASERS THEN WE SHALL MOVE THE PIECE WITH LOWEST POSINDEX (which is nearest to home)*/
		#region Turn_LOGIC
		Debug.Log("CPU has choices because more than 1 are playable");
		List <Goti_Status> safeGotiya = new List<Goti_Status>();
		bool noOneMoved = true;

		if(noOneMoved)				//TOP PRIORITY TO CUT PLAYER's PIECE
		{
			foreach(Goti_Status g in playAblePieces)
			{
				if(!g.isSafe)
				{
					if(g.canCutSomeOne && noOneMoved){
						g.PerformMovement();
						Debug.Log("CPU played "+g.name+" because it was cutting enemy piece");
						noOneMoved = false;
					}
				}
			}
		}

		if(noOneMoved)	//TOP PRIORITY TO CUT PLAYER's PIECE EVEN IF YOU ARE SAFE
		{
			foreach(Goti_Status g in playAblePieces)
			{
				if(g.isSafe)
				{
					if(g.canCutSomeOne && noOneMoved){
						g.PerformMovement();
						Debug.Log("CPU played "+g.name+" because it was cutting enemy piece");
						noOneMoved = false;
					}
				}
			}
		}
		if(noOneMoved)	//SECOND PRIORITY IN SAFE IS TO SEE IF YOU ARE A CHASER OR NOT
		{
			foreach(Goti_Status g in playAblePieces)
			{
				if(g.isSafe)
				{
					if(safeGotiya.Count == playAblePieces.Count && noOneMoved){
						foreach(Goti_Status gs in safeGotiya)
						{
							if(!gs.isChaser && noOneMoved){
								Debug.Log("CPU moved "+g.name+" since all pieces were safe it was not being chased by player");
								noOneMoved = false;
								gs.PerformMovement();
							}
						}
						if(noOneMoved){
							playAblePieces[playAblePieces.Count-1].PerformMovement();
							Debug.Log("CPU moved "+g.name+" since all pieces were safe and all were being chased by player");
							noOneMoved = false;
						}
					}
				}
			}
		}
		if(noOneMoved)				//THEN TO BECOME SAFE
		{
			foreach(Goti_Status g in playAblePieces)
			{
				if(!g.isSafe)
				{
					if(g.becomingSafe && noOneMoved){
						g.PerformMovement();
						Debug.Log("CPU played "+g.name+" because it becoming safe");
						noOneMoved = false;
					}
				}
			}
		}
			

		if(noOneMoved)				//THEN TO AVOID GETTING ATTACK BY OTHER PLAYER's PIECE
		{
			foreach(Goti_Status g in playAblePieces)
			{
				if(!g.isSafe)
				{
					if(!g.mightGetAttacked && noOneMoved){
						g.PerformMovement();
						Debug.Log("CPU played "+g.name+" because it has least chances of getting cut");
						noOneMoved = false;
					}
				}
			}
		}
		if(noOneMoved)				//THEN TO CHASING OTHER PLAYER's PIECE
		{
			foreach(Goti_Status g in playAblePieces)
			{
				if(!g.isSafe)
				{
					if(g.canChase && noOneMoved){
						if(cms.diceInput<g.chaseGap){
							g.PerformMovement();
							Debug.Log("CPU played "+g.name+" because it could chase player");
							noOneMoved = false;
						}
					}
				}
			}
		}
		if(noOneMoved)				//THEN TO CHASE SOMEONE EVEN IF YOU ARE SAFE
		{
			foreach(Goti_Status g in playAblePieces)
			{
				if(g.isSafe)
				{
					if(g.canChase && noOneMoved){
						if(cms.diceInput<g.chaseGap){
							g.PerformMovement();
							Debug.Log("CPU played "+g.name+" because it could chase player");
							noOneMoved = false;
						}
					}
				}
			}
		}
		if(noOneMoved)				//THEN TO BECOMING SAFE EVEN IF YOU ARE SAFE
		{
			foreach(Goti_Status g in playAblePieces)
			{
				if(g.isSafe)
				{
					if(g.becomingSafe && noOneMoved){
						g.PerformMovement();
						Debug.Log("CPU played "+g.name+" because it was becoming safe");
						noOneMoved = false;
					}
				}
			}
		}
		if(noOneMoved)				//THEN TO AVOID BEING HIT IF TOU ARE SAFE
		{
			foreach(Goti_Status g in playAblePieces)
			{
				if(g.isSafe)
				{
					if(!g.mightGetAttacked && noOneMoved){
						g.PerformMovement();
						Debug.Log("CPU played "+g.name+" because it had least chance of being attacked");
						noOneMoved = false;
					}
				}
			}
		}
		if(noOneMoved)				//THEN TO WINNING
		{
			foreach(Goti_Status g in playAblePieces)
			{
				if(!g.isSafe)
				{
					if(g.winning && noOneMoved){
						g.PerformMovement();
						Debug.Log("CPU played "+g.name+" because it was winning");
						noOneMoved = false;
					}
				}
			}
		}
		if(noOneMoved){
			foreach(Goti_Status gsa in playAblePieces)
			{
				if(noOneMoved){
					gsa.PerformMovement();
					Debug.Log("CPU moved because there was no futile turn it could detect");
				}
				noOneMoved = false;
			}
		}

		Debug.Log ("CPU had choice and moved");
		#endregion
	}
}
