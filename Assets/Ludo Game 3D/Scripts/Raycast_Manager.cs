﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Raycast_Manager : MonoBehaviour {

	public Ray mouseToScreen;

	void Update () {
		if (Camera.main != null) {
			mouseToScreen = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast (mouseToScreen, out hit)) {
				if (hit.collider.tag == "Goti") {
					Goti_Status gs = hit.collider.GetComponent<Goti_Status> ();
					if (gs.actionPhase && Input.GetMouseButtonDown (0)) {
						gs.PerformMovement ();
						gameObject.SetActive (false);
					}
				}
			}
		}
	}
}
