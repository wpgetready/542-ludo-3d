﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using admob;

public class AdMob_Manager : MonoBehaviour 
{

	public string androidInterstitial,
	androidBanner, iosInterstitial, iosBanner;

	public static AdMob_Manager Instance{ set; get;}
	public bool showBannerAtStart = false;

	void Start () {
		Instance = this;
		#if UNITY_EDITOR
		Debug.Log("Unable to initialize admob on EDITOR");
		#elif UNITY_ANDROID
		Admob.Instance ().initAdmob (androidBanner,androidInterstitial);
		Admob.Instance ().loadInterstitial ();
		#elif UNITY_IOS
		Admob.Instance ().initAdmob (iosBanner,iosInterstitial);
		Admob.Instance ().loadInterstitial ();
		#endif
		if (showBannerAtStart)
			ShowBannerAd ();
	}


	public void ShowImageAd()
	{
		#if UNITY_EDITOR
		Debug.Log("Unable to show admob image ad on EDITOR");
		#elif UNITY_ANDROID	|| UNITY_IOS
		if (Admob.Instance ().isInterstitialReady()) 
		{
		Admob.Instance ().showInterstitial ();
		}
		else
		Debug.LogError("Admob interstitial cant be loaded");
		#endif
	}

	public void ShowBannerAd()
	{
		#if UNITY_ANDROID
		Admob.Instance ().initAdmob (androidBanner, androidInterstitial);
		Admob.Instance ().showBannerRelative(AdSize.Banner, AdPosition.BOTTOM_LEFT, 0);
		#elif UNITY_IOS
		Admob.Instance ().initAdmob (iosBanner, iosInterstitial);
		Admob.Instance ().showBannerRelative(AdSize.Banner, AdPosition.BOTTOM_LEFT, 0);
		#endif
	}

	public void ReGetInterstitial()
	{
		#if UNITY_ANDROID || UNITY_IOS
		Admob.Instance ().loadInterstitial ();
		#endif
	}
}
