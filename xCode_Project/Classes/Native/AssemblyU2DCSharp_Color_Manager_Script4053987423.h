﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// Dice_Script
struct Dice_Script_t640145349;
// Turn_Manager
struct Turn_Manager_t2388373579;
// Raycast_Manager
struct Raycast_Manager_t4268834365;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// UnityEngine.Camera
struct Camera_t189460977;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Color_Manager_Script
struct  Color_Manager_Script_t4053987423  : public MonoBehaviour_t1158329972
{
public:
	// System.String Color_Manager_Script::myName
	String_t* ___myName_2;
	// System.Boolean Color_Manager_Script::amIPlayin
	bool ___amIPlayin_3;
	// UnityEngine.GameObject[] Color_Manager_Script::meriGotiya
	GameObjectU5BU5D_t3057952154* ___meriGotiya_4;
	// Dice_Script Color_Manager_Script::dc
	Dice_Script_t640145349 * ___dc_5;
	// Turn_Manager Color_Manager_Script::tm
	Turn_Manager_t2388373579 * ___tm_6;
	// System.Int32 Color_Manager_Script::diceInput
	int32_t ___diceInput_7;
	// System.Boolean Color_Manager_Script::canIPlay
	bool ___canIPlay_8;
	// Raycast_Manager Color_Manager_Script::rm
	Raycast_Manager_t4268834365 * ___rm_9;
	// UnityEngine.Transform[] Color_Manager_Script::myPath
	TransformU5BU5D_t3764228911* ___myPath_10;
	// UnityEngine.Transform[] Color_Manager_Script::safePoints
	TransformU5BU5D_t3764228911* ___safePoints_11;
	// System.Int32 Color_Manager_Script::noOfPlaying
	int32_t ___noOfPlaying_12;
	// System.Boolean Color_Manager_Script::CPU
	bool ___CPU_13;
	// System.Int32 Color_Manager_Script::noOfGotiyaWin
	int32_t ___noOfGotiyaWin_14;
	// UnityEngine.GameObject Color_Manager_Script::OnWinObj
	GameObject_t1756533147 * ___OnWinObj_15;
	// System.Boolean Color_Manager_Script::haveWon
	bool ___haveWon_16;
	// System.Boolean Color_Manager_Script::haveLost
	bool ___haveLost_17;
	// UnityEngine.AudioSource Color_Manager_Script::mukkeKiAwaaz
	AudioSource_t1135106623 * ___mukkeKiAwaaz_18;
	// UnityEngine.Camera Color_Manager_Script::greenLane
	Camera_t189460977 * ___greenLane_19;
	// UnityEngine.Camera Color_Manager_Script::redLane
	Camera_t189460977 * ___redLane_20;
	// UnityEngine.Camera Color_Manager_Script::yellowLane
	Camera_t189460977 * ___yellowLane_21;
	// UnityEngine.Camera Color_Manager_Script::blueLane
	Camera_t189460977 * ___blueLane_22;
	// UnityEngine.Camera Color_Manager_Script::myMainCamera
	Camera_t189460977 * ___myMainCamera_23;

public:
	inline static int32_t get_offset_of_myName_2() { return static_cast<int32_t>(offsetof(Color_Manager_Script_t4053987423, ___myName_2)); }
	inline String_t* get_myName_2() const { return ___myName_2; }
	inline String_t** get_address_of_myName_2() { return &___myName_2; }
	inline void set_myName_2(String_t* value)
	{
		___myName_2 = value;
		Il2CppCodeGenWriteBarrier(&___myName_2, value);
	}

	inline static int32_t get_offset_of_amIPlayin_3() { return static_cast<int32_t>(offsetof(Color_Manager_Script_t4053987423, ___amIPlayin_3)); }
	inline bool get_amIPlayin_3() const { return ___amIPlayin_3; }
	inline bool* get_address_of_amIPlayin_3() { return &___amIPlayin_3; }
	inline void set_amIPlayin_3(bool value)
	{
		___amIPlayin_3 = value;
	}

	inline static int32_t get_offset_of_meriGotiya_4() { return static_cast<int32_t>(offsetof(Color_Manager_Script_t4053987423, ___meriGotiya_4)); }
	inline GameObjectU5BU5D_t3057952154* get_meriGotiya_4() const { return ___meriGotiya_4; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_meriGotiya_4() { return &___meriGotiya_4; }
	inline void set_meriGotiya_4(GameObjectU5BU5D_t3057952154* value)
	{
		___meriGotiya_4 = value;
		Il2CppCodeGenWriteBarrier(&___meriGotiya_4, value);
	}

	inline static int32_t get_offset_of_dc_5() { return static_cast<int32_t>(offsetof(Color_Manager_Script_t4053987423, ___dc_5)); }
	inline Dice_Script_t640145349 * get_dc_5() const { return ___dc_5; }
	inline Dice_Script_t640145349 ** get_address_of_dc_5() { return &___dc_5; }
	inline void set_dc_5(Dice_Script_t640145349 * value)
	{
		___dc_5 = value;
		Il2CppCodeGenWriteBarrier(&___dc_5, value);
	}

	inline static int32_t get_offset_of_tm_6() { return static_cast<int32_t>(offsetof(Color_Manager_Script_t4053987423, ___tm_6)); }
	inline Turn_Manager_t2388373579 * get_tm_6() const { return ___tm_6; }
	inline Turn_Manager_t2388373579 ** get_address_of_tm_6() { return &___tm_6; }
	inline void set_tm_6(Turn_Manager_t2388373579 * value)
	{
		___tm_6 = value;
		Il2CppCodeGenWriteBarrier(&___tm_6, value);
	}

	inline static int32_t get_offset_of_diceInput_7() { return static_cast<int32_t>(offsetof(Color_Manager_Script_t4053987423, ___diceInput_7)); }
	inline int32_t get_diceInput_7() const { return ___diceInput_7; }
	inline int32_t* get_address_of_diceInput_7() { return &___diceInput_7; }
	inline void set_diceInput_7(int32_t value)
	{
		___diceInput_7 = value;
	}

	inline static int32_t get_offset_of_canIPlay_8() { return static_cast<int32_t>(offsetof(Color_Manager_Script_t4053987423, ___canIPlay_8)); }
	inline bool get_canIPlay_8() const { return ___canIPlay_8; }
	inline bool* get_address_of_canIPlay_8() { return &___canIPlay_8; }
	inline void set_canIPlay_8(bool value)
	{
		___canIPlay_8 = value;
	}

	inline static int32_t get_offset_of_rm_9() { return static_cast<int32_t>(offsetof(Color_Manager_Script_t4053987423, ___rm_9)); }
	inline Raycast_Manager_t4268834365 * get_rm_9() const { return ___rm_9; }
	inline Raycast_Manager_t4268834365 ** get_address_of_rm_9() { return &___rm_9; }
	inline void set_rm_9(Raycast_Manager_t4268834365 * value)
	{
		___rm_9 = value;
		Il2CppCodeGenWriteBarrier(&___rm_9, value);
	}

	inline static int32_t get_offset_of_myPath_10() { return static_cast<int32_t>(offsetof(Color_Manager_Script_t4053987423, ___myPath_10)); }
	inline TransformU5BU5D_t3764228911* get_myPath_10() const { return ___myPath_10; }
	inline TransformU5BU5D_t3764228911** get_address_of_myPath_10() { return &___myPath_10; }
	inline void set_myPath_10(TransformU5BU5D_t3764228911* value)
	{
		___myPath_10 = value;
		Il2CppCodeGenWriteBarrier(&___myPath_10, value);
	}

	inline static int32_t get_offset_of_safePoints_11() { return static_cast<int32_t>(offsetof(Color_Manager_Script_t4053987423, ___safePoints_11)); }
	inline TransformU5BU5D_t3764228911* get_safePoints_11() const { return ___safePoints_11; }
	inline TransformU5BU5D_t3764228911** get_address_of_safePoints_11() { return &___safePoints_11; }
	inline void set_safePoints_11(TransformU5BU5D_t3764228911* value)
	{
		___safePoints_11 = value;
		Il2CppCodeGenWriteBarrier(&___safePoints_11, value);
	}

	inline static int32_t get_offset_of_noOfPlaying_12() { return static_cast<int32_t>(offsetof(Color_Manager_Script_t4053987423, ___noOfPlaying_12)); }
	inline int32_t get_noOfPlaying_12() const { return ___noOfPlaying_12; }
	inline int32_t* get_address_of_noOfPlaying_12() { return &___noOfPlaying_12; }
	inline void set_noOfPlaying_12(int32_t value)
	{
		___noOfPlaying_12 = value;
	}

	inline static int32_t get_offset_of_CPU_13() { return static_cast<int32_t>(offsetof(Color_Manager_Script_t4053987423, ___CPU_13)); }
	inline bool get_CPU_13() const { return ___CPU_13; }
	inline bool* get_address_of_CPU_13() { return &___CPU_13; }
	inline void set_CPU_13(bool value)
	{
		___CPU_13 = value;
	}

	inline static int32_t get_offset_of_noOfGotiyaWin_14() { return static_cast<int32_t>(offsetof(Color_Manager_Script_t4053987423, ___noOfGotiyaWin_14)); }
	inline int32_t get_noOfGotiyaWin_14() const { return ___noOfGotiyaWin_14; }
	inline int32_t* get_address_of_noOfGotiyaWin_14() { return &___noOfGotiyaWin_14; }
	inline void set_noOfGotiyaWin_14(int32_t value)
	{
		___noOfGotiyaWin_14 = value;
	}

	inline static int32_t get_offset_of_OnWinObj_15() { return static_cast<int32_t>(offsetof(Color_Manager_Script_t4053987423, ___OnWinObj_15)); }
	inline GameObject_t1756533147 * get_OnWinObj_15() const { return ___OnWinObj_15; }
	inline GameObject_t1756533147 ** get_address_of_OnWinObj_15() { return &___OnWinObj_15; }
	inline void set_OnWinObj_15(GameObject_t1756533147 * value)
	{
		___OnWinObj_15 = value;
		Il2CppCodeGenWriteBarrier(&___OnWinObj_15, value);
	}

	inline static int32_t get_offset_of_haveWon_16() { return static_cast<int32_t>(offsetof(Color_Manager_Script_t4053987423, ___haveWon_16)); }
	inline bool get_haveWon_16() const { return ___haveWon_16; }
	inline bool* get_address_of_haveWon_16() { return &___haveWon_16; }
	inline void set_haveWon_16(bool value)
	{
		___haveWon_16 = value;
	}

	inline static int32_t get_offset_of_haveLost_17() { return static_cast<int32_t>(offsetof(Color_Manager_Script_t4053987423, ___haveLost_17)); }
	inline bool get_haveLost_17() const { return ___haveLost_17; }
	inline bool* get_address_of_haveLost_17() { return &___haveLost_17; }
	inline void set_haveLost_17(bool value)
	{
		___haveLost_17 = value;
	}

	inline static int32_t get_offset_of_mukkeKiAwaaz_18() { return static_cast<int32_t>(offsetof(Color_Manager_Script_t4053987423, ___mukkeKiAwaaz_18)); }
	inline AudioSource_t1135106623 * get_mukkeKiAwaaz_18() const { return ___mukkeKiAwaaz_18; }
	inline AudioSource_t1135106623 ** get_address_of_mukkeKiAwaaz_18() { return &___mukkeKiAwaaz_18; }
	inline void set_mukkeKiAwaaz_18(AudioSource_t1135106623 * value)
	{
		___mukkeKiAwaaz_18 = value;
		Il2CppCodeGenWriteBarrier(&___mukkeKiAwaaz_18, value);
	}

	inline static int32_t get_offset_of_greenLane_19() { return static_cast<int32_t>(offsetof(Color_Manager_Script_t4053987423, ___greenLane_19)); }
	inline Camera_t189460977 * get_greenLane_19() const { return ___greenLane_19; }
	inline Camera_t189460977 ** get_address_of_greenLane_19() { return &___greenLane_19; }
	inline void set_greenLane_19(Camera_t189460977 * value)
	{
		___greenLane_19 = value;
		Il2CppCodeGenWriteBarrier(&___greenLane_19, value);
	}

	inline static int32_t get_offset_of_redLane_20() { return static_cast<int32_t>(offsetof(Color_Manager_Script_t4053987423, ___redLane_20)); }
	inline Camera_t189460977 * get_redLane_20() const { return ___redLane_20; }
	inline Camera_t189460977 ** get_address_of_redLane_20() { return &___redLane_20; }
	inline void set_redLane_20(Camera_t189460977 * value)
	{
		___redLane_20 = value;
		Il2CppCodeGenWriteBarrier(&___redLane_20, value);
	}

	inline static int32_t get_offset_of_yellowLane_21() { return static_cast<int32_t>(offsetof(Color_Manager_Script_t4053987423, ___yellowLane_21)); }
	inline Camera_t189460977 * get_yellowLane_21() const { return ___yellowLane_21; }
	inline Camera_t189460977 ** get_address_of_yellowLane_21() { return &___yellowLane_21; }
	inline void set_yellowLane_21(Camera_t189460977 * value)
	{
		___yellowLane_21 = value;
		Il2CppCodeGenWriteBarrier(&___yellowLane_21, value);
	}

	inline static int32_t get_offset_of_blueLane_22() { return static_cast<int32_t>(offsetof(Color_Manager_Script_t4053987423, ___blueLane_22)); }
	inline Camera_t189460977 * get_blueLane_22() const { return ___blueLane_22; }
	inline Camera_t189460977 ** get_address_of_blueLane_22() { return &___blueLane_22; }
	inline void set_blueLane_22(Camera_t189460977 * value)
	{
		___blueLane_22 = value;
		Il2CppCodeGenWriteBarrier(&___blueLane_22, value);
	}

	inline static int32_t get_offset_of_myMainCamera_23() { return static_cast<int32_t>(offsetof(Color_Manager_Script_t4053987423, ___myMainCamera_23)); }
	inline Camera_t189460977 * get_myMainCamera_23() const { return ___myMainCamera_23; }
	inline Camera_t189460977 ** get_address_of_myMainCamera_23() { return &___myMainCamera_23; }
	inline void set_myMainCamera_23(Camera_t189460977 * value)
	{
		___myMainCamera_23 = value;
		Il2CppCodeGenWriteBarrier(&___myMainCamera_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
