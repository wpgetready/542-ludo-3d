﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Game_Manager
struct Game_Manager_t4105539090;

#include "codegen/il2cpp-codegen.h"

// System.Void Game_Manager::.ctor()
extern "C"  void Game_Manager__ctor_m3655742037 (Game_Manager_t4105539090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Game_Manager Game_Manager::get__gminstance()
extern "C"  Game_Manager_t4105539090 * Game_Manager_get__gminstance_m2308798245 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Game_Manager::Start()
extern "C"  void Game_Manager_Start_m2210529981 (Game_Manager_t4105539090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Game_Manager::Update()
extern "C"  void Game_Manager_Update_m1417075124 (Game_Manager_t4105539090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Game_Manager::SetPlayers(System.Int32)
extern "C"  void Game_Manager_SetPlayers_m277409050 (Game_Manager_t4105539090 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Game_Manager::GetPlayers()
extern "C"  int32_t Game_Manager_GetPlayers_m971560877 (Game_Manager_t4105539090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Game_Manager::ChangeArena()
extern "C"  void Game_Manager_ChangeArena_m3365242478 (Game_Manager_t4105539090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
