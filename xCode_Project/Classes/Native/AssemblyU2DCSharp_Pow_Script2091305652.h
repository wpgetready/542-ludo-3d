﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Camera
struct Camera_t189460977;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pow_Script
struct  Pow_Script_t2091305652  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Pow_Script::destroyTime
	float ___destroyTime_2;
	// UnityEngine.Camera Pow_Script::targetCam
	Camera_t189460977 * ___targetCam_3;

public:
	inline static int32_t get_offset_of_destroyTime_2() { return static_cast<int32_t>(offsetof(Pow_Script_t2091305652, ___destroyTime_2)); }
	inline float get_destroyTime_2() const { return ___destroyTime_2; }
	inline float* get_address_of_destroyTime_2() { return &___destroyTime_2; }
	inline void set_destroyTime_2(float value)
	{
		___destroyTime_2 = value;
	}

	inline static int32_t get_offset_of_targetCam_3() { return static_cast<int32_t>(offsetof(Pow_Script_t2091305652, ___targetCam_3)); }
	inline Camera_t189460977 * get_targetCam_3() const { return ___targetCam_3; }
	inline Camera_t189460977 ** get_address_of_targetCam_3() { return &___targetCam_3; }
	inline void set_targetCam_3(Camera_t189460977 * value)
	{
		___targetCam_3 = value;
		Il2CppCodeGenWriteBarrier(&___targetCam_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
