﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Goti_Status
struct Goti_Status_t1473870168;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Collider
struct Collider_t3497673348;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"

// System.Void Goti_Status::.ctor()
extern "C"  void Goti_Status__ctor_m4251937469 (Goti_Status_t1473870168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Goti_Status::Start()
extern "C"  void Goti_Status_Start_m1169207645 (Goti_Status_t1473870168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Goti_Status::Update()
extern "C"  void Goti_Status_Update_m1463045634 (Goti_Status_t1473870168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Goti_Status::ShowICanPlay()
extern "C"  void Goti_Status_ShowICanPlay_m3291303315 (Goti_Status_t1473870168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Goti_Status::StopShowing()
extern "C"  void Goti_Status_StopShowing_m4087610708 (Goti_Status_t1473870168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Goti_Status::PerformMovement()
extern "C"  void Goti_Status_PerformMovement_m702764983 (Goti_Status_t1473870168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Goti_Status::AfterMovement()
extern "C"  void Goti_Status_AfterMovement_m2604936664 (Goti_Status_t1473870168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Goti_Status::GotiWin()
extern "C"  void Goti_Status_GotiWin_m1675532522 (Goti_Status_t1473870168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Goti_Status::CutGoti(System.Single)
extern "C"  Il2CppObject * Goti_Status_CutGoti_m1406000177 (Goti_Status_t1473870168 * __this, float ___f0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Goti_Status::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void Goti_Status_OnTriggerEnter_m3436573705 (Goti_Status_t1473870168 * __this, Collider_t3497673348 * ___col0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Goti_Status::AfterAttack()
extern "C"  void Goti_Status_AfterAttack_m2009128161 (Goti_Status_t1473870168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Goti_Status::NextTurn()
extern "C"  void Goti_Status_NextTurn_m3875928647 (Goti_Status_t1473870168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Goti_Status::RepeatTurn()
extern "C"  void Goti_Status_RepeatTurn_m1200754475 (Goti_Status_t1473870168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Goti_Status::FlagLogics()
extern "C"  void Goti_Status_FlagLogics_m2683927470 (Goti_Status_t1473870168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Goti_Status::ResetAllCameras()
extern "C"  void Goti_Status_ResetAllCameras_m1082687783 (Goti_Status_t1473870168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
