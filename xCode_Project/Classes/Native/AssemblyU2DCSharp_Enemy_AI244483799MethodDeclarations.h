﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Enemy_AI
struct Enemy_AI_t244483799;

#include "codegen/il2cpp-codegen.h"

// System.Void Enemy_AI::.ctor()
extern "C"  void Enemy_AI__ctor_m3907679114 (Enemy_AI_t244483799 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Enemy_AI::Start()
extern "C"  void Enemy_AI_Start_m1919036822 (Enemy_AI_t244483799 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Enemy_AI::Update()
extern "C"  void Enemy_AI_Update_m1237139619 (Enemy_AI_t244483799 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Enemy_AI::StopRolling()
extern "C"  void Enemy_AI_StopRolling_m1450032615 (Enemy_AI_t244483799 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Enemy_AI::MovePiece()
extern "C"  void Enemy_AI_MovePiece_m1145793173 (Enemy_AI_t244483799 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Enemy_AI::isAnyOneInVicinity()
extern "C"  bool Enemy_AI_isAnyOneInVicinity_m306585268 (Enemy_AI_t244483799 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Enemy_AI::TurnNotOn6()
extern "C"  void Enemy_AI_TurnNotOn6_m871805665 (Enemy_AI_t244483799 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
