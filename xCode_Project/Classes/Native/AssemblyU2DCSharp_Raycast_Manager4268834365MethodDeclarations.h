﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Raycast_Manager
struct Raycast_Manager_t4268834365;

#include "codegen/il2cpp-codegen.h"

// System.Void Raycast_Manager::.ctor()
extern "C"  void Raycast_Manager__ctor_m127067030 (Raycast_Manager_t4268834365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Raycast_Manager::Update()
extern "C"  void Raycast_Manager_Update_m1479873385 (Raycast_Manager_t4268834365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
