﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// admob.AdPosition
struct AdPosition_t1947228246;

#include "codegen/il2cpp-codegen.h"

// System.Void admob.AdPosition::.ctor()
extern "C"  void AdPosition__ctor_m417448268 (AdPosition_t1947228246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void admob.AdPosition::.cctor()
extern "C"  void AdPosition__cctor_m196470457 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
