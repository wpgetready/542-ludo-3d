﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Color_Manager_Script>
struct List_1_t3423108555;
// UnityEngine.UI.Text
struct Text_t356221433;
// Color_Manager_Script
struct Color_Manager_Script_t4053987423;
// Raycast_Manager
struct Raycast_Manager_t4268834365;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// Dice_Script
struct Dice_Script_t640145349;
// System.String
struct String_t;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// UnityEngine.Camera
struct Camera_t189460977;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Turn_Manager
struct  Turn_Manager_t2388373579  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<Color_Manager_Script> Turn_Manager::listOfPlayers
	List_1_t3423108555 * ___listOfPlayers_2;
	// UnityEngine.UI.Text Turn_Manager::notificationText
	Text_t356221433 * ___notificationText_3;
	// Color_Manager_Script Turn_Manager::currentTurn
	Color_Manager_Script_t4053987423 * ___currentTurn_4;
	// Color_Manager_Script Turn_Manager::nextTurn
	Color_Manager_Script_t4053987423 * ___nextTurn_5;
	// System.Int32 Turn_Manager::indexTurn
	int32_t ___indexTurn_6;
	// Raycast_Manager Turn_Manager::rm
	Raycast_Manager_t4268834365 * ___rm_7;
	// UnityEngine.GameObject Turn_Manager::board
	GameObject_t1756533147 * ___board_8;
	// UnityEngine.GameObject Turn_Manager::diceRollButt
	GameObject_t1756533147 * ___diceRollButt_9;
	// System.Boolean Turn_Manager::startRolling
	bool ___startRolling_10;
	// Dice_Script Turn_Manager::dc
	Dice_Script_t640145349 * ___dc_11;
	// System.Boolean Turn_Manager::calledOnce
	bool ___calledOnce_12;
	// System.Boolean Turn_Manager::isPaused
	bool ___isPaused_13;
	// UnityEngine.GameObject Turn_Manager::pauseUI
	GameObject_t1756533147 * ___pauseUI_14;
	// System.Int32 Turn_Manager::wonPos
	int32_t ___wonPos_15;
	// System.Boolean Turn_Manager::isCPUGame
	bool ___isCPUGame_16;
	// UnityEngine.GameObject Turn_Manager::gameCompletionObj
	GameObject_t1756533147 * ___gameCompletionObj_17;
	// UnityEngine.GameObject Turn_Manager::ListObj
	GameObject_t1756533147 * ___ListObj_18;
	// System.Int32 Turn_Manager::noOfPlayers
	int32_t ___noOfPlayers_19;
	// System.String Turn_Manager::first
	String_t* ___first_20;
	// System.String Turn_Manager::second
	String_t* ___second_21;
	// System.String Turn_Manager::third
	String_t* ___third_22;
	// System.String Turn_Manager::fourth
	String_t* ___fourth_23;
	// UnityEngine.AudioSource Turn_Manager::asDice
	AudioSource_t1135106623 * ___asDice_24;
	// UnityEngine.AudioSource Turn_Manager::victoryAS
	AudioSource_t1135106623 * ___victoryAS_25;
	// UnityEngine.Camera Turn_Manager::myMain
	Camera_t189460977 * ___myMain_26;
	// UnityEngine.Camera Turn_Manager::victoryCam
	Camera_t189460977 * ___victoryCam_27;
	// UnityEngine.GameObject Turn_Manager::victoryObj
	GameObject_t1756533147 * ___victoryObj_28;

public:
	inline static int32_t get_offset_of_listOfPlayers_2() { return static_cast<int32_t>(offsetof(Turn_Manager_t2388373579, ___listOfPlayers_2)); }
	inline List_1_t3423108555 * get_listOfPlayers_2() const { return ___listOfPlayers_2; }
	inline List_1_t3423108555 ** get_address_of_listOfPlayers_2() { return &___listOfPlayers_2; }
	inline void set_listOfPlayers_2(List_1_t3423108555 * value)
	{
		___listOfPlayers_2 = value;
		Il2CppCodeGenWriteBarrier(&___listOfPlayers_2, value);
	}

	inline static int32_t get_offset_of_notificationText_3() { return static_cast<int32_t>(offsetof(Turn_Manager_t2388373579, ___notificationText_3)); }
	inline Text_t356221433 * get_notificationText_3() const { return ___notificationText_3; }
	inline Text_t356221433 ** get_address_of_notificationText_3() { return &___notificationText_3; }
	inline void set_notificationText_3(Text_t356221433 * value)
	{
		___notificationText_3 = value;
		Il2CppCodeGenWriteBarrier(&___notificationText_3, value);
	}

	inline static int32_t get_offset_of_currentTurn_4() { return static_cast<int32_t>(offsetof(Turn_Manager_t2388373579, ___currentTurn_4)); }
	inline Color_Manager_Script_t4053987423 * get_currentTurn_4() const { return ___currentTurn_4; }
	inline Color_Manager_Script_t4053987423 ** get_address_of_currentTurn_4() { return &___currentTurn_4; }
	inline void set_currentTurn_4(Color_Manager_Script_t4053987423 * value)
	{
		___currentTurn_4 = value;
		Il2CppCodeGenWriteBarrier(&___currentTurn_4, value);
	}

	inline static int32_t get_offset_of_nextTurn_5() { return static_cast<int32_t>(offsetof(Turn_Manager_t2388373579, ___nextTurn_5)); }
	inline Color_Manager_Script_t4053987423 * get_nextTurn_5() const { return ___nextTurn_5; }
	inline Color_Manager_Script_t4053987423 ** get_address_of_nextTurn_5() { return &___nextTurn_5; }
	inline void set_nextTurn_5(Color_Manager_Script_t4053987423 * value)
	{
		___nextTurn_5 = value;
		Il2CppCodeGenWriteBarrier(&___nextTurn_5, value);
	}

	inline static int32_t get_offset_of_indexTurn_6() { return static_cast<int32_t>(offsetof(Turn_Manager_t2388373579, ___indexTurn_6)); }
	inline int32_t get_indexTurn_6() const { return ___indexTurn_6; }
	inline int32_t* get_address_of_indexTurn_6() { return &___indexTurn_6; }
	inline void set_indexTurn_6(int32_t value)
	{
		___indexTurn_6 = value;
	}

	inline static int32_t get_offset_of_rm_7() { return static_cast<int32_t>(offsetof(Turn_Manager_t2388373579, ___rm_7)); }
	inline Raycast_Manager_t4268834365 * get_rm_7() const { return ___rm_7; }
	inline Raycast_Manager_t4268834365 ** get_address_of_rm_7() { return &___rm_7; }
	inline void set_rm_7(Raycast_Manager_t4268834365 * value)
	{
		___rm_7 = value;
		Il2CppCodeGenWriteBarrier(&___rm_7, value);
	}

	inline static int32_t get_offset_of_board_8() { return static_cast<int32_t>(offsetof(Turn_Manager_t2388373579, ___board_8)); }
	inline GameObject_t1756533147 * get_board_8() const { return ___board_8; }
	inline GameObject_t1756533147 ** get_address_of_board_8() { return &___board_8; }
	inline void set_board_8(GameObject_t1756533147 * value)
	{
		___board_8 = value;
		Il2CppCodeGenWriteBarrier(&___board_8, value);
	}

	inline static int32_t get_offset_of_diceRollButt_9() { return static_cast<int32_t>(offsetof(Turn_Manager_t2388373579, ___diceRollButt_9)); }
	inline GameObject_t1756533147 * get_diceRollButt_9() const { return ___diceRollButt_9; }
	inline GameObject_t1756533147 ** get_address_of_diceRollButt_9() { return &___diceRollButt_9; }
	inline void set_diceRollButt_9(GameObject_t1756533147 * value)
	{
		___diceRollButt_9 = value;
		Il2CppCodeGenWriteBarrier(&___diceRollButt_9, value);
	}

	inline static int32_t get_offset_of_startRolling_10() { return static_cast<int32_t>(offsetof(Turn_Manager_t2388373579, ___startRolling_10)); }
	inline bool get_startRolling_10() const { return ___startRolling_10; }
	inline bool* get_address_of_startRolling_10() { return &___startRolling_10; }
	inline void set_startRolling_10(bool value)
	{
		___startRolling_10 = value;
	}

	inline static int32_t get_offset_of_dc_11() { return static_cast<int32_t>(offsetof(Turn_Manager_t2388373579, ___dc_11)); }
	inline Dice_Script_t640145349 * get_dc_11() const { return ___dc_11; }
	inline Dice_Script_t640145349 ** get_address_of_dc_11() { return &___dc_11; }
	inline void set_dc_11(Dice_Script_t640145349 * value)
	{
		___dc_11 = value;
		Il2CppCodeGenWriteBarrier(&___dc_11, value);
	}

	inline static int32_t get_offset_of_calledOnce_12() { return static_cast<int32_t>(offsetof(Turn_Manager_t2388373579, ___calledOnce_12)); }
	inline bool get_calledOnce_12() const { return ___calledOnce_12; }
	inline bool* get_address_of_calledOnce_12() { return &___calledOnce_12; }
	inline void set_calledOnce_12(bool value)
	{
		___calledOnce_12 = value;
	}

	inline static int32_t get_offset_of_isPaused_13() { return static_cast<int32_t>(offsetof(Turn_Manager_t2388373579, ___isPaused_13)); }
	inline bool get_isPaused_13() const { return ___isPaused_13; }
	inline bool* get_address_of_isPaused_13() { return &___isPaused_13; }
	inline void set_isPaused_13(bool value)
	{
		___isPaused_13 = value;
	}

	inline static int32_t get_offset_of_pauseUI_14() { return static_cast<int32_t>(offsetof(Turn_Manager_t2388373579, ___pauseUI_14)); }
	inline GameObject_t1756533147 * get_pauseUI_14() const { return ___pauseUI_14; }
	inline GameObject_t1756533147 ** get_address_of_pauseUI_14() { return &___pauseUI_14; }
	inline void set_pauseUI_14(GameObject_t1756533147 * value)
	{
		___pauseUI_14 = value;
		Il2CppCodeGenWriteBarrier(&___pauseUI_14, value);
	}

	inline static int32_t get_offset_of_wonPos_15() { return static_cast<int32_t>(offsetof(Turn_Manager_t2388373579, ___wonPos_15)); }
	inline int32_t get_wonPos_15() const { return ___wonPos_15; }
	inline int32_t* get_address_of_wonPos_15() { return &___wonPos_15; }
	inline void set_wonPos_15(int32_t value)
	{
		___wonPos_15 = value;
	}

	inline static int32_t get_offset_of_isCPUGame_16() { return static_cast<int32_t>(offsetof(Turn_Manager_t2388373579, ___isCPUGame_16)); }
	inline bool get_isCPUGame_16() const { return ___isCPUGame_16; }
	inline bool* get_address_of_isCPUGame_16() { return &___isCPUGame_16; }
	inline void set_isCPUGame_16(bool value)
	{
		___isCPUGame_16 = value;
	}

	inline static int32_t get_offset_of_gameCompletionObj_17() { return static_cast<int32_t>(offsetof(Turn_Manager_t2388373579, ___gameCompletionObj_17)); }
	inline GameObject_t1756533147 * get_gameCompletionObj_17() const { return ___gameCompletionObj_17; }
	inline GameObject_t1756533147 ** get_address_of_gameCompletionObj_17() { return &___gameCompletionObj_17; }
	inline void set_gameCompletionObj_17(GameObject_t1756533147 * value)
	{
		___gameCompletionObj_17 = value;
		Il2CppCodeGenWriteBarrier(&___gameCompletionObj_17, value);
	}

	inline static int32_t get_offset_of_ListObj_18() { return static_cast<int32_t>(offsetof(Turn_Manager_t2388373579, ___ListObj_18)); }
	inline GameObject_t1756533147 * get_ListObj_18() const { return ___ListObj_18; }
	inline GameObject_t1756533147 ** get_address_of_ListObj_18() { return &___ListObj_18; }
	inline void set_ListObj_18(GameObject_t1756533147 * value)
	{
		___ListObj_18 = value;
		Il2CppCodeGenWriteBarrier(&___ListObj_18, value);
	}

	inline static int32_t get_offset_of_noOfPlayers_19() { return static_cast<int32_t>(offsetof(Turn_Manager_t2388373579, ___noOfPlayers_19)); }
	inline int32_t get_noOfPlayers_19() const { return ___noOfPlayers_19; }
	inline int32_t* get_address_of_noOfPlayers_19() { return &___noOfPlayers_19; }
	inline void set_noOfPlayers_19(int32_t value)
	{
		___noOfPlayers_19 = value;
	}

	inline static int32_t get_offset_of_first_20() { return static_cast<int32_t>(offsetof(Turn_Manager_t2388373579, ___first_20)); }
	inline String_t* get_first_20() const { return ___first_20; }
	inline String_t** get_address_of_first_20() { return &___first_20; }
	inline void set_first_20(String_t* value)
	{
		___first_20 = value;
		Il2CppCodeGenWriteBarrier(&___first_20, value);
	}

	inline static int32_t get_offset_of_second_21() { return static_cast<int32_t>(offsetof(Turn_Manager_t2388373579, ___second_21)); }
	inline String_t* get_second_21() const { return ___second_21; }
	inline String_t** get_address_of_second_21() { return &___second_21; }
	inline void set_second_21(String_t* value)
	{
		___second_21 = value;
		Il2CppCodeGenWriteBarrier(&___second_21, value);
	}

	inline static int32_t get_offset_of_third_22() { return static_cast<int32_t>(offsetof(Turn_Manager_t2388373579, ___third_22)); }
	inline String_t* get_third_22() const { return ___third_22; }
	inline String_t** get_address_of_third_22() { return &___third_22; }
	inline void set_third_22(String_t* value)
	{
		___third_22 = value;
		Il2CppCodeGenWriteBarrier(&___third_22, value);
	}

	inline static int32_t get_offset_of_fourth_23() { return static_cast<int32_t>(offsetof(Turn_Manager_t2388373579, ___fourth_23)); }
	inline String_t* get_fourth_23() const { return ___fourth_23; }
	inline String_t** get_address_of_fourth_23() { return &___fourth_23; }
	inline void set_fourth_23(String_t* value)
	{
		___fourth_23 = value;
		Il2CppCodeGenWriteBarrier(&___fourth_23, value);
	}

	inline static int32_t get_offset_of_asDice_24() { return static_cast<int32_t>(offsetof(Turn_Manager_t2388373579, ___asDice_24)); }
	inline AudioSource_t1135106623 * get_asDice_24() const { return ___asDice_24; }
	inline AudioSource_t1135106623 ** get_address_of_asDice_24() { return &___asDice_24; }
	inline void set_asDice_24(AudioSource_t1135106623 * value)
	{
		___asDice_24 = value;
		Il2CppCodeGenWriteBarrier(&___asDice_24, value);
	}

	inline static int32_t get_offset_of_victoryAS_25() { return static_cast<int32_t>(offsetof(Turn_Manager_t2388373579, ___victoryAS_25)); }
	inline AudioSource_t1135106623 * get_victoryAS_25() const { return ___victoryAS_25; }
	inline AudioSource_t1135106623 ** get_address_of_victoryAS_25() { return &___victoryAS_25; }
	inline void set_victoryAS_25(AudioSource_t1135106623 * value)
	{
		___victoryAS_25 = value;
		Il2CppCodeGenWriteBarrier(&___victoryAS_25, value);
	}

	inline static int32_t get_offset_of_myMain_26() { return static_cast<int32_t>(offsetof(Turn_Manager_t2388373579, ___myMain_26)); }
	inline Camera_t189460977 * get_myMain_26() const { return ___myMain_26; }
	inline Camera_t189460977 ** get_address_of_myMain_26() { return &___myMain_26; }
	inline void set_myMain_26(Camera_t189460977 * value)
	{
		___myMain_26 = value;
		Il2CppCodeGenWriteBarrier(&___myMain_26, value);
	}

	inline static int32_t get_offset_of_victoryCam_27() { return static_cast<int32_t>(offsetof(Turn_Manager_t2388373579, ___victoryCam_27)); }
	inline Camera_t189460977 * get_victoryCam_27() const { return ___victoryCam_27; }
	inline Camera_t189460977 ** get_address_of_victoryCam_27() { return &___victoryCam_27; }
	inline void set_victoryCam_27(Camera_t189460977 * value)
	{
		___victoryCam_27 = value;
		Il2CppCodeGenWriteBarrier(&___victoryCam_27, value);
	}

	inline static int32_t get_offset_of_victoryObj_28() { return static_cast<int32_t>(offsetof(Turn_Manager_t2388373579, ___victoryObj_28)); }
	inline GameObject_t1756533147 * get_victoryObj_28() const { return ___victoryObj_28; }
	inline GameObject_t1756533147 ** get_address_of_victoryObj_28() { return &___victoryObj_28; }
	inline void set_victoryObj_28(GameObject_t1756533147 * value)
	{
		___victoryObj_28 = value;
		Il2CppCodeGenWriteBarrier(&___victoryObj_28, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
