﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Ray2469606224.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Raycast_Manager
struct  Raycast_Manager_t4268834365  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Ray Raycast_Manager::mouseToScreen
	Ray_t2469606224  ___mouseToScreen_2;

public:
	inline static int32_t get_offset_of_mouseToScreen_2() { return static_cast<int32_t>(offsetof(Raycast_Manager_t4268834365, ___mouseToScreen_2)); }
	inline Ray_t2469606224  get_mouseToScreen_2() const { return ___mouseToScreen_2; }
	inline Ray_t2469606224 * get_address_of_mouseToScreen_2() { return &___mouseToScreen_2; }
	inline void set_mouseToScreen_2(Ray_t2469606224  value)
	{
		___mouseToScreen_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
