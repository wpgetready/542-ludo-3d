﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// Goti_Status
struct Goti_Status_t1473870168;
// Color_Manager_Script
struct Color_Manager_Script_t4053987423;

#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_Goti_Status1473870168.h"
#include "AssemblyU2DCSharp_Color_Manager_Script4053987423.h"

#pragma once
// Goti_Status[]
struct Goti_StatusU5BU5D_t3854859849  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Goti_Status_t1473870168 * m_Items[1];

public:
	inline Goti_Status_t1473870168 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Goti_Status_t1473870168 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Goti_Status_t1473870168 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Goti_Status_t1473870168 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Goti_Status_t1473870168 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Goti_Status_t1473870168 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Color_Manager_Script[]
struct Color_Manager_ScriptU5BU5D_t4081434438  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Color_Manager_Script_t4053987423 * m_Items[1];

public:
	inline Color_Manager_Script_t4053987423 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color_Manager_Script_t4053987423 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color_Manager_Script_t4053987423 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Color_Manager_Script_t4053987423 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color_Manager_Script_t4053987423 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color_Manager_Script_t4053987423 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
