﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// dance
struct dance_t2594252983;

#include "codegen/il2cpp-codegen.h"

// System.Void dance::.ctor()
extern "C"  void dance__ctor_m2809252018 (dance_t2594252983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void dance::Start()
extern "C"  void dance_Start_m1515533566 (dance_t2594252983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void dance::Update()
extern "C"  void dance_Update_m3558283735 (dance_t2594252983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
