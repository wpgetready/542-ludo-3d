﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AdMob_Manager
struct AdMob_Manager_t3979489515;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AdMob_Manager3979489515.h"

// System.Void AdMob_Manager::.ctor()
extern "C"  void AdMob_Manager__ctor_m816106580 (AdMob_Manager_t3979489515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdMob_Manager::set_Instance(AdMob_Manager)
extern "C"  void AdMob_Manager_set_Instance_m2033218227 (Il2CppObject * __this /* static, unused */, AdMob_Manager_t3979489515 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AdMob_Manager AdMob_Manager::get_Instance()
extern "C"  AdMob_Manager_t3979489515 * AdMob_Manager_get_Instance_m2706670290 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdMob_Manager::Start()
extern "C"  void AdMob_Manager_Start_m3573778096 (AdMob_Manager_t3979489515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdMob_Manager::ShowImageAd()
extern "C"  void AdMob_Manager_ShowImageAd_m789447395 (AdMob_Manager_t3979489515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdMob_Manager::ShowBannerAd()
extern "C"  void AdMob_Manager_ShowBannerAd_m1706154128 (AdMob_Manager_t3979489515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdMob_Manager::ReGetInterstitial()
extern "C"  void AdMob_Manager_ReGetInterstitial_m990499913 (AdMob_Manager_t3979489515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
