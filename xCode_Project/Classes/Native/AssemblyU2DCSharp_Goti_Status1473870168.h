﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Color_Manager_Script
struct Color_Manager_Script_t4053987423;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Animator
struct Animator_t69676727;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;
// Enemy_AI
struct Enemy_AI_t244483799;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Camera
struct Camera_t189460977;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Goti_Status_typeOfState402692453.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Goti_Status
struct  Goti_Status_t1473870168  : public MonoBehaviour_t1158329972
{
public:
	// Goti_Status/typeOfState Goti_Status::myState
	int32_t ___myState_2;
	// System.Boolean Goti_Status::canBePlayed
	bool ___canBePlayed_3;
	// Color_Manager_Script Goti_Status::myManger
	Color_Manager_Script_t4053987423 * ___myManger_4;
	// System.Boolean Goti_Status::actionPhase
	bool ___actionPhase_5;
	// System.Boolean Goti_Status::yoICanPlay
	bool ___yoICanPlay_6;
	// UnityEngine.Vector3 Goti_Status::finalPos
	Vector3_t2243707580  ___finalPos_7;
	// System.Int32 Goti_Status::curIndexPos
	int32_t ___curIndexPos_8;
	// System.Boolean Goti_Status::startRunning
	bool ___startRunning_9;
	// System.Single Goti_Status::runSpeed
	float ___runSpeed_10;
	// System.Single Goti_Status::hieght
	float ___hieght_11;
	// System.Boolean Goti_Status::calledOnce
	bool ___calledOnce_12;
	// UnityEngine.Vector3 Goti_Status::myStartPos
	Vector3_t2243707580  ___myStartPos_13;
	// System.Single Goti_Status::initY
	float ___initY_14;
	// System.Int32 Goti_Status::restrictionOutput
	int32_t ___restrictionOutput_15;
	// System.Int32 Goti_Status::pathL
	int32_t ___pathL_16;
	// UnityEngine.Transform Goti_Status::myWinPos
	Transform_t3275118058 * ___myWinPos_17;
	// System.Boolean Goti_Status::hasCollided
	bool ___hasCollided_18;
	// System.Boolean Goti_Status::isSafe
	bool ___isSafe_19;
	// UnityEngine.Animator Goti_Status::anime
	Animator_t69676727 * ___anime_20;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Goti_Status::pathToFollow
	List_1_t1612828712 * ___pathToFollow_21;
	// System.Int32 Goti_Status::listCounter
	int32_t ___listCounter_22;
	// UnityEngine.Vector3 Goti_Status::initCPos
	Vector3_t2243707580  ___initCPos_23;
	// UnityEngine.Vector3 Goti_Status::pos
	Vector3_t2243707580  ___pos_24;
	// System.Single Goti_Status::animaOffsetX
	float ___animaOffsetX_25;
	// System.Single Goti_Status::animaOffsetY
	float ___animaOffsetY_26;
	// System.Single Goti_Status::animaOffsetZ
	float ___animaOffsetZ_27;
	// System.Single Goti_Status::walkAnimaX
	float ___walkAnimaX_28;
	// System.Single Goti_Status::walkAnimaY
	float ___walkAnimaY_29;
	// System.Single Goti_Status::attackOffX
	float ___attackOffX_30;
	// System.Single Goti_Status::attackOffZ
	float ___attackOffZ_31;
	// Enemy_AI Goti_Status::eai
	Enemy_AI_t244483799 * ___eai_32;
	// System.Int32 Goti_Status::worldPosToBeReached
	int32_t ___worldPosToBeReached_33;
	// System.Boolean Goti_Status::canCutSomeOne
	bool ___canCutSomeOne_34;
	// System.Boolean Goti_Status::becomingSafe
	bool ___becomingSafe_35;
	// System.Boolean Goti_Status::winning
	bool ___winning_36;
	// System.Boolean Goti_Status::canChase
	bool ___canChase_37;
	// System.Boolean Goti_Status::mightGetAttacked
	bool ___mightGetAttacked_38;
	// System.Boolean Goti_Status::doingNothing
	bool ___doingNothing_39;
	// System.Boolean Goti_Status::isChaser
	bool ___isChaser_40;
	// System.Int32 Goti_Status::chaseGap
	int32_t ___chaseGap_41;
	// System.Boolean Goti_Status::performedNext
	bool ___performedNext_42;
	// System.Int32 Goti_Status::myWorldPosition
	int32_t ___myWorldPosition_43;
	// UnityEngine.Transform Goti_Status::safePoint
	Transform_t3275118058 * ___safePoint_44;
	// System.Boolean Goti_Status::attacking
	bool ___attacking_45;
	// System.Boolean Goti_Status::gettingHit
	bool ___gettingHit_46;
	// UnityEngine.AudioSource Goti_Status::asWalking
	AudioSource_t1135106623 * ___asWalking_47;
	// UnityEngine.GameObject Goti_Status::powObj
	GameObject_t1756533147 * ___powObj_48;
	// UnityEngine.AudioSource Goti_Status::openingAS
	AudioSource_t1135106623 * ___openingAS_49;
	// UnityEngine.Camera Goti_Status::myTargetCam
	Camera_t189460977 * ___myTargetCam_50;

public:
	inline static int32_t get_offset_of_myState_2() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___myState_2)); }
	inline int32_t get_myState_2() const { return ___myState_2; }
	inline int32_t* get_address_of_myState_2() { return &___myState_2; }
	inline void set_myState_2(int32_t value)
	{
		___myState_2 = value;
	}

	inline static int32_t get_offset_of_canBePlayed_3() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___canBePlayed_3)); }
	inline bool get_canBePlayed_3() const { return ___canBePlayed_3; }
	inline bool* get_address_of_canBePlayed_3() { return &___canBePlayed_3; }
	inline void set_canBePlayed_3(bool value)
	{
		___canBePlayed_3 = value;
	}

	inline static int32_t get_offset_of_myManger_4() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___myManger_4)); }
	inline Color_Manager_Script_t4053987423 * get_myManger_4() const { return ___myManger_4; }
	inline Color_Manager_Script_t4053987423 ** get_address_of_myManger_4() { return &___myManger_4; }
	inline void set_myManger_4(Color_Manager_Script_t4053987423 * value)
	{
		___myManger_4 = value;
		Il2CppCodeGenWriteBarrier(&___myManger_4, value);
	}

	inline static int32_t get_offset_of_actionPhase_5() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___actionPhase_5)); }
	inline bool get_actionPhase_5() const { return ___actionPhase_5; }
	inline bool* get_address_of_actionPhase_5() { return &___actionPhase_5; }
	inline void set_actionPhase_5(bool value)
	{
		___actionPhase_5 = value;
	}

	inline static int32_t get_offset_of_yoICanPlay_6() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___yoICanPlay_6)); }
	inline bool get_yoICanPlay_6() const { return ___yoICanPlay_6; }
	inline bool* get_address_of_yoICanPlay_6() { return &___yoICanPlay_6; }
	inline void set_yoICanPlay_6(bool value)
	{
		___yoICanPlay_6 = value;
	}

	inline static int32_t get_offset_of_finalPos_7() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___finalPos_7)); }
	inline Vector3_t2243707580  get_finalPos_7() const { return ___finalPos_7; }
	inline Vector3_t2243707580 * get_address_of_finalPos_7() { return &___finalPos_7; }
	inline void set_finalPos_7(Vector3_t2243707580  value)
	{
		___finalPos_7 = value;
	}

	inline static int32_t get_offset_of_curIndexPos_8() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___curIndexPos_8)); }
	inline int32_t get_curIndexPos_8() const { return ___curIndexPos_8; }
	inline int32_t* get_address_of_curIndexPos_8() { return &___curIndexPos_8; }
	inline void set_curIndexPos_8(int32_t value)
	{
		___curIndexPos_8 = value;
	}

	inline static int32_t get_offset_of_startRunning_9() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___startRunning_9)); }
	inline bool get_startRunning_9() const { return ___startRunning_9; }
	inline bool* get_address_of_startRunning_9() { return &___startRunning_9; }
	inline void set_startRunning_9(bool value)
	{
		___startRunning_9 = value;
	}

	inline static int32_t get_offset_of_runSpeed_10() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___runSpeed_10)); }
	inline float get_runSpeed_10() const { return ___runSpeed_10; }
	inline float* get_address_of_runSpeed_10() { return &___runSpeed_10; }
	inline void set_runSpeed_10(float value)
	{
		___runSpeed_10 = value;
	}

	inline static int32_t get_offset_of_hieght_11() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___hieght_11)); }
	inline float get_hieght_11() const { return ___hieght_11; }
	inline float* get_address_of_hieght_11() { return &___hieght_11; }
	inline void set_hieght_11(float value)
	{
		___hieght_11 = value;
	}

	inline static int32_t get_offset_of_calledOnce_12() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___calledOnce_12)); }
	inline bool get_calledOnce_12() const { return ___calledOnce_12; }
	inline bool* get_address_of_calledOnce_12() { return &___calledOnce_12; }
	inline void set_calledOnce_12(bool value)
	{
		___calledOnce_12 = value;
	}

	inline static int32_t get_offset_of_myStartPos_13() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___myStartPos_13)); }
	inline Vector3_t2243707580  get_myStartPos_13() const { return ___myStartPos_13; }
	inline Vector3_t2243707580 * get_address_of_myStartPos_13() { return &___myStartPos_13; }
	inline void set_myStartPos_13(Vector3_t2243707580  value)
	{
		___myStartPos_13 = value;
	}

	inline static int32_t get_offset_of_initY_14() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___initY_14)); }
	inline float get_initY_14() const { return ___initY_14; }
	inline float* get_address_of_initY_14() { return &___initY_14; }
	inline void set_initY_14(float value)
	{
		___initY_14 = value;
	}

	inline static int32_t get_offset_of_restrictionOutput_15() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___restrictionOutput_15)); }
	inline int32_t get_restrictionOutput_15() const { return ___restrictionOutput_15; }
	inline int32_t* get_address_of_restrictionOutput_15() { return &___restrictionOutput_15; }
	inline void set_restrictionOutput_15(int32_t value)
	{
		___restrictionOutput_15 = value;
	}

	inline static int32_t get_offset_of_pathL_16() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___pathL_16)); }
	inline int32_t get_pathL_16() const { return ___pathL_16; }
	inline int32_t* get_address_of_pathL_16() { return &___pathL_16; }
	inline void set_pathL_16(int32_t value)
	{
		___pathL_16 = value;
	}

	inline static int32_t get_offset_of_myWinPos_17() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___myWinPos_17)); }
	inline Transform_t3275118058 * get_myWinPos_17() const { return ___myWinPos_17; }
	inline Transform_t3275118058 ** get_address_of_myWinPos_17() { return &___myWinPos_17; }
	inline void set_myWinPos_17(Transform_t3275118058 * value)
	{
		___myWinPos_17 = value;
		Il2CppCodeGenWriteBarrier(&___myWinPos_17, value);
	}

	inline static int32_t get_offset_of_hasCollided_18() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___hasCollided_18)); }
	inline bool get_hasCollided_18() const { return ___hasCollided_18; }
	inline bool* get_address_of_hasCollided_18() { return &___hasCollided_18; }
	inline void set_hasCollided_18(bool value)
	{
		___hasCollided_18 = value;
	}

	inline static int32_t get_offset_of_isSafe_19() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___isSafe_19)); }
	inline bool get_isSafe_19() const { return ___isSafe_19; }
	inline bool* get_address_of_isSafe_19() { return &___isSafe_19; }
	inline void set_isSafe_19(bool value)
	{
		___isSafe_19 = value;
	}

	inline static int32_t get_offset_of_anime_20() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___anime_20)); }
	inline Animator_t69676727 * get_anime_20() const { return ___anime_20; }
	inline Animator_t69676727 ** get_address_of_anime_20() { return &___anime_20; }
	inline void set_anime_20(Animator_t69676727 * value)
	{
		___anime_20 = value;
		Il2CppCodeGenWriteBarrier(&___anime_20, value);
	}

	inline static int32_t get_offset_of_pathToFollow_21() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___pathToFollow_21)); }
	inline List_1_t1612828712 * get_pathToFollow_21() const { return ___pathToFollow_21; }
	inline List_1_t1612828712 ** get_address_of_pathToFollow_21() { return &___pathToFollow_21; }
	inline void set_pathToFollow_21(List_1_t1612828712 * value)
	{
		___pathToFollow_21 = value;
		Il2CppCodeGenWriteBarrier(&___pathToFollow_21, value);
	}

	inline static int32_t get_offset_of_listCounter_22() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___listCounter_22)); }
	inline int32_t get_listCounter_22() const { return ___listCounter_22; }
	inline int32_t* get_address_of_listCounter_22() { return &___listCounter_22; }
	inline void set_listCounter_22(int32_t value)
	{
		___listCounter_22 = value;
	}

	inline static int32_t get_offset_of_initCPos_23() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___initCPos_23)); }
	inline Vector3_t2243707580  get_initCPos_23() const { return ___initCPos_23; }
	inline Vector3_t2243707580 * get_address_of_initCPos_23() { return &___initCPos_23; }
	inline void set_initCPos_23(Vector3_t2243707580  value)
	{
		___initCPos_23 = value;
	}

	inline static int32_t get_offset_of_pos_24() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___pos_24)); }
	inline Vector3_t2243707580  get_pos_24() const { return ___pos_24; }
	inline Vector3_t2243707580 * get_address_of_pos_24() { return &___pos_24; }
	inline void set_pos_24(Vector3_t2243707580  value)
	{
		___pos_24 = value;
	}

	inline static int32_t get_offset_of_animaOffsetX_25() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___animaOffsetX_25)); }
	inline float get_animaOffsetX_25() const { return ___animaOffsetX_25; }
	inline float* get_address_of_animaOffsetX_25() { return &___animaOffsetX_25; }
	inline void set_animaOffsetX_25(float value)
	{
		___animaOffsetX_25 = value;
	}

	inline static int32_t get_offset_of_animaOffsetY_26() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___animaOffsetY_26)); }
	inline float get_animaOffsetY_26() const { return ___animaOffsetY_26; }
	inline float* get_address_of_animaOffsetY_26() { return &___animaOffsetY_26; }
	inline void set_animaOffsetY_26(float value)
	{
		___animaOffsetY_26 = value;
	}

	inline static int32_t get_offset_of_animaOffsetZ_27() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___animaOffsetZ_27)); }
	inline float get_animaOffsetZ_27() const { return ___animaOffsetZ_27; }
	inline float* get_address_of_animaOffsetZ_27() { return &___animaOffsetZ_27; }
	inline void set_animaOffsetZ_27(float value)
	{
		___animaOffsetZ_27 = value;
	}

	inline static int32_t get_offset_of_walkAnimaX_28() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___walkAnimaX_28)); }
	inline float get_walkAnimaX_28() const { return ___walkAnimaX_28; }
	inline float* get_address_of_walkAnimaX_28() { return &___walkAnimaX_28; }
	inline void set_walkAnimaX_28(float value)
	{
		___walkAnimaX_28 = value;
	}

	inline static int32_t get_offset_of_walkAnimaY_29() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___walkAnimaY_29)); }
	inline float get_walkAnimaY_29() const { return ___walkAnimaY_29; }
	inline float* get_address_of_walkAnimaY_29() { return &___walkAnimaY_29; }
	inline void set_walkAnimaY_29(float value)
	{
		___walkAnimaY_29 = value;
	}

	inline static int32_t get_offset_of_attackOffX_30() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___attackOffX_30)); }
	inline float get_attackOffX_30() const { return ___attackOffX_30; }
	inline float* get_address_of_attackOffX_30() { return &___attackOffX_30; }
	inline void set_attackOffX_30(float value)
	{
		___attackOffX_30 = value;
	}

	inline static int32_t get_offset_of_attackOffZ_31() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___attackOffZ_31)); }
	inline float get_attackOffZ_31() const { return ___attackOffZ_31; }
	inline float* get_address_of_attackOffZ_31() { return &___attackOffZ_31; }
	inline void set_attackOffZ_31(float value)
	{
		___attackOffZ_31 = value;
	}

	inline static int32_t get_offset_of_eai_32() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___eai_32)); }
	inline Enemy_AI_t244483799 * get_eai_32() const { return ___eai_32; }
	inline Enemy_AI_t244483799 ** get_address_of_eai_32() { return &___eai_32; }
	inline void set_eai_32(Enemy_AI_t244483799 * value)
	{
		___eai_32 = value;
		Il2CppCodeGenWriteBarrier(&___eai_32, value);
	}

	inline static int32_t get_offset_of_worldPosToBeReached_33() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___worldPosToBeReached_33)); }
	inline int32_t get_worldPosToBeReached_33() const { return ___worldPosToBeReached_33; }
	inline int32_t* get_address_of_worldPosToBeReached_33() { return &___worldPosToBeReached_33; }
	inline void set_worldPosToBeReached_33(int32_t value)
	{
		___worldPosToBeReached_33 = value;
	}

	inline static int32_t get_offset_of_canCutSomeOne_34() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___canCutSomeOne_34)); }
	inline bool get_canCutSomeOne_34() const { return ___canCutSomeOne_34; }
	inline bool* get_address_of_canCutSomeOne_34() { return &___canCutSomeOne_34; }
	inline void set_canCutSomeOne_34(bool value)
	{
		___canCutSomeOne_34 = value;
	}

	inline static int32_t get_offset_of_becomingSafe_35() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___becomingSafe_35)); }
	inline bool get_becomingSafe_35() const { return ___becomingSafe_35; }
	inline bool* get_address_of_becomingSafe_35() { return &___becomingSafe_35; }
	inline void set_becomingSafe_35(bool value)
	{
		___becomingSafe_35 = value;
	}

	inline static int32_t get_offset_of_winning_36() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___winning_36)); }
	inline bool get_winning_36() const { return ___winning_36; }
	inline bool* get_address_of_winning_36() { return &___winning_36; }
	inline void set_winning_36(bool value)
	{
		___winning_36 = value;
	}

	inline static int32_t get_offset_of_canChase_37() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___canChase_37)); }
	inline bool get_canChase_37() const { return ___canChase_37; }
	inline bool* get_address_of_canChase_37() { return &___canChase_37; }
	inline void set_canChase_37(bool value)
	{
		___canChase_37 = value;
	}

	inline static int32_t get_offset_of_mightGetAttacked_38() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___mightGetAttacked_38)); }
	inline bool get_mightGetAttacked_38() const { return ___mightGetAttacked_38; }
	inline bool* get_address_of_mightGetAttacked_38() { return &___mightGetAttacked_38; }
	inline void set_mightGetAttacked_38(bool value)
	{
		___mightGetAttacked_38 = value;
	}

	inline static int32_t get_offset_of_doingNothing_39() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___doingNothing_39)); }
	inline bool get_doingNothing_39() const { return ___doingNothing_39; }
	inline bool* get_address_of_doingNothing_39() { return &___doingNothing_39; }
	inline void set_doingNothing_39(bool value)
	{
		___doingNothing_39 = value;
	}

	inline static int32_t get_offset_of_isChaser_40() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___isChaser_40)); }
	inline bool get_isChaser_40() const { return ___isChaser_40; }
	inline bool* get_address_of_isChaser_40() { return &___isChaser_40; }
	inline void set_isChaser_40(bool value)
	{
		___isChaser_40 = value;
	}

	inline static int32_t get_offset_of_chaseGap_41() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___chaseGap_41)); }
	inline int32_t get_chaseGap_41() const { return ___chaseGap_41; }
	inline int32_t* get_address_of_chaseGap_41() { return &___chaseGap_41; }
	inline void set_chaseGap_41(int32_t value)
	{
		___chaseGap_41 = value;
	}

	inline static int32_t get_offset_of_performedNext_42() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___performedNext_42)); }
	inline bool get_performedNext_42() const { return ___performedNext_42; }
	inline bool* get_address_of_performedNext_42() { return &___performedNext_42; }
	inline void set_performedNext_42(bool value)
	{
		___performedNext_42 = value;
	}

	inline static int32_t get_offset_of_myWorldPosition_43() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___myWorldPosition_43)); }
	inline int32_t get_myWorldPosition_43() const { return ___myWorldPosition_43; }
	inline int32_t* get_address_of_myWorldPosition_43() { return &___myWorldPosition_43; }
	inline void set_myWorldPosition_43(int32_t value)
	{
		___myWorldPosition_43 = value;
	}

	inline static int32_t get_offset_of_safePoint_44() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___safePoint_44)); }
	inline Transform_t3275118058 * get_safePoint_44() const { return ___safePoint_44; }
	inline Transform_t3275118058 ** get_address_of_safePoint_44() { return &___safePoint_44; }
	inline void set_safePoint_44(Transform_t3275118058 * value)
	{
		___safePoint_44 = value;
		Il2CppCodeGenWriteBarrier(&___safePoint_44, value);
	}

	inline static int32_t get_offset_of_attacking_45() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___attacking_45)); }
	inline bool get_attacking_45() const { return ___attacking_45; }
	inline bool* get_address_of_attacking_45() { return &___attacking_45; }
	inline void set_attacking_45(bool value)
	{
		___attacking_45 = value;
	}

	inline static int32_t get_offset_of_gettingHit_46() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___gettingHit_46)); }
	inline bool get_gettingHit_46() const { return ___gettingHit_46; }
	inline bool* get_address_of_gettingHit_46() { return &___gettingHit_46; }
	inline void set_gettingHit_46(bool value)
	{
		___gettingHit_46 = value;
	}

	inline static int32_t get_offset_of_asWalking_47() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___asWalking_47)); }
	inline AudioSource_t1135106623 * get_asWalking_47() const { return ___asWalking_47; }
	inline AudioSource_t1135106623 ** get_address_of_asWalking_47() { return &___asWalking_47; }
	inline void set_asWalking_47(AudioSource_t1135106623 * value)
	{
		___asWalking_47 = value;
		Il2CppCodeGenWriteBarrier(&___asWalking_47, value);
	}

	inline static int32_t get_offset_of_powObj_48() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___powObj_48)); }
	inline GameObject_t1756533147 * get_powObj_48() const { return ___powObj_48; }
	inline GameObject_t1756533147 ** get_address_of_powObj_48() { return &___powObj_48; }
	inline void set_powObj_48(GameObject_t1756533147 * value)
	{
		___powObj_48 = value;
		Il2CppCodeGenWriteBarrier(&___powObj_48, value);
	}

	inline static int32_t get_offset_of_openingAS_49() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___openingAS_49)); }
	inline AudioSource_t1135106623 * get_openingAS_49() const { return ___openingAS_49; }
	inline AudioSource_t1135106623 ** get_address_of_openingAS_49() { return &___openingAS_49; }
	inline void set_openingAS_49(AudioSource_t1135106623 * value)
	{
		___openingAS_49 = value;
		Il2CppCodeGenWriteBarrier(&___openingAS_49, value);
	}

	inline static int32_t get_offset_of_myTargetCam_50() { return static_cast<int32_t>(offsetof(Goti_Status_t1473870168, ___myTargetCam_50)); }
	inline Camera_t189460977 * get_myTargetCam_50() const { return ___myTargetCam_50; }
	inline Camera_t189460977 ** get_address_of_myTargetCam_50() { return &___myTargetCam_50; }
	inline void set_myTargetCam_50(Camera_t189460977 * value)
	{
		___myTargetCam_50 = value;
		Il2CppCodeGenWriteBarrier(&___myTargetCam_50, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
