﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WorldPos
struct WorldPos_t3236901198;

#include "codegen/il2cpp-codegen.h"

// System.Void WorldPos::.ctor()
extern "C"  void WorldPos__ctor_m2110560133 (WorldPos_t3236901198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
