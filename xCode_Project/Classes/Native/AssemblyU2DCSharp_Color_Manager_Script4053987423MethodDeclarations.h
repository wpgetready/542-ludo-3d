﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Color_Manager_Script
struct Color_Manager_Script_t4053987423;

#include "codegen/il2cpp-codegen.h"

// System.Void Color_Manager_Script::.ctor()
extern "C"  void Color_Manager_Script__ctor_m3095357310 (Color_Manager_Script_t4053987423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Color_Manager_Script::Start()
extern "C"  void Color_Manager_Script_Start_m3824912202 (Color_Manager_Script_t4053987423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Color_Manager_Script::Update()
extern "C"  void Color_Manager_Script_Update_m1168648999 (Color_Manager_Script_t4053987423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Color_Manager_Script::StopTheDice()
extern "C"  void Color_Manager_Script_StopTheDice_m250861038 (Color_Manager_Script_t4053987423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Color_Manager_Script::AfterDiceAction()
extern "C"  void Color_Manager_Script_AfterDiceAction_m3710082315 (Color_Manager_Script_t4053987423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Color_Manager_Script::StopShowingAll()
extern "C"  void Color_Manager_Script_StopShowingAll_m978555850 (Color_Manager_Script_t4053987423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Color_Manager_Script::CalculatePlayables()
extern "C"  void Color_Manager_Script_CalculatePlayables_m3860738715 (Color_Manager_Script_t4053987423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Color_Manager_Script::CalculateIfIWin()
extern "C"  void Color_Manager_Script_CalculateIfIWin_m481506854 (Color_Manager_Script_t4053987423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Color_Manager_Script::IWIN()
extern "C"  void Color_Manager_Script_IWIN_m536760483 (Color_Manager_Script_t4053987423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Color_Manager_Script::ILOST()
extern "C"  void Color_Manager_Script_ILOST_m326932739 (Color_Manager_Script_t4053987423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Color_Manager_Script::DisableAllCameras()
extern "C"  void Color_Manager_Script_DisableAllCameras_m2295878905 (Color_Manager_Script_t4053987423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Color_Manager_Script::EnableMainCamera(System.Boolean)
extern "C"  void Color_Manager_Script_EnableMainCamera_m3356488996 (Color_Manager_Script_t4053987423 * __this, bool ___flag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
