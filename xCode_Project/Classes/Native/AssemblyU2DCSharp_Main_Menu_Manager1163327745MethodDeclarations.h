﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Main_Menu_Manager
struct Main_Menu_Manager_t1163327745;

#include "codegen/il2cpp-codegen.h"

// System.Void Main_Menu_Manager::.ctor()
extern "C"  void Main_Menu_Manager__ctor_m2204695144 (Main_Menu_Manager_t1163327745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Main_Menu_Manager::Start()
extern "C"  void Main_Menu_Manager_Start_m177546312 (Main_Menu_Manager_t1163327745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Main_Menu_Manager::Update()
extern "C"  void Main_Menu_Manager_Update_m2834293253 (Main_Menu_Manager_t1163327745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Main_Menu_Manager::NewGame()
extern "C"  void Main_Menu_Manager_NewGame_m3857764328 (Main_Menu_Manager_t1163327745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Main_Menu_Manager::PACPU()
extern "C"  void Main_Menu_Manager_PACPU_m3742451775 (Main_Menu_Manager_t1163327745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Main_Menu_Manager::Credits()
extern "C"  void Main_Menu_Manager_Credits_m3199251246 (Main_Menu_Manager_t1163327745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Main_Menu_Manager::Settings()
extern "C"  void Main_Menu_Manager_Settings_m1217263657 (Main_Menu_Manager_t1163327745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Main_Menu_Manager::Exit()
extern "C"  void Main_Menu_Manager_Exit_m3598658330 (Main_Menu_Manager_t1163327745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Main_Menu_Manager::StartLocalGame()
extern "C"  void Main_Menu_Manager_StartLocalGame_m3065072519 (Main_Menu_Manager_t1163327745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Main_Menu_Manager::BackToMainScreen()
extern "C"  void Main_Menu_Manager_BackToMainScreen_m3865629351 (Main_Menu_Manager_t1163327745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Main_Menu_Manager::Local2P()
extern "C"  void Main_Menu_Manager_Local2P_m3022977275 (Main_Menu_Manager_t1163327745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Main_Menu_Manager::Local3P()
extern "C"  void Main_Menu_Manager_Local3P_m3022977244 (Main_Menu_Manager_t1163327745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Main_Menu_Manager::Local4P()
extern "C"  void Main_Menu_Manager_Local4P_m3022977341 (Main_Menu_Manager_t1163327745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Main_Menu_Manager::ToggleSound()
extern "C"  void Main_Menu_Manager_ToggleSound_m2110619177 (Main_Menu_Manager_t1163327745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Main_Menu_Manager::HGToggleListener(System.Boolean)
extern "C"  void Main_Menu_Manager_HGToggleListener_m2449447290 (Main_Menu_Manager_t1163327745 * __this, bool ___flag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Main_Menu_Manager::RemoveSplash()
extern "C"  void Main_Menu_Manager_RemoveSplash_m3221060547 (Main_Menu_Manager_t1163327745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Main_Menu_Manager::<Start>m__0(System.Boolean)
extern "C"  void Main_Menu_Manager_U3CStartU3Em__0_m935656048 (Main_Menu_Manager_t1163327745 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
