﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Arena_Manager
struct Arena_Manager_t4218382287;

#include "codegen/il2cpp-codegen.h"

// System.Void Arena_Manager::.ctor()
extern "C"  void Arena_Manager__ctor_m3163061250 (Arena_Manager_t4218382287 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Arena_Manager::Start()
extern "C"  void Arena_Manager_Start_m3574877078 (Arena_Manager_t4218382287 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
