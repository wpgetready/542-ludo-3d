﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Arena_Manager
struct  Arena_Manager_t4218382287  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject[] Arena_Manager::arenas
	GameObjectU5BU5D_t3057952154* ___arenas_2;

public:
	inline static int32_t get_offset_of_arenas_2() { return static_cast<int32_t>(offsetof(Arena_Manager_t4218382287, ___arenas_2)); }
	inline GameObjectU5BU5D_t3057952154* get_arenas_2() const { return ___arenas_2; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_arenas_2() { return &___arenas_2; }
	inline void set_arenas_2(GameObjectU5BU5D_t3057952154* value)
	{
		___arenas_2 = value;
		Il2CppCodeGenWriteBarrier(&___arenas_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
