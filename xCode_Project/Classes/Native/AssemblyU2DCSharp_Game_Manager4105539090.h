﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Game_Manager
struct Game_Manager_t4105539090;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Game_Manager
struct  Game_Manager_t4105539090  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 Game_Manager::noOfPlayers
	int32_t ___noOfPlayers_2;
	// System.Int32 Game_Manager::arenaIndex
	int32_t ___arenaIndex_4;
	// System.Boolean Game_Manager::isSoundOn
	bool ___isSoundOn_5;
	// System.Boolean Game_Manager::hasSplashShown
	bool ___hasSplashShown_6;

public:
	inline static int32_t get_offset_of_noOfPlayers_2() { return static_cast<int32_t>(offsetof(Game_Manager_t4105539090, ___noOfPlayers_2)); }
	inline int32_t get_noOfPlayers_2() const { return ___noOfPlayers_2; }
	inline int32_t* get_address_of_noOfPlayers_2() { return &___noOfPlayers_2; }
	inline void set_noOfPlayers_2(int32_t value)
	{
		___noOfPlayers_2 = value;
	}

	inline static int32_t get_offset_of_arenaIndex_4() { return static_cast<int32_t>(offsetof(Game_Manager_t4105539090, ___arenaIndex_4)); }
	inline int32_t get_arenaIndex_4() const { return ___arenaIndex_4; }
	inline int32_t* get_address_of_arenaIndex_4() { return &___arenaIndex_4; }
	inline void set_arenaIndex_4(int32_t value)
	{
		___arenaIndex_4 = value;
	}

	inline static int32_t get_offset_of_isSoundOn_5() { return static_cast<int32_t>(offsetof(Game_Manager_t4105539090, ___isSoundOn_5)); }
	inline bool get_isSoundOn_5() const { return ___isSoundOn_5; }
	inline bool* get_address_of_isSoundOn_5() { return &___isSoundOn_5; }
	inline void set_isSoundOn_5(bool value)
	{
		___isSoundOn_5 = value;
	}

	inline static int32_t get_offset_of_hasSplashShown_6() { return static_cast<int32_t>(offsetof(Game_Manager_t4105539090, ___hasSplashShown_6)); }
	inline bool get_hasSplashShown_6() const { return ___hasSplashShown_6; }
	inline bool* get_address_of_hasSplashShown_6() { return &___hasSplashShown_6; }
	inline void set_hasSplashShown_6(bool value)
	{
		___hasSplashShown_6 = value;
	}
};

struct Game_Manager_t4105539090_StaticFields
{
public:
	// Game_Manager Game_Manager::gmInstance
	Game_Manager_t4105539090 * ___gmInstance_3;

public:
	inline static int32_t get_offset_of_gmInstance_3() { return static_cast<int32_t>(offsetof(Game_Manager_t4105539090_StaticFields, ___gmInstance_3)); }
	inline Game_Manager_t4105539090 * get_gmInstance_3() const { return ___gmInstance_3; }
	inline Game_Manager_t4105539090 ** get_address_of_gmInstance_3() { return &___gmInstance_3; }
	inline void set_gmInstance_3(Game_Manager_t4105539090 * value)
	{
		___gmInstance_3 = value;
		Il2CppCodeGenWriteBarrier(&___gmInstance_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
