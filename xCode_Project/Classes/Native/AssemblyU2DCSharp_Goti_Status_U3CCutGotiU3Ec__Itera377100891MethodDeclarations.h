﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Goti_Status/<CutGoti>c__Iterator0
struct U3CCutGotiU3Ec__Iterator0_t377100891;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Goti_Status/<CutGoti>c__Iterator0::.ctor()
extern "C"  void U3CCutGotiU3Ec__Iterator0__ctor_m3139146000 (U3CCutGotiU3Ec__Iterator0_t377100891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Goti_Status/<CutGoti>c__Iterator0::MoveNext()
extern "C"  bool U3CCutGotiU3Ec__Iterator0_MoveNext_m943435336 (U3CCutGotiU3Ec__Iterator0_t377100891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Goti_Status/<CutGoti>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCutGotiU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m889004950 (U3CCutGotiU3Ec__Iterator0_t377100891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Goti_Status/<CutGoti>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCutGotiU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m4227727422 (U3CCutGotiU3Ec__Iterator0_t377100891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Goti_Status/<CutGoti>c__Iterator0::Dispose()
extern "C"  void U3CCutGotiU3Ec__Iterator0_Dispose_m1037140661 (U3CCutGotiU3Ec__Iterator0_t377100891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Goti_Status/<CutGoti>c__Iterator0::Reset()
extern "C"  void U3CCutGotiU3Ec__Iterator0_Reset_m1475454659 (U3CCutGotiU3Ec__Iterator0_t377100891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
