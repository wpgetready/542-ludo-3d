﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim1808633952.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Fram1120735295.h"
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttri1027170048.h"
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAtt665825653.h"
#include "UnityEngine_UnityEngine_Logger3328995178.h"
#include "UnityEngine_UnityEngine_Scripting_UsedByNativeCode3212052468.h"
#include "UnityEngine_UnityEngine_Scripting_RequiredByNative1913052472.h"
#include "UnityEngine_UnityEngine_Serialization_FormerlySeri3673080018.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules1810425448.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleA1390152093.h"
#include "UnityEngine_UnityEngineInternal_GenericStack3718539591.h"
#include "UnityEngine_UnityEngineInternal_NetFxCoreExtension4275971970.h"
#include "Facebook_Unity_Settings_U3CModuleU3E3783534214.h"
#include "Facebook_Unity_Settings_Facebook_Unity_Settings_Fa2167659529.h"
#include "Facebook_Unity_Settings_Facebook_Unity_Settings_Fa2639189684.h"
#include "Facebook_Unity_Settings_Facebook_Unity_Settings_Fa2066630273.h"
#include "Facebook_Unity_U3CModuleU3E3783534214.h"
#include "Facebook_Unity_Facebook_Unity_AccessToken2518141643.h"
#include "Facebook_Unity_Facebook_Unity_CallbackManager4242095184.h"
#include "Facebook_Unity_Facebook_Unity_ComponentFactory2270101009.h"
#include "Facebook_Unity_Facebook_Unity_ComponentFactory_IfNo643838650.h"
#include "Facebook_Unity_Facebook_Unity_Constants3246017429.h"
#include "Facebook_Unity_Facebook_Unity_FB1612658294.h"
#include "Facebook_Unity_Facebook_Unity_FB_OnDLLLoaded1727406294.h"
#include "Facebook_Unity_Facebook_Unity_FB_Canvas2415435652.h"
#include "Facebook_Unity_Facebook_Unity_FB_Mobile3800732530.h"
#include "Facebook_Unity_Facebook_Unity_FB_CompiledFacebookL3305868676.h"
#include "Facebook_Unity_Facebook_Unity_FB_U3CInitU3Ec__Anon4214738661.h"
#include "Facebook_Unity_Facebook_Unity_FacebookBase2463540723.h"
#include "Facebook_Unity_Facebook_Unity_InitDelegate3410465555.h"
#include "Facebook_Unity_Facebook_Unity_HideUnityDelegate712804158.h"
#include "Facebook_Unity_Facebook_Unity_FacebookGameObject2922570967.h"
#include "Facebook_Unity_Facebook_Unity_FacebookSdkVersion1532426400.h"
#include "Facebook_Unity_Facebook_Unity_FacebookUnityPlatfor1867507902.h"
#include "Facebook_Unity_Facebook_Unity_MethodArguments735664811.h"
#include "Facebook_Unity_Facebook_Unity_ShareDialogMode1445392044.h"
#include "Facebook_Unity_Facebook_Unity_OGActionType1978093408.h"
#include "Facebook_Unity_Facebook_Unity_Canvas_CanvasFaceboo1390391306.h"
#include "Facebook_Unity_Facebook_Unity_Canvas_CanvasFaceboo3137418185.h"
#include "Facebook_Unity_Facebook_Unity_Canvas_CanvasFaceboo1732848805.h"
#include "Facebook_Unity_Facebook_Unity_Canvas_CanvasFaceboo1985905999.h"
#include "Facebook_Unity_Facebook_Unity_Canvas_CanvasJSWrapp3429491632.h"
#include "Facebook_Unity_Facebook_Unity_Canvas_JsBridge2435492180.h"
#include "Facebook_Unity_Facebook_Unity_Arcade_ArcadeFacebook622171242.h"
#include "Facebook_Unity_Facebook_Unity_Arcade_ArcadeFaceboo3968443998.h"
#include "Facebook_Unity_Facebook_Unity_Arcade_ArcadeFaceboo1372893397.h"
#include "Facebook_Unity_Facebook_Unity_Arcade_ArcadeFaceboo1353419607.h"
#include "Facebook_Unity_Facebook_Unity_Arcade_ArcadeFaceboo3930015791.h"
#include "Facebook_Unity_Facebook_Unity_Mobile_MobileFaceboo2099931062.h"
#include "Facebook_Unity_Facebook_Unity_Mobile_MobileFacebook668179329.h"
#include "Facebook_Unity_Facebook_Unity_Mobile_Android_Andro3038444049.h"
#include "Facebook_Unity_Facebook_Unity_Mobile_Android_Andro2149209708.h"
#include "Facebook_Unity_Facebook_Unity_Mobile_Android_Andro3776634754.h"
#include "Facebook_Unity_Facebook_Unity_Mobile_IOS_IOSFaceboo847746195.h"
#include "Facebook_Unity_Facebook_Unity_Mobile_IOS_IOSFacebo4261124525.h"
#include "Facebook_Unity_Facebook_Unity_Mobile_IOS_IOSFacebo3391486318.h"
#include "Facebook_Unity_Facebook_Unity_Mobile_IOS_IOSFacebo3879535236.h"
#include "Facebook_Unity_Facebook_Unity_Editor_EditorFaceboo1229262421.h"
#include "Facebook_Unity_Facebook_Unity_Editor_EditorFacebookG63110230.h"
#include "Facebook_Unity_Facebook_Unity_Editor_EditorFaceboo2109505584.h"
#include "Facebook_Unity_Facebook_Unity_Editor_EditorFaceboo2244501619.h"
#include "Facebook_Unity_Facebook_Unity_Editor_EditorWrapper2290865494.h"
#include "Facebook_Unity_Facebook_Unity_Editor_Dialogs_Empty2854396319.h"
#include "Facebook_Unity_Facebook_Unity_Editor_Dialogs_MockLo531765055.h"
#include "Facebook_Unity_Facebook_Unity_Editor_Dialogs_MockL1402467746.h"
#include "Facebook_Unity_Facebook_Unity_Editor_Dialogs_MockS1542668653.h"
#include "Facebook_Unity_Facebook_Unity_AccessTokenRefreshRes170484741.h"
#include "Facebook_Unity_Facebook_Unity_AppInviteResult780050231.h"
#include "Facebook_Unity_Facebook_Unity_AppLinkResult2690573342.h"
#include "Facebook_Unity_Facebook_Unity_AppRequestResult2863124367.h"
#include "Facebook_Unity_Facebook_Unity_GraphResult2230822669.h"
#include "Facebook_Unity_Facebook_Unity_GroupCreateResult605038084.h"
#include "Facebook_Unity_Facebook_Unity_GroupJoinResult2959566706.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1500 = { sizeof (AnimationPlayableUtilities_t1808633952), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1501 = { sizeof (FrameData_t1120735295)+ sizeof (Il2CppObject), sizeof(FrameData_t1120735295 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1501[4] = 
{
	FrameData_t1120735295::get_offset_of_m_UpdateId_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t1120735295::get_offset_of_m_Time_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t1120735295::get_offset_of_m_LastTime_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t1120735295::get_offset_of_m_TimeScale_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1502 = { sizeof (DefaultValueAttribute_t1027170048), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1502[1] = 
{
	DefaultValueAttribute_t1027170048::get_offset_of_DefaultValue_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1503 = { sizeof (ExcludeFromDocsAttribute_t665825653), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1504 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1505 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1506 = { sizeof (Logger_t3328995178), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1506[3] = 
{
	Logger_t3328995178::get_offset_of_U3ClogHandlerU3Ek__BackingField_0(),
	Logger_t3328995178::get_offset_of_U3ClogEnabledU3Ek__BackingField_1(),
	Logger_t3328995178::get_offset_of_U3CfilterLogTypeU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1507 = { sizeof (UsedByNativeCodeAttribute_t3212052468), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1508 = { sizeof (RequiredByNativeCodeAttribute_t1913052472), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1509 = { sizeof (FormerlySerializedAsAttribute_t3673080018), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1509[1] = 
{
	FormerlySerializedAsAttribute_t3673080018::get_offset_of_m_oldName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1510 = { sizeof (TypeInferenceRules_t1810425448)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1510[5] = 
{
	TypeInferenceRules_t1810425448::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1511 = { sizeof (TypeInferenceRuleAttribute_t1390152093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1511[1] = 
{
	TypeInferenceRuleAttribute_t1390152093::get_offset_of__rule_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1512 = { sizeof (GenericStack_t3718539591), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1513 = { sizeof (NetFxCoreExtensions_t4275971970), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1514 = { sizeof (U3CModuleU3E_t3783534219), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1515 = { sizeof (FacebookSettings_t2167659529), -1, sizeof(FacebookSettings_t2167659529_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1515[18] = 
{
	0,
	0,
	0,
	FacebookSettings_t2167659529_StaticFields::get_offset_of_onChangeCallbacks_5(),
	FacebookSettings_t2167659529_StaticFields::get_offset_of_instance_6(),
	FacebookSettings_t2167659529::get_offset_of_selectedAppIndex_7(),
	FacebookSettings_t2167659529::get_offset_of_clientTokens_8(),
	FacebookSettings_t2167659529::get_offset_of_appIds_9(),
	FacebookSettings_t2167659529::get_offset_of_appLabels_10(),
	FacebookSettings_t2167659529::get_offset_of_cookie_11(),
	FacebookSettings_t2167659529::get_offset_of_logging_12(),
	FacebookSettings_t2167659529::get_offset_of_status_13(),
	FacebookSettings_t2167659529::get_offset_of_xfbml_14(),
	FacebookSettings_t2167659529::get_offset_of_frictionlessRequests_15(),
	FacebookSettings_t2167659529::get_offset_of_iosURLSuffix_16(),
	FacebookSettings_t2167659529::get_offset_of_appLinkSchemes_17(),
	FacebookSettings_t2167659529::get_offset_of_uploadAccessToken_18(),
	FacebookSettings_t2167659529_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1516 = { sizeof (OnChangeCallback_t2639189684), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1517 = { sizeof (UrlSchemes_t2066630273), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1517[1] = 
{
	UrlSchemes_t2066630273::get_offset_of_list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1518 = { sizeof (U3CModuleU3E_t3783534220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1519 = { sizeof (AccessToken_t2518141643), -1, sizeof(AccessToken_t2518141643_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1519[6] = 
{
	AccessToken_t2518141643_StaticFields::get_offset_of_U3CCurrentAccessTokenU3Ek__BackingField_0(),
	AccessToken_t2518141643::get_offset_of_U3CTokenStringU3Ek__BackingField_1(),
	AccessToken_t2518141643::get_offset_of_U3CExpirationTimeU3Ek__BackingField_2(),
	AccessToken_t2518141643::get_offset_of_U3CPermissionsU3Ek__BackingField_3(),
	AccessToken_t2518141643::get_offset_of_U3CUserIdU3Ek__BackingField_4(),
	AccessToken_t2518141643::get_offset_of_U3CLastRefreshU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1520 = { sizeof (CallbackManager_t4242095184), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1520[2] = 
{
	CallbackManager_t4242095184::get_offset_of_facebookDelegates_0(),
	CallbackManager_t4242095184::get_offset_of_nextAsyncId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1521 = { sizeof (ComponentFactory_t2270101009), -1, sizeof(ComponentFactory_t2270101009_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1521[1] = 
{
	ComponentFactory_t2270101009_StaticFields::get_offset_of_facebookGameObject_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1522 = { sizeof (IfNotExist_t643838650)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1522[3] = 
{
	IfNotExist_t643838650::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1523 = { sizeof (Constants_t3246017429), -1, sizeof(Constants_t3246017429_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1523[1] = 
{
	Constants_t3246017429_StaticFields::get_offset_of_currentPlatform_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1524 = { sizeof (FB_t1612658294), -1, sizeof(FB_t1612658294_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1524[8] = 
{
	0,
	FB_t1612658294_StaticFields::get_offset_of_facebook_3(),
	FB_t1612658294_StaticFields::get_offset_of_isInitCalled_4(),
	FB_t1612658294_StaticFields::get_offset_of_facebookDomain_5(),
	FB_t1612658294_StaticFields::get_offset_of_graphApiVersion_6(),
	FB_t1612658294_StaticFields::get_offset_of_U3CAppIdU3Ek__BackingField_7(),
	FB_t1612658294_StaticFields::get_offset_of_U3CClientTokenU3Ek__BackingField_8(),
	FB_t1612658294_StaticFields::get_offset_of_U3COnDLLLoadedDelegateU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1525 = { sizeof (OnDLLLoaded_t1727406294), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1526 = { sizeof (Canvas_t2415435652), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1527 = { sizeof (Mobile_t3800732530), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1528 = { sizeof (CompiledFacebookLoader_t3305868676), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1529 = { sizeof (U3CInitU3Ec__AnonStorey0_t4214738661), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1529[10] = 
{
	U3CInitU3Ec__AnonStorey0_t4214738661::get_offset_of_onInitComplete_0(),
	U3CInitU3Ec__AnonStorey0_t4214738661::get_offset_of_appId_1(),
	U3CInitU3Ec__AnonStorey0_t4214738661::get_offset_of_cookie_2(),
	U3CInitU3Ec__AnonStorey0_t4214738661::get_offset_of_logging_3(),
	U3CInitU3Ec__AnonStorey0_t4214738661::get_offset_of_status_4(),
	U3CInitU3Ec__AnonStorey0_t4214738661::get_offset_of_xfbml_5(),
	U3CInitU3Ec__AnonStorey0_t4214738661::get_offset_of_authResponse_6(),
	U3CInitU3Ec__AnonStorey0_t4214738661::get_offset_of_frictionlessRequests_7(),
	U3CInitU3Ec__AnonStorey0_t4214738661::get_offset_of_javascriptSDKLocale_8(),
	U3CInitU3Ec__AnonStorey0_t4214738661::get_offset_of_onHideUnity_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1530 = { sizeof (FacebookBase_t2463540723), -1, sizeof(FacebookBase_t2463540723_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1530[4] = 
{
	FacebookBase_t2463540723::get_offset_of_onInitCompleteDelegate_0(),
	FacebookBase_t2463540723::get_offset_of_U3CInitializedU3Ek__BackingField_1(),
	FacebookBase_t2463540723::get_offset_of_U3CCallbackManagerU3Ek__BackingField_2(),
	FacebookBase_t2463540723_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1531 = { sizeof (InitDelegate_t3410465555), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1532 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1533 = { sizeof (HideUnityDelegate_t712804158), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1534 = { sizeof (FacebookGameObject_t2922570967), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1534[1] = 
{
	FacebookGameObject_t2922570967::get_offset_of_U3CFacebookU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1535 = { sizeof (FacebookSdkVersion_t1532426400), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1536 = { sizeof (FacebookUnityPlatform_t1867507902)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1536[6] = 
{
	FacebookUnityPlatform_t1867507902::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1537 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1538 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1539 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1540 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1541 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1542 = { sizeof (MethodArguments_t735664811), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1542[1] = 
{
	MethodArguments_t735664811::get_offset_of_arguments_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1543 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1543[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1544 = { sizeof (ShareDialogMode_t1445392044)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1544[5] = 
{
	ShareDialogMode_t1445392044::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1545 = { sizeof (OGActionType_t1978093408)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1545[4] = 
{
	OGActionType_t1978093408::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1546 = { sizeof (CanvasFacebook_t1390391306), -1, sizeof(CanvasFacebook_t1390391306_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1546[6] = 
{
	CanvasFacebook_t1390391306::get_offset_of_appId_4(),
	CanvasFacebook_t1390391306::get_offset_of_appLinkUrl_5(),
	CanvasFacebook_t1390391306::get_offset_of_canvasJSWrapper_6(),
	CanvasFacebook_t1390391306::get_offset_of_onHideUnityDelegate_7(),
	CanvasFacebook_t1390391306::get_offset_of_U3CLimitEventUsageU3Ek__BackingField_8(),
	CanvasFacebook_t1390391306_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1547 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1547[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1548 = { sizeof (U3CFormatAuthResponseU3Ec__AnonStorey0_t3137418185), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1548[2] = 
{
	U3CFormatAuthResponseU3Ec__AnonStorey0_t3137418185::get_offset_of_result_0(),
	U3CFormatAuthResponseU3Ec__AnonStorey0_t3137418185::get_offset_of_callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1549 = { sizeof (CanvasFacebookGameObject_t1732848805), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1550 = { sizeof (CanvasFacebookLoader_t1985905999), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1551 = { sizeof (CanvasJSWrapper_t3429491632), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1552 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1553 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1554 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1555 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1556 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1557 = { sizeof (JsBridge_t2435492180), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1557[1] = 
{
	JsBridge_t2435492180::get_offset_of_facebook_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1558 = { sizeof (ArcadeFacebook_t622171242), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1558[3] = 
{
	ArcadeFacebook_t622171242::get_offset_of_appId_4(),
	ArcadeFacebook_t622171242::get_offset_of_arcadeWrapper_5(),
	ArcadeFacebook_t622171242::get_offset_of_U3CLimitEventUsageU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1559 = { sizeof (OnComplete_t3968443998), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1560 = { sizeof (ArcadeFacebookGameObject_t1372893397), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1561 = { sizeof (U3CWaitForPipeResponseU3Ec__Iterator0_t1353419607), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1561[6] = 
{
	U3CWaitForPipeResponseU3Ec__Iterator0_t1353419607::get_offset_of_onCompleteDelegate_0(),
	U3CWaitForPipeResponseU3Ec__Iterator0_t1353419607::get_offset_of_callbackId_1(),
	U3CWaitForPipeResponseU3Ec__Iterator0_t1353419607::get_offset_of_U24this_2(),
	U3CWaitForPipeResponseU3Ec__Iterator0_t1353419607::get_offset_of_U24current_3(),
	U3CWaitForPipeResponseU3Ec__Iterator0_t1353419607::get_offset_of_U24disposing_4(),
	U3CWaitForPipeResponseU3Ec__Iterator0_t1353419607::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1562 = { sizeof (ArcadeFacebookLoader_t3930015791), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1563 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1564 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1565 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1566 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1567 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1568 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1569 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1570 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1571 = { sizeof (MobileFacebook_t2099931062), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1571[1] = 
{
	MobileFacebook_t2099931062::get_offset_of_shareDialogMode_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1572 = { sizeof (MobileFacebookGameObject_t668179329), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1573 = { sizeof (AndroidFacebook_t3038444049), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1573[3] = 
{
	AndroidFacebook_t3038444049::get_offset_of_limitEventUsage_5(),
	AndroidFacebook_t3038444049::get_offset_of_androidWrapper_6(),
	AndroidFacebook_t3038444049::get_offset_of_U3CKeyHashU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1574 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1574[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1575 = { sizeof (AndroidFacebookGameObject_t2149209708), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1576 = { sizeof (AndroidFacebookLoader_t3776634754), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1577 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1578 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1579 = { sizeof (IOSFacebook_t847746195), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1579[2] = 
{
	IOSFacebook_t847746195::get_offset_of_limitEventUsage_5(),
	IOSFacebook_t847746195::get_offset_of_iosWrapper_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1580 = { sizeof (NativeDict_t4261124525), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1580[3] = 
{
	NativeDict_t4261124525::get_offset_of_U3CNumEntriesU3Ek__BackingField_0(),
	NativeDict_t4261124525::get_offset_of_U3CKeysU3Ek__BackingField_1(),
	NativeDict_t4261124525::get_offset_of_U3CValuesU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1581 = { sizeof (IOSFacebookGameObject_t3391486318), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1582 = { sizeof (IOSFacebookLoader_t3879535236), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1583 = { sizeof (EditorFacebook_t1229262421), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1583[3] = 
{
	EditorFacebook_t1229262421::get_offset_of_editorWrapper_4(),
	EditorFacebook_t1229262421::get_offset_of_U3CLimitEventUsageU3Ek__BackingField_5(),
	EditorFacebook_t1229262421::get_offset_of_U3CShareDialogModeU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1584 = { sizeof (EditorFacebookGameObject_t63110230), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1585 = { sizeof (EditorFacebookLoader_t2109505584), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1586 = { sizeof (EditorFacebookMockDialog_t2244501619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1586[4] = 
{
	EditorFacebookMockDialog_t2244501619::get_offset_of_modalRect_2(),
	EditorFacebookMockDialog_t2244501619::get_offset_of_modalStyle_3(),
	EditorFacebookMockDialog_t2244501619::get_offset_of_U3CCallbackU3Ek__BackingField_4(),
	EditorFacebookMockDialog_t2244501619::get_offset_of_U3CCallbackIDU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1587 = { sizeof (EditorWrapper_t2290865494), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1587[1] = 
{
	EditorWrapper_t2290865494::get_offset_of_callbackHandler_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1588 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1589 = { sizeof (EmptyMockDialog_t2854396319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1589[1] = 
{
	EmptyMockDialog_t2854396319::get_offset_of_U3CEmptyDialogTitleU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1590 = { sizeof (MockLoginDialog_t531765055), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1590[1] = 
{
	MockLoginDialog_t531765055::get_offset_of_accessToken_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1591 = { sizeof (U3CSendSuccessResultU3Ec__AnonStorey0_t1402467746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1591[2] = 
{
	U3CSendSuccessResultU3Ec__AnonStorey0_t1402467746::get_offset_of_facebookID_0(),
	U3CSendSuccessResultU3Ec__AnonStorey0_t1402467746::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1592 = { sizeof (MockShareDialog_t1542668653), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1592[1] = 
{
	MockShareDialog_t1542668653::get_offset_of_U3CSubTitleU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1593 = { sizeof (AccessTokenRefreshResult_t170484741), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1593[1] = 
{
	AccessTokenRefreshResult_t170484741::get_offset_of_U3CAccessTokenU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1594 = { sizeof (AppInviteResult_t780050231), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1595 = { sizeof (AppLinkResult_t2690573342), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1595[4] = 
{
	AppLinkResult_t2690573342::get_offset_of_U3CUrlU3Ek__BackingField_6(),
	AppLinkResult_t2690573342::get_offset_of_U3CTargetUrlU3Ek__BackingField_7(),
	AppLinkResult_t2690573342::get_offset_of_U3CRefU3Ek__BackingField_8(),
	AppLinkResult_t2690573342::get_offset_of_U3CExtrasU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1596 = { sizeof (AppRequestResult_t2863124367), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1596[2] = 
{
	AppRequestResult_t2863124367::get_offset_of_U3CRequestIDU3Ek__BackingField_6(),
	AppRequestResult_t2863124367::get_offset_of_U3CToU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1597 = { sizeof (GraphResult_t2230822669), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1597[2] = 
{
	GraphResult_t2230822669::get_offset_of_U3CResultListU3Ek__BackingField_6(),
	GraphResult_t2230822669::get_offset_of_U3CTextureU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1598 = { sizeof (GroupCreateResult_t605038084), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1598[1] = 
{
	GroupCreateResult_t605038084::get_offset_of_U3CGroupIdU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1599 = { sizeof (GroupJoinResult_t2959566706), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
