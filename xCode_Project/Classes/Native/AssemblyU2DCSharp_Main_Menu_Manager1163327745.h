﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.UI.Toggle
struct Toggle_t3976754468;
// UnityEngine.UI.RawImage
struct RawImage_t2749640213;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Main_Menu_Manager
struct  Main_Menu_Manager_t1163327745  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Main_Menu_Manager::localSetupScreen
	GameObject_t1756533147 * ___localSetupScreen_2;
	// UnityEngine.GameObject Main_Menu_Manager::creditsScreen
	GameObject_t1756533147 * ___creditsScreen_3;
	// UnityEngine.GameObject Main_Menu_Manager::settingsScreen
	GameObject_t1756533147 * ___settingsScreen_4;
	// UnityEngine.UI.Button Main_Menu_Manager::soundButton
	Button_t2872111280 * ___soundButton_5;
	// UnityEngine.Sprite Main_Menu_Manager::soundOn
	Sprite_t309593783 * ___soundOn_6;
	// UnityEngine.Sprite Main_Menu_Manager::soundOff
	Sprite_t309593783 * ___soundOff_7;
	// System.Boolean Main_Menu_Manager::isSoundOn
	bool ___isSoundOn_8;
	// UnityEngine.UI.Toggle Main_Menu_Manager::highGraphicsToggle
	Toggle_t3976754468 * ___highGraphicsToggle_9;
	// UnityEngine.UI.RawImage Main_Menu_Manager::splashScreen
	RawImage_t2749640213 * ___splashScreen_10;
	// System.Single Main_Menu_Manager::splashFadeOutSpeed
	float ___splashFadeOutSpeed_11;
	// System.Boolean Main_Menu_Manager::startRemovingSplash
	bool ___startRemovingSplash_12;

public:
	inline static int32_t get_offset_of_localSetupScreen_2() { return static_cast<int32_t>(offsetof(Main_Menu_Manager_t1163327745, ___localSetupScreen_2)); }
	inline GameObject_t1756533147 * get_localSetupScreen_2() const { return ___localSetupScreen_2; }
	inline GameObject_t1756533147 ** get_address_of_localSetupScreen_2() { return &___localSetupScreen_2; }
	inline void set_localSetupScreen_2(GameObject_t1756533147 * value)
	{
		___localSetupScreen_2 = value;
		Il2CppCodeGenWriteBarrier(&___localSetupScreen_2, value);
	}

	inline static int32_t get_offset_of_creditsScreen_3() { return static_cast<int32_t>(offsetof(Main_Menu_Manager_t1163327745, ___creditsScreen_3)); }
	inline GameObject_t1756533147 * get_creditsScreen_3() const { return ___creditsScreen_3; }
	inline GameObject_t1756533147 ** get_address_of_creditsScreen_3() { return &___creditsScreen_3; }
	inline void set_creditsScreen_3(GameObject_t1756533147 * value)
	{
		___creditsScreen_3 = value;
		Il2CppCodeGenWriteBarrier(&___creditsScreen_3, value);
	}

	inline static int32_t get_offset_of_settingsScreen_4() { return static_cast<int32_t>(offsetof(Main_Menu_Manager_t1163327745, ___settingsScreen_4)); }
	inline GameObject_t1756533147 * get_settingsScreen_4() const { return ___settingsScreen_4; }
	inline GameObject_t1756533147 ** get_address_of_settingsScreen_4() { return &___settingsScreen_4; }
	inline void set_settingsScreen_4(GameObject_t1756533147 * value)
	{
		___settingsScreen_4 = value;
		Il2CppCodeGenWriteBarrier(&___settingsScreen_4, value);
	}

	inline static int32_t get_offset_of_soundButton_5() { return static_cast<int32_t>(offsetof(Main_Menu_Manager_t1163327745, ___soundButton_5)); }
	inline Button_t2872111280 * get_soundButton_5() const { return ___soundButton_5; }
	inline Button_t2872111280 ** get_address_of_soundButton_5() { return &___soundButton_5; }
	inline void set_soundButton_5(Button_t2872111280 * value)
	{
		___soundButton_5 = value;
		Il2CppCodeGenWriteBarrier(&___soundButton_5, value);
	}

	inline static int32_t get_offset_of_soundOn_6() { return static_cast<int32_t>(offsetof(Main_Menu_Manager_t1163327745, ___soundOn_6)); }
	inline Sprite_t309593783 * get_soundOn_6() const { return ___soundOn_6; }
	inline Sprite_t309593783 ** get_address_of_soundOn_6() { return &___soundOn_6; }
	inline void set_soundOn_6(Sprite_t309593783 * value)
	{
		___soundOn_6 = value;
		Il2CppCodeGenWriteBarrier(&___soundOn_6, value);
	}

	inline static int32_t get_offset_of_soundOff_7() { return static_cast<int32_t>(offsetof(Main_Menu_Manager_t1163327745, ___soundOff_7)); }
	inline Sprite_t309593783 * get_soundOff_7() const { return ___soundOff_7; }
	inline Sprite_t309593783 ** get_address_of_soundOff_7() { return &___soundOff_7; }
	inline void set_soundOff_7(Sprite_t309593783 * value)
	{
		___soundOff_7 = value;
		Il2CppCodeGenWriteBarrier(&___soundOff_7, value);
	}

	inline static int32_t get_offset_of_isSoundOn_8() { return static_cast<int32_t>(offsetof(Main_Menu_Manager_t1163327745, ___isSoundOn_8)); }
	inline bool get_isSoundOn_8() const { return ___isSoundOn_8; }
	inline bool* get_address_of_isSoundOn_8() { return &___isSoundOn_8; }
	inline void set_isSoundOn_8(bool value)
	{
		___isSoundOn_8 = value;
	}

	inline static int32_t get_offset_of_highGraphicsToggle_9() { return static_cast<int32_t>(offsetof(Main_Menu_Manager_t1163327745, ___highGraphicsToggle_9)); }
	inline Toggle_t3976754468 * get_highGraphicsToggle_9() const { return ___highGraphicsToggle_9; }
	inline Toggle_t3976754468 ** get_address_of_highGraphicsToggle_9() { return &___highGraphicsToggle_9; }
	inline void set_highGraphicsToggle_9(Toggle_t3976754468 * value)
	{
		___highGraphicsToggle_9 = value;
		Il2CppCodeGenWriteBarrier(&___highGraphicsToggle_9, value);
	}

	inline static int32_t get_offset_of_splashScreen_10() { return static_cast<int32_t>(offsetof(Main_Menu_Manager_t1163327745, ___splashScreen_10)); }
	inline RawImage_t2749640213 * get_splashScreen_10() const { return ___splashScreen_10; }
	inline RawImage_t2749640213 ** get_address_of_splashScreen_10() { return &___splashScreen_10; }
	inline void set_splashScreen_10(RawImage_t2749640213 * value)
	{
		___splashScreen_10 = value;
		Il2CppCodeGenWriteBarrier(&___splashScreen_10, value);
	}

	inline static int32_t get_offset_of_splashFadeOutSpeed_11() { return static_cast<int32_t>(offsetof(Main_Menu_Manager_t1163327745, ___splashFadeOutSpeed_11)); }
	inline float get_splashFadeOutSpeed_11() const { return ___splashFadeOutSpeed_11; }
	inline float* get_address_of_splashFadeOutSpeed_11() { return &___splashFadeOutSpeed_11; }
	inline void set_splashFadeOutSpeed_11(float value)
	{
		___splashFadeOutSpeed_11 = value;
	}

	inline static int32_t get_offset_of_startRemovingSplash_12() { return static_cast<int32_t>(offsetof(Main_Menu_Manager_t1163327745, ___startRemovingSplash_12)); }
	inline bool get_startRemovingSplash_12() const { return ___startRemovingSplash_12; }
	inline bool* get_address_of_startRemovingSplash_12() { return &___startRemovingSplash_12; }
	inline void set_startRemovingSplash_12(bool value)
	{
		___startRemovingSplash_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
