﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// admob.AdSize
struct AdSize_t3770813302;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// admob.AdSize
struct  AdSize_t3770813302  : public Il2CppObject
{
public:
	// System.Int32 admob.AdSize::width
	int32_t ___width_0;
	// System.Int32 admob.AdSize::height
	int32_t ___height_1;

public:
	inline static int32_t get_offset_of_width_0() { return static_cast<int32_t>(offsetof(AdSize_t3770813302, ___width_0)); }
	inline int32_t get_width_0() const { return ___width_0; }
	inline int32_t* get_address_of_width_0() { return &___width_0; }
	inline void set_width_0(int32_t value)
	{
		___width_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(AdSize_t3770813302, ___height_1)); }
	inline int32_t get_height_1() const { return ___height_1; }
	inline int32_t* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(int32_t value)
	{
		___height_1 = value;
	}
};

struct AdSize_t3770813302_StaticFields
{
public:
	// admob.AdSize admob.AdSize::Banner
	AdSize_t3770813302 * ___Banner_2;
	// admob.AdSize admob.AdSize::MediumRectangle
	AdSize_t3770813302 * ___MediumRectangle_3;
	// admob.AdSize admob.AdSize::IABBanner
	AdSize_t3770813302 * ___IABBanner_4;
	// admob.AdSize admob.AdSize::Leaderboard
	AdSize_t3770813302 * ___Leaderboard_5;
	// admob.AdSize admob.AdSize::WideSkyscraper
	AdSize_t3770813302 * ___WideSkyscraper_6;
	// admob.AdSize admob.AdSize::SmartBanner
	AdSize_t3770813302 * ___SmartBanner_7;

public:
	inline static int32_t get_offset_of_Banner_2() { return static_cast<int32_t>(offsetof(AdSize_t3770813302_StaticFields, ___Banner_2)); }
	inline AdSize_t3770813302 * get_Banner_2() const { return ___Banner_2; }
	inline AdSize_t3770813302 ** get_address_of_Banner_2() { return &___Banner_2; }
	inline void set_Banner_2(AdSize_t3770813302 * value)
	{
		___Banner_2 = value;
		Il2CppCodeGenWriteBarrier(&___Banner_2, value);
	}

	inline static int32_t get_offset_of_MediumRectangle_3() { return static_cast<int32_t>(offsetof(AdSize_t3770813302_StaticFields, ___MediumRectangle_3)); }
	inline AdSize_t3770813302 * get_MediumRectangle_3() const { return ___MediumRectangle_3; }
	inline AdSize_t3770813302 ** get_address_of_MediumRectangle_3() { return &___MediumRectangle_3; }
	inline void set_MediumRectangle_3(AdSize_t3770813302 * value)
	{
		___MediumRectangle_3 = value;
		Il2CppCodeGenWriteBarrier(&___MediumRectangle_3, value);
	}

	inline static int32_t get_offset_of_IABBanner_4() { return static_cast<int32_t>(offsetof(AdSize_t3770813302_StaticFields, ___IABBanner_4)); }
	inline AdSize_t3770813302 * get_IABBanner_4() const { return ___IABBanner_4; }
	inline AdSize_t3770813302 ** get_address_of_IABBanner_4() { return &___IABBanner_4; }
	inline void set_IABBanner_4(AdSize_t3770813302 * value)
	{
		___IABBanner_4 = value;
		Il2CppCodeGenWriteBarrier(&___IABBanner_4, value);
	}

	inline static int32_t get_offset_of_Leaderboard_5() { return static_cast<int32_t>(offsetof(AdSize_t3770813302_StaticFields, ___Leaderboard_5)); }
	inline AdSize_t3770813302 * get_Leaderboard_5() const { return ___Leaderboard_5; }
	inline AdSize_t3770813302 ** get_address_of_Leaderboard_5() { return &___Leaderboard_5; }
	inline void set_Leaderboard_5(AdSize_t3770813302 * value)
	{
		___Leaderboard_5 = value;
		Il2CppCodeGenWriteBarrier(&___Leaderboard_5, value);
	}

	inline static int32_t get_offset_of_WideSkyscraper_6() { return static_cast<int32_t>(offsetof(AdSize_t3770813302_StaticFields, ___WideSkyscraper_6)); }
	inline AdSize_t3770813302 * get_WideSkyscraper_6() const { return ___WideSkyscraper_6; }
	inline AdSize_t3770813302 ** get_address_of_WideSkyscraper_6() { return &___WideSkyscraper_6; }
	inline void set_WideSkyscraper_6(AdSize_t3770813302 * value)
	{
		___WideSkyscraper_6 = value;
		Il2CppCodeGenWriteBarrier(&___WideSkyscraper_6, value);
	}

	inline static int32_t get_offset_of_SmartBanner_7() { return static_cast<int32_t>(offsetof(AdSize_t3770813302_StaticFields, ___SmartBanner_7)); }
	inline AdSize_t3770813302 * get_SmartBanner_7() const { return ___SmartBanner_7; }
	inline AdSize_t3770813302 ** get_address_of_SmartBanner_7() { return &___SmartBanner_7; }
	inline void set_SmartBanner_7(AdSize_t3770813302 * value)
	{
		___SmartBanner_7 = value;
		Il2CppCodeGenWriteBarrier(&___SmartBanner_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
