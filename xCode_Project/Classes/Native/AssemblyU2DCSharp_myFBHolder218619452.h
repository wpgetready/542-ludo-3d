﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Image
struct Image_t2042527209;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// myFBHolder
struct  myFBHolder_t218619452  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject myFBHolder::UIFBLoggedIn
	GameObject_t1756533147 * ___UIFBLoggedIn_2;
	// UnityEngine.GameObject myFBHolder::UIFBNotLoggedIn
	GameObject_t1756533147 * ___UIFBNotLoggedIn_3;
	// UnityEngine.UI.Text myFBHolder::UIFBName
	Text_t356221433 * ___UIFBName_4;
	// UnityEngine.UI.Image myFBHolder::profilePic
	Image_t2042527209 * ___profilePic_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> myFBHolder::profile
	Dictionary_2_t3943999495 * ___profile_6;
	// System.String myFBHolder::userName
	String_t* ___userName_7;
	// System.String myFBHolder::FBUrl
	String_t* ___FBUrl_8;

public:
	inline static int32_t get_offset_of_UIFBLoggedIn_2() { return static_cast<int32_t>(offsetof(myFBHolder_t218619452, ___UIFBLoggedIn_2)); }
	inline GameObject_t1756533147 * get_UIFBLoggedIn_2() const { return ___UIFBLoggedIn_2; }
	inline GameObject_t1756533147 ** get_address_of_UIFBLoggedIn_2() { return &___UIFBLoggedIn_2; }
	inline void set_UIFBLoggedIn_2(GameObject_t1756533147 * value)
	{
		___UIFBLoggedIn_2 = value;
		Il2CppCodeGenWriteBarrier(&___UIFBLoggedIn_2, value);
	}

	inline static int32_t get_offset_of_UIFBNotLoggedIn_3() { return static_cast<int32_t>(offsetof(myFBHolder_t218619452, ___UIFBNotLoggedIn_3)); }
	inline GameObject_t1756533147 * get_UIFBNotLoggedIn_3() const { return ___UIFBNotLoggedIn_3; }
	inline GameObject_t1756533147 ** get_address_of_UIFBNotLoggedIn_3() { return &___UIFBNotLoggedIn_3; }
	inline void set_UIFBNotLoggedIn_3(GameObject_t1756533147 * value)
	{
		___UIFBNotLoggedIn_3 = value;
		Il2CppCodeGenWriteBarrier(&___UIFBNotLoggedIn_3, value);
	}

	inline static int32_t get_offset_of_UIFBName_4() { return static_cast<int32_t>(offsetof(myFBHolder_t218619452, ___UIFBName_4)); }
	inline Text_t356221433 * get_UIFBName_4() const { return ___UIFBName_4; }
	inline Text_t356221433 ** get_address_of_UIFBName_4() { return &___UIFBName_4; }
	inline void set_UIFBName_4(Text_t356221433 * value)
	{
		___UIFBName_4 = value;
		Il2CppCodeGenWriteBarrier(&___UIFBName_4, value);
	}

	inline static int32_t get_offset_of_profilePic_5() { return static_cast<int32_t>(offsetof(myFBHolder_t218619452, ___profilePic_5)); }
	inline Image_t2042527209 * get_profilePic_5() const { return ___profilePic_5; }
	inline Image_t2042527209 ** get_address_of_profilePic_5() { return &___profilePic_5; }
	inline void set_profilePic_5(Image_t2042527209 * value)
	{
		___profilePic_5 = value;
		Il2CppCodeGenWriteBarrier(&___profilePic_5, value);
	}

	inline static int32_t get_offset_of_profile_6() { return static_cast<int32_t>(offsetof(myFBHolder_t218619452, ___profile_6)); }
	inline Dictionary_2_t3943999495 * get_profile_6() const { return ___profile_6; }
	inline Dictionary_2_t3943999495 ** get_address_of_profile_6() { return &___profile_6; }
	inline void set_profile_6(Dictionary_2_t3943999495 * value)
	{
		___profile_6 = value;
		Il2CppCodeGenWriteBarrier(&___profile_6, value);
	}

	inline static int32_t get_offset_of_userName_7() { return static_cast<int32_t>(offsetof(myFBHolder_t218619452, ___userName_7)); }
	inline String_t* get_userName_7() const { return ___userName_7; }
	inline String_t** get_address_of_userName_7() { return &___userName_7; }
	inline void set_userName_7(String_t* value)
	{
		___userName_7 = value;
		Il2CppCodeGenWriteBarrier(&___userName_7, value);
	}

	inline static int32_t get_offset_of_FBUrl_8() { return static_cast<int32_t>(offsetof(myFBHolder_t218619452, ___FBUrl_8)); }
	inline String_t* get_FBUrl_8() const { return ___FBUrl_8; }
	inline String_t** get_address_of_FBUrl_8() { return &___FBUrl_8; }
	inline void set_FBUrl_8(String_t* value)
	{
		___FBUrl_8 = value;
		Il2CppCodeGenWriteBarrier(&___FBUrl_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
