﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// AdMob_Manager
struct AdMob_Manager_t3979489515;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdMob_Manager
struct  AdMob_Manager_t3979489515  : public MonoBehaviour_t1158329972
{
public:
	// System.String AdMob_Manager::androidInterstitial
	String_t* ___androidInterstitial_2;
	// System.String AdMob_Manager::androidBanner
	String_t* ___androidBanner_3;
	// System.String AdMob_Manager::iosInterstitial
	String_t* ___iosInterstitial_4;
	// System.String AdMob_Manager::iosBanner
	String_t* ___iosBanner_5;
	// System.Boolean AdMob_Manager::showBannerAtStart
	bool ___showBannerAtStart_7;

public:
	inline static int32_t get_offset_of_androidInterstitial_2() { return static_cast<int32_t>(offsetof(AdMob_Manager_t3979489515, ___androidInterstitial_2)); }
	inline String_t* get_androidInterstitial_2() const { return ___androidInterstitial_2; }
	inline String_t** get_address_of_androidInterstitial_2() { return &___androidInterstitial_2; }
	inline void set_androidInterstitial_2(String_t* value)
	{
		___androidInterstitial_2 = value;
		Il2CppCodeGenWriteBarrier(&___androidInterstitial_2, value);
	}

	inline static int32_t get_offset_of_androidBanner_3() { return static_cast<int32_t>(offsetof(AdMob_Manager_t3979489515, ___androidBanner_3)); }
	inline String_t* get_androidBanner_3() const { return ___androidBanner_3; }
	inline String_t** get_address_of_androidBanner_3() { return &___androidBanner_3; }
	inline void set_androidBanner_3(String_t* value)
	{
		___androidBanner_3 = value;
		Il2CppCodeGenWriteBarrier(&___androidBanner_3, value);
	}

	inline static int32_t get_offset_of_iosInterstitial_4() { return static_cast<int32_t>(offsetof(AdMob_Manager_t3979489515, ___iosInterstitial_4)); }
	inline String_t* get_iosInterstitial_4() const { return ___iosInterstitial_4; }
	inline String_t** get_address_of_iosInterstitial_4() { return &___iosInterstitial_4; }
	inline void set_iosInterstitial_4(String_t* value)
	{
		___iosInterstitial_4 = value;
		Il2CppCodeGenWriteBarrier(&___iosInterstitial_4, value);
	}

	inline static int32_t get_offset_of_iosBanner_5() { return static_cast<int32_t>(offsetof(AdMob_Manager_t3979489515, ___iosBanner_5)); }
	inline String_t* get_iosBanner_5() const { return ___iosBanner_5; }
	inline String_t** get_address_of_iosBanner_5() { return &___iosBanner_5; }
	inline void set_iosBanner_5(String_t* value)
	{
		___iosBanner_5 = value;
		Il2CppCodeGenWriteBarrier(&___iosBanner_5, value);
	}

	inline static int32_t get_offset_of_showBannerAtStart_7() { return static_cast<int32_t>(offsetof(AdMob_Manager_t3979489515, ___showBannerAtStart_7)); }
	inline bool get_showBannerAtStart_7() const { return ___showBannerAtStart_7; }
	inline bool* get_address_of_showBannerAtStart_7() { return &___showBannerAtStart_7; }
	inline void set_showBannerAtStart_7(bool value)
	{
		___showBannerAtStart_7 = value;
	}
};

struct AdMob_Manager_t3979489515_StaticFields
{
public:
	// AdMob_Manager AdMob_Manager::<Instance>k__BackingField
	AdMob_Manager_t3979489515 * ___U3CInstanceU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AdMob_Manager_t3979489515_StaticFields, ___U3CInstanceU3Ek__BackingField_6)); }
	inline AdMob_Manager_t3979489515 * get_U3CInstanceU3Ek__BackingField_6() const { return ___U3CInstanceU3Ek__BackingField_6; }
	inline AdMob_Manager_t3979489515 ** get_address_of_U3CInstanceU3Ek__BackingField_6() { return &___U3CInstanceU3Ek__BackingField_6; }
	inline void set_U3CInstanceU3Ek__BackingField_6(AdMob_Manager_t3979489515 * value)
	{
		___U3CInstanceU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CInstanceU3Ek__BackingField_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
