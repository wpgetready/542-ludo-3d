﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// admob.AdmobEvent
struct  AdmobEvent_t4123996035  : public Il2CppObject
{
public:

public:
};

struct AdmobEvent_t4123996035_StaticFields
{
public:
	// System.String admob.AdmobEvent::onAdLoaded
	String_t* ___onAdLoaded_0;
	// System.String admob.AdmobEvent::onAdFailedToLoad
	String_t* ___onAdFailedToLoad_1;
	// System.String admob.AdmobEvent::onAdOpened
	String_t* ___onAdOpened_2;
	// System.String admob.AdmobEvent::adViewWillDismissScreen
	String_t* ___adViewWillDismissScreen_3;
	// System.String admob.AdmobEvent::onAdClosed
	String_t* ___onAdClosed_4;
	// System.String admob.AdmobEvent::onAdLeftApplication
	String_t* ___onAdLeftApplication_5;
	// System.String admob.AdmobEvent::onRewardedVideoStarted
	String_t* ___onRewardedVideoStarted_6;
	// System.String admob.AdmobEvent::onRewarded
	String_t* ___onRewarded_7;

public:
	inline static int32_t get_offset_of_onAdLoaded_0() { return static_cast<int32_t>(offsetof(AdmobEvent_t4123996035_StaticFields, ___onAdLoaded_0)); }
	inline String_t* get_onAdLoaded_0() const { return ___onAdLoaded_0; }
	inline String_t** get_address_of_onAdLoaded_0() { return &___onAdLoaded_0; }
	inline void set_onAdLoaded_0(String_t* value)
	{
		___onAdLoaded_0 = value;
		Il2CppCodeGenWriteBarrier(&___onAdLoaded_0, value);
	}

	inline static int32_t get_offset_of_onAdFailedToLoad_1() { return static_cast<int32_t>(offsetof(AdmobEvent_t4123996035_StaticFields, ___onAdFailedToLoad_1)); }
	inline String_t* get_onAdFailedToLoad_1() const { return ___onAdFailedToLoad_1; }
	inline String_t** get_address_of_onAdFailedToLoad_1() { return &___onAdFailedToLoad_1; }
	inline void set_onAdFailedToLoad_1(String_t* value)
	{
		___onAdFailedToLoad_1 = value;
		Il2CppCodeGenWriteBarrier(&___onAdFailedToLoad_1, value);
	}

	inline static int32_t get_offset_of_onAdOpened_2() { return static_cast<int32_t>(offsetof(AdmobEvent_t4123996035_StaticFields, ___onAdOpened_2)); }
	inline String_t* get_onAdOpened_2() const { return ___onAdOpened_2; }
	inline String_t** get_address_of_onAdOpened_2() { return &___onAdOpened_2; }
	inline void set_onAdOpened_2(String_t* value)
	{
		___onAdOpened_2 = value;
		Il2CppCodeGenWriteBarrier(&___onAdOpened_2, value);
	}

	inline static int32_t get_offset_of_adViewWillDismissScreen_3() { return static_cast<int32_t>(offsetof(AdmobEvent_t4123996035_StaticFields, ___adViewWillDismissScreen_3)); }
	inline String_t* get_adViewWillDismissScreen_3() const { return ___adViewWillDismissScreen_3; }
	inline String_t** get_address_of_adViewWillDismissScreen_3() { return &___adViewWillDismissScreen_3; }
	inline void set_adViewWillDismissScreen_3(String_t* value)
	{
		___adViewWillDismissScreen_3 = value;
		Il2CppCodeGenWriteBarrier(&___adViewWillDismissScreen_3, value);
	}

	inline static int32_t get_offset_of_onAdClosed_4() { return static_cast<int32_t>(offsetof(AdmobEvent_t4123996035_StaticFields, ___onAdClosed_4)); }
	inline String_t* get_onAdClosed_4() const { return ___onAdClosed_4; }
	inline String_t** get_address_of_onAdClosed_4() { return &___onAdClosed_4; }
	inline void set_onAdClosed_4(String_t* value)
	{
		___onAdClosed_4 = value;
		Il2CppCodeGenWriteBarrier(&___onAdClosed_4, value);
	}

	inline static int32_t get_offset_of_onAdLeftApplication_5() { return static_cast<int32_t>(offsetof(AdmobEvent_t4123996035_StaticFields, ___onAdLeftApplication_5)); }
	inline String_t* get_onAdLeftApplication_5() const { return ___onAdLeftApplication_5; }
	inline String_t** get_address_of_onAdLeftApplication_5() { return &___onAdLeftApplication_5; }
	inline void set_onAdLeftApplication_5(String_t* value)
	{
		___onAdLeftApplication_5 = value;
		Il2CppCodeGenWriteBarrier(&___onAdLeftApplication_5, value);
	}

	inline static int32_t get_offset_of_onRewardedVideoStarted_6() { return static_cast<int32_t>(offsetof(AdmobEvent_t4123996035_StaticFields, ___onRewardedVideoStarted_6)); }
	inline String_t* get_onRewardedVideoStarted_6() const { return ___onRewardedVideoStarted_6; }
	inline String_t** get_address_of_onRewardedVideoStarted_6() { return &___onRewardedVideoStarted_6; }
	inline void set_onRewardedVideoStarted_6(String_t* value)
	{
		___onRewardedVideoStarted_6 = value;
		Il2CppCodeGenWriteBarrier(&___onRewardedVideoStarted_6, value);
	}

	inline static int32_t get_offset_of_onRewarded_7() { return static_cast<int32_t>(offsetof(AdmobEvent_t4123996035_StaticFields, ___onRewarded_7)); }
	inline String_t* get_onRewarded_7() const { return ___onRewarded_7; }
	inline String_t** get_address_of_onRewarded_7() { return &___onRewarded_7; }
	inline void set_onRewarded_7(String_t* value)
	{
		___onRewarded_7 = value;
		Il2CppCodeGenWriteBarrier(&___onRewarded_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
