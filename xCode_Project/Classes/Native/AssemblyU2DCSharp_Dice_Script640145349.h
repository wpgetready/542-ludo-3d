﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;
// Turn_Manager
struct Turn_Manager_t2388373579;
// UnityEngine.Renderer
struct Renderer_t257310565;
// UnityEngine.UI.Image
struct Image_t2042527209;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Dice_Script
struct  Dice_Script_t640145349  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform Dice_Script::thisTransform
	Transform_t3275118058 * ___thisTransform_2;
	// System.Single Dice_Script::diceRollSpeed
	float ___diceRollSpeed_3;
	// System.Int32 Dice_Script::dInp
	int32_t ___dInp_4;
	// Turn_Manager Dice_Script::tm
	Turn_Manager_t2388373579 * ___tm_5;
	// UnityEngine.Renderer Dice_Script::diceR
	Renderer_t257310565 * ___diceR_6;
	// UnityEngine.UI.Image Dice_Script::diceArrow
	Image_t2042527209 * ___diceArrow_7;
	// System.Boolean Dice_Script::startArrowAnim
	bool ___startArrowAnim_8;
	// UnityEngine.Vector3 Dice_Script::initArrow
	Vector3_t2243707580  ___initArrow_9;
	// System.Single Dice_Script::arrowMovementRange
	float ___arrowMovementRange_10;
	// System.Single Dice_Script::arrowMovementSpeed
	float ___arrowMovementSpeed_11;

public:
	inline static int32_t get_offset_of_thisTransform_2() { return static_cast<int32_t>(offsetof(Dice_Script_t640145349, ___thisTransform_2)); }
	inline Transform_t3275118058 * get_thisTransform_2() const { return ___thisTransform_2; }
	inline Transform_t3275118058 ** get_address_of_thisTransform_2() { return &___thisTransform_2; }
	inline void set_thisTransform_2(Transform_t3275118058 * value)
	{
		___thisTransform_2 = value;
		Il2CppCodeGenWriteBarrier(&___thisTransform_2, value);
	}

	inline static int32_t get_offset_of_diceRollSpeed_3() { return static_cast<int32_t>(offsetof(Dice_Script_t640145349, ___diceRollSpeed_3)); }
	inline float get_diceRollSpeed_3() const { return ___diceRollSpeed_3; }
	inline float* get_address_of_diceRollSpeed_3() { return &___diceRollSpeed_3; }
	inline void set_diceRollSpeed_3(float value)
	{
		___diceRollSpeed_3 = value;
	}

	inline static int32_t get_offset_of_dInp_4() { return static_cast<int32_t>(offsetof(Dice_Script_t640145349, ___dInp_4)); }
	inline int32_t get_dInp_4() const { return ___dInp_4; }
	inline int32_t* get_address_of_dInp_4() { return &___dInp_4; }
	inline void set_dInp_4(int32_t value)
	{
		___dInp_4 = value;
	}

	inline static int32_t get_offset_of_tm_5() { return static_cast<int32_t>(offsetof(Dice_Script_t640145349, ___tm_5)); }
	inline Turn_Manager_t2388373579 * get_tm_5() const { return ___tm_5; }
	inline Turn_Manager_t2388373579 ** get_address_of_tm_5() { return &___tm_5; }
	inline void set_tm_5(Turn_Manager_t2388373579 * value)
	{
		___tm_5 = value;
		Il2CppCodeGenWriteBarrier(&___tm_5, value);
	}

	inline static int32_t get_offset_of_diceR_6() { return static_cast<int32_t>(offsetof(Dice_Script_t640145349, ___diceR_6)); }
	inline Renderer_t257310565 * get_diceR_6() const { return ___diceR_6; }
	inline Renderer_t257310565 ** get_address_of_diceR_6() { return &___diceR_6; }
	inline void set_diceR_6(Renderer_t257310565 * value)
	{
		___diceR_6 = value;
		Il2CppCodeGenWriteBarrier(&___diceR_6, value);
	}

	inline static int32_t get_offset_of_diceArrow_7() { return static_cast<int32_t>(offsetof(Dice_Script_t640145349, ___diceArrow_7)); }
	inline Image_t2042527209 * get_diceArrow_7() const { return ___diceArrow_7; }
	inline Image_t2042527209 ** get_address_of_diceArrow_7() { return &___diceArrow_7; }
	inline void set_diceArrow_7(Image_t2042527209 * value)
	{
		___diceArrow_7 = value;
		Il2CppCodeGenWriteBarrier(&___diceArrow_7, value);
	}

	inline static int32_t get_offset_of_startArrowAnim_8() { return static_cast<int32_t>(offsetof(Dice_Script_t640145349, ___startArrowAnim_8)); }
	inline bool get_startArrowAnim_8() const { return ___startArrowAnim_8; }
	inline bool* get_address_of_startArrowAnim_8() { return &___startArrowAnim_8; }
	inline void set_startArrowAnim_8(bool value)
	{
		___startArrowAnim_8 = value;
	}

	inline static int32_t get_offset_of_initArrow_9() { return static_cast<int32_t>(offsetof(Dice_Script_t640145349, ___initArrow_9)); }
	inline Vector3_t2243707580  get_initArrow_9() const { return ___initArrow_9; }
	inline Vector3_t2243707580 * get_address_of_initArrow_9() { return &___initArrow_9; }
	inline void set_initArrow_9(Vector3_t2243707580  value)
	{
		___initArrow_9 = value;
	}

	inline static int32_t get_offset_of_arrowMovementRange_10() { return static_cast<int32_t>(offsetof(Dice_Script_t640145349, ___arrowMovementRange_10)); }
	inline float get_arrowMovementRange_10() const { return ___arrowMovementRange_10; }
	inline float* get_address_of_arrowMovementRange_10() { return &___arrowMovementRange_10; }
	inline void set_arrowMovementRange_10(float value)
	{
		___arrowMovementRange_10 = value;
	}

	inline static int32_t get_offset_of_arrowMovementSpeed_11() { return static_cast<int32_t>(offsetof(Dice_Script_t640145349, ___arrowMovementSpeed_11)); }
	inline float get_arrowMovementSpeed_11() const { return ___arrowMovementSpeed_11; }
	inline float* get_address_of_arrowMovementSpeed_11() { return &___arrowMovementSpeed_11; }
	inline void set_arrowMovementSpeed_11(float value)
	{
		___arrowMovementSpeed_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
