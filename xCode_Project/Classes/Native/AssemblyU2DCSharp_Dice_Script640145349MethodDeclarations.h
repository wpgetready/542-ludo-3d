﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Dice_Script
struct Dice_Script_t640145349;

#include "codegen/il2cpp-codegen.h"

// System.Void Dice_Script::.ctor()
extern "C"  void Dice_Script__ctor_m3517106066 (Dice_Script_t640145349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Dice_Script::Start()
extern "C"  void Dice_Script_Start_m2194034762 (Dice_Script_t640145349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Dice_Script::Update()
extern "C"  void Dice_Script_Update_m4037671213 (Dice_Script_t640145349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Dice_Script::RollDice()
extern "C"  void Dice_Script_RollDice_m839356228 (Dice_Script_t640145349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Dice_Script::StopDice()
extern "C"  void Dice_Script_StopDice_m3232255983 (Dice_Script_t640145349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Dice_Script::EnableArrow()
extern "C"  void Dice_Script_EnableArrow_m2954576550 (Dice_Script_t640145349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Dice_Script::DisableArrow()
extern "C"  void Dice_Script_DisableArrow_m36271865 (Dice_Script_t640145349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
