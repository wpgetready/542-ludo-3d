﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// admob.Admob
struct Admob_t546240967;
// admob.Admob/AdmobEventHandler
struct AdmobEventHandler_t2983421020;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// admob.Admob/AdmobAdCallBack
struct AdmobAdCallBack_t390902866;
// admob.AdSize
struct AdSize_t3770813302;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// admob.AdmobEvent
struct AdmobEvent_t4123996035;
// admob.AdPosition
struct AdPosition_t1947228246;
// MonoPInvokeCallbackAttribute
struct MonoPInvokeCallbackAttribute_t1970456718;
// System.Type
struct Type_t;
// UnityStandardAssets.ImageEffects.ColorCorrectionCurves
struct ColorCorrectionCurves_t3523901841;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityStandardAssets.ImageEffects.ColorCorrectionLookup
struct ColorCorrectionLookup_t2385315007;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityStandardAssets.ImageEffects.ColorCorrectionRamp
struct ColorCorrectionRamp_t1672908095;
// UnityStandardAssets.ImageEffects.ImageEffectBase
struct ImageEffectBase_t517806655;
// UnityEngine.Material
struct Material_t193706927;
// UnityStandardAssets.ImageEffects.ImageEffects
struct ImageEffects_t2907318477;
// UnityStandardAssets.ImageEffects.PostEffectsBase
struct PostEffectsBase_t2152133263;
// UnityEngine.Shader
struct Shader_t2430389951;
// UnityStandardAssets.ImageEffects.PostEffectsHelper
struct PostEffectsHelper_t1845967802;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_admob_Admob546240967.h"
#include "AssemblyU2DCSharpU2Dfirstpass_admob_Admob546240967MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_admob_Admob_AdmobEve2983421020.h"
#include "mscorlib_System_Delegate3022476291MethodDeclarations.h"
#include "mscorlib_System_Delegate3022476291.h"
#include "mscorlib_System_Threading_Interlocked1625106012MethodDeclarations.h"
#include "mscorlib_System_Threading_Interlocked1625106012.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_admob_Admob_AdmobAdCa390902866.h"
#include "AssemblyU2DCSharpU2Dfirstpass_admob_Admob_AdmobAdCa390902866MethodDeclarations.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Int322071877448.h"
#include "AssemblyU2DCSharpU2Dfirstpass_admob_AdSize3770813302.h"
#include "AssemblyU2DCSharpU2Dfirstpass_admob_AdSize3770813302MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_admob_Admob_AdmobEve2983421020MethodDeclarations.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "AssemblyU2DCSharpU2Dfirstpass_admob_AdmobEvent4123996035.h"
#include "AssemblyU2DCSharpU2Dfirstpass_admob_AdmobEvent4123996035MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_admob_AdPosition1947228246.h"
#include "AssemblyU2DCSharpU2Dfirstpass_admob_AdPosition1947228246MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_MonoPInvokeCallbackA1970456718.h"
#include "AssemblyU2DCSharpU2Dfirstpass_MonoPInvokeCallbackA1970456718MethodDeclarations.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Attribute542643598MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3523901841.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3523901841MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationCurve3306541151MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color2020392075MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2152133263MethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Keyframe1449471340.h"
#include "UnityEngine_UnityEngine_Keyframe1449471340MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "UnityEngine_UnityEngine_AnimationCurve3306541151.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2243626319MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3694918262.h"
#include "UnityEngine_UnityEngine_Shader2430389951.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_TextureFormat1386130234.h"
#include "UnityEngine_UnityEngine_HideFlags1434274199.h"
#include "UnityEngine_UnityEngine_TextureWrapMode3683976566.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2152133263.h"
#include "UnityEngine_UnityEngine_Mathf2336485820MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"
#include "UnityEngine_UnityEngine_Graphics2412809155MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera189460977MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material193706927MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "UnityEngine_UnityEngine_DepthTextureMode1156392273.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3694918262MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2385315007.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2385315007MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SystemInfo2353426895MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture3D814112374.h"
#include "UnityEngine_UnityEngine_Texture3D814112374MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "UnityEngine_UnityEngine_QualitySettings3238033062MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ColorSpace627621177.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1672908095.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1672908095MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_I517806655MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_I517806655.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Behaviour955675639MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Shader2430389951MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2907318477.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2907318477MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "UnityEngine_UnityEngine_Vector42243707581MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3857795355MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3857795355.h"
#include "mscorlib_ArrayTypes.h"
#include "UnityEngine_UnityEngine_RenderTextureFormat3360518468.h"
#include "UnityEngine_UnityEngine_GL1765937205MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1845967802.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1845967802MethodDeclarations.h"

// !!0 System.Threading.Interlocked::CompareExchange<System.Object>(!!0&,!!0,!!0)
extern "C"  Il2CppObject * Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject ** p0, Il2CppObject * p1, Il2CppObject * p2, const MethodInfo* method);
#define Interlocked_CompareExchange_TisIl2CppObject_m2145889806(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject **, Il2CppObject *, Il2CppObject *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 System.Threading.Interlocked::CompareExchange<admob.Admob/AdmobEventHandler>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisAdmobEventHandler_t2983421020_m1649506743(__this /* static, unused */, p0, p1, p2, method) ((  AdmobEventHandler_t2983421020 * (*) (Il2CppObject * /* static, unused */, AdmobEventHandler_t2983421020 **, AdmobEventHandler_t2983421020 *, AdmobEventHandler_t2983421020 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m4109961936(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t189460977_m3276577584(__this, method) ((  Camera_t189460977 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void admob.Admob::.ctor()
extern "C"  void Admob__ctor_m3904493015 (Admob_t546240967 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void admob.Admob::add_bannerEventHandler(admob.Admob/AdmobEventHandler)
extern Il2CppClass* AdmobEventHandler_t2983421020_il2cpp_TypeInfo_var;
extern const uint32_t Admob_add_bannerEventHandler_m1557830709_MetadataUsageId;
extern "C"  void Admob_add_bannerEventHandler_m1557830709 (Admob_t546240967 * __this, AdmobEventHandler_t2983421020 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Admob_add_bannerEventHandler_m1557830709_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AdmobEventHandler_t2983421020 * V_0 = NULL;
	AdmobEventHandler_t2983421020 * V_1 = NULL;
	{
		AdmobEventHandler_t2983421020 * L_0 = __this->get_bannerEventHandler_0();
		V_0 = L_0;
	}

IL_0007:
	{
		AdmobEventHandler_t2983421020 * L_1 = V_0;
		V_1 = L_1;
		AdmobEventHandler_t2983421020 ** L_2 = __this->get_address_of_bannerEventHandler_0();
		AdmobEventHandler_t2983421020 * L_3 = V_1;
		AdmobEventHandler_t2983421020 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		AdmobEventHandler_t2983421020 * L_6 = V_0;
		AdmobEventHandler_t2983421020 * L_7 = InterlockedCompareExchangeImpl<AdmobEventHandler_t2983421020 *>(L_2, ((AdmobEventHandler_t2983421020 *)CastclassSealed(L_5, AdmobEventHandler_t2983421020_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		AdmobEventHandler_t2983421020 * L_8 = V_0;
		AdmobEventHandler_t2983421020 * L_9 = V_1;
		if ((!(((Il2CppObject*)(AdmobEventHandler_t2983421020 *)L_8) == ((Il2CppObject*)(AdmobEventHandler_t2983421020 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void admob.Admob::remove_bannerEventHandler(admob.Admob/AdmobEventHandler)
extern Il2CppClass* AdmobEventHandler_t2983421020_il2cpp_TypeInfo_var;
extern const uint32_t Admob_remove_bannerEventHandler_m2591577026_MetadataUsageId;
extern "C"  void Admob_remove_bannerEventHandler_m2591577026 (Admob_t546240967 * __this, AdmobEventHandler_t2983421020 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Admob_remove_bannerEventHandler_m2591577026_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AdmobEventHandler_t2983421020 * V_0 = NULL;
	AdmobEventHandler_t2983421020 * V_1 = NULL;
	{
		AdmobEventHandler_t2983421020 * L_0 = __this->get_bannerEventHandler_0();
		V_0 = L_0;
	}

IL_0007:
	{
		AdmobEventHandler_t2983421020 * L_1 = V_0;
		V_1 = L_1;
		AdmobEventHandler_t2983421020 ** L_2 = __this->get_address_of_bannerEventHandler_0();
		AdmobEventHandler_t2983421020 * L_3 = V_1;
		AdmobEventHandler_t2983421020 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		AdmobEventHandler_t2983421020 * L_6 = V_0;
		AdmobEventHandler_t2983421020 * L_7 = InterlockedCompareExchangeImpl<AdmobEventHandler_t2983421020 *>(L_2, ((AdmobEventHandler_t2983421020 *)CastclassSealed(L_5, AdmobEventHandler_t2983421020_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		AdmobEventHandler_t2983421020 * L_8 = V_0;
		AdmobEventHandler_t2983421020 * L_9 = V_1;
		if ((!(((Il2CppObject*)(AdmobEventHandler_t2983421020 *)L_8) == ((Il2CppObject*)(AdmobEventHandler_t2983421020 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void admob.Admob::add_interstitialEventHandler(admob.Admob/AdmobEventHandler)
extern Il2CppClass* AdmobEventHandler_t2983421020_il2cpp_TypeInfo_var;
extern const uint32_t Admob_add_interstitialEventHandler_m528202391_MetadataUsageId;
extern "C"  void Admob_add_interstitialEventHandler_m528202391 (Admob_t546240967 * __this, AdmobEventHandler_t2983421020 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Admob_add_interstitialEventHandler_m528202391_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AdmobEventHandler_t2983421020 * V_0 = NULL;
	AdmobEventHandler_t2983421020 * V_1 = NULL;
	{
		AdmobEventHandler_t2983421020 * L_0 = __this->get_interstitialEventHandler_1();
		V_0 = L_0;
	}

IL_0007:
	{
		AdmobEventHandler_t2983421020 * L_1 = V_0;
		V_1 = L_1;
		AdmobEventHandler_t2983421020 ** L_2 = __this->get_address_of_interstitialEventHandler_1();
		AdmobEventHandler_t2983421020 * L_3 = V_1;
		AdmobEventHandler_t2983421020 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		AdmobEventHandler_t2983421020 * L_6 = V_0;
		AdmobEventHandler_t2983421020 * L_7 = InterlockedCompareExchangeImpl<AdmobEventHandler_t2983421020 *>(L_2, ((AdmobEventHandler_t2983421020 *)CastclassSealed(L_5, AdmobEventHandler_t2983421020_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		AdmobEventHandler_t2983421020 * L_8 = V_0;
		AdmobEventHandler_t2983421020 * L_9 = V_1;
		if ((!(((Il2CppObject*)(AdmobEventHandler_t2983421020 *)L_8) == ((Il2CppObject*)(AdmobEventHandler_t2983421020 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void admob.Admob::remove_interstitialEventHandler(admob.Admob/AdmobEventHandler)
extern Il2CppClass* AdmobEventHandler_t2983421020_il2cpp_TypeInfo_var;
extern const uint32_t Admob_remove_interstitialEventHandler_m2729753070_MetadataUsageId;
extern "C"  void Admob_remove_interstitialEventHandler_m2729753070 (Admob_t546240967 * __this, AdmobEventHandler_t2983421020 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Admob_remove_interstitialEventHandler_m2729753070_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AdmobEventHandler_t2983421020 * V_0 = NULL;
	AdmobEventHandler_t2983421020 * V_1 = NULL;
	{
		AdmobEventHandler_t2983421020 * L_0 = __this->get_interstitialEventHandler_1();
		V_0 = L_0;
	}

IL_0007:
	{
		AdmobEventHandler_t2983421020 * L_1 = V_0;
		V_1 = L_1;
		AdmobEventHandler_t2983421020 ** L_2 = __this->get_address_of_interstitialEventHandler_1();
		AdmobEventHandler_t2983421020 * L_3 = V_1;
		AdmobEventHandler_t2983421020 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		AdmobEventHandler_t2983421020 * L_6 = V_0;
		AdmobEventHandler_t2983421020 * L_7 = InterlockedCompareExchangeImpl<AdmobEventHandler_t2983421020 *>(L_2, ((AdmobEventHandler_t2983421020 *)CastclassSealed(L_5, AdmobEventHandler_t2983421020_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		AdmobEventHandler_t2983421020 * L_8 = V_0;
		AdmobEventHandler_t2983421020 * L_9 = V_1;
		if ((!(((Il2CppObject*)(AdmobEventHandler_t2983421020 *)L_8) == ((Il2CppObject*)(AdmobEventHandler_t2983421020 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void admob.Admob::add_rewardedVideoEventHandler(admob.Admob/AdmobEventHandler)
extern Il2CppClass* AdmobEventHandler_t2983421020_il2cpp_TypeInfo_var;
extern const uint32_t Admob_add_rewardedVideoEventHandler_m698753172_MetadataUsageId;
extern "C"  void Admob_add_rewardedVideoEventHandler_m698753172 (Admob_t546240967 * __this, AdmobEventHandler_t2983421020 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Admob_add_rewardedVideoEventHandler_m698753172_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AdmobEventHandler_t2983421020 * V_0 = NULL;
	AdmobEventHandler_t2983421020 * V_1 = NULL;
	{
		AdmobEventHandler_t2983421020 * L_0 = __this->get_rewardedVideoEventHandler_2();
		V_0 = L_0;
	}

IL_0007:
	{
		AdmobEventHandler_t2983421020 * L_1 = V_0;
		V_1 = L_1;
		AdmobEventHandler_t2983421020 ** L_2 = __this->get_address_of_rewardedVideoEventHandler_2();
		AdmobEventHandler_t2983421020 * L_3 = V_1;
		AdmobEventHandler_t2983421020 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		AdmobEventHandler_t2983421020 * L_6 = V_0;
		AdmobEventHandler_t2983421020 * L_7 = InterlockedCompareExchangeImpl<AdmobEventHandler_t2983421020 *>(L_2, ((AdmobEventHandler_t2983421020 *)CastclassSealed(L_5, AdmobEventHandler_t2983421020_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		AdmobEventHandler_t2983421020 * L_8 = V_0;
		AdmobEventHandler_t2983421020 * L_9 = V_1;
		if ((!(((Il2CppObject*)(AdmobEventHandler_t2983421020 *)L_8) == ((Il2CppObject*)(AdmobEventHandler_t2983421020 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void admob.Admob::remove_rewardedVideoEventHandler(admob.Admob/AdmobEventHandler)
extern Il2CppClass* AdmobEventHandler_t2983421020_il2cpp_TypeInfo_var;
extern const uint32_t Admob_remove_rewardedVideoEventHandler_m2765341749_MetadataUsageId;
extern "C"  void Admob_remove_rewardedVideoEventHandler_m2765341749 (Admob_t546240967 * __this, AdmobEventHandler_t2983421020 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Admob_remove_rewardedVideoEventHandler_m2765341749_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AdmobEventHandler_t2983421020 * V_0 = NULL;
	AdmobEventHandler_t2983421020 * V_1 = NULL;
	{
		AdmobEventHandler_t2983421020 * L_0 = __this->get_rewardedVideoEventHandler_2();
		V_0 = L_0;
	}

IL_0007:
	{
		AdmobEventHandler_t2983421020 * L_1 = V_0;
		V_1 = L_1;
		AdmobEventHandler_t2983421020 ** L_2 = __this->get_address_of_rewardedVideoEventHandler_2();
		AdmobEventHandler_t2983421020 * L_3 = V_1;
		AdmobEventHandler_t2983421020 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		AdmobEventHandler_t2983421020 * L_6 = V_0;
		AdmobEventHandler_t2983421020 * L_7 = InterlockedCompareExchangeImpl<AdmobEventHandler_t2983421020 *>(L_2, ((AdmobEventHandler_t2983421020 *)CastclassSealed(L_5, AdmobEventHandler_t2983421020_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		AdmobEventHandler_t2983421020 * L_8 = V_0;
		AdmobEventHandler_t2983421020 * L_9 = V_1;
		if ((!(((Il2CppObject*)(AdmobEventHandler_t2983421020 *)L_8) == ((Il2CppObject*)(AdmobEventHandler_t2983421020 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void admob.Admob::add_nativeBannerEventHandler(admob.Admob/AdmobEventHandler)
extern Il2CppClass* AdmobEventHandler_t2983421020_il2cpp_TypeInfo_var;
extern const uint32_t Admob_add_nativeBannerEventHandler_m2314856020_MetadataUsageId;
extern "C"  void Admob_add_nativeBannerEventHandler_m2314856020 (Admob_t546240967 * __this, AdmobEventHandler_t2983421020 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Admob_add_nativeBannerEventHandler_m2314856020_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AdmobEventHandler_t2983421020 * V_0 = NULL;
	AdmobEventHandler_t2983421020 * V_1 = NULL;
	{
		AdmobEventHandler_t2983421020 * L_0 = __this->get_nativeBannerEventHandler_3();
		V_0 = L_0;
	}

IL_0007:
	{
		AdmobEventHandler_t2983421020 * L_1 = V_0;
		V_1 = L_1;
		AdmobEventHandler_t2983421020 ** L_2 = __this->get_address_of_nativeBannerEventHandler_3();
		AdmobEventHandler_t2983421020 * L_3 = V_1;
		AdmobEventHandler_t2983421020 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		AdmobEventHandler_t2983421020 * L_6 = V_0;
		AdmobEventHandler_t2983421020 * L_7 = InterlockedCompareExchangeImpl<AdmobEventHandler_t2983421020 *>(L_2, ((AdmobEventHandler_t2983421020 *)CastclassSealed(L_5, AdmobEventHandler_t2983421020_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		AdmobEventHandler_t2983421020 * L_8 = V_0;
		AdmobEventHandler_t2983421020 * L_9 = V_1;
		if ((!(((Il2CppObject*)(AdmobEventHandler_t2983421020 *)L_8) == ((Il2CppObject*)(AdmobEventHandler_t2983421020 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void admob.Admob::remove_nativeBannerEventHandler(admob.Admob/AdmobEventHandler)
extern Il2CppClass* AdmobEventHandler_t2983421020_il2cpp_TypeInfo_var;
extern const uint32_t Admob_remove_nativeBannerEventHandler_m4236789151_MetadataUsageId;
extern "C"  void Admob_remove_nativeBannerEventHandler_m4236789151 (Admob_t546240967 * __this, AdmobEventHandler_t2983421020 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Admob_remove_nativeBannerEventHandler_m4236789151_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AdmobEventHandler_t2983421020 * V_0 = NULL;
	AdmobEventHandler_t2983421020 * V_1 = NULL;
	{
		AdmobEventHandler_t2983421020 * L_0 = __this->get_nativeBannerEventHandler_3();
		V_0 = L_0;
	}

IL_0007:
	{
		AdmobEventHandler_t2983421020 * L_1 = V_0;
		V_1 = L_1;
		AdmobEventHandler_t2983421020 ** L_2 = __this->get_address_of_nativeBannerEventHandler_3();
		AdmobEventHandler_t2983421020 * L_3 = V_1;
		AdmobEventHandler_t2983421020 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		AdmobEventHandler_t2983421020 * L_6 = V_0;
		AdmobEventHandler_t2983421020 * L_7 = InterlockedCompareExchangeImpl<AdmobEventHandler_t2983421020 *>(L_2, ((AdmobEventHandler_t2983421020 *)CastclassSealed(L_5, AdmobEventHandler_t2983421020_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		AdmobEventHandler_t2983421020 * L_8 = V_0;
		AdmobEventHandler_t2983421020 * L_9 = V_1;
		if ((!(((Il2CppObject*)(AdmobEventHandler_t2983421020 *)L_8) == ((Il2CppObject*)(AdmobEventHandler_t2983421020 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// admob.Admob admob.Admob::Instance()
extern Il2CppClass* Admob_t546240967_il2cpp_TypeInfo_var;
extern const uint32_t Admob_Instance_m352023099_MetadataUsageId;
extern "C"  Admob_t546240967 * Admob_Instance_m352023099 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Admob_Instance_m352023099_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Admob_t546240967 * L_0 = ((Admob_t546240967_StaticFields*)Admob_t546240967_il2cpp_TypeInfo_var->static_fields)->get__instance_4();
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		Admob_t546240967 * L_1 = (Admob_t546240967 *)il2cpp_codegen_object_new(Admob_t546240967_il2cpp_TypeInfo_var);
		Admob__ctor_m3904493015(L_1, /*hidden argument*/NULL);
		((Admob_t546240967_StaticFields*)Admob_t546240967_il2cpp_TypeInfo_var->static_fields)->set__instance_4(L_1);
		Admob_t546240967 * L_2 = ((Admob_t546240967_StaticFields*)Admob_t546240967_il2cpp_TypeInfo_var->static_fields)->get__instance_4();
		NullCheck(L_2);
		Admob_preInitAdmob_m392180999(L_2, /*hidden argument*/NULL);
	}

IL_001e:
	{
		Admob_t546240967 * L_3 = ((Admob_t546240967_StaticFields*)Admob_t546240967_il2cpp_TypeInfo_var->static_fields)->get__instance_4();
		return L_3;
	}
}
// System.Void admob.Admob::preInitAdmob()
extern "C"  void Admob_preInitAdmob_m392180999 (Admob_t546240967 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
extern "C" void DEFAULT_CALL _kminitAdmob(char*, char*, Il2CppMethodPointer);
// System.Void admob.Admob::_kminitAdmob(System.String,System.String,admob.Admob/AdmobAdCallBack)
extern "C"  void Admob__kminitAdmob_m3213489909 (Il2CppObject * __this /* static, unused */, String_t* ___bannerid0, String_t* ___fullid1, AdmobAdCallBack_t390902866 * ___callback2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*, Il2CppMethodPointer);

	// Marshaling of parameter '___bannerid0' to native representation
	char* ____bannerid0_marshaled = NULL;
	____bannerid0_marshaled = il2cpp_codegen_marshal_string(___bannerid0);

	// Marshaling of parameter '___fullid1' to native representation
	char* ____fullid1_marshaled = NULL;
	____fullid1_marshaled = il2cpp_codegen_marshal_string(___fullid1);

	// Marshaling of parameter '___callback2' to native representation
	Il2CppMethodPointer ____callback2_marshaled = NULL;
	____callback2_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback2));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_kminitAdmob)(____bannerid0_marshaled, ____fullid1_marshaled, ____callback2_marshaled);

	// Marshaling cleanup of parameter '___bannerid0' native representation
	il2cpp_codegen_marshal_free(____bannerid0_marshaled);
	____bannerid0_marshaled = NULL;

	// Marshaling cleanup of parameter '___fullid1' native representation
	il2cpp_codegen_marshal_free(____fullid1_marshaled);
	____fullid1_marshaled = NULL;

}
// System.Void admob.Admob::initAdmob(System.String,System.String)
extern Il2CppClass* Admob_t546240967_il2cpp_TypeInfo_var;
extern Il2CppClass* AdmobAdCallBack_t390902866_il2cpp_TypeInfo_var;
extern const MethodInfo* Admob_onAdmobEventCallBack_m1864886544_MethodInfo_var;
extern const uint32_t Admob_initAdmob_m1941857206_MetadataUsageId;
extern "C"  void Admob_initAdmob_m1941857206 (Admob_t546240967 * __this, String_t* ___bannerID0, String_t* ___fullID1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Admob_initAdmob_m1941857206_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B2_1 = NULL;
	String_t* G_B1_0 = NULL;
	String_t* G_B1_1 = NULL;
	{
		String_t* L_0 = ___bannerID0;
		String_t* L_1 = ___fullID1;
		AdmobAdCallBack_t390902866 * L_2 = ((Admob_t546240967_StaticFields*)Admob_t546240967_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache0_5();
		G_B1_0 = L_1;
		G_B1_1 = L_0;
		if (L_2)
		{
			G_B2_0 = L_1;
			G_B2_1 = L_0;
			goto IL_001a;
		}
	}
	{
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)Admob_onAdmobEventCallBack_m1864886544_MethodInfo_var);
		AdmobAdCallBack_t390902866 * L_4 = (AdmobAdCallBack_t390902866 *)il2cpp_codegen_object_new(AdmobAdCallBack_t390902866_il2cpp_TypeInfo_var);
		AdmobAdCallBack__ctor_m2242352453(L_4, NULL, L_3, /*hidden argument*/NULL);
		((Admob_t546240967_StaticFields*)Admob_t546240967_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache0_5(L_4);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
	}

IL_001a:
	{
		AdmobAdCallBack_t390902866 * L_5 = ((Admob_t546240967_StaticFields*)Admob_t546240967_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache0_5();
		Admob__kminitAdmob_m3213489909(NULL /*static, unused*/, G_B2_1, G_B2_0, L_5, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL _kmshowNativeBannerAbsolute(int32_t, int32_t, int32_t, int32_t, char*, char*);
// System.Void admob.Admob::_kmshowNativeBannerAbsolute(System.Int32,System.Int32,System.Int32,System.Int32,System.String,System.String)
extern "C"  void Admob__kmshowNativeBannerAbsolute_m3098491723 (Il2CppObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, int32_t ___x2, int32_t ___y3, String_t* ___nativeID4, String_t* ___instanceName5, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t, int32_t, int32_t, char*, char*);

	// Marshaling of parameter '___nativeID4' to native representation
	char* ____nativeID4_marshaled = NULL;
	____nativeID4_marshaled = il2cpp_codegen_marshal_string(___nativeID4);

	// Marshaling of parameter '___instanceName5' to native representation
	char* ____instanceName5_marshaled = NULL;
	____instanceName5_marshaled = il2cpp_codegen_marshal_string(___instanceName5);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_kmshowNativeBannerAbsolute)(___width0, ___height1, ___x2, ___y3, ____nativeID4_marshaled, ____instanceName5_marshaled);

	// Marshaling cleanup of parameter '___nativeID4' native representation
	il2cpp_codegen_marshal_free(____nativeID4_marshaled);
	____nativeID4_marshaled = NULL;

	// Marshaling cleanup of parameter '___instanceName5' native representation
	il2cpp_codegen_marshal_free(____instanceName5_marshaled);
	____instanceName5_marshaled = NULL;

}
// System.Void admob.Admob::showNativeBannerAbsolute(admob.AdSize,System.Int32,System.Int32,System.String,System.String)
extern "C"  void Admob_showNativeBannerAbsolute_m2200024897 (Admob_t546240967 * __this, AdSize_t3770813302 * ___size0, int32_t ___x1, int32_t ___y2, String_t* ___nativeID3, String_t* ___instanceName4, const MethodInfo* method)
{
	{
		AdSize_t3770813302 * L_0 = ___size0;
		NullCheck(L_0);
		int32_t L_1 = AdSize_get_Width_m317946431(L_0, /*hidden argument*/NULL);
		AdSize_t3770813302 * L_2 = ___size0;
		NullCheck(L_2);
		int32_t L_3 = AdSize_get_Height_m955561220(L_2, /*hidden argument*/NULL);
		int32_t L_4 = ___x1;
		int32_t L_5 = ___y2;
		String_t* L_6 = ___nativeID3;
		String_t* L_7 = ___instanceName4;
		Admob__kmshowNativeBannerAbsolute_m3098491723(NULL /*static, unused*/, L_1, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL _kmshowNativeBannerRelative(int32_t, int32_t, int32_t, int32_t, char*, char*);
// System.Void admob.Admob::_kmshowNativeBannerRelative(System.Int32,System.Int32,System.Int32,System.Int32,System.String,System.String)
extern "C"  void Admob__kmshowNativeBannerRelative_m3881486200 (Il2CppObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, int32_t ___position2, int32_t ___marginY3, String_t* ___nativeID4, String_t* ___instanceName5, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t, int32_t, int32_t, char*, char*);

	// Marshaling of parameter '___nativeID4' to native representation
	char* ____nativeID4_marshaled = NULL;
	____nativeID4_marshaled = il2cpp_codegen_marshal_string(___nativeID4);

	// Marshaling of parameter '___instanceName5' to native representation
	char* ____instanceName5_marshaled = NULL;
	____instanceName5_marshaled = il2cpp_codegen_marshal_string(___instanceName5);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_kmshowNativeBannerRelative)(___width0, ___height1, ___position2, ___marginY3, ____nativeID4_marshaled, ____instanceName5_marshaled);

	// Marshaling cleanup of parameter '___nativeID4' native representation
	il2cpp_codegen_marshal_free(____nativeID4_marshaled);
	____nativeID4_marshaled = NULL;

	// Marshaling cleanup of parameter '___instanceName5' native representation
	il2cpp_codegen_marshal_free(____instanceName5_marshaled);
	____instanceName5_marshaled = NULL;

}
// System.Void admob.Admob::showNativeBannerRelative(admob.AdSize,System.Int32,System.Int32,System.String,System.String)
extern "C"  void Admob_showNativeBannerRelative_m4052082498 (Admob_t546240967 * __this, AdSize_t3770813302 * ___size0, int32_t ___position1, int32_t ___marginY2, String_t* ___nativeID3, String_t* ___instanceName4, const MethodInfo* method)
{
	{
		AdSize_t3770813302 * L_0 = ___size0;
		NullCheck(L_0);
		int32_t L_1 = AdSize_get_Width_m317946431(L_0, /*hidden argument*/NULL);
		AdSize_t3770813302 * L_2 = ___size0;
		NullCheck(L_2);
		int32_t L_3 = AdSize_get_Height_m955561220(L_2, /*hidden argument*/NULL);
		int32_t L_4 = ___position1;
		int32_t L_5 = ___marginY2;
		String_t* L_6 = ___nativeID3;
		String_t* L_7 = ___instanceName4;
		Admob__kmshowNativeBannerRelative_m3881486200(NULL /*static, unused*/, L_1, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL _kmremoveNativeBanner(char*);
// System.Void admob.Admob::_kmremoveNativeBanner(System.String)
extern "C"  void Admob__kmremoveNativeBanner_m3510797403 (Il2CppObject * __this /* static, unused */, String_t* ___instanceName0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___instanceName0' to native representation
	char* ____instanceName0_marshaled = NULL;
	____instanceName0_marshaled = il2cpp_codegen_marshal_string(___instanceName0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_kmremoveNativeBanner)(____instanceName0_marshaled);

	// Marshaling cleanup of parameter '___instanceName0' native representation
	il2cpp_codegen_marshal_free(____instanceName0_marshaled);
	____instanceName0_marshaled = NULL;

}
// System.Void admob.Admob::removeNativeBanner(System.String)
extern "C"  void Admob_removeNativeBanner_m348786124 (Admob_t546240967 * __this, String_t* ___instanceName0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___instanceName0;
		Admob__kmremoveNativeBanner_m3510797403(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL _kmshowBannerAbsolute(int32_t, int32_t, int32_t, int32_t, char*);
// System.Void admob.Admob::_kmshowBannerAbsolute(System.Int32,System.Int32,System.Int32,System.Int32,System.String)
extern "C"  void Admob__kmshowBannerAbsolute_m3495462606 (Il2CppObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, int32_t ___x2, int32_t ___y3, String_t* ___instanceName4, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t, int32_t, int32_t, char*);

	// Marshaling of parameter '___instanceName4' to native representation
	char* ____instanceName4_marshaled = NULL;
	____instanceName4_marshaled = il2cpp_codegen_marshal_string(___instanceName4);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_kmshowBannerAbsolute)(___width0, ___height1, ___x2, ___y3, ____instanceName4_marshaled);

	// Marshaling cleanup of parameter '___instanceName4' native representation
	il2cpp_codegen_marshal_free(____instanceName4_marshaled);
	____instanceName4_marshaled = NULL;

}
// System.Void admob.Admob::showBannerAbsolute(admob.AdSize,System.Int32,System.Int32,System.String)
extern "C"  void Admob_showBannerAbsolute_m450906656 (Admob_t546240967 * __this, AdSize_t3770813302 * ___size0, int32_t ___x1, int32_t ___y2, String_t* ___instanceName3, const MethodInfo* method)
{
	{
		AdSize_t3770813302 * L_0 = ___size0;
		NullCheck(L_0);
		int32_t L_1 = AdSize_get_Width_m317946431(L_0, /*hidden argument*/NULL);
		AdSize_t3770813302 * L_2 = ___size0;
		NullCheck(L_2);
		int32_t L_3 = AdSize_get_Height_m955561220(L_2, /*hidden argument*/NULL);
		int32_t L_4 = ___x1;
		int32_t L_5 = ___y2;
		String_t* L_6 = ___instanceName3;
		Admob__kmshowBannerAbsolute_m3495462606(NULL /*static, unused*/, L_1, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL _kmshowBannerRelative(int32_t, int32_t, int32_t, int32_t, char*);
// System.Void admob.Admob::_kmshowBannerRelative(System.Int32,System.Int32,System.Int32,System.Int32,System.String)
extern "C"  void Admob__kmshowBannerRelative_m1922121111 (Il2CppObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, int32_t ___position2, int32_t ___marginY3, String_t* ___instanceName4, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t, int32_t, int32_t, char*);

	// Marshaling of parameter '___instanceName4' to native representation
	char* ____instanceName4_marshaled = NULL;
	____instanceName4_marshaled = il2cpp_codegen_marshal_string(___instanceName4);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_kmshowBannerRelative)(___width0, ___height1, ___position2, ___marginY3, ____instanceName4_marshaled);

	// Marshaling cleanup of parameter '___instanceName4' native representation
	il2cpp_codegen_marshal_free(____instanceName4_marshaled);
	____instanceName4_marshaled = NULL;

}
// System.Void admob.Admob::showBannerRelative(admob.AdSize,System.Int32,System.Int32,System.String)
extern "C"  void Admob_showBannerRelative_m3640333681 (Admob_t546240967 * __this, AdSize_t3770813302 * ___size0, int32_t ___position1, int32_t ___marginY2, String_t* ___instanceName3, const MethodInfo* method)
{
	{
		AdSize_t3770813302 * L_0 = ___size0;
		NullCheck(L_0);
		int32_t L_1 = AdSize_get_Width_m317946431(L_0, /*hidden argument*/NULL);
		AdSize_t3770813302 * L_2 = ___size0;
		NullCheck(L_2);
		int32_t L_3 = AdSize_get_Height_m955561220(L_2, /*hidden argument*/NULL);
		int32_t L_4 = ___position1;
		int32_t L_5 = ___marginY2;
		String_t* L_6 = ___instanceName3;
		Admob__kmshowBannerRelative_m1922121111(NULL /*static, unused*/, L_1, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL _kmremoveBanner(char*);
// System.Void admob.Admob::_kmremoveBanner(System.String)
extern "C"  void Admob__kmremoveBanner_m3583702054 (Il2CppObject * __this /* static, unused */, String_t* ___instanceName0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___instanceName0' to native representation
	char* ____instanceName0_marshaled = NULL;
	____instanceName0_marshaled = il2cpp_codegen_marshal_string(___instanceName0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_kmremoveBanner)(____instanceName0_marshaled);

	// Marshaling cleanup of parameter '___instanceName0' native representation
	il2cpp_codegen_marshal_free(____instanceName0_marshaled);
	____instanceName0_marshaled = NULL;

}
// System.Void admob.Admob::removeBanner(System.String)
extern "C"  void Admob_removeBanner_m2206939851 (Admob_t546240967 * __this, String_t* ___instanceName0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___instanceName0;
		Admob__kmremoveBanner_m3583702054(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL _kmloadInterstitial();
// System.Void admob.Admob::_kmloadInterstitial()
extern "C"  void Admob__kmloadInterstitial_m2535838764 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_kmloadInterstitial)();

}
// System.Void admob.Admob::loadInterstitial()
extern "C"  void Admob_loadInterstitial_m2602394663 (Admob_t546240967 * __this, const MethodInfo* method)
{
	{
		Admob__kmloadInterstitial_m2535838764(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
extern "C" int32_t DEFAULT_CALL _kmisInterstitialReady();
// System.Boolean admob.Admob::_kmisInterstitialReady()
extern "C"  bool Admob__kmisInterstitialReady_m3914756345 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(_kmisInterstitialReady)();

	return static_cast<bool>(returnValue);
}
// System.Boolean admob.Admob::isInterstitialReady()
extern "C"  bool Admob_isInterstitialReady_m3353041644 (Admob_t546240967 * __this, const MethodInfo* method)
{
	{
		bool L_0 = Admob__kmisInterstitialReady_m3914756345(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C" void DEFAULT_CALL _kmshowInterstitial();
// System.Void admob.Admob::_kmshowInterstitial()
extern "C"  void Admob__kmshowInterstitial_m1396908119 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_kmshowInterstitial)();

}
// System.Void admob.Admob::showInterstitial()
extern "C"  void Admob_showInterstitial_m2032088296 (Admob_t546240967 * __this, const MethodInfo* method)
{
	{
		Admob__kmshowInterstitial_m1396908119(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL _kmloadRewardedVideo(char*);
// System.Void admob.Admob::_kmloadRewardedVideo(System.String)
extern "C"  void Admob__kmloadRewardedVideo_m1533307933 (Il2CppObject * __this /* static, unused */, String_t* ___rewardedVideoID0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___rewardedVideoID0' to native representation
	char* ____rewardedVideoID0_marshaled = NULL;
	____rewardedVideoID0_marshaled = il2cpp_codegen_marshal_string(___rewardedVideoID0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_kmloadRewardedVideo)(____rewardedVideoID0_marshaled);

	// Marshaling cleanup of parameter '___rewardedVideoID0' native representation
	il2cpp_codegen_marshal_free(____rewardedVideoID0_marshaled);
	____rewardedVideoID0_marshaled = NULL;

}
// System.Void admob.Admob::loadRewardedVideo(System.String)
extern "C"  void Admob_loadRewardedVideo_m4191770420 (Admob_t546240967 * __this, String_t* ___rewardedVideoID0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___rewardedVideoID0;
		Admob__kmloadRewardedVideo_m1533307933(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
extern "C" int32_t DEFAULT_CALL _kmisRewardedVideoReady();
// System.Boolean admob.Admob::_kmisRewardedVideoReady()
extern "C"  bool Admob__kmisRewardedVideoReady_m3707665492 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(_kmisRewardedVideoReady)();

	return static_cast<bool>(returnValue);
}
// System.Boolean admob.Admob::isRewardedVideoReady()
extern "C"  bool Admob_isRewardedVideoReady_m89423453 (Admob_t546240967 * __this, const MethodInfo* method)
{
	{
		bool L_0 = Admob__kmisRewardedVideoReady_m3707665492(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C" void DEFAULT_CALL _kmshowRewardedVideo();
// System.Void admob.Admob::_kmshowRewardedVideo()
extern "C"  void Admob__kmshowRewardedVideo_m3329502270 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_kmshowRewardedVideo)();

}
// System.Void admob.Admob::showRewardedVideo()
extern "C"  void Admob_showRewardedVideo_m908495731 (Admob_t546240967 * __this, const MethodInfo* method)
{
	{
		Admob__kmshowRewardedVideo_m3329502270(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL _kmsetTesting(int32_t);
// System.Void admob.Admob::_kmsetTesting(System.Boolean)
extern "C"  void Admob__kmsetTesting_m830384613 (Il2CppObject * __this /* static, unused */, bool ___v0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_kmsetTesting)(static_cast<int32_t>(___v0));

}
// System.Void admob.Admob::setTesting(System.Boolean)
extern "C"  void Admob_setTesting_m4242041198 (Admob_t546240967 * __this, bool ___v0, const MethodInfo* method)
{
	{
		bool L_0 = ___v0;
		Admob__kmsetTesting_m830384613(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL _kmsetForChildren(int32_t);
// System.Void admob.Admob::_kmsetForChildren(System.Boolean)
extern "C"  void Admob__kmsetForChildren_m3748844775 (Il2CppObject * __this /* static, unused */, bool ___v0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_kmsetForChildren)(static_cast<int32_t>(___v0));

}
// System.Void admob.Admob::setForChildren(System.Boolean)
extern "C"  void Admob_setForChildren_m3468175206 (Admob_t546240967 * __this, bool ___v0, const MethodInfo* method)
{
	{
		bool L_0 = ___v0;
		Admob__kmsetForChildren_m3748844775(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void admob.Admob::onAdmobEventCallBack(System.String,System.String,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1222408196;
extern Il2CppCodeGenString* _stringLiteral3011292000;
extern Il2CppCodeGenString* _stringLiteral858169867;
extern Il2CppCodeGenString* _stringLiteral4099895337;
extern const uint32_t Admob_onAdmobEventCallBack_m1864886544_MetadataUsageId;
extern "C"  void Admob_onAdmobEventCallBack_m1864886544 (Il2CppObject * __this /* static, unused */, String_t* ___adtype0, String_t* ___eventName1, String_t* ___msg2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Admob_onAdmobEventCallBack_m1864886544_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___adtype0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_0, _stringLiteral1222408196, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0035;
		}
	}
	{
		Admob_t546240967 * L_2 = Admob_Instance_m352023099(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		AdmobEventHandler_t2983421020 * L_3 = L_2->get_bannerEventHandler_0();
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		Admob_t546240967 * L_4 = Admob_Instance_m352023099(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		AdmobEventHandler_t2983421020 * L_5 = L_4->get_bannerEventHandler_0();
		String_t* L_6 = ___eventName1;
		String_t* L_7 = ___msg2;
		NullCheck(L_5);
		AdmobEventHandler_Invoke_m1278357557(L_5, L_6, L_7, /*hidden argument*/NULL);
	}

IL_0030:
	{
		goto IL_00cf;
	}

IL_0035:
	{
		String_t* L_8 = ___adtype0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_8, _stringLiteral3011292000, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_006a;
		}
	}
	{
		Admob_t546240967 * L_10 = Admob_Instance_m352023099(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		AdmobEventHandler_t2983421020 * L_11 = L_10->get_interstitialEventHandler_1();
		if (!L_11)
		{
			goto IL_0065;
		}
	}
	{
		Admob_t546240967 * L_12 = Admob_Instance_m352023099(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		AdmobEventHandler_t2983421020 * L_13 = L_12->get_interstitialEventHandler_1();
		String_t* L_14 = ___eventName1;
		String_t* L_15 = ___msg2;
		NullCheck(L_13);
		AdmobEventHandler_Invoke_m1278357557(L_13, L_14, L_15, /*hidden argument*/NULL);
	}

IL_0065:
	{
		goto IL_00cf;
	}

IL_006a:
	{
		String_t* L_16 = ___adtype0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_17 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_16, _stringLiteral858169867, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_009f;
		}
	}
	{
		Admob_t546240967 * L_18 = Admob_Instance_m352023099(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_18);
		AdmobEventHandler_t2983421020 * L_19 = L_18->get_rewardedVideoEventHandler_2();
		if (!L_19)
		{
			goto IL_009a;
		}
	}
	{
		Admob_t546240967 * L_20 = Admob_Instance_m352023099(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_20);
		AdmobEventHandler_t2983421020 * L_21 = L_20->get_rewardedVideoEventHandler_2();
		String_t* L_22 = ___eventName1;
		String_t* L_23 = ___msg2;
		NullCheck(L_21);
		AdmobEventHandler_Invoke_m1278357557(L_21, L_22, L_23, /*hidden argument*/NULL);
	}

IL_009a:
	{
		goto IL_00cf;
	}

IL_009f:
	{
		String_t* L_24 = ___adtype0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_25 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_24, _stringLiteral4099895337, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00cf;
		}
	}
	{
		Admob_t546240967 * L_26 = Admob_Instance_m352023099(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_26);
		AdmobEventHandler_t2983421020 * L_27 = L_26->get_nativeBannerEventHandler_3();
		if (!L_27)
		{
			goto IL_00cf;
		}
	}
	{
		Admob_t546240967 * L_28 = Admob_Instance_m352023099(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_28);
		AdmobEventHandler_t2983421020 * L_29 = L_28->get_nativeBannerEventHandler_3();
		String_t* L_30 = ___eventName1;
		String_t* L_31 = ___msg2;
		NullCheck(L_29);
		AdmobEventHandler_Invoke_m1278357557(L_29, L_30, L_31, /*hidden argument*/NULL);
	}

IL_00cf:
	{
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_Admob_onAdmobEventCallBack_m1864886544(char* ___adtype0, char* ___eventName1, char* ___msg2)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___adtype0' to managed representation
	String_t* ____adtype0_unmarshaled = NULL;
	____adtype0_unmarshaled = il2cpp_codegen_marshal_string_result(___adtype0);

	// Marshaling of parameter '___eventName1' to managed representation
	String_t* ____eventName1_unmarshaled = NULL;
	____eventName1_unmarshaled = il2cpp_codegen_marshal_string_result(___eventName1);

	// Marshaling of parameter '___msg2' to managed representation
	String_t* ____msg2_unmarshaled = NULL;
	____msg2_unmarshaled = il2cpp_codegen_marshal_string_result(___msg2);

	// Managed method invocation
	::Admob_onAdmobEventCallBack_m1864886544(NULL, ____adtype0_unmarshaled, ____eventName1_unmarshaled, ____msg2_unmarshaled, NULL);

}
// System.Void admob.Admob/AdmobAdCallBack::.ctor(System.Object,System.IntPtr)
extern "C"  void AdmobAdCallBack__ctor_m2242352453 (AdmobAdCallBack_t390902866 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void admob.Admob/AdmobAdCallBack::Invoke(System.String,System.String,System.String)
extern "C"  void AdmobAdCallBack_Invoke_m1861044811 (AdmobAdCallBack_t390902866 * __this, String_t* ___adtype0, String_t* ___eventName1, String_t* ___msg2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		AdmobAdCallBack_Invoke_m1861044811((AdmobAdCallBack_t390902866 *)__this->get_prev_9(),___adtype0, ___eventName1, ___msg2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___adtype0, String_t* ___eventName1, String_t* ___msg2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___adtype0, ___eventName1, ___msg2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___adtype0, String_t* ___eventName1, String_t* ___msg2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___adtype0, ___eventName1, ___msg2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___eventName1, String_t* ___msg2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___adtype0, ___eventName1, ___msg2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_AdmobAdCallBack_t390902866 (AdmobAdCallBack_t390902866 * __this, String_t* ___adtype0, String_t* ___eventName1, String_t* ___msg2, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*, char*, char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___adtype0' to native representation
	char* ____adtype0_marshaled = NULL;
	____adtype0_marshaled = il2cpp_codegen_marshal_string(___adtype0);

	// Marshaling of parameter '___eventName1' to native representation
	char* ____eventName1_marshaled = NULL;
	____eventName1_marshaled = il2cpp_codegen_marshal_string(___eventName1);

	// Marshaling of parameter '___msg2' to native representation
	char* ____msg2_marshaled = NULL;
	____msg2_marshaled = il2cpp_codegen_marshal_string(___msg2);

	// Native function invocation
	il2cppPInvokeFunc(____adtype0_marshaled, ____eventName1_marshaled, ____msg2_marshaled);

	// Marshaling cleanup of parameter '___adtype0' native representation
	il2cpp_codegen_marshal_free(____adtype0_marshaled);
	____adtype0_marshaled = NULL;

	// Marshaling cleanup of parameter '___eventName1' native representation
	il2cpp_codegen_marshal_free(____eventName1_marshaled);
	____eventName1_marshaled = NULL;

	// Marshaling cleanup of parameter '___msg2' native representation
	il2cpp_codegen_marshal_free(____msg2_marshaled);
	____msg2_marshaled = NULL;

}
// System.IAsyncResult admob.Admob/AdmobAdCallBack::BeginInvoke(System.String,System.String,System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * AdmobAdCallBack_BeginInvoke_m1105238270 (AdmobAdCallBack_t390902866 * __this, String_t* ___adtype0, String_t* ___eventName1, String_t* ___msg2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	void *__d_args[4] = {0};
	__d_args[0] = ___adtype0;
	__d_args[1] = ___eventName1;
	__d_args[2] = ___msg2;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void admob.Admob/AdmobAdCallBack::EndInvoke(System.IAsyncResult)
extern "C"  void AdmobAdCallBack_EndInvoke_m928280963 (AdmobAdCallBack_t390902866 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void admob.Admob/AdmobEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void AdmobEventHandler__ctor_m943312861 (AdmobEventHandler_t2983421020 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void admob.Admob/AdmobEventHandler::Invoke(System.String,System.String)
extern "C"  void AdmobEventHandler_Invoke_m1278357557 (AdmobEventHandler_t2983421020 * __this, String_t* ___eventName0, String_t* ___msg1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		AdmobEventHandler_Invoke_m1278357557((AdmobEventHandler_t2983421020 *)__this->get_prev_9(),___eventName0, ___msg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___eventName0, String_t* ___msg1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___eventName0, ___msg1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___eventName0, String_t* ___msg1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___eventName0, ___msg1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___msg1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___eventName0, ___msg1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_AdmobEventHandler_t2983421020 (AdmobEventHandler_t2983421020 * __this, String_t* ___eventName0, String_t* ___msg1, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*, char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___eventName0' to native representation
	char* ____eventName0_marshaled = NULL;
	____eventName0_marshaled = il2cpp_codegen_marshal_string(___eventName0);

	// Marshaling of parameter '___msg1' to native representation
	char* ____msg1_marshaled = NULL;
	____msg1_marshaled = il2cpp_codegen_marshal_string(___msg1);

	// Native function invocation
	il2cppPInvokeFunc(____eventName0_marshaled, ____msg1_marshaled);

	// Marshaling cleanup of parameter '___eventName0' native representation
	il2cpp_codegen_marshal_free(____eventName0_marshaled);
	____eventName0_marshaled = NULL;

	// Marshaling cleanup of parameter '___msg1' native representation
	il2cpp_codegen_marshal_free(____msg1_marshaled);
	____msg1_marshaled = NULL;

}
// System.IAsyncResult admob.Admob/AdmobEventHandler::BeginInvoke(System.String,System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * AdmobEventHandler_BeginInvoke_m880931180 (AdmobEventHandler_t2983421020 * __this, String_t* ___eventName0, String_t* ___msg1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___eventName0;
	__d_args[1] = ___msg1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void admob.Admob/AdmobEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void AdmobEventHandler_EndInvoke_m3142124079 (AdmobEventHandler_t2983421020 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void admob.AdmobEvent::.ctor()
extern "C"  void AdmobEvent__ctor_m2257084687 (AdmobEvent_t4123996035 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void admob.AdmobEvent::.cctor()
extern Il2CppClass* AdmobEvent_t4123996035_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3012284099;
extern Il2CppCodeGenString* _stringLiteral3432291362;
extern Il2CppCodeGenString* _stringLiteral344657805;
extern Il2CppCodeGenString* _stringLiteral791142518;
extern Il2CppCodeGenString* _stringLiteral4016041990;
extern Il2CppCodeGenString* _stringLiteral2433883841;
extern Il2CppCodeGenString* _stringLiteral1454528267;
extern Il2CppCodeGenString* _stringLiteral2871947203;
extern const uint32_t AdmobEvent__cctor_m2614563490_MetadataUsageId;
extern "C"  void AdmobEvent__cctor_m2614563490 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AdmobEvent__cctor_m2614563490_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((AdmobEvent_t4123996035_StaticFields*)AdmobEvent_t4123996035_il2cpp_TypeInfo_var->static_fields)->set_onAdLoaded_0(_stringLiteral3012284099);
		((AdmobEvent_t4123996035_StaticFields*)AdmobEvent_t4123996035_il2cpp_TypeInfo_var->static_fields)->set_onAdFailedToLoad_1(_stringLiteral3432291362);
		((AdmobEvent_t4123996035_StaticFields*)AdmobEvent_t4123996035_il2cpp_TypeInfo_var->static_fields)->set_onAdOpened_2(_stringLiteral344657805);
		((AdmobEvent_t4123996035_StaticFields*)AdmobEvent_t4123996035_il2cpp_TypeInfo_var->static_fields)->set_adViewWillDismissScreen_3(_stringLiteral791142518);
		((AdmobEvent_t4123996035_StaticFields*)AdmobEvent_t4123996035_il2cpp_TypeInfo_var->static_fields)->set_onAdClosed_4(_stringLiteral4016041990);
		((AdmobEvent_t4123996035_StaticFields*)AdmobEvent_t4123996035_il2cpp_TypeInfo_var->static_fields)->set_onAdLeftApplication_5(_stringLiteral2433883841);
		((AdmobEvent_t4123996035_StaticFields*)AdmobEvent_t4123996035_il2cpp_TypeInfo_var->static_fields)->set_onRewardedVideoStarted_6(_stringLiteral1454528267);
		((AdmobEvent_t4123996035_StaticFields*)AdmobEvent_t4123996035_il2cpp_TypeInfo_var->static_fields)->set_onRewarded_7(_stringLiteral2871947203);
		return;
	}
}
// System.Void admob.AdPosition::.ctor()
extern "C"  void AdPosition__ctor_m417448268 (AdPosition_t1947228246 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void admob.AdPosition::.cctor()
extern Il2CppClass* AdPosition_t1947228246_il2cpp_TypeInfo_var;
extern const uint32_t AdPosition__cctor_m196470457_MetadataUsageId;
extern "C"  void AdPosition__cctor_m196470457 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AdPosition__cctor_m196470457_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((AdPosition_t1947228246_StaticFields*)AdPosition_t1947228246_il2cpp_TypeInfo_var->static_fields)->set_TOP_LEFT_1(1);
		((AdPosition_t1947228246_StaticFields*)AdPosition_t1947228246_il2cpp_TypeInfo_var->static_fields)->set_TOP_CENTER_2(2);
		((AdPosition_t1947228246_StaticFields*)AdPosition_t1947228246_il2cpp_TypeInfo_var->static_fields)->set_TOP_RIGHT_3(3);
		((AdPosition_t1947228246_StaticFields*)AdPosition_t1947228246_il2cpp_TypeInfo_var->static_fields)->set_MIDDLE_LEFT_4(4);
		((AdPosition_t1947228246_StaticFields*)AdPosition_t1947228246_il2cpp_TypeInfo_var->static_fields)->set_MIDDLE_CENTER_5(5);
		((AdPosition_t1947228246_StaticFields*)AdPosition_t1947228246_il2cpp_TypeInfo_var->static_fields)->set_MIDDLE_RIGHT_6(6);
		((AdPosition_t1947228246_StaticFields*)AdPosition_t1947228246_il2cpp_TypeInfo_var->static_fields)->set_BOTTOM_LEFT_7(7);
		((AdPosition_t1947228246_StaticFields*)AdPosition_t1947228246_il2cpp_TypeInfo_var->static_fields)->set_BOTTOM_CENTER_8(8);
		((AdPosition_t1947228246_StaticFields*)AdPosition_t1947228246_il2cpp_TypeInfo_var->static_fields)->set_BOTTOM_RIGHT_9(((int32_t)9));
		return;
	}
}
// System.Void admob.AdSize::.ctor(System.Int32,System.Int32)
extern "C"  void AdSize__ctor_m3068073238 (AdSize_t3770813302 * __this, int32_t ___width0, int32_t ___height1, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___width0;
		__this->set_width_0(L_0);
		int32_t L_1 = ___height1;
		__this->set_height_1(L_1);
		return;
	}
}
// System.Int32 admob.AdSize::get_Width()
extern "C"  int32_t AdSize_get_Width_m317946431 (AdSize_t3770813302 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_width_0();
		return L_0;
	}
}
// System.Int32 admob.AdSize::get_Height()
extern "C"  int32_t AdSize_get_Height_m955561220 (AdSize_t3770813302 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_height_1();
		return L_0;
	}
}
// System.Void admob.AdSize::.cctor()
extern Il2CppClass* AdSize_t3770813302_il2cpp_TypeInfo_var;
extern const uint32_t AdSize__cctor_m3201987233_MetadataUsageId;
extern "C"  void AdSize__cctor_m3201987233 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AdSize__cctor_m3201987233_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AdSize_t3770813302 * L_0 = (AdSize_t3770813302 *)il2cpp_codegen_object_new(AdSize_t3770813302_il2cpp_TypeInfo_var);
		AdSize__ctor_m3068073238(L_0, ((int32_t)320), ((int32_t)50), /*hidden argument*/NULL);
		((AdSize_t3770813302_StaticFields*)AdSize_t3770813302_il2cpp_TypeInfo_var->static_fields)->set_Banner_2(L_0);
		AdSize_t3770813302 * L_1 = (AdSize_t3770813302 *)il2cpp_codegen_object_new(AdSize_t3770813302_il2cpp_TypeInfo_var);
		AdSize__ctor_m3068073238(L_1, ((int32_t)300), ((int32_t)250), /*hidden argument*/NULL);
		((AdSize_t3770813302_StaticFields*)AdSize_t3770813302_il2cpp_TypeInfo_var->static_fields)->set_MediumRectangle_3(L_1);
		AdSize_t3770813302 * L_2 = (AdSize_t3770813302 *)il2cpp_codegen_object_new(AdSize_t3770813302_il2cpp_TypeInfo_var);
		AdSize__ctor_m3068073238(L_2, ((int32_t)468), ((int32_t)60), /*hidden argument*/NULL);
		((AdSize_t3770813302_StaticFields*)AdSize_t3770813302_il2cpp_TypeInfo_var->static_fields)->set_IABBanner_4(L_2);
		AdSize_t3770813302 * L_3 = (AdSize_t3770813302 *)il2cpp_codegen_object_new(AdSize_t3770813302_il2cpp_TypeInfo_var);
		AdSize__ctor_m3068073238(L_3, ((int32_t)728), ((int32_t)90), /*hidden argument*/NULL);
		((AdSize_t3770813302_StaticFields*)AdSize_t3770813302_il2cpp_TypeInfo_var->static_fields)->set_Leaderboard_5(L_3);
		AdSize_t3770813302 * L_4 = (AdSize_t3770813302 *)il2cpp_codegen_object_new(AdSize_t3770813302_il2cpp_TypeInfo_var);
		AdSize__ctor_m3068073238(L_4, ((int32_t)120), ((int32_t)600), /*hidden argument*/NULL);
		((AdSize_t3770813302_StaticFields*)AdSize_t3770813302_il2cpp_TypeInfo_var->static_fields)->set_WideSkyscraper_6(L_4);
		AdSize_t3770813302 * L_5 = (AdSize_t3770813302 *)il2cpp_codegen_object_new(AdSize_t3770813302_il2cpp_TypeInfo_var);
		AdSize__ctor_m3068073238(L_5, (-1), ((int32_t)-2), /*hidden argument*/NULL);
		((AdSize_t3770813302_StaticFields*)AdSize_t3770813302_il2cpp_TypeInfo_var->static_fields)->set_SmartBanner_7(L_5);
		return;
	}
}
// System.Void MonoPInvokeCallbackAttribute::.ctor(System.Type)
extern "C"  void MonoPInvokeCallbackAttribute__ctor_m1119843856 (MonoPInvokeCallbackAttribute_t1970456718 * __this, Type_t * ___type0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::.ctor()
extern Il2CppClass* KeyframeU5BU5D_t449065829_il2cpp_TypeInfo_var;
extern Il2CppClass* AnimationCurve_t3306541151_il2cpp_TypeInfo_var;
extern const uint32_t ColorCorrectionCurves__ctor_m103965010_MetadataUsageId;
extern "C"  void ColorCorrectionCurves__ctor_m103965010 (ColorCorrectionCurves_t3523901841 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorCorrectionCurves__ctor_m103965010_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		KeyframeU5BU5D_t449065829* L_0 = ((KeyframeU5BU5D_t449065829*)SZArrayNew(KeyframeU5BU5D_t449065829_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_0);
		Keyframe_t1449471340  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Keyframe__ctor_m2042404667(&L_1, (0.0f), (0.0f), /*hidden argument*/NULL);
		(*(Keyframe_t1449471340 *)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_1;
		KeyframeU5BU5D_t449065829* L_2 = L_0;
		NullCheck(L_2);
		Keyframe_t1449471340  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Keyframe__ctor_m2042404667(&L_3, (1.0f), (1.0f), /*hidden argument*/NULL);
		(*(Keyframe_t1449471340 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_3;
		AnimationCurve_t3306541151 * L_4 = (AnimationCurve_t3306541151 *)il2cpp_codegen_object_new(AnimationCurve_t3306541151_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_m2814448007(L_4, L_2, /*hidden argument*/NULL);
		__this->set_redChannel_6(L_4);
		KeyframeU5BU5D_t449065829* L_5 = ((KeyframeU5BU5D_t449065829*)SZArrayNew(KeyframeU5BU5D_t449065829_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_5);
		Keyframe_t1449471340  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Keyframe__ctor_m2042404667(&L_6, (0.0f), (0.0f), /*hidden argument*/NULL);
		(*(Keyframe_t1449471340 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_6;
		KeyframeU5BU5D_t449065829* L_7 = L_5;
		NullCheck(L_7);
		Keyframe_t1449471340  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Keyframe__ctor_m2042404667(&L_8, (1.0f), (1.0f), /*hidden argument*/NULL);
		(*(Keyframe_t1449471340 *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_8;
		AnimationCurve_t3306541151 * L_9 = (AnimationCurve_t3306541151 *)il2cpp_codegen_object_new(AnimationCurve_t3306541151_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_m2814448007(L_9, L_7, /*hidden argument*/NULL);
		__this->set_greenChannel_7(L_9);
		KeyframeU5BU5D_t449065829* L_10 = ((KeyframeU5BU5D_t449065829*)SZArrayNew(KeyframeU5BU5D_t449065829_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_10);
		Keyframe_t1449471340  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Keyframe__ctor_m2042404667(&L_11, (0.0f), (0.0f), /*hidden argument*/NULL);
		(*(Keyframe_t1449471340 *)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_11;
		KeyframeU5BU5D_t449065829* L_12 = L_10;
		NullCheck(L_12);
		Keyframe_t1449471340  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Keyframe__ctor_m2042404667(&L_13, (1.0f), (1.0f), /*hidden argument*/NULL);
		(*(Keyframe_t1449471340 *)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_13;
		AnimationCurve_t3306541151 * L_14 = (AnimationCurve_t3306541151 *)il2cpp_codegen_object_new(AnimationCurve_t3306541151_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_m2814448007(L_14, L_12, /*hidden argument*/NULL);
		__this->set_blueChannel_8(L_14);
		KeyframeU5BU5D_t449065829* L_15 = ((KeyframeU5BU5D_t449065829*)SZArrayNew(KeyframeU5BU5D_t449065829_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_15);
		Keyframe_t1449471340  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Keyframe__ctor_m2042404667(&L_16, (0.0f), (0.0f), /*hidden argument*/NULL);
		(*(Keyframe_t1449471340 *)((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_16;
		KeyframeU5BU5D_t449065829* L_17 = L_15;
		NullCheck(L_17);
		Keyframe_t1449471340  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Keyframe__ctor_m2042404667(&L_18, (1.0f), (1.0f), /*hidden argument*/NULL);
		(*(Keyframe_t1449471340 *)((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_18;
		AnimationCurve_t3306541151 * L_19 = (AnimationCurve_t3306541151 *)il2cpp_codegen_object_new(AnimationCurve_t3306541151_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_m2814448007(L_19, L_17, /*hidden argument*/NULL);
		__this->set_zCurve_10(L_19);
		KeyframeU5BU5D_t449065829* L_20 = ((KeyframeU5BU5D_t449065829*)SZArrayNew(KeyframeU5BU5D_t449065829_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_20);
		Keyframe_t1449471340  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Keyframe__ctor_m2042404667(&L_21, (0.0f), (0.0f), /*hidden argument*/NULL);
		(*(Keyframe_t1449471340 *)((L_20)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_21;
		KeyframeU5BU5D_t449065829* L_22 = L_20;
		NullCheck(L_22);
		Keyframe_t1449471340  L_23;
		memset(&L_23, 0, sizeof(L_23));
		Keyframe__ctor_m2042404667(&L_23, (1.0f), (1.0f), /*hidden argument*/NULL);
		(*(Keyframe_t1449471340 *)((L_22)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_23;
		AnimationCurve_t3306541151 * L_24 = (AnimationCurve_t3306541151 *)il2cpp_codegen_object_new(AnimationCurve_t3306541151_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_m2814448007(L_24, L_22, /*hidden argument*/NULL);
		__this->set_depthRedChannel_11(L_24);
		KeyframeU5BU5D_t449065829* L_25 = ((KeyframeU5BU5D_t449065829*)SZArrayNew(KeyframeU5BU5D_t449065829_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_25);
		Keyframe_t1449471340  L_26;
		memset(&L_26, 0, sizeof(L_26));
		Keyframe__ctor_m2042404667(&L_26, (0.0f), (0.0f), /*hidden argument*/NULL);
		(*(Keyframe_t1449471340 *)((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_26;
		KeyframeU5BU5D_t449065829* L_27 = L_25;
		NullCheck(L_27);
		Keyframe_t1449471340  L_28;
		memset(&L_28, 0, sizeof(L_28));
		Keyframe__ctor_m2042404667(&L_28, (1.0f), (1.0f), /*hidden argument*/NULL);
		(*(Keyframe_t1449471340 *)((L_27)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_28;
		AnimationCurve_t3306541151 * L_29 = (AnimationCurve_t3306541151 *)il2cpp_codegen_object_new(AnimationCurve_t3306541151_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_m2814448007(L_29, L_27, /*hidden argument*/NULL);
		__this->set_depthGreenChannel_12(L_29);
		KeyframeU5BU5D_t449065829* L_30 = ((KeyframeU5BU5D_t449065829*)SZArrayNew(KeyframeU5BU5D_t449065829_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_30);
		Keyframe_t1449471340  L_31;
		memset(&L_31, 0, sizeof(L_31));
		Keyframe__ctor_m2042404667(&L_31, (0.0f), (0.0f), /*hidden argument*/NULL);
		(*(Keyframe_t1449471340 *)((L_30)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_31;
		KeyframeU5BU5D_t449065829* L_32 = L_30;
		NullCheck(L_32);
		Keyframe_t1449471340  L_33;
		memset(&L_33, 0, sizeof(L_33));
		Keyframe__ctor_m2042404667(&L_33, (1.0f), (1.0f), /*hidden argument*/NULL);
		(*(Keyframe_t1449471340 *)((L_32)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_33;
		AnimationCurve_t3306541151 * L_34 = (AnimationCurve_t3306541151 *)il2cpp_codegen_object_new(AnimationCurve_t3306541151_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_m2814448007(L_34, L_32, /*hidden argument*/NULL);
		__this->set_depthBlueChannel_13(L_34);
		__this->set_saturation_20((1.0f));
		Color_t2020392075  L_35 = Color_get_white_m3987539815(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_selectiveFromColor_22(L_35);
		Color_t2020392075  L_36 = Color_get_white_m3987539815(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_selectiveToColor_23(L_36);
		__this->set_updateTextures_25((bool)1);
		__this->set_updateTexturesOnStartup_29((bool)1);
		PostEffectsBase__ctor_m154078936(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::Start()
extern "C"  void ColorCorrectionCurves_Start_m1873828366 (ColorCorrectionCurves_t3523901841 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase_Start_m3321895624(__this, /*hidden argument*/NULL);
		__this->set_updateTexturesOnStartup_29((bool)1);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::Awake()
extern "C"  void ColorCorrectionCurves_Awake_m737869439 (ColorCorrectionCurves_t3523901841 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.ColorCorrectionCurves::CheckResources()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Texture2D_t3542995729_il2cpp_TypeInfo_var;
extern const uint32_t ColorCorrectionCurves_CheckResources_m2648565961_MetadataUsageId;
extern "C"  bool ColorCorrectionCurves_CheckResources_m2648565961 (ColorCorrectionCurves_t3523901841 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorCorrectionCurves_CheckResources_m2648565961_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_mode_24();
		PostEffectsBase_CheckSupport_m286341078(__this, (bool)((((int32_t)L_0) == ((int32_t)1))? 1 : 0), /*hidden argument*/NULL);
		Shader_t2430389951 * L_1 = __this->get_simpleColorCorrectionCurvesShader_27();
		Material_t193706927 * L_2 = __this->get_ccMaterial_14();
		Material_t193706927 * L_3 = PostEffectsBase_CheckShaderAndCreateMaterial_m1396976238(__this, L_1, L_2, /*hidden argument*/NULL);
		__this->set_ccMaterial_14(L_3);
		Shader_t2430389951 * L_4 = __this->get_colorCorrectionCurvesShader_26();
		Material_t193706927 * L_5 = __this->get_ccDepthMaterial_15();
		Material_t193706927 * L_6 = PostEffectsBase_CheckShaderAndCreateMaterial_m1396976238(__this, L_4, L_5, /*hidden argument*/NULL);
		__this->set_ccDepthMaterial_15(L_6);
		Shader_t2430389951 * L_7 = __this->get_colorCorrectionSelectiveShader_28();
		Material_t193706927 * L_8 = __this->get_selectiveCcMaterial_16();
		Material_t193706927 * L_9 = PostEffectsBase_CheckShaderAndCreateMaterial_m1396976238(__this, L_7, L_8, /*hidden argument*/NULL);
		__this->set_selectiveCcMaterial_16(L_9);
		Texture2D_t3542995729 * L_10 = __this->get_rgbChannelTex_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_007c;
		}
	}
	{
		Texture2D_t3542995729 * L_12 = (Texture2D_t3542995729 *)il2cpp_codegen_object_new(Texture2D_t3542995729_il2cpp_TypeInfo_var);
		Texture2D__ctor_m313689359(L_12, ((int32_t)256), 4, 5, (bool)0, (bool)1, /*hidden argument*/NULL);
		__this->set_rgbChannelTex_17(L_12);
	}

IL_007c:
	{
		Texture2D_t3542995729 * L_13 = __this->get_rgbDepthChannelTex_18();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_00a0;
		}
	}
	{
		Texture2D_t3542995729 * L_15 = (Texture2D_t3542995729 *)il2cpp_codegen_object_new(Texture2D_t3542995729_il2cpp_TypeInfo_var);
		Texture2D__ctor_m313689359(L_15, ((int32_t)256), 4, 5, (bool)0, (bool)1, /*hidden argument*/NULL);
		__this->set_rgbDepthChannelTex_18(L_15);
	}

IL_00a0:
	{
		Texture2D_t3542995729 * L_16 = __this->get_zCurveTex_19();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_17 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_00c4;
		}
	}
	{
		Texture2D_t3542995729 * L_18 = (Texture2D_t3542995729 *)il2cpp_codegen_object_new(Texture2D_t3542995729_il2cpp_TypeInfo_var);
		Texture2D__ctor_m313689359(L_18, ((int32_t)256), 1, 5, (bool)0, (bool)1, /*hidden argument*/NULL);
		__this->set_zCurveTex_19(L_18);
	}

IL_00c4:
	{
		Texture2D_t3542995729 * L_19 = __this->get_rgbChannelTex_17();
		NullCheck(L_19);
		Object_set_hideFlags_m2204253440(L_19, ((int32_t)52), /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_20 = __this->get_rgbDepthChannelTex_18();
		NullCheck(L_20);
		Object_set_hideFlags_m2204253440(L_20, ((int32_t)52), /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_21 = __this->get_zCurveTex_19();
		NullCheck(L_21);
		Object_set_hideFlags_m2204253440(L_21, ((int32_t)52), /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_22 = __this->get_rgbChannelTex_17();
		NullCheck(L_22);
		Texture_set_wrapMode_m333956747(L_22, 1, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_23 = __this->get_rgbDepthChannelTex_18();
		NullCheck(L_23);
		Texture_set_wrapMode_m333956747(L_23, 1, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_24 = __this->get_zCurveTex_19();
		NullCheck(L_24);
		Texture_set_wrapMode_m333956747(L_24, 1, /*hidden argument*/NULL);
		bool L_25 = ((PostEffectsBase_t2152133263 *)__this)->get_isSupported_4();
		if (L_25)
		{
			goto IL_0120;
		}
	}
	{
		PostEffectsBase_ReportAutoDisable_m2502513401(__this, /*hidden argument*/NULL);
	}

IL_0120:
	{
		bool L_26 = ((PostEffectsBase_t2152133263 *)__this)->get_isSupported_4();
		return L_26;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::UpdateParameters()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t ColorCorrectionCurves_UpdateParameters_m2926562869_MetadataUsageId;
extern "C"  void ColorCorrectionCurves_UpdateParameters_m2926562869 (ColorCorrectionCurves_t3523901841 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorCorrectionCurves_UpdateParameters_m2926562869_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckResources() */, __this);
		AnimationCurve_t3306541151 * L_0 = __this->get_redChannel_6();
		if (!L_0)
		{
			goto IL_0216;
		}
	}
	{
		AnimationCurve_t3306541151 * L_1 = __this->get_greenChannel_7();
		if (!L_1)
		{
			goto IL_0216;
		}
	}
	{
		AnimationCurve_t3306541151 * L_2 = __this->get_blueChannel_8();
		if (!L_2)
		{
			goto IL_0216;
		}
	}
	{
		V_0 = (0.0f);
		goto IL_01ea;
	}

IL_0033:
	{
		AnimationCurve_t3306541151 * L_3 = __this->get_redChannel_6();
		float L_4 = V_0;
		NullCheck(L_3);
		float L_5 = AnimationCurve_Evaluate_m3698879322(L_3, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_6 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_5, (0.0f), (1.0f), /*hidden argument*/NULL);
		V_1 = L_6;
		AnimationCurve_t3306541151 * L_7 = __this->get_greenChannel_7();
		float L_8 = V_0;
		NullCheck(L_7);
		float L_9 = AnimationCurve_Evaluate_m3698879322(L_7, L_8, /*hidden argument*/NULL);
		float L_10 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_9, (0.0f), (1.0f), /*hidden argument*/NULL);
		V_2 = L_10;
		AnimationCurve_t3306541151 * L_11 = __this->get_blueChannel_8();
		float L_12 = V_0;
		NullCheck(L_11);
		float L_13 = AnimationCurve_Evaluate_m3698879322(L_11, L_12, /*hidden argument*/NULL);
		float L_14 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_13, (0.0f), (1.0f), /*hidden argument*/NULL);
		V_3 = L_14;
		Texture2D_t3542995729 * L_15 = __this->get_rgbChannelTex_17();
		float L_16 = V_0;
		float L_17 = floorf(((float)((float)L_16*(float)(255.0f))));
		float L_18 = V_1;
		float L_19 = V_1;
		float L_20 = V_1;
		Color_t2020392075  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Color__ctor_m3811852957(&L_21, L_18, L_19, L_20, /*hidden argument*/NULL);
		NullCheck(L_15);
		Texture2D_SetPixel_m609991514(L_15, (((int32_t)((int32_t)L_17))), 0, L_21, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_22 = __this->get_rgbChannelTex_17();
		float L_23 = V_0;
		float L_24 = floorf(((float)((float)L_23*(float)(255.0f))));
		float L_25 = V_2;
		float L_26 = V_2;
		float L_27 = V_2;
		Color_t2020392075  L_28;
		memset(&L_28, 0, sizeof(L_28));
		Color__ctor_m3811852957(&L_28, L_25, L_26, L_27, /*hidden argument*/NULL);
		NullCheck(L_22);
		Texture2D_SetPixel_m609991514(L_22, (((int32_t)((int32_t)L_24))), 1, L_28, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_29 = __this->get_rgbChannelTex_17();
		float L_30 = V_0;
		float L_31 = floorf(((float)((float)L_30*(float)(255.0f))));
		float L_32 = V_3;
		float L_33 = V_3;
		float L_34 = V_3;
		Color_t2020392075  L_35;
		memset(&L_35, 0, sizeof(L_35));
		Color__ctor_m3811852957(&L_35, L_32, L_33, L_34, /*hidden argument*/NULL);
		NullCheck(L_29);
		Texture2D_SetPixel_m609991514(L_29, (((int32_t)((int32_t)L_31))), 2, L_35, /*hidden argument*/NULL);
		AnimationCurve_t3306541151 * L_36 = __this->get_zCurve_10();
		float L_37 = V_0;
		NullCheck(L_36);
		float L_38 = AnimationCurve_Evaluate_m3698879322(L_36, L_37, /*hidden argument*/NULL);
		float L_39 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_38, (0.0f), (1.0f), /*hidden argument*/NULL);
		V_4 = L_39;
		Texture2D_t3542995729 * L_40 = __this->get_zCurveTex_19();
		float L_41 = V_0;
		float L_42 = floorf(((float)((float)L_41*(float)(255.0f))));
		float L_43 = V_4;
		float L_44 = V_4;
		float L_45 = V_4;
		Color_t2020392075  L_46;
		memset(&L_46, 0, sizeof(L_46));
		Color__ctor_m3811852957(&L_46, L_43, L_44, L_45, /*hidden argument*/NULL);
		NullCheck(L_40);
		Texture2D_SetPixel_m609991514(L_40, (((int32_t)((int32_t)L_42))), 0, L_46, /*hidden argument*/NULL);
		AnimationCurve_t3306541151 * L_47 = __this->get_depthRedChannel_11();
		float L_48 = V_0;
		NullCheck(L_47);
		float L_49 = AnimationCurve_Evaluate_m3698879322(L_47, L_48, /*hidden argument*/NULL);
		float L_50 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_49, (0.0f), (1.0f), /*hidden argument*/NULL);
		V_1 = L_50;
		AnimationCurve_t3306541151 * L_51 = __this->get_depthGreenChannel_12();
		float L_52 = V_0;
		NullCheck(L_51);
		float L_53 = AnimationCurve_Evaluate_m3698879322(L_51, L_52, /*hidden argument*/NULL);
		float L_54 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_53, (0.0f), (1.0f), /*hidden argument*/NULL);
		V_2 = L_54;
		AnimationCurve_t3306541151 * L_55 = __this->get_depthBlueChannel_13();
		float L_56 = V_0;
		NullCheck(L_55);
		float L_57 = AnimationCurve_Evaluate_m3698879322(L_55, L_56, /*hidden argument*/NULL);
		float L_58 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_57, (0.0f), (1.0f), /*hidden argument*/NULL);
		V_3 = L_58;
		Texture2D_t3542995729 * L_59 = __this->get_rgbDepthChannelTex_18();
		float L_60 = V_0;
		float L_61 = floorf(((float)((float)L_60*(float)(255.0f))));
		float L_62 = V_1;
		float L_63 = V_1;
		float L_64 = V_1;
		Color_t2020392075  L_65;
		memset(&L_65, 0, sizeof(L_65));
		Color__ctor_m3811852957(&L_65, L_62, L_63, L_64, /*hidden argument*/NULL);
		NullCheck(L_59);
		Texture2D_SetPixel_m609991514(L_59, (((int32_t)((int32_t)L_61))), 0, L_65, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_66 = __this->get_rgbDepthChannelTex_18();
		float L_67 = V_0;
		float L_68 = floorf(((float)((float)L_67*(float)(255.0f))));
		float L_69 = V_2;
		float L_70 = V_2;
		float L_71 = V_2;
		Color_t2020392075  L_72;
		memset(&L_72, 0, sizeof(L_72));
		Color__ctor_m3811852957(&L_72, L_69, L_70, L_71, /*hidden argument*/NULL);
		NullCheck(L_66);
		Texture2D_SetPixel_m609991514(L_66, (((int32_t)((int32_t)L_68))), 1, L_72, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_73 = __this->get_rgbDepthChannelTex_18();
		float L_74 = V_0;
		float L_75 = floorf(((float)((float)L_74*(float)(255.0f))));
		float L_76 = V_3;
		float L_77 = V_3;
		float L_78 = V_3;
		Color_t2020392075  L_79;
		memset(&L_79, 0, sizeof(L_79));
		Color__ctor_m3811852957(&L_79, L_76, L_77, L_78, /*hidden argument*/NULL);
		NullCheck(L_73);
		Texture2D_SetPixel_m609991514(L_73, (((int32_t)((int32_t)L_75))), 2, L_79, /*hidden argument*/NULL);
		float L_80 = V_0;
		V_0 = ((float)((float)L_80+(float)(0.003921569f)));
	}

IL_01ea:
	{
		float L_81 = V_0;
		if ((((float)L_81) <= ((float)(1.0f))))
		{
			goto IL_0033;
		}
	}
	{
		Texture2D_t3542995729 * L_82 = __this->get_rgbChannelTex_17();
		NullCheck(L_82);
		Texture2D_Apply_m3543341930(L_82, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_83 = __this->get_rgbDepthChannelTex_18();
		NullCheck(L_83);
		Texture2D_Apply_m3543341930(L_83, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_84 = __this->get_zCurveTex_19();
		NullCheck(L_84);
		Texture2D_Apply_m3543341930(L_84, /*hidden argument*/NULL);
	}

IL_0216:
	{
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::UpdateTextures()
extern "C"  void ColorCorrectionCurves_UpdateTextures_m2041584495 (ColorCorrectionCurves_t3523901841 * __this, const MethodInfo* method)
{
	{
		ColorCorrectionCurves_UpdateParameters_m2926562869(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppClass* Graphics_t2412809155_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCamera_t189460977_m3276577584_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral801877921;
extern Il2CppCodeGenString* _stringLiteral1242056122;
extern Il2CppCodeGenString* _stringLiteral2219977516;
extern Il2CppCodeGenString* _stringLiteral262621631;
extern Il2CppCodeGenString* _stringLiteral1275727883;
extern Il2CppCodeGenString* _stringLiteral3422970734;
extern const uint32_t ColorCorrectionCurves_OnRenderImage_m2584221662_MetadataUsageId;
extern "C"  void ColorCorrectionCurves_OnRenderImage_m2584221662 (ColorCorrectionCurves_t3523901841 * __this, RenderTexture_t2666733923 * ___source0, RenderTexture_t2666733923 * ___destination1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorCorrectionCurves_OnRenderImage_m2584221662_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RenderTexture_t2666733923 * V_0 = NULL;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckResources() */, __this);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		RenderTexture_t2666733923 * L_1 = ___source0;
		RenderTexture_t2666733923 * L_2 = ___destination1;
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t2412809155_il2cpp_TypeInfo_var);
		Graphics_Blit_m2123328641(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return;
	}

IL_0013:
	{
		bool L_3 = __this->get_updateTexturesOnStartup_29();
		if (!L_3)
		{
			goto IL_002b;
		}
	}
	{
		ColorCorrectionCurves_UpdateParameters_m2926562869(__this, /*hidden argument*/NULL);
		__this->set_updateTexturesOnStartup_29((bool)0);
	}

IL_002b:
	{
		bool L_4 = __this->get_useDepthCorrection_9();
		if (!L_4)
		{
			goto IL_0049;
		}
	}
	{
		Camera_t189460977 * L_5 = Component_GetComponent_TisCamera_t189460977_m3276577584(__this, /*hidden argument*/Component_GetComponent_TisCamera_t189460977_m3276577584_MethodInfo_var);
		Camera_t189460977 * L_6 = L_5;
		NullCheck(L_6);
		int32_t L_7 = Camera_get_depthTextureMode_m924131993(L_6, /*hidden argument*/NULL);
		NullCheck(L_6);
		Camera_set_depthTextureMode_m1269215020(L_6, ((int32_t)((int32_t)L_7|(int32_t)1)), /*hidden argument*/NULL);
	}

IL_0049:
	{
		RenderTexture_t2666733923 * L_8 = ___destination1;
		V_0 = L_8;
		bool L_9 = __this->get_selectiveCc_21();
		if (!L_9)
		{
			goto IL_0068;
		}
	}
	{
		RenderTexture_t2666733923 * L_10 = ___source0;
		NullCheck(L_10);
		int32_t L_11 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_10);
		RenderTexture_t2666733923 * L_12 = ___source0;
		NullCheck(L_12);
		int32_t L_13 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_12);
		RenderTexture_t2666733923 * L_14 = RenderTexture_GetTemporary_m2226167488(NULL /*static, unused*/, L_11, L_13, /*hidden argument*/NULL);
		V_0 = L_14;
	}

IL_0068:
	{
		bool L_15 = __this->get_useDepthCorrection_9();
		if (!L_15)
		{
			goto IL_00dd;
		}
	}
	{
		Material_t193706927 * L_16 = __this->get_ccDepthMaterial_15();
		Texture2D_t3542995729 * L_17 = __this->get_rgbChannelTex_17();
		NullCheck(L_16);
		Material_SetTexture_m141095205(L_16, _stringLiteral801877921, L_17, /*hidden argument*/NULL);
		Material_t193706927 * L_18 = __this->get_ccDepthMaterial_15();
		Texture2D_t3542995729 * L_19 = __this->get_zCurveTex_19();
		NullCheck(L_18);
		Material_SetTexture_m141095205(L_18, _stringLiteral1242056122, L_19, /*hidden argument*/NULL);
		Material_t193706927 * L_20 = __this->get_ccDepthMaterial_15();
		Texture2D_t3542995729 * L_21 = __this->get_rgbDepthChannelTex_18();
		NullCheck(L_20);
		Material_SetTexture_m141095205(L_20, _stringLiteral2219977516, L_21, /*hidden argument*/NULL);
		Material_t193706927 * L_22 = __this->get_ccDepthMaterial_15();
		float L_23 = __this->get_saturation_20();
		NullCheck(L_22);
		Material_SetFloat_m1926275467(L_22, _stringLiteral262621631, L_23, /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_24 = ___source0;
		RenderTexture_t2666733923 * L_25 = V_0;
		Material_t193706927 * L_26 = __this->get_ccDepthMaterial_15();
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t2412809155_il2cpp_TypeInfo_var);
		Graphics_Blit_m4129959705(NULL /*static, unused*/, L_24, L_25, L_26, /*hidden argument*/NULL);
		goto IL_0116;
	}

IL_00dd:
	{
		Material_t193706927 * L_27 = __this->get_ccMaterial_14();
		Texture2D_t3542995729 * L_28 = __this->get_rgbChannelTex_17();
		NullCheck(L_27);
		Material_SetTexture_m141095205(L_27, _stringLiteral801877921, L_28, /*hidden argument*/NULL);
		Material_t193706927 * L_29 = __this->get_ccMaterial_14();
		float L_30 = __this->get_saturation_20();
		NullCheck(L_29);
		Material_SetFloat_m1926275467(L_29, _stringLiteral262621631, L_30, /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_31 = ___source0;
		RenderTexture_t2666733923 * L_32 = V_0;
		Material_t193706927 * L_33 = __this->get_ccMaterial_14();
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t2412809155_il2cpp_TypeInfo_var);
		Graphics_Blit_m4129959705(NULL /*static, unused*/, L_31, L_32, L_33, /*hidden argument*/NULL);
	}

IL_0116:
	{
		bool L_34 = __this->get_selectiveCc_21();
		if (!L_34)
		{
			goto IL_0160;
		}
	}
	{
		Material_t193706927 * L_35 = __this->get_selectiveCcMaterial_16();
		Color_t2020392075  L_36 = __this->get_selectiveFromColor_22();
		NullCheck(L_35);
		Material_SetColor_m650857509(L_35, _stringLiteral1275727883, L_36, /*hidden argument*/NULL);
		Material_t193706927 * L_37 = __this->get_selectiveCcMaterial_16();
		Color_t2020392075  L_38 = __this->get_selectiveToColor_23();
		NullCheck(L_37);
		Material_SetColor_m650857509(L_37, _stringLiteral3422970734, L_38, /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_39 = V_0;
		RenderTexture_t2666733923 * L_40 = ___destination1;
		Material_t193706927 * L_41 = __this->get_selectiveCcMaterial_16();
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t2412809155_il2cpp_TypeInfo_var);
		Graphics_Blit_m4129959705(NULL /*static, unused*/, L_39, L_40, L_41, /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_42 = V_0;
		RenderTexture_ReleaseTemporary_m1186631014(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
	}

IL_0160:
	{
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionLookup::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t ColorCorrectionLookup__ctor_m4122754856_MetadataUsageId;
extern "C"  void ColorCorrectionLookup__ctor_m4122754856 (ColorCorrectionLookup_t2385315007 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorCorrectionLookup__ctor_m4122754856_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_basedOnTempTex_9(L_0);
		PostEffectsBase__ctor_m154078936(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.ColorCorrectionLookup::CheckResources()
extern "C"  bool ColorCorrectionLookup_CheckResources_m3110598415 (ColorCorrectionLookup_t2385315007 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase_CheckSupport_m286341078(__this, (bool)0, /*hidden argument*/NULL);
		Shader_t2430389951 * L_0 = __this->get_shader_6();
		Material_t193706927 * L_1 = __this->get_material_7();
		Material_t193706927 * L_2 = PostEffectsBase_CheckShaderAndCreateMaterial_m1396976238(__this, L_0, L_1, /*hidden argument*/NULL);
		__this->set_material_7(L_2);
		bool L_3 = ((PostEffectsBase_t2152133263 *)__this)->get_isSupported_4();
		if (!L_3)
		{
			goto IL_0035;
		}
	}
	{
		bool L_4 = SystemInfo_get_supports3DTextures_m3197410274(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_003b;
		}
	}

IL_0035:
	{
		PostEffectsBase_ReportAutoDisable_m2502513401(__this, /*hidden argument*/NULL);
	}

IL_003b:
	{
		bool L_5 = ((PostEffectsBase_t2152133263 *)__this)->get_isSupported_4();
		return L_5;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionLookup::OnDisable()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t ColorCorrectionLookup_OnDisable_m3140290877_MetadataUsageId;
extern "C"  void ColorCorrectionLookup_OnDisable_m3140290877 (ColorCorrectionLookup_t2385315007 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorCorrectionLookup_OnDisable_m3140290877_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_material_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_material_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m95027445(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_material_7((Material_t193706927 *)NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionLookup::OnDestroy()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t ColorCorrectionLookup_OnDestroy_m4258511571_MetadataUsageId;
extern "C"  void ColorCorrectionLookup_OnDestroy_m4258511571 (ColorCorrectionLookup_t2385315007 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorCorrectionLookup_OnDestroy_m4258511571_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Texture3D_t814112374 * L_0 = __this->get_converted3DLut_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Texture3D_t814112374 * L_2 = __this->get_converted3DLut_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m95027445(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		__this->set_converted3DLut_8((Texture3D_t814112374 *)NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionLookup::SetIdentityLut()
extern Il2CppClass* ColorU5BU5D_t672350442_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Texture3D_t814112374_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t ColorCorrectionLookup_SetIdentityLut_m1059134669_MetadataUsageId;
extern "C"  void ColorCorrectionLookup_SetIdentityLut_m1059134669 (ColorCorrectionLookup_t2385315007 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorCorrectionLookup_SetIdentityLut_m1059134669_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	ColorU5BU5D_t672350442* V_1 = NULL;
	float V_2 = 0.0f;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		V_0 = ((int32_t)16);
		int32_t L_0 = V_0;
		int32_t L_1 = V_0;
		int32_t L_2 = V_0;
		V_1 = ((ColorU5BU5D_t672350442*)SZArrayNew(ColorU5BU5D_t672350442_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_0*(int32_t)L_1))*(int32_t)L_2))));
		int32_t L_3 = V_0;
		V_2 = ((float)((float)(1.0f)/(float)((float)((float)((float)((float)(1.0f)*(float)(((float)((float)L_3)))))-(float)(1.0f)))));
		V_3 = 0;
		goto IL_009c;
	}

IL_002a:
	{
		V_4 = 0;
		goto IL_0090;
	}

IL_0032:
	{
		V_5 = 0;
		goto IL_0082;
	}

IL_003a:
	{
		ColorU5BU5D_t672350442* L_4 = V_1;
		int32_t L_5 = V_3;
		int32_t L_6 = V_4;
		int32_t L_7 = V_0;
		int32_t L_8 = V_5;
		int32_t L_9 = V_0;
		int32_t L_10 = V_0;
		NullCheck(L_4);
		int32_t L_11 = V_3;
		float L_12 = V_2;
		int32_t L_13 = V_4;
		float L_14 = V_2;
		int32_t L_15 = V_5;
		float L_16 = V_2;
		Color_t2020392075  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Color__ctor_m1909920690(&L_17, ((float)((float)((float)((float)(((float)((float)L_11)))*(float)(1.0f)))*(float)L_12)), ((float)((float)((float)((float)(((float)((float)L_13)))*(float)(1.0f)))*(float)L_14)), ((float)((float)((float)((float)(((float)((float)L_15)))*(float)(1.0f)))*(float)L_16)), (1.0f), /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)((int32_t)((int32_t)L_5+(int32_t)((int32_t)((int32_t)L_6*(int32_t)L_7))))+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_8*(int32_t)L_9))*(int32_t)L_10)))))))) = L_17;
		int32_t L_18 = V_5;
		V_5 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0082:
	{
		int32_t L_19 = V_5;
		int32_t L_20 = V_0;
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_003a;
		}
	}
	{
		int32_t L_21 = V_4;
		V_4 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_0090:
	{
		int32_t L_22 = V_4;
		int32_t L_23 = V_0;
		if ((((int32_t)L_22) < ((int32_t)L_23)))
		{
			goto IL_0032;
		}
	}
	{
		int32_t L_24 = V_3;
		V_3 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_009c:
	{
		int32_t L_25 = V_3;
		int32_t L_26 = V_0;
		if ((((int32_t)L_25) < ((int32_t)L_26)))
		{
			goto IL_002a;
		}
	}
	{
		Texture3D_t814112374 * L_27 = __this->get_converted3DLut_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_28 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_00be;
		}
	}
	{
		Texture3D_t814112374 * L_29 = __this->get_converted3DLut_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m95027445(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
	}

IL_00be:
	{
		int32_t L_30 = V_0;
		int32_t L_31 = V_0;
		int32_t L_32 = V_0;
		Texture3D_t814112374 * L_33 = (Texture3D_t814112374 *)il2cpp_codegen_object_new(Texture3D_t814112374_il2cpp_TypeInfo_var);
		Texture3D__ctor_m3177496444(L_33, L_30, L_31, L_32, 5, (bool)0, /*hidden argument*/NULL);
		__this->set_converted3DLut_8(L_33);
		Texture3D_t814112374 * L_34 = __this->get_converted3DLut_8();
		ColorU5BU5D_t672350442* L_35 = V_1;
		NullCheck(L_34);
		Texture3D_SetPixels_m946476760(L_34, L_35, /*hidden argument*/NULL);
		Texture3D_t814112374 * L_36 = __this->get_converted3DLut_8();
		NullCheck(L_36);
		Texture3D_Apply_m1438724325(L_36, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_37 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_basedOnTempTex_9(L_37);
		return;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.ColorCorrectionLookup::ValidDimensions(UnityEngine.Texture2D)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t ColorCorrectionLookup_ValidDimensions_m2993190419_MetadataUsageId;
extern "C"  bool ColorCorrectionLookup_ValidDimensions_m2993190419 (ColorCorrectionLookup_t2385315007 * __this, Texture2D_t3542995729 * ___tex2d0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorCorrectionLookup_ValidDimensions_m2993190419_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Texture2D_t3542995729 * L_0 = ___tex2d0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Texture2D_t3542995729 * L_2 = ___tex2d0;
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_2);
		V_0 = L_3;
		int32_t L_4 = V_0;
		Texture2D_t3542995729 * L_5 = ___tex2d0;
		NullCheck(L_5);
		int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_5);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_7 = sqrtf((((float)((float)L_6))));
		int32_t L_8 = Mathf_FloorToInt_m4005035722(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if ((((int32_t)L_4) == ((int32_t)L_8)))
		{
			goto IL_002d;
		}
	}
	{
		return (bool)0;
	}

IL_002d:
	{
		return (bool)1;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionLookup::Convert(UnityEngine.Texture2D,System.String)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* ColorU5BU5D_t672350442_il2cpp_TypeInfo_var;
extern Il2CppClass* Texture3D_t814112374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3828305079;
extern Il2CppCodeGenString* _stringLiteral3979686238;
extern Il2CppCodeGenString* _stringLiteral3300851433;
extern const uint32_t ColorCorrectionLookup_Convert_m2446822723_MetadataUsageId;
extern "C"  void ColorCorrectionLookup_Convert_m2446822723 (ColorCorrectionLookup_t2385315007 * __this, Texture2D_t3542995729 * ___temp2DTex0, String_t* ___path1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorCorrectionLookup_Convert_m2446822723_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	ColorU5BU5D_t672350442* V_1 = NULL;
	ColorU5BU5D_t672350442* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	{
		Texture2D_t3542995729 * L_0 = ___temp2DTex0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0126;
		}
	}
	{
		Texture2D_t3542995729 * L_2 = ___temp2DTex0;
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_2);
		Texture2D_t3542995729 * L_4 = ___temp2DTex0;
		NullCheck(L_4);
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_4);
		V_0 = ((int32_t)((int32_t)L_3*(int32_t)L_5));
		Texture2D_t3542995729 * L_6 = ___temp2DTex0;
		NullCheck(L_6);
		int32_t L_7 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_6);
		V_0 = L_7;
		Texture2D_t3542995729 * L_8 = ___temp2DTex0;
		bool L_9 = ColorCorrectionLookup_ValidDimensions_m2993190419(__this, L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0052;
		}
	}
	{
		Texture2D_t3542995729 * L_10 = ___temp2DTex0;
		NullCheck(L_10);
		String_t* L_11 = Object_get_name_m2079638459(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral3828305079, L_11, _stringLiteral3979686238, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		String_t* L_13 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_basedOnTempTex_9(L_13);
		return;
	}

IL_0052:
	{
		Texture2D_t3542995729 * L_14 = ___temp2DTex0;
		NullCheck(L_14);
		ColorU5BU5D_t672350442* L_15 = Texture2D_GetPixels_m643504232(L_14, /*hidden argument*/NULL);
		V_1 = L_15;
		ColorU5BU5D_t672350442* L_16 = V_1;
		NullCheck(L_16);
		V_2 = ((ColorU5BU5D_t672350442*)SZArrayNew(ColorU5BU5D_t672350442_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_16)->max_length))))));
		V_3 = 0;
		goto IL_00d1;
	}

IL_0069:
	{
		V_4 = 0;
		goto IL_00c5;
	}

IL_0071:
	{
		V_5 = 0;
		goto IL_00b7;
	}

IL_0079:
	{
		int32_t L_17 = V_0;
		int32_t L_18 = V_4;
		V_6 = ((int32_t)((int32_t)((int32_t)((int32_t)L_17-(int32_t)L_18))-(int32_t)1));
		ColorU5BU5D_t672350442* L_19 = V_2;
		int32_t L_20 = V_3;
		int32_t L_21 = V_4;
		int32_t L_22 = V_0;
		int32_t L_23 = V_5;
		int32_t L_24 = V_0;
		int32_t L_25 = V_0;
		NullCheck(L_19);
		ColorU5BU5D_t672350442* L_26 = V_1;
		int32_t L_27 = V_5;
		int32_t L_28 = V_0;
		int32_t L_29 = V_3;
		int32_t L_30 = V_6;
		int32_t L_31 = V_0;
		int32_t L_32 = V_0;
		NullCheck(L_26);
		(*(Color_t2020392075 *)((L_19)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)((int32_t)((int32_t)L_20+(int32_t)((int32_t)((int32_t)L_21*(int32_t)L_22))))+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_23*(int32_t)L_24))*(int32_t)L_25)))))))) = (*(Color_t2020392075 *)((L_26)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_27*(int32_t)L_28))+(int32_t)L_29))+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_30*(int32_t)L_31))*(int32_t)L_32))))))));
		int32_t L_33 = V_5;
		V_5 = ((int32_t)((int32_t)L_33+(int32_t)1));
	}

IL_00b7:
	{
		int32_t L_34 = V_5;
		int32_t L_35 = V_0;
		if ((((int32_t)L_34) < ((int32_t)L_35)))
		{
			goto IL_0079;
		}
	}
	{
		int32_t L_36 = V_4;
		V_4 = ((int32_t)((int32_t)L_36+(int32_t)1));
	}

IL_00c5:
	{
		int32_t L_37 = V_4;
		int32_t L_38 = V_0;
		if ((((int32_t)L_37) < ((int32_t)L_38)))
		{
			goto IL_0071;
		}
	}
	{
		int32_t L_39 = V_3;
		V_3 = ((int32_t)((int32_t)L_39+(int32_t)1));
	}

IL_00d1:
	{
		int32_t L_40 = V_3;
		int32_t L_41 = V_0;
		if ((((int32_t)L_40) < ((int32_t)L_41)))
		{
			goto IL_0069;
		}
	}
	{
		Texture3D_t814112374 * L_42 = __this->get_converted3DLut_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_43 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
		if (!L_43)
		{
			goto IL_00f3;
		}
	}
	{
		Texture3D_t814112374 * L_44 = __this->get_converted3DLut_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m95027445(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
	}

IL_00f3:
	{
		int32_t L_45 = V_0;
		int32_t L_46 = V_0;
		int32_t L_47 = V_0;
		Texture3D_t814112374 * L_48 = (Texture3D_t814112374 *)il2cpp_codegen_object_new(Texture3D_t814112374_il2cpp_TypeInfo_var);
		Texture3D__ctor_m3177496444(L_48, L_45, L_46, L_47, 5, (bool)0, /*hidden argument*/NULL);
		__this->set_converted3DLut_8(L_48);
		Texture3D_t814112374 * L_49 = __this->get_converted3DLut_8();
		ColorU5BU5D_t672350442* L_50 = V_2;
		NullCheck(L_49);
		Texture3D_SetPixels_m946476760(L_49, L_50, /*hidden argument*/NULL);
		Texture3D_t814112374 * L_51 = __this->get_converted3DLut_8();
		NullCheck(L_51);
		Texture3D_Apply_m1438724325(L_51, /*hidden argument*/NULL);
		String_t* L_52 = ___path1;
		__this->set_basedOnTempTex_9(L_52);
		goto IL_0130;
	}

IL_0126:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral3300851433, /*hidden argument*/NULL);
	}

IL_0130:
	{
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionLookup::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppClass* Graphics_t2412809155_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral855466991;
extern Il2CppCodeGenString* _stringLiteral219023528;
extern Il2CppCodeGenString* _stringLiteral3517553826;
extern const uint32_t ColorCorrectionLookup_OnRenderImage_m1666458312_MetadataUsageId;
extern "C"  void ColorCorrectionLookup_OnRenderImage_m1666458312 (ColorCorrectionLookup_t2385315007 * __this, RenderTexture_t2666733923 * ___source0, RenderTexture_t2666733923 * ___destination1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorCorrectionLookup_OnRenderImage_m1666458312_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Material_t193706927 * G_B7_0 = NULL;
	RenderTexture_t2666733923 * G_B7_1 = NULL;
	RenderTexture_t2666733923 * G_B7_2 = NULL;
	Material_t193706927 * G_B6_0 = NULL;
	RenderTexture_t2666733923 * G_B6_1 = NULL;
	RenderTexture_t2666733923 * G_B6_2 = NULL;
	int32_t G_B8_0 = 0;
	Material_t193706927 * G_B8_1 = NULL;
	RenderTexture_t2666733923 * G_B8_2 = NULL;
	RenderTexture_t2666733923 * G_B8_3 = NULL;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckResources() */, __this);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		bool L_1 = SystemInfo_get_supports3DTextures_m3197410274(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001d;
		}
	}

IL_0015:
	{
		RenderTexture_t2666733923 * L_2 = ___source0;
		RenderTexture_t2666733923 * L_3 = ___destination1;
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t2412809155_il2cpp_TypeInfo_var);
		Graphics_Blit_m2123328641(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		return;
	}

IL_001d:
	{
		Texture3D_t814112374 * L_4 = __this->get_converted3DLut_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_4, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0034;
		}
	}
	{
		ColorCorrectionLookup_SetIdentityLut_m1059134669(__this, /*hidden argument*/NULL);
	}

IL_0034:
	{
		Texture3D_t814112374 * L_6 = __this->get_converted3DLut_8();
		NullCheck(L_6);
		int32_t L_7 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_6);
		V_0 = L_7;
		Texture3D_t814112374 * L_8 = __this->get_converted3DLut_8();
		NullCheck(L_8);
		Texture_set_wrapMode_m333956747(L_8, 1, /*hidden argument*/NULL);
		Material_t193706927 * L_9 = __this->get_material_7();
		int32_t L_10 = V_0;
		int32_t L_11 = V_0;
		NullCheck(L_9);
		Material_SetFloat_m1926275467(L_9, _stringLiteral855466991, ((float)((float)(((float)((float)((int32_t)((int32_t)L_10-(int32_t)1)))))/(float)((float)((float)(1.0f)*(float)(((float)((float)L_11))))))), /*hidden argument*/NULL);
		Material_t193706927 * L_12 = __this->get_material_7();
		int32_t L_13 = V_0;
		NullCheck(L_12);
		Material_SetFloat_m1926275467(L_12, _stringLiteral219023528, ((float)((float)(1.0f)/(float)((float)((float)(2.0f)*(float)(((float)((float)L_13))))))), /*hidden argument*/NULL);
		Material_t193706927 * L_14 = __this->get_material_7();
		Texture3D_t814112374 * L_15 = __this->get_converted3DLut_8();
		NullCheck(L_14);
		Material_SetTexture_m141095205(L_14, _stringLiteral3517553826, L_15, /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_16 = ___source0;
		RenderTexture_t2666733923 * L_17 = ___destination1;
		Material_t193706927 * L_18 = __this->get_material_7();
		int32_t L_19 = QualitySettings_get_activeColorSpace_m580875098(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B6_0 = L_18;
		G_B6_1 = L_17;
		G_B6_2 = L_16;
		if ((!(((uint32_t)L_19) == ((uint32_t)1))))
		{
			G_B7_0 = L_18;
			G_B7_1 = L_17;
			G_B7_2 = L_16;
			goto IL_00b6;
		}
	}
	{
		G_B8_0 = 1;
		G_B8_1 = G_B6_0;
		G_B8_2 = G_B6_1;
		G_B8_3 = G_B6_2;
		goto IL_00b7;
	}

IL_00b6:
	{
		G_B8_0 = 0;
		G_B8_1 = G_B7_0;
		G_B8_2 = G_B7_1;
		G_B8_3 = G_B7_2;
	}

IL_00b7:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t2412809155_il2cpp_TypeInfo_var);
		Graphics_Blit_m2987760672(NULL /*static, unused*/, G_B8_3, G_B8_2, G_B8_1, G_B8_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionRamp::.ctor()
extern "C"  void ColorCorrectionRamp__ctor_m2902176530 (ColorCorrectionRamp_t1672908095 * __this, const MethodInfo* method)
{
	{
		ImageEffectBase__ctor_m4213568848(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionRamp::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppClass* Graphics_t2412809155_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2775783932;
extern const uint32_t ColorCorrectionRamp_OnRenderImage_m1511984458_MetadataUsageId;
extern "C"  void ColorCorrectionRamp_OnRenderImage_m1511984458 (ColorCorrectionRamp_t1672908095 * __this, RenderTexture_t2666733923 * ___source0, RenderTexture_t2666733923 * ___destination1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorCorrectionRamp_OnRenderImage_m1511984458_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = ImageEffectBase_get_material_m288459129(__this, /*hidden argument*/NULL);
		Texture_t2243626319 * L_1 = __this->get_textureRamp_4();
		NullCheck(L_0);
		Material_SetTexture_m141095205(L_0, _stringLiteral2775783932, L_1, /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_2 = ___source0;
		RenderTexture_t2666733923 * L_3 = ___destination1;
		Material_t193706927 * L_4 = ImageEffectBase_get_material_m288459129(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t2412809155_il2cpp_TypeInfo_var);
		Graphics_Blit_m4129959705(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ImageEffectBase::.ctor()
extern "C"  void ImageEffectBase__ctor_m4213568848 (ImageEffectBase_t517806655 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ImageEffectBase::Start()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t ImageEffectBase_Start_m2152569424_MetadataUsageId;
extern "C"  void ImageEffectBase_Start_m2152569424 (ImageEffectBase_t517806655 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImageEffectBase_Start_m2152569424_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = SystemInfo_get_supportsImageEffects_m406299852(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		Behaviour_set_enabled_m1796096907(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}

IL_0012:
	{
		Shader_t2430389951 * L_1 = __this->get_shader_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0032;
		}
	}
	{
		Shader_t2430389951 * L_3 = __this->get_shader_2();
		NullCheck(L_3);
		bool L_4 = Shader_get_isSupported_m344486701(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0039;
		}
	}

IL_0032:
	{
		Behaviour_set_enabled_m1796096907(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_0039:
	{
		return;
	}
}
// UnityEngine.Material UnityStandardAssets.ImageEffects.ImageEffectBase::get_material()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Material_t193706927_il2cpp_TypeInfo_var;
extern const uint32_t ImageEffectBase_get_material_m288459129_MetadataUsageId;
extern "C"  Material_t193706927 * ImageEffectBase_get_material_m288459129 (ImageEffectBase_t517806655 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImageEffectBase_get_material_m288459129_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_m_Material_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t2430389951 * L_2 = __this->get_shader_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1897560860(L_3, L_2, /*hidden argument*/NULL);
		__this->set_m_Material_3(L_3);
		Material_t193706927 * L_4 = __this->get_m_Material_3();
		NullCheck(L_4);
		Object_set_hideFlags_m2204253440(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t193706927 * L_5 = __this->get_m_Material_3();
		return L_5;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ImageEffectBase::OnDisable()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t ImageEffectBase_OnDisable_m170446245_MetadataUsageId;
extern "C"  void ImageEffectBase_OnDisable_m170446245 (ImageEffectBase_t517806655 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImageEffectBase_OnDisable_m170446245_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_m_Material_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_m_Material_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m95027445(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ImageEffects::.ctor()
extern "C"  void ImageEffects__ctor_m1545720380 (ImageEffects_t2907318477 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ImageEffects::RenderDistortion(UnityEngine.Material,UnityEngine.RenderTexture,UnityEngine.RenderTexture,System.Single,UnityEngine.Vector2,UnityEngine.Vector2)
extern Il2CppClass* Graphics_t2412809155_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral221875328;
extern Il2CppCodeGenString* _stringLiteral2679782504;
extern Il2CppCodeGenString* _stringLiteral2781519382;
extern const uint32_t ImageEffects_RenderDistortion_m3071633592_MetadataUsageId;
extern "C"  void ImageEffects_RenderDistortion_m3071633592 (Il2CppObject * __this /* static, unused */, Material_t193706927 * ___material0, RenderTexture_t2666733923 * ___source1, RenderTexture_t2666733923 * ___destination2, float ___angle3, Vector2_t2243707579  ___center4, Vector2_t2243707579  ___radius5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImageEffects_RenderDistortion_m3071633592_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Matrix4x4_t2933234003  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		RenderTexture_t2666733923 * L_0 = ___source1;
		NullCheck(L_0);
		Vector2_t2243707579  L_1 = Texture_get_texelSize_m4226268553(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		float L_2 = (&V_1)->get_y_1();
		V_0 = (bool)((((float)L_2) < ((float)(0.0f)))? 1 : 0);
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_0034;
		}
	}
	{
		float L_4 = (&___center4)->get_y_1();
		(&___center4)->set_y_1(((float)((float)(1.0f)-(float)L_4)));
		float L_5 = ___angle3;
		___angle3 = ((-L_5));
	}

IL_0034:
	{
		Vector3_t2243707580  L_6 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_7 = ___angle3;
		Quaternion_t4030073918  L_8 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (0.0f), (0.0f), L_7, /*hidden argument*/NULL);
		Vector3_t2243707580  L_9 = Vector3_get_one_m627547232(NULL /*static, unused*/, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_10 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_6, L_8, L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		Material_t193706927 * L_11 = ___material0;
		Matrix4x4_t2933234003  L_12 = V_2;
		NullCheck(L_11);
		Material_SetMatrix_m1387972957(L_11, _stringLiteral221875328, L_12, /*hidden argument*/NULL);
		Material_t193706927 * L_13 = ___material0;
		float L_14 = (&___center4)->get_x_0();
		float L_15 = (&___center4)->get_y_1();
		float L_16 = (&___radius5)->get_x_0();
		float L_17 = (&___radius5)->get_y_1();
		Vector4_t2243707581  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector4__ctor_m1222289168(&L_18, L_14, L_15, L_16, L_17, /*hidden argument*/NULL);
		NullCheck(L_13);
		Material_SetVector_m3298399397(L_13, _stringLiteral2679782504, L_18, /*hidden argument*/NULL);
		Material_t193706927 * L_19 = ___material0;
		float L_20 = ___angle3;
		NullCheck(L_19);
		Material_SetFloat_m1926275467(L_19, _stringLiteral2781519382, ((float)((float)L_20*(float)(0.0174532924f))), /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_21 = ___source1;
		RenderTexture_t2666733923 * L_22 = ___destination2;
		Material_t193706927 * L_23 = ___material0;
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t2412809155_il2cpp_TypeInfo_var);
		Graphics_Blit_m4129959705(NULL /*static, unused*/, L_21, L_22, L_23, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ImageEffects::Blit(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppClass* Graphics_t2412809155_il2cpp_TypeInfo_var;
extern const uint32_t ImageEffects_Blit_m311753913_MetadataUsageId;
extern "C"  void ImageEffects_Blit_m311753913 (Il2CppObject * __this /* static, unused */, RenderTexture_t2666733923 * ___source0, RenderTexture_t2666733923 * ___dest1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImageEffects_Blit_m311753913_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RenderTexture_t2666733923 * L_0 = ___source0;
		RenderTexture_t2666733923 * L_1 = ___dest1;
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t2412809155_il2cpp_TypeInfo_var);
		Graphics_Blit_m2123328641(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.ImageEffects::BlitWithMaterial(UnityEngine.Material,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppClass* Graphics_t2412809155_il2cpp_TypeInfo_var;
extern const uint32_t ImageEffects_BlitWithMaterial_m345798142_MetadataUsageId;
extern "C"  void ImageEffects_BlitWithMaterial_m345798142 (Il2CppObject * __this /* static, unused */, Material_t193706927 * ___material0, RenderTexture_t2666733923 * ___source1, RenderTexture_t2666733923 * ___dest2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImageEffects_BlitWithMaterial_m345798142_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RenderTexture_t2666733923 * L_0 = ___source1;
		RenderTexture_t2666733923 * L_1 = ___dest2;
		Material_t193706927 * L_2 = ___material0;
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t2412809155_il2cpp_TypeInfo_var);
		Graphics_Blit_m4129959705(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::.ctor()
extern Il2CppClass* List_1_t3857795355_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1324212819_MethodInfo_var;
extern const uint32_t PostEffectsBase__ctor_m154078936_MetadataUsageId;
extern "C"  void PostEffectsBase__ctor_m154078936 (PostEffectsBase_t2152133263 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PostEffectsBase__ctor_m154078936_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_supportHDRTextures_2((bool)1);
		__this->set_isSupported_4((bool)1);
		List_1_t3857795355 * L_0 = (List_1_t3857795355 *)il2cpp_codegen_object_new(List_1_t3857795355_il2cpp_TypeInfo_var);
		List_1__ctor_m1324212819(L_0, /*hidden argument*/List_1__ctor_m1324212819_MethodInfo_var);
		__this->set_createdMaterials_5(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material UnityStandardAssets.ImageEffects.PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* Material_t193706927_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m1886901167_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1614939434;
extern Il2CppCodeGenString* _stringLiteral3335173538;
extern Il2CppCodeGenString* _stringLiteral3324069660;
extern Il2CppCodeGenString* _stringLiteral994760984;
extern const uint32_t PostEffectsBase_CheckShaderAndCreateMaterial_m1396976238_MetadataUsageId;
extern "C"  Material_t193706927 * PostEffectsBase_CheckShaderAndCreateMaterial_m1396976238 (PostEffectsBase_t2152133263 * __this, Shader_t2430389951 * ___s0, Material_t193706927 * ___m2Create1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PostEffectsBase_CheckShaderAndCreateMaterial_m1396976238_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Shader_t2430389951 * L_0 = ___s0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0029;
		}
	}
	{
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1614939434, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Behaviour_set_enabled_m1796096907(__this, (bool)0, /*hidden argument*/NULL);
		return (Material_t193706927 *)NULL;
	}

IL_0029:
	{
		Shader_t2430389951 * L_4 = ___s0;
		NullCheck(L_4);
		bool L_5 = Shader_get_isSupported_m344486701(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0052;
		}
	}
	{
		Material_t193706927 * L_6 = ___m2Create1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0052;
		}
	}
	{
		Material_t193706927 * L_8 = ___m2Create1;
		NullCheck(L_8);
		Shader_t2430389951 * L_9 = Material_get_shader_m2320486867(L_8, /*hidden argument*/NULL);
		Shader_t2430389951 * L_10 = ___s0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0052;
		}
	}
	{
		Material_t193706927 * L_12 = ___m2Create1;
		return L_12;
	}

IL_0052:
	{
		Shader_t2430389951 * L_13 = ___s0;
		NullCheck(L_13);
		bool L_14 = Shader_get_isSupported_m344486701(L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_009f;
		}
	}
	{
		PostEffectsBase_NotSupported_m3628363867(__this, /*hidden argument*/NULL);
		StringU5BU5D_t1642385972* L_15 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral3335173538);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3335173538);
		StringU5BU5D_t1642385972* L_16 = L_15;
		Shader_t2430389951 * L_17 = ___s0;
		NullCheck(L_17);
		String_t* L_18 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_17);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_18);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_18);
		StringU5BU5D_t1642385972* L_19 = L_16;
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, _stringLiteral3324069660);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral3324069660);
		StringU5BU5D_t1642385972* L_20 = L_19;
		String_t* L_21 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, __this);
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_21);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_21);
		StringU5BU5D_t1642385972* L_22 = L_20;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, _stringLiteral994760984);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral994760984);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m626692867(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		return (Material_t193706927 *)NULL;
	}

IL_009f:
	{
		Shader_t2430389951 * L_24 = ___s0;
		Material_t193706927 * L_25 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1897560860(L_25, L_24, /*hidden argument*/NULL);
		___m2Create1 = L_25;
		List_1_t3857795355 * L_26 = __this->get_createdMaterials_5();
		Material_t193706927 * L_27 = ___m2Create1;
		NullCheck(L_26);
		List_1_Add_m1886901167(L_26, L_27, /*hidden argument*/List_1_Add_m1886901167_MethodInfo_var);
		Material_t193706927 * L_28 = ___m2Create1;
		NullCheck(L_28);
		Object_set_hideFlags_m2204253440(L_28, ((int32_t)52), /*hidden argument*/NULL);
		Material_t193706927 * L_29 = ___m2Create1;
		return L_29;
	}
}
// UnityEngine.Material UnityStandardAssets.ImageEffects.PostEffectsBase::CreateMaterial(UnityEngine.Shader,UnityEngine.Material)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* Material_t193706927_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m1886901167_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1614939434;
extern const uint32_t PostEffectsBase_CreateMaterial_m4276295864_MetadataUsageId;
extern "C"  Material_t193706927 * PostEffectsBase_CreateMaterial_m4276295864 (PostEffectsBase_t2152133263 * __this, Shader_t2430389951 * ___s0, Material_t193706927 * ___m2Create1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PostEffectsBase_CreateMaterial_m4276295864_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Shader_t2430389951 * L_0 = ___s0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1614939434, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return (Material_t193706927 *)NULL;
	}

IL_0022:
	{
		Material_t193706927 * L_4 = ___m2Create1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_004b;
		}
	}
	{
		Material_t193706927 * L_6 = ___m2Create1;
		NullCheck(L_6);
		Shader_t2430389951 * L_7 = Material_get_shader_m2320486867(L_6, /*hidden argument*/NULL);
		Shader_t2430389951 * L_8 = ___s0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_004b;
		}
	}
	{
		Shader_t2430389951 * L_10 = ___s0;
		NullCheck(L_10);
		bool L_11 = Shader_get_isSupported_m344486701(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_004b;
		}
	}
	{
		Material_t193706927 * L_12 = ___m2Create1;
		return L_12;
	}

IL_004b:
	{
		Shader_t2430389951 * L_13 = ___s0;
		NullCheck(L_13);
		bool L_14 = Shader_get_isSupported_m344486701(L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_0058;
		}
	}
	{
		return (Material_t193706927 *)NULL;
	}

IL_0058:
	{
		Shader_t2430389951 * L_15 = ___s0;
		Material_t193706927 * L_16 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1897560860(L_16, L_15, /*hidden argument*/NULL);
		___m2Create1 = L_16;
		List_1_t3857795355 * L_17 = __this->get_createdMaterials_5();
		Material_t193706927 * L_18 = ___m2Create1;
		NullCheck(L_17);
		List_1_Add_m1886901167(L_17, L_18, /*hidden argument*/List_1_Add_m1886901167_MethodInfo_var);
		Material_t193706927 * L_19 = ___m2Create1;
		NullCheck(L_19);
		Object_set_hideFlags_m2204253440(L_19, ((int32_t)52), /*hidden argument*/NULL);
		Material_t193706927 * L_20 = ___m2Create1;
		return L_20;
	}
}
// System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::OnEnable()
extern "C"  void PostEffectsBase_OnEnable_m2430438472 (PostEffectsBase_t2152133263 * __this, const MethodInfo* method)
{
	{
		__this->set_isSupported_4((bool)1);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::OnDestroy()
extern "C"  void PostEffectsBase_OnDestroy_m2991969835 (PostEffectsBase_t2152133263 * __this, const MethodInfo* method)
{
	{
		PostEffectsBase_RemoveCreatedMaterials_m3648892036(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::RemoveCreatedMaterials()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m3742330308_MethodInfo_var;
extern const MethodInfo* List_1_RemoveAt_m3301597985_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3177347435_MethodInfo_var;
extern const uint32_t PostEffectsBase_RemoveCreatedMaterials_m3648892036_MetadataUsageId;
extern "C"  void PostEffectsBase_RemoveCreatedMaterials_m3648892036 (PostEffectsBase_t2152133263 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PostEffectsBase_RemoveCreatedMaterials_m3648892036_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Material_t193706927 * V_0 = NULL;
	{
		goto IL_0024;
	}

IL_0005:
	{
		List_1_t3857795355 * L_0 = __this->get_createdMaterials_5();
		NullCheck(L_0);
		Material_t193706927 * L_1 = List_1_get_Item_m3742330308(L_0, 0, /*hidden argument*/List_1_get_Item_m3742330308_MethodInfo_var);
		V_0 = L_1;
		List_1_t3857795355 * L_2 = __this->get_createdMaterials_5();
		NullCheck(L_2);
		List_1_RemoveAt_m3301597985(L_2, 0, /*hidden argument*/List_1_RemoveAt_m3301597985_MethodInfo_var);
		Material_t193706927 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0024:
	{
		List_1_t3857795355 * L_4 = __this->get_createdMaterials_5();
		NullCheck(L_4);
		int32_t L_5 = List_1_get_Count_m3177347435(L_4, /*hidden argument*/List_1_get_Count_m3177347435_MethodInfo_var);
		if ((((int32_t)L_5) > ((int32_t)0)))
		{
			goto IL_0005;
		}
	}
	{
		return;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckSupport()
extern "C"  bool PostEffectsBase_CheckSupport_m1953254801 (PostEffectsBase_t2152133263 * __this, const MethodInfo* method)
{
	{
		bool L_0 = PostEffectsBase_CheckSupport_m286341078(__this, (bool)0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckResources()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3419527273;
extern Il2CppCodeGenString* _stringLiteral1500926541;
extern const uint32_t PostEffectsBase_CheckResources_m3232165183_MetadataUsageId;
extern "C"  bool PostEffectsBase_CheckResources_m3232165183 (PostEffectsBase_t2152133263 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PostEffectsBase_CheckResources_m3232165183_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral3419527273, L_0, _stringLiteral1500926541, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		bool L_2 = __this->get_isSupported_4();
		return L_2;
	}
}
// System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::Start()
extern "C"  void PostEffectsBase_Start_m3321895624 (PostEffectsBase_t2152133263 * __this, const MethodInfo* method)
{
	{
		VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckResources() */, __this);
		return;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckSupport(System.Boolean)
extern const MethodInfo* Component_GetComponent_TisCamera_t189460977_m3276577584_MethodInfo_var;
extern const uint32_t PostEffectsBase_CheckSupport_m286341078_MetadataUsageId;
extern "C"  bool PostEffectsBase_CheckSupport_m286341078 (PostEffectsBase_t2152133263 * __this, bool ___needDepth0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PostEffectsBase_CheckSupport_m286341078_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PostEffectsBase_t2152133263 * G_B2_0 = NULL;
	PostEffectsBase_t2152133263 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	PostEffectsBase_t2152133263 * G_B3_1 = NULL;
	{
		__this->set_isSupported_4((bool)1);
		bool L_0 = SystemInfo_SupportsRenderTextureFormat_m3185388893(NULL /*static, unused*/, 2, /*hidden argument*/NULL);
		__this->set_supportHDRTextures_2(L_0);
		int32_t L_1 = SystemInfo_get_graphicsShaderLevel_m3553502635(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if ((((int32_t)L_1) < ((int32_t)((int32_t)50))))
		{
			G_B2_0 = __this;
			goto IL_0027;
		}
	}
	{
		bool L_2 = SystemInfo_get_supportsComputeShaders_m396355012(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_2));
		G_B3_1 = G_B1_0;
		goto IL_0028;
	}

IL_0027:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_0028:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_supportDX11_3((bool)G_B3_0);
		bool L_3 = SystemInfo_get_supportsImageEffects_m406299852(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_003f;
		}
	}
	{
		PostEffectsBase_NotSupported_m3628363867(__this, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_003f:
	{
		bool L_4 = ___needDepth0;
		if (!L_4)
		{
			goto IL_0058;
		}
	}
	{
		bool L_5 = SystemInfo_SupportsRenderTextureFormat_m3185388893(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0058;
		}
	}
	{
		PostEffectsBase_NotSupported_m3628363867(__this, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_0058:
	{
		bool L_6 = ___needDepth0;
		if (!L_6)
		{
			goto IL_0071;
		}
	}
	{
		Camera_t189460977 * L_7 = Component_GetComponent_TisCamera_t189460977_m3276577584(__this, /*hidden argument*/Component_GetComponent_TisCamera_t189460977_m3276577584_MethodInfo_var);
		Camera_t189460977 * L_8 = L_7;
		NullCheck(L_8);
		int32_t L_9 = Camera_get_depthTextureMode_m924131993(L_8, /*hidden argument*/NULL);
		NullCheck(L_8);
		Camera_set_depthTextureMode_m1269215020(L_8, ((int32_t)((int32_t)L_9|(int32_t)1)), /*hidden argument*/NULL);
	}

IL_0071:
	{
		return (bool)1;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckSupport(System.Boolean,System.Boolean)
extern "C"  bool PostEffectsBase_CheckSupport_m1834872057 (PostEffectsBase_t2152133263 * __this, bool ___needDepth0, bool ___needHdr1, const MethodInfo* method)
{
	{
		bool L_0 = ___needDepth0;
		bool L_1 = PostEffectsBase_CheckSupport_m286341078(__this, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		return (bool)0;
	}

IL_000e:
	{
		bool L_2 = ___needHdr1;
		if (!L_2)
		{
			goto IL_0027;
		}
	}
	{
		bool L_3 = __this->get_supportHDRTextures_2();
		if (L_3)
		{
			goto IL_0027;
		}
	}
	{
		PostEffectsBase_NotSupported_m3628363867(__this, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_0027:
	{
		return (bool)1;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::Dx11Support()
extern "C"  bool PostEffectsBase_Dx11Support_m4161884587 (PostEffectsBase_t2152133263 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_supportDX11_3();
		return L_0;
	}
}
// System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::ReportAutoDisable()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1665515977;
extern Il2CppCodeGenString* _stringLiteral3518994444;
extern const uint32_t PostEffectsBase_ReportAutoDisable_m2502513401_MetadataUsageId;
extern "C"  void PostEffectsBase_ReportAutoDisable_m2502513401 (PostEffectsBase_t2152133263 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PostEffectsBase_ReportAutoDisable_m2502513401_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral1665515977, L_0, _stringLiteral3518994444, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckShader(UnityEngine.Shader)
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3335173538;
extern Il2CppCodeGenString* _stringLiteral3324069660;
extern Il2CppCodeGenString* _stringLiteral249731859;
extern const uint32_t PostEffectsBase_CheckShader_m3354041777_MetadataUsageId;
extern "C"  bool PostEffectsBase_CheckShader_m3354041777 (PostEffectsBase_t2152133263 * __this, Shader_t2430389951 * ___s0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PostEffectsBase_CheckShader_m3354041777_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_t1642385972* L_0 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral3335173538);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3335173538);
		StringU5BU5D_t1642385972* L_1 = L_0;
		Shader_t2430389951 * L_2 = ___s0;
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t1642385972* L_4 = L_1;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral3324069660);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral3324069660);
		StringU5BU5D_t1642385972* L_5 = L_4;
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, __this);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_6);
		StringU5BU5D_t1642385972* L_7 = L_5;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral249731859);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral249731859);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m626692867(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		Shader_t2430389951 * L_9 = ___s0;
		NullCheck(L_9);
		bool L_10 = Shader_get_isSupported_m344486701(L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_004d;
		}
	}
	{
		PostEffectsBase_NotSupported_m3628363867(__this, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_004d:
	{
		return (bool)0;
	}
}
// System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::NotSupported()
extern "C"  void PostEffectsBase_NotSupported_m3628363867 (PostEffectsBase_t2152133263 * __this, const MethodInfo* method)
{
	{
		Behaviour_set_enabled_m1796096907(__this, (bool)0, /*hidden argument*/NULL);
		__this->set_isSupported_4((bool)0);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::DrawBorder(UnityEngine.RenderTexture,UnityEngine.Material)
extern "C"  void PostEffectsBase_DrawBorder_m1795230774 (PostEffectsBase_t2152133263 * __this, RenderTexture_t2666733923 * ___dest0, Material_t193706927 * ___material1, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	bool V_4 = false;
	int32_t V_5 = 0;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	{
		RenderTexture_t2666733923 * L_0 = ___dest0;
		RenderTexture_set_active_m55464043(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_4 = (bool)1;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_LoadOrtho_m3764403102(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_5 = 0;
		goto IL_0281;
	}

IL_001b:
	{
		Material_t193706927 * L_1 = ___material1;
		int32_t L_2 = V_5;
		NullCheck(L_1);
		Material_SetPass_m2448940266(L_1, L_2, /*hidden argument*/NULL);
		bool L_3 = V_4;
		if (!L_3)
		{
			goto IL_003e;
		}
	}
	{
		V_6 = (1.0f);
		V_7 = (0.0f);
		goto IL_004c;
	}

IL_003e:
	{
		V_6 = (0.0f);
		V_7 = (1.0f);
	}

IL_004c:
	{
		V_0 = (0.0f);
		RenderTexture_t2666733923 * L_4 = ___dest0;
		NullCheck(L_4);
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_4);
		V_1 = ((float)((float)(1.0f)/(float)((float)((float)(((float)((float)L_5)))*(float)(1.0f)))));
		V_2 = (0.0f);
		V_3 = (1.0f);
		GL_Begin_m3874173032(NULL /*static, unused*/, 7, /*hidden argument*/NULL);
		float L_6 = V_6;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (0.0f), L_6, /*hidden argument*/NULL);
		float L_7 = V_0;
		float L_8 = V_2;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_7, L_8, (0.1f), /*hidden argument*/NULL);
		float L_9 = V_6;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (1.0f), L_9, /*hidden argument*/NULL);
		float L_10 = V_1;
		float L_11 = V_2;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_10, L_11, (0.1f), /*hidden argument*/NULL);
		float L_12 = V_7;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (1.0f), L_12, /*hidden argument*/NULL);
		float L_13 = V_1;
		float L_14 = V_3;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_13, L_14, (0.1f), /*hidden argument*/NULL);
		float L_15 = V_7;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (0.0f), L_15, /*hidden argument*/NULL);
		float L_16 = V_0;
		float L_17 = V_3;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_16, L_17, (0.1f), /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_18 = ___dest0;
		NullCheck(L_18);
		int32_t L_19 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_18);
		V_0 = ((float)((float)(1.0f)-(float)((float)((float)(1.0f)/(float)((float)((float)(((float)((float)L_19)))*(float)(1.0f)))))));
		V_1 = (1.0f);
		V_2 = (0.0f);
		V_3 = (1.0f);
		float L_20 = V_6;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (0.0f), L_20, /*hidden argument*/NULL);
		float L_21 = V_0;
		float L_22 = V_2;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_21, L_22, (0.1f), /*hidden argument*/NULL);
		float L_23 = V_6;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (1.0f), L_23, /*hidden argument*/NULL);
		float L_24 = V_1;
		float L_25 = V_2;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_24, L_25, (0.1f), /*hidden argument*/NULL);
		float L_26 = V_7;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (1.0f), L_26, /*hidden argument*/NULL);
		float L_27 = V_1;
		float L_28 = V_3;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_27, L_28, (0.1f), /*hidden argument*/NULL);
		float L_29 = V_7;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (0.0f), L_29, /*hidden argument*/NULL);
		float L_30 = V_0;
		float L_31 = V_3;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_30, L_31, (0.1f), /*hidden argument*/NULL);
		V_0 = (0.0f);
		V_1 = (1.0f);
		V_2 = (0.0f);
		RenderTexture_t2666733923 * L_32 = ___dest0;
		NullCheck(L_32);
		int32_t L_33 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_32);
		V_3 = ((float)((float)(1.0f)/(float)((float)((float)(((float)((float)L_33)))*(float)(1.0f)))));
		float L_34 = V_6;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (0.0f), L_34, /*hidden argument*/NULL);
		float L_35 = V_0;
		float L_36 = V_2;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_35, L_36, (0.1f), /*hidden argument*/NULL);
		float L_37 = V_6;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (1.0f), L_37, /*hidden argument*/NULL);
		float L_38 = V_1;
		float L_39 = V_2;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_38, L_39, (0.1f), /*hidden argument*/NULL);
		float L_40 = V_7;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (1.0f), L_40, /*hidden argument*/NULL);
		float L_41 = V_1;
		float L_42 = V_3;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_41, L_42, (0.1f), /*hidden argument*/NULL);
		float L_43 = V_7;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (0.0f), L_43, /*hidden argument*/NULL);
		float L_44 = V_0;
		float L_45 = V_3;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_44, L_45, (0.1f), /*hidden argument*/NULL);
		V_0 = (0.0f);
		V_1 = (1.0f);
		RenderTexture_t2666733923 * L_46 = ___dest0;
		NullCheck(L_46);
		int32_t L_47 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_46);
		V_2 = ((float)((float)(1.0f)-(float)((float)((float)(1.0f)/(float)((float)((float)(((float)((float)L_47)))*(float)(1.0f)))))));
		V_3 = (1.0f);
		float L_48 = V_6;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (0.0f), L_48, /*hidden argument*/NULL);
		float L_49 = V_0;
		float L_50 = V_2;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_49, L_50, (0.1f), /*hidden argument*/NULL);
		float L_51 = V_6;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (1.0f), L_51, /*hidden argument*/NULL);
		float L_52 = V_1;
		float L_53 = V_2;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_52, L_53, (0.1f), /*hidden argument*/NULL);
		float L_54 = V_7;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (1.0f), L_54, /*hidden argument*/NULL);
		float L_55 = V_1;
		float L_56 = V_3;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_55, L_56, (0.1f), /*hidden argument*/NULL);
		float L_57 = V_7;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (0.0f), L_57, /*hidden argument*/NULL);
		float L_58 = V_0;
		float L_59 = V_3;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_58, L_59, (0.1f), /*hidden argument*/NULL);
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_60 = V_5;
		V_5 = ((int32_t)((int32_t)L_60+(int32_t)1));
	}

IL_0281:
	{
		int32_t L_61 = V_5;
		Material_t193706927 * L_62 = ___material1;
		NullCheck(L_62);
		int32_t L_63 = Material_get_passCount_m1778920671(L_62, /*hidden argument*/NULL);
		if ((((int32_t)L_61) < ((int32_t)L_63)))
		{
			goto IL_001b;
		}
	}
	{
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.PostEffectsHelper::.ctor()
extern "C"  void PostEffectsHelper__ctor_m2923551651 (PostEffectsHelper_t1845967802 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.PostEffectsHelper::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3333882936;
extern const uint32_t PostEffectsHelper_OnRenderImage_m3279810143_MetadataUsageId;
extern "C"  void PostEffectsHelper_OnRenderImage_m3279810143 (PostEffectsHelper_t1845967802 * __this, RenderTexture_t2666733923 * ___source0, RenderTexture_t2666733923 * ___destination1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PostEffectsHelper_OnRenderImage_m3279810143_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3333882936, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.PostEffectsHelper::DrawLowLevelPlaneAlignedWithCamera(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityEngine.Material,UnityEngine.Camera)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4026354833;
extern const uint32_t PostEffectsHelper_DrawLowLevelPlaneAlignedWithCamera_m3544702975_MetadataUsageId;
extern "C"  void PostEffectsHelper_DrawLowLevelPlaneAlignedWithCamera_m3544702975 (Il2CppObject * __this /* static, unused */, float ___dist0, RenderTexture_t2666733923 * ___source1, RenderTexture_t2666733923 * ___dest2, Material_t193706927 * ___material3, Camera_t189460977 * ___cameraForProjectionMatrix4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PostEffectsHelper_DrawLowLevelPlaneAlignedWithCamera_m3544702975_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	int32_t V_10 = 0;
	float V_11 = 0.0f;
	float V_12 = 0.0f;
	{
		RenderTexture_t2666733923 * L_0 = ___dest2;
		RenderTexture_set_active_m55464043(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Material_t193706927 * L_1 = ___material3;
		RenderTexture_t2666733923 * L_2 = ___source1;
		NullCheck(L_1);
		Material_SetTexture_m141095205(L_1, _stringLiteral4026354833, L_2, /*hidden argument*/NULL);
		V_0 = (bool)1;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_LoadIdentity_m2076463132(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_3 = ___cameraForProjectionMatrix4;
		NullCheck(L_3);
		Matrix4x4_t2933234003  L_4 = Camera_get_projectionMatrix_m2365994324(L_3, /*hidden argument*/NULL);
		GL_LoadProjectionMatrix_m2842415498(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Camera_t189460977 * L_5 = ___cameraForProjectionMatrix4;
		NullCheck(L_5);
		float L_6 = Camera_get_fieldOfView_m3384007405(L_5, /*hidden argument*/NULL);
		V_1 = ((float)((float)((float)((float)L_6*(float)(0.5f)))*(float)(0.0174532924f)));
		float L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_8 = cosf(L_7);
		float L_9 = V_1;
		float L_10 = sinf(L_9);
		V_2 = ((float)((float)L_8/(float)L_10));
		Camera_t189460977 * L_11 = ___cameraForProjectionMatrix4;
		NullCheck(L_11);
		float L_12 = Camera_get_aspect_m935361871(L_11, /*hidden argument*/NULL);
		V_3 = L_12;
		float L_13 = V_3;
		float L_14 = V_2;
		V_4 = ((float)((float)L_13/(float)((-L_14))));
		float L_15 = V_3;
		float L_16 = V_2;
		V_5 = ((float)((float)L_15/(float)L_16));
		float L_17 = V_2;
		V_6 = ((float)((float)(1.0f)/(float)((-L_17))));
		float L_18 = V_2;
		V_7 = ((float)((float)(1.0f)/(float)L_18));
		V_8 = (1.0f);
		float L_19 = V_4;
		float L_20 = ___dist0;
		float L_21 = V_8;
		V_4 = ((float)((float)L_19*(float)((float)((float)L_20*(float)L_21))));
		float L_22 = V_5;
		float L_23 = ___dist0;
		float L_24 = V_8;
		V_5 = ((float)((float)L_22*(float)((float)((float)L_23*(float)L_24))));
		float L_25 = V_6;
		float L_26 = ___dist0;
		float L_27 = V_8;
		V_6 = ((float)((float)L_25*(float)((float)((float)L_26*(float)L_27))));
		float L_28 = V_7;
		float L_29 = ___dist0;
		float L_30 = V_8;
		V_7 = ((float)((float)L_28*(float)((float)((float)L_29*(float)L_30))));
		float L_31 = ___dist0;
		V_9 = ((-L_31));
		V_10 = 0;
		goto IL_0146;
	}

IL_00a9:
	{
		Material_t193706927 * L_32 = ___material3;
		int32_t L_33 = V_10;
		NullCheck(L_32);
		Material_SetPass_m2448940266(L_32, L_33, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 7, /*hidden argument*/NULL);
		bool L_34 = V_0;
		if (!L_34)
		{
			goto IL_00d1;
		}
	}
	{
		V_11 = (1.0f);
		V_12 = (0.0f);
		goto IL_00df;
	}

IL_00d1:
	{
		V_11 = (0.0f);
		V_12 = (1.0f);
	}

IL_00df:
	{
		float L_35 = V_11;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (0.0f), L_35, /*hidden argument*/NULL);
		float L_36 = V_4;
		float L_37 = V_6;
		float L_38 = V_9;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_36, L_37, L_38, /*hidden argument*/NULL);
		float L_39 = V_11;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (1.0f), L_39, /*hidden argument*/NULL);
		float L_40 = V_5;
		float L_41 = V_6;
		float L_42 = V_9;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_40, L_41, L_42, /*hidden argument*/NULL);
		float L_43 = V_12;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (1.0f), L_43, /*hidden argument*/NULL);
		float L_44 = V_5;
		float L_45 = V_7;
		float L_46 = V_9;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_44, L_45, L_46, /*hidden argument*/NULL);
		float L_47 = V_12;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (0.0f), L_47, /*hidden argument*/NULL);
		float L_48 = V_4;
		float L_49 = V_7;
		float L_50 = V_9;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_48, L_49, L_50, /*hidden argument*/NULL);
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_51 = V_10;
		V_10 = ((int32_t)((int32_t)L_51+(int32_t)1));
	}

IL_0146:
	{
		int32_t L_52 = V_10;
		Material_t193706927 * L_53 = ___material3;
		NullCheck(L_53);
		int32_t L_54 = Material_get_passCount_m1778920671(L_53, /*hidden argument*/NULL);
		if ((((int32_t)L_52) < ((int32_t)L_54)))
		{
			goto IL_00a9;
		}
	}
	{
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.PostEffectsHelper::DrawBorder(UnityEngine.RenderTexture,UnityEngine.Material)
extern "C"  void PostEffectsHelper_DrawBorder_m2048598613 (Il2CppObject * __this /* static, unused */, RenderTexture_t2666733923 * ___dest0, Material_t193706927 * ___material1, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	bool V_4 = false;
	int32_t V_5 = 0;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	{
		RenderTexture_t2666733923 * L_0 = ___dest0;
		RenderTexture_set_active_m55464043(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_4 = (bool)1;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_LoadOrtho_m3764403102(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_5 = 0;
		goto IL_0281;
	}

IL_001b:
	{
		Material_t193706927 * L_1 = ___material1;
		int32_t L_2 = V_5;
		NullCheck(L_1);
		Material_SetPass_m2448940266(L_1, L_2, /*hidden argument*/NULL);
		bool L_3 = V_4;
		if (!L_3)
		{
			goto IL_003e;
		}
	}
	{
		V_6 = (1.0f);
		V_7 = (0.0f);
		goto IL_004c;
	}

IL_003e:
	{
		V_6 = (0.0f);
		V_7 = (1.0f);
	}

IL_004c:
	{
		V_0 = (0.0f);
		RenderTexture_t2666733923 * L_4 = ___dest0;
		NullCheck(L_4);
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_4);
		V_1 = ((float)((float)(1.0f)/(float)((float)((float)(((float)((float)L_5)))*(float)(1.0f)))));
		V_2 = (0.0f);
		V_3 = (1.0f);
		GL_Begin_m3874173032(NULL /*static, unused*/, 7, /*hidden argument*/NULL);
		float L_6 = V_6;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (0.0f), L_6, /*hidden argument*/NULL);
		float L_7 = V_0;
		float L_8 = V_2;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_7, L_8, (0.1f), /*hidden argument*/NULL);
		float L_9 = V_6;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (1.0f), L_9, /*hidden argument*/NULL);
		float L_10 = V_1;
		float L_11 = V_2;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_10, L_11, (0.1f), /*hidden argument*/NULL);
		float L_12 = V_7;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (1.0f), L_12, /*hidden argument*/NULL);
		float L_13 = V_1;
		float L_14 = V_3;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_13, L_14, (0.1f), /*hidden argument*/NULL);
		float L_15 = V_7;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (0.0f), L_15, /*hidden argument*/NULL);
		float L_16 = V_0;
		float L_17 = V_3;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_16, L_17, (0.1f), /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_18 = ___dest0;
		NullCheck(L_18);
		int32_t L_19 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_18);
		V_0 = ((float)((float)(1.0f)-(float)((float)((float)(1.0f)/(float)((float)((float)(((float)((float)L_19)))*(float)(1.0f)))))));
		V_1 = (1.0f);
		V_2 = (0.0f);
		V_3 = (1.0f);
		float L_20 = V_6;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (0.0f), L_20, /*hidden argument*/NULL);
		float L_21 = V_0;
		float L_22 = V_2;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_21, L_22, (0.1f), /*hidden argument*/NULL);
		float L_23 = V_6;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (1.0f), L_23, /*hidden argument*/NULL);
		float L_24 = V_1;
		float L_25 = V_2;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_24, L_25, (0.1f), /*hidden argument*/NULL);
		float L_26 = V_7;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (1.0f), L_26, /*hidden argument*/NULL);
		float L_27 = V_1;
		float L_28 = V_3;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_27, L_28, (0.1f), /*hidden argument*/NULL);
		float L_29 = V_7;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (0.0f), L_29, /*hidden argument*/NULL);
		float L_30 = V_0;
		float L_31 = V_3;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_30, L_31, (0.1f), /*hidden argument*/NULL);
		V_0 = (0.0f);
		V_1 = (1.0f);
		V_2 = (0.0f);
		RenderTexture_t2666733923 * L_32 = ___dest0;
		NullCheck(L_32);
		int32_t L_33 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_32);
		V_3 = ((float)((float)(1.0f)/(float)((float)((float)(((float)((float)L_33)))*(float)(1.0f)))));
		float L_34 = V_6;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (0.0f), L_34, /*hidden argument*/NULL);
		float L_35 = V_0;
		float L_36 = V_2;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_35, L_36, (0.1f), /*hidden argument*/NULL);
		float L_37 = V_6;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (1.0f), L_37, /*hidden argument*/NULL);
		float L_38 = V_1;
		float L_39 = V_2;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_38, L_39, (0.1f), /*hidden argument*/NULL);
		float L_40 = V_7;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (1.0f), L_40, /*hidden argument*/NULL);
		float L_41 = V_1;
		float L_42 = V_3;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_41, L_42, (0.1f), /*hidden argument*/NULL);
		float L_43 = V_7;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (0.0f), L_43, /*hidden argument*/NULL);
		float L_44 = V_0;
		float L_45 = V_3;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_44, L_45, (0.1f), /*hidden argument*/NULL);
		V_0 = (0.0f);
		V_1 = (1.0f);
		RenderTexture_t2666733923 * L_46 = ___dest0;
		NullCheck(L_46);
		int32_t L_47 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_46);
		V_2 = ((float)((float)(1.0f)-(float)((float)((float)(1.0f)/(float)((float)((float)(((float)((float)L_47)))*(float)(1.0f)))))));
		V_3 = (1.0f);
		float L_48 = V_6;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (0.0f), L_48, /*hidden argument*/NULL);
		float L_49 = V_0;
		float L_50 = V_2;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_49, L_50, (0.1f), /*hidden argument*/NULL);
		float L_51 = V_6;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (1.0f), L_51, /*hidden argument*/NULL);
		float L_52 = V_1;
		float L_53 = V_2;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_52, L_53, (0.1f), /*hidden argument*/NULL);
		float L_54 = V_7;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (1.0f), L_54, /*hidden argument*/NULL);
		float L_55 = V_1;
		float L_56 = V_3;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_55, L_56, (0.1f), /*hidden argument*/NULL);
		float L_57 = V_7;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (0.0f), L_57, /*hidden argument*/NULL);
		float L_58 = V_0;
		float L_59 = V_3;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_58, L_59, (0.1f), /*hidden argument*/NULL);
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_60 = V_5;
		V_5 = ((int32_t)((int32_t)L_60+(int32_t)1));
	}

IL_0281:
	{
		int32_t L_61 = V_5;
		Material_t193706927 * L_62 = ___material1;
		NullCheck(L_62);
		int32_t L_63 = Material_get_passCount_m1778920671(L_62, /*hidden argument*/NULL);
		if ((((int32_t)L_61) < ((int32_t)L_63)))
		{
			goto IL_001b;
		}
	}
	{
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.ImageEffects.PostEffectsHelper::DrawLowLevelQuad(System.Single,System.Single,System.Single,System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityEngine.Material)
extern Il2CppCodeGenString* _stringLiteral4026354833;
extern const uint32_t PostEffectsHelper_DrawLowLevelQuad_m335766524_MetadataUsageId;
extern "C"  void PostEffectsHelper_DrawLowLevelQuad_m335766524 (Il2CppObject * __this /* static, unused */, float ___x10, float ___x21, float ___y12, float ___y23, RenderTexture_t2666733923 * ___source4, RenderTexture_t2666733923 * ___dest5, Material_t193706927 * ___material6, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PostEffectsHelper_DrawLowLevelQuad_m335766524_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		RenderTexture_t2666733923 * L_0 = ___dest5;
		RenderTexture_set_active_m55464043(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Material_t193706927 * L_1 = ___material6;
		RenderTexture_t2666733923 * L_2 = ___source4;
		NullCheck(L_1);
		Material_SetTexture_m141095205(L_1, _stringLiteral4026354833, L_2, /*hidden argument*/NULL);
		V_0 = (bool)1;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_LoadOrtho_m3764403102(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = 0;
		goto IL_00bf;
	}

IL_0028:
	{
		Material_t193706927 * L_3 = ___material6;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		Material_SetPass_m2448940266(L_3, L_4, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 7, /*hidden argument*/NULL);
		bool L_5 = V_0;
		if (!L_5)
		{
			goto IL_004e;
		}
	}
	{
		V_2 = (1.0f);
		V_3 = (0.0f);
		goto IL_005a;
	}

IL_004e:
	{
		V_2 = (0.0f);
		V_3 = (1.0f);
	}

IL_005a:
	{
		float L_6 = V_2;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (0.0f), L_6, /*hidden argument*/NULL);
		float L_7 = ___x10;
		float L_8 = ___y12;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_7, L_8, (0.1f), /*hidden argument*/NULL);
		float L_9 = V_2;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (1.0f), L_9, /*hidden argument*/NULL);
		float L_10 = ___x21;
		float L_11 = ___y12;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_10, L_11, (0.1f), /*hidden argument*/NULL);
		float L_12 = V_3;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (1.0f), L_12, /*hidden argument*/NULL);
		float L_13 = ___x21;
		float L_14 = ___y23;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_13, L_14, (0.1f), /*hidden argument*/NULL);
		float L_15 = V_3;
		GL_TexCoord2_m86781742(NULL /*static, unused*/, (0.0f), L_15, /*hidden argument*/NULL);
		float L_16 = ___x10;
		float L_17 = ___y23;
		GL_Vertex3_m3998822656(NULL /*static, unused*/, L_16, L_17, (0.1f), /*hidden argument*/NULL);
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_18 = V_1;
		V_1 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_00bf:
	{
		int32_t L_19 = V_1;
		Material_t193706927 * L_20 = ___material6;
		NullCheck(L_20);
		int32_t L_21 = Material_get_passCount_m1778920671(L_20, /*hidden argument*/NULL);
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0028;
		}
	}
	{
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
