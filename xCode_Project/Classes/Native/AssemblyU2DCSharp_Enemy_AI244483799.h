﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Turn_Manager
struct Turn_Manager_t2388373579;
// Color_Manager_Script
struct Color_Manager_Script_t4053987423;
// Goti_Status[]
struct Goti_StatusU5BU5D_t3854859849;
// System.Collections.Generic.List`1<Goti_Status>
struct List_1_t842991300;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Enemy_AI
struct  Enemy_AI_t244483799  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean Enemy_AI::rollDice
	bool ___rollDice_2;
	// Turn_Manager Enemy_AI::tm
	Turn_Manager_t2388373579 * ___tm_3;
	// Color_Manager_Script Enemy_AI::cms
	Color_Manager_Script_t4053987423 * ___cms_4;
	// Goti_Status[] Enemy_AI::myPieces
	Goti_StatusU5BU5D_t3854859849* ___myPieces_5;
	// Goti_Status[] Enemy_AI::enemyPieces
	Goti_StatusU5BU5D_t3854859849* ___enemyPieces_6;
	// System.Collections.Generic.List`1<Goti_Status> Enemy_AI::playAblePieces
	List_1_t842991300 * ___playAblePieces_7;
	// System.Collections.Generic.List`1<Goti_Status> Enemy_AI::closedPieces
	List_1_t842991300 * ___closedPieces_8;
	// System.Collections.Generic.List`1<Goti_Status> Enemy_AI::wonPieces
	List_1_t842991300 * ___wonPieces_9;
	// UnityEngine.Transform[] Enemy_AI::vicinityPath
	TransformU5BU5D_t3764228911* ___vicinityPath_10;

public:
	inline static int32_t get_offset_of_rollDice_2() { return static_cast<int32_t>(offsetof(Enemy_AI_t244483799, ___rollDice_2)); }
	inline bool get_rollDice_2() const { return ___rollDice_2; }
	inline bool* get_address_of_rollDice_2() { return &___rollDice_2; }
	inline void set_rollDice_2(bool value)
	{
		___rollDice_2 = value;
	}

	inline static int32_t get_offset_of_tm_3() { return static_cast<int32_t>(offsetof(Enemy_AI_t244483799, ___tm_3)); }
	inline Turn_Manager_t2388373579 * get_tm_3() const { return ___tm_3; }
	inline Turn_Manager_t2388373579 ** get_address_of_tm_3() { return &___tm_3; }
	inline void set_tm_3(Turn_Manager_t2388373579 * value)
	{
		___tm_3 = value;
		Il2CppCodeGenWriteBarrier(&___tm_3, value);
	}

	inline static int32_t get_offset_of_cms_4() { return static_cast<int32_t>(offsetof(Enemy_AI_t244483799, ___cms_4)); }
	inline Color_Manager_Script_t4053987423 * get_cms_4() const { return ___cms_4; }
	inline Color_Manager_Script_t4053987423 ** get_address_of_cms_4() { return &___cms_4; }
	inline void set_cms_4(Color_Manager_Script_t4053987423 * value)
	{
		___cms_4 = value;
		Il2CppCodeGenWriteBarrier(&___cms_4, value);
	}

	inline static int32_t get_offset_of_myPieces_5() { return static_cast<int32_t>(offsetof(Enemy_AI_t244483799, ___myPieces_5)); }
	inline Goti_StatusU5BU5D_t3854859849* get_myPieces_5() const { return ___myPieces_5; }
	inline Goti_StatusU5BU5D_t3854859849** get_address_of_myPieces_5() { return &___myPieces_5; }
	inline void set_myPieces_5(Goti_StatusU5BU5D_t3854859849* value)
	{
		___myPieces_5 = value;
		Il2CppCodeGenWriteBarrier(&___myPieces_5, value);
	}

	inline static int32_t get_offset_of_enemyPieces_6() { return static_cast<int32_t>(offsetof(Enemy_AI_t244483799, ___enemyPieces_6)); }
	inline Goti_StatusU5BU5D_t3854859849* get_enemyPieces_6() const { return ___enemyPieces_6; }
	inline Goti_StatusU5BU5D_t3854859849** get_address_of_enemyPieces_6() { return &___enemyPieces_6; }
	inline void set_enemyPieces_6(Goti_StatusU5BU5D_t3854859849* value)
	{
		___enemyPieces_6 = value;
		Il2CppCodeGenWriteBarrier(&___enemyPieces_6, value);
	}

	inline static int32_t get_offset_of_playAblePieces_7() { return static_cast<int32_t>(offsetof(Enemy_AI_t244483799, ___playAblePieces_7)); }
	inline List_1_t842991300 * get_playAblePieces_7() const { return ___playAblePieces_7; }
	inline List_1_t842991300 ** get_address_of_playAblePieces_7() { return &___playAblePieces_7; }
	inline void set_playAblePieces_7(List_1_t842991300 * value)
	{
		___playAblePieces_7 = value;
		Il2CppCodeGenWriteBarrier(&___playAblePieces_7, value);
	}

	inline static int32_t get_offset_of_closedPieces_8() { return static_cast<int32_t>(offsetof(Enemy_AI_t244483799, ___closedPieces_8)); }
	inline List_1_t842991300 * get_closedPieces_8() const { return ___closedPieces_8; }
	inline List_1_t842991300 ** get_address_of_closedPieces_8() { return &___closedPieces_8; }
	inline void set_closedPieces_8(List_1_t842991300 * value)
	{
		___closedPieces_8 = value;
		Il2CppCodeGenWriteBarrier(&___closedPieces_8, value);
	}

	inline static int32_t get_offset_of_wonPieces_9() { return static_cast<int32_t>(offsetof(Enemy_AI_t244483799, ___wonPieces_9)); }
	inline List_1_t842991300 * get_wonPieces_9() const { return ___wonPieces_9; }
	inline List_1_t842991300 ** get_address_of_wonPieces_9() { return &___wonPieces_9; }
	inline void set_wonPieces_9(List_1_t842991300 * value)
	{
		___wonPieces_9 = value;
		Il2CppCodeGenWriteBarrier(&___wonPieces_9, value);
	}

	inline static int32_t get_offset_of_vicinityPath_10() { return static_cast<int32_t>(offsetof(Enemy_AI_t244483799, ___vicinityPath_10)); }
	inline TransformU5BU5D_t3764228911* get_vicinityPath_10() const { return ___vicinityPath_10; }
	inline TransformU5BU5D_t3764228911** get_address_of_vicinityPath_10() { return &___vicinityPath_10; }
	inline void set_vicinityPath_10(TransformU5BU5D_t3764228911* value)
	{
		___vicinityPath_10 = value;
		Il2CppCodeGenWriteBarrier(&___vicinityPath_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
