﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// AdMob_Manager
struct AdMob_Manager_t3979489515;
// admobdemo
struct admobdemo_t4253778382;
// System.String
struct String_t;
// Arena_Manager
struct Arena_Manager_t4218382287;
// Color_Manager_Script
struct Color_Manager_Script_t4053987423;
// Goti_Status
struct Goti_Status_t1473870168;
// System.Object
struct Il2CppObject;
// Enemy_AI
struct Enemy_AI_t244483799;
// dance
struct dance_t2594252983;
// Dice_Script
struct Dice_Script_t640145349;
// Facebook.Unity.Example.AccessTokenMenu
struct AccessTokenMenu_t400501458;
// Facebook.Unity.Example.AppEvents
struct AppEvents_t3685765292;
// Facebook.Unity.Example.AppInvites
struct AppInvites_t3031115107;
// Facebook.Unity.Example.AppLinks
struct AppLinks_t3144027900;
// Facebook.Unity.Example.AppRequests
struct AppRequests_t1683098915;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2981576340;
// System.Collections.IEnumerable
struct IEnumerable_t2911409499;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// Facebook.Unity.Example.ConsoleBase
struct ConsoleBase_t4290192428;
// System.Collections.Generic.Stack`1<System.String>
struct Stack_1_t3116948387;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.GUIStyle
struct GUIStyle_t1799908754;
// System.Type
struct Type_t;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t2321347278;
// Facebook.Unity.Example.DialogShare
struct DialogShare_t329767461;
// Facebook.Unity.Example.GameGroups
struct GameGroups_t3348180240;
// Facebook.Unity.IGroupCreateResult
struct IGroupCreateResult_t2512549813;
// Facebook.Unity.IGraphResult
struct IGraphResult_t3984946686;
// Facebook.Unity.Example.GraphRequest
struct GraphRequest_t3721637485;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0
struct U3CTakeScreenshotU3Ec__Iterator0_t3616411949;
// Facebook.Unity.Example.LogView
struct LogView_t3192394209;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// Facebook.Unity.Example.MainMenu
struct MainMenu_t3906043390;
// Facebook.Unity.Example.MenuBase
struct MenuBase_t1506935956;
// Facebook.Unity.IResult
struct IResult_t3447678270;
// Facebook.Unity.Example.Pay
struct Pay_t1773509418;
// Game_Manager
struct Game_Manager_t4105539090;
// UnityEngine.Collider
struct Collider_t3497673348;
// WorldPos
struct WorldPos_t3236901198;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// Pow_Script
struct Pow_Script_t2091305652;
// Goti_Status/<CutGoti>c__Iterator0
struct U3CCutGotiU3Ec__Iterator0_t377100891;
// Main_Menu_Manager
struct Main_Menu_Manager_t1163327745;
// myFBHolder
struct myFBHolder_t218619452;
// Facebook.Unity.ILoginResult
struct ILoginResult_t403585443;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// Facebook.Unity.IShareResult
struct IShareResult_t830127229;
// Facebook.Unity.IAppInviteResult
struct IAppInviteResult_t3529555166;
// Facebook.Unity.IAppRequestResult
struct IAppRequestResult_t1874118006;
// Raycast_Manager
struct Raycast_Manager_t4268834365;
// Turn_Manager
struct Turn_Manager_t2388373579;
// Color_Manager_Script[]
struct Color_Manager_ScriptU5BU5D_t4081434438;
// UnityEngine.Animator
struct Animator_t69676727;
// UnityEngine.UI.Text
struct Text_t356221433;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DCSharp_AdMob_Manager3979489515.h"
#include "AssemblyU2DCSharp_AdMob_Manager3979489515MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_admob_Admob546240967MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_admob_Admob546240967.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharpU2Dfirstpass_admob_AdSize3770813302.h"
#include "AssemblyU2DCSharpU2Dfirstpass_admob_AdSize3770813302MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_admob_AdPosition1947228246.h"
#include "AssemblyU2DCSharpU2Dfirstpass_admob_AdPosition1947228246MethodDeclarations.h"
#include "mscorlib_System_Int322071877448.h"
#include "AssemblyU2DCSharp_admobdemo4253778382.h"
#include "AssemblyU2DCSharp_admobdemo4253778382MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_admob_Admob_AdmobEve2983421020MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_admob_Admob_AdmobEve2983421020.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "UnityEngine_UnityEngine_GUI4082743951MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Rect3681755626MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_admob_AdmobEvent4123996035.h"
#include "AssemblyU2DCSharpU2Dfirstpass_admob_AdmobEvent4123996035MethodDeclarations.h"
#include "AssemblyU2DCSharp_Arena_Manager4218382287.h"
#include "AssemblyU2DCSharp_Arena_Manager4218382287MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "AssemblyU2DCSharp_Game_Manager4105539090MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_ArrayTypes.h"
#include "AssemblyU2DCSharp_Game_Manager4105539090.h"
#include "AssemblyU2DCSharp_Color_Manager_Script4053987423.h"
#include "AssemblyU2DCSharp_Color_Manager_Script4053987423MethodDeclarations.h"
#include "AssemblyU2DCSharp_Goti_Status1473870168.h"
#include "AssemblyU2DCSharp_Dice_Script640145349.h"
#include "AssemblyU2DCSharp_Turn_Manager2388373579MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "AssemblyU2DCSharp_Enemy_AI244483799MethodDeclarations.h"
#include "AssemblyU2DCSharp_Goti_Status1473870168MethodDeclarations.h"
#include "AssemblyU2DCSharp_Turn_Manager2388373579.h"
#include "AssemblyU2DCSharp_Raycast_Manager4268834365.h"
#include "AssemblyU2DCSharp_Enemy_AI244483799.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "AssemblyU2DCSharp_Goti_Status_typeOfState402692453.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Behaviour955675639MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "AssemblyU2DCSharp_dance2594252983.h"
#include "AssemblyU2DCSharp_dance2594252983MethodDeclarations.h"
#include "AssemblyU2DCSharp_Dice_Script640145349MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2042527209.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Input1785128008MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time31991979MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf2336485820MethodDeclarations.h"
#include "UnityEngine_UnityEngine_KeyCode2283395152.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Random1170710517MethodDeclarations.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_List_1_gen842991300MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen842991300.h"
#include "UnityEngine_UnityEngine_Vector22243707579MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat377720974.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat377720974MethodDeclarations.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_AccessToke400501458.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_AccessToke400501458MethodDeclarations.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_MenuBase1506935956MethodDeclarations.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_ConsoleBa4290192428MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_FacebookDelegate_1_g1759649537MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_FB_Mobile3800732530MethodDeclarations.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_MenuBase1506935956.h"
#include "Facebook_Unity_Facebook_Unity_FacebookDelegate_1_g1759649537.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_AppEvents3685765292.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_AppEvents3685765292MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge309261261MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_FB1612658294MethodDeclarations.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_LogView3192394209MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen339576247.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge309261261.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_AppInvite3031115107.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_AppInvite3031115107MethodDeclarations.h"
#include "System_System_Uri19570940MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_FacebookDelegate_1_g2564900615MethodDeclarations.h"
#include "System_System_Uri19570940.h"
#include "Facebook_Unity_Facebook_Unity_FacebookDelegate_1_g2564900615.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_AppLinks3144027900.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_AppLinks3144027900MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_FacebookDelegate_1_g2578089594MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_Constants3246017429MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_FacebookDelegate_1_g2578089594.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_AppReques1683098915.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_AppReques1683098915MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_OGActionType1978093408.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_FacebookDelegate_1_ge909463455MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayout2579273657MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable2148412300MethodDeclarations.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427.h"
#include "mscorlib_System_Nullable_1_gen241159723.h"
#include "Facebook_Unity_Facebook_Unity_FacebookDelegate_1_ge909463455.h"
#include "mscorlib_System_Nullable_1_gen334943763MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayoutOption4183744904.h"
#include "UnityEngine_UnityEngine_GUIStyle1799908754.h"
#include "mscorlib_System_Nullable_1_gen241159723MethodDeclarations.h"
#include "mscorlib_System_Char3454481338.h"
#include "System_Core_System_Linq_Enumerable2148412300.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_ConsoleBa4290192428.h"
#include "UnityEngine_UnityEngine_Screen786852042MethodDeclarations.h"
#include "System_System_Collections_Generic_Stack_1_gen3116948387.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "mscorlib_System_Nullable_1_gen339576247MethodDeclarations.h"
#include "mscorlib_System_Math2022911894MethodDeclarations.h"
#include "mscorlib_System_Double4078015681.h"
#include "UnityEngine_UnityEngine_GUISkin1436893342MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyle1799908754MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectOffset3387826427MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUISkin1436893342.h"
#include "UnityEngine_UnityEngine_TextAnchor112990806.h"
#include "UnityEngine_UnityEngine_RectOffset3387826427.h"
#include "UnityEngine_UnityEngine_Application354826772MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScreenOrientation4019489636.h"
#include "mscorlib_System_Type1303803226.h"
#include "System_System_Collections_Generic_Stack_1_gen3116948387MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManager90660965MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_DialogShar329767461.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_DialogShar329767461MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_FacebookDelegate_1_g4160439974MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_FacebookDelegate_1_g4160439974.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_GameGroup3348180240.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_GameGroup3348180240MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_FacebookDelegate_1_g1547895262MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_FacebookDelegate_1_g1547895262.h"
#include "Facebook_Unity_Facebook_Unity_FacebookDelegate_1_g3020292135MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_FacebookDelegate_1_g3020292135.h"
#include "Facebook_Unity_Facebook_Unity_HttpMethod3673888207.h"
#include "Facebook_Unity_Facebook_Unity_FacebookDelegate_1_ge607253590MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_FacebookDelegate_1_ge607253590.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3943999495MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3943999495.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_GraphRequ3721637485.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_GraphRequ3721637485MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_GraphRequ3616411949MethodDeclarations.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_GraphRequ3616411949.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame1785723201MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWWForm3950226929MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame1785723201.h"
#include "UnityEngine_UnityEngine_TextureFormat1386130234.h"
#include "mscorlib_System_Byte3683104436.h"
#include "UnityEngine_UnityEngine_WWWForm3950226929.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_LogView3192394209.h"
#include "mscorlib_System_DateTime693205669MethodDeclarations.h"
#include "mscorlib_System_DateTime693205669.h"
#include "UnityEngine_UnityEngine_Touch407273883.h"
#include "UnityEngine_UnityEngine_Touch407273883MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TouchPhase2458120420.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_MainMenu3906043390.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_MainMenu3906043390MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_InitDelegate3410465555MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_HideUnityDelegate712804158MethodDeclarations.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_AccessToken2518141643MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_InitDelegate3410465555.h"
#include "Facebook_Unity_Facebook_Unity_HideUnityDelegate712804158.h"
#include "UnityEngine_UnityEngine_GUIContent4210063000.h"
#include "UnityEngine_UnityEngine_GUIContent4210063000MethodDeclarations.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "Facebook_Unity_Facebook_Unity_AccessToken2518141643.h"
#include "Facebook_Unity_Facebook_Unity_FacebookDelegate_1_g3733898188MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_FacebookDelegate_1_g3733898188.h"
#include "mscorlib_System_Enum2459695545MethodDeclarations.h"
#include "mscorlib_System_Array3829468939MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_ShareDialogMode1445392044.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_Pay1773509418.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_Pay1773509418MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_FacebookDelegate_1_g1895462303MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_FB_Canvas2415435652MethodDeclarations.h"
#include "Facebook_Unity_Facebook_Unity_FacebookDelegate_1_g1895462303.h"
#include "UnityEngine_UnityEngine_Camera189460977MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animator69676727MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1612828712MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animator69676727.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1612828712.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623.h"
#include "AssemblyU2DCSharp_Goti_Status_U3CCutGotiU3Ec__Itera377100891MethodDeclarations.h"
#include "AssemblyU2DCSharp_Goti_Status_U3CCutGotiU3Ec__Itera377100891.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"
#include "AssemblyU2DCSharp_WorldPos3236901198.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "AssemblyU2DCSharp_Pow_Script2091305652.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067.h"
#include "AssemblyU2DCSharp_Goti_Status_typeOfState402692453MethodDeclarations.h"
#include "AssemblyU2DCSharp_Main_Menu_Manager1163327745.h"
#include "AssemblyU2DCSharp_Main_Menu_Manager1163327745MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen897193173MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen3863924733MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_RawImage2749640213.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle3976754468.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleEvent1896830814.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen897193173.h"
#include "UnityEngine_UnityEngine_Color2020392075MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic2426225576MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic2426225576.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable1490392188MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2042527209MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Button2872111280.h"
#include "UnityEngine_UnityEngine_Sprite309593783.h"
#include "UnityEngine_UnityEngine_QualitySettings3238033062MethodDeclarations.h"
#include "AssemblyU2DCSharp_myFBHolder218619452.h"
#include "AssemblyU2DCSharp_myFBHolder218619452MethodDeclarations.h"
#include "Facebook_Unity_Facebook_MiniJSON_Json151732106MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Sprite309593783MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pow_Script2091305652MethodDeclarations.h"
#include "AssemblyU2DCSharp_Raycast_Manager4268834365MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Physics634932869MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"
#include "UnityEngine_UnityEngine_Ray2469606224.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3423108555MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3423108555.h"
#include "UnityEngine_UnityEngine_Renderer257310565MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material193706927MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer257310565.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2957838229.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2957838229MethodDeclarations.h"
#include "AssemblyU2DCSharp_WorldPos3236901198MethodDeclarations.h"

// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2812611596(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<Goti_Status>()
#define GameObject_GetComponent_TisGoti_Status_t1473870168_m3243051065(__this, method) ((  Goti_Status_t1473870168 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m4109961936(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<Enemy_AI>()
#define Component_GetComponent_TisEnemy_AI_t244483799_m3521222472(__this, method) ((  Enemy_AI_t244483799 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<Color_Manager_Script>()
#define Component_GetComponent_TisColor_Manager_Script_t4053987423_m2545735956(__this, method) ((  Color_Manager_Script_t4053987423 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::OfType<System.Object>(System.Collections.IEnumerable)
extern "C"  Il2CppObject* Enumerable_OfType_TisIl2CppObject_m32700024_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define Enumerable_OfType_TisIl2CppObject_m32700024(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Enumerable_OfType_TisIl2CppObject_m32700024_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  List_1_t2058570427 * Enumerable_ToList_TisIl2CppObject_m463720312_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_ToList_TisIl2CppObject_m463720312(__this /* static, unused */, p0, method) ((  List_1_t2058570427 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToList_TisIl2CppObject_m463720312_gshared)(__this /* static, unused */, p0, method)
// System.Boolean System.Linq.Enumerable::Any<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  bool Enumerable_Any_TisIl2CppObject_m1451474880_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_Any_TisIl2CppObject_m1451474880(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Any_TisIl2CppObject_m1451474880_gshared)(__this /* static, unused */, p0, method)
// System.Boolean System.Linq.Enumerable::Any<System.String>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_Any_TisString_t_m3014604563(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Any_TisIl2CppObject_m1451474880_gshared)(__this /* static, unused */, p0, method)
// !!0[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  ObjectU5BU5D_t3614634134* Enumerable_ToArray_TisIl2CppObject_m349912619_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_ToArray_TisIl2CppObject_m349912619(__this /* static, unused */, p0, method) ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisIl2CppObject_m349912619_gshared)(__this /* static, unused */, p0, method)
// !!0[] System.Linq.Enumerable::ToArray<System.String>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToArray_TisString_t_m1652456295(__this /* static, unused */, p0, method) ((  StringU5BU5D_t1642385972* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisIl2CppObject_m349912619_gshared)(__this /* static, unused */, p0, method)
// System.Boolean System.Linq.Enumerable::Contains<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,!!0)
extern "C"  bool Enumerable_Contains_TisIl2CppObject_m2438455970_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Il2CppObject * p1, const MethodInfo* method);
#define Enumerable_Contains_TisIl2CppObject_m2438455970(__this /* static, unused */, p0, p1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject *, const MethodInfo*))Enumerable_Contains_TisIl2CppObject_m2438455970_gshared)(__this /* static, unused */, p0, p1, method)
// System.Boolean System.Linq.Enumerable::Contains<System.String>(System.Collections.Generic.IEnumerable`1<!!0>,!!0)
#define Enumerable_Contains_TisString_t_m2925925544(__this /* static, unused */, p0, p1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, String_t*, const MethodInfo*))Enumerable_Contains_TisIl2CppObject_m2438455970_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 UnityEngine.Component::GetComponent<Goti_Status>()
#define Component_GetComponent_TisGoti_Status_t1473870168_m3287759645(__this, method) ((  Goti_Status_t1473870168 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<WorldPos>()
#define Component_GetComponent_TisWorldPos_t3236901198_m3876800169(__this, method) ((  WorldPos_t3236901198 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m3829784634_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Vector3_t2243707580  p1, Quaternion_t4030073918  p2, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m3829784634(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Vector3_t2243707580 , Quaternion_t4030073918 , const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3829784634_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
#define Object_Instantiate_TisGameObject_t1756533147_m3064851704(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, Vector3_t2243707580 , Quaternion_t4030073918 , const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3829784634_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 UnityEngine.GameObject::GetComponent<Pow_Script>()
#define GameObject_GetComponent_TisPow_Script_t2091305652_m2549334947(__this, method) ((  Pow_Script_t2091305652 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0[] UnityEngine.Object::FindObjectsOfType<System.Object>()
extern "C"  ObjectU5BU5D_t3614634134* Object_FindObjectsOfType_TisIl2CppObject_m1140156812_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Object_FindObjectsOfType_TisIl2CppObject_m1140156812(__this /* static, unused */, method) ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectsOfType_TisIl2CppObject_m1140156812_gshared)(__this /* static, unused */, method)
// !!0[] UnityEngine.Object::FindObjectsOfType<Color_Manager_Script>()
#define Object_FindObjectsOfType_TisColor_Manager_Script_t4053987423_m79499997(__this /* static, unused */, method) ((  Color_Manager_ScriptU5BU5D_t4081434438* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectsOfType_TisIl2CppObject_m1140156812_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
#define Component_GetComponent_TisAnimator_t69676727_m475627522(__this, method) ((  Animator_t69676727 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Text>()
#define Component_GetComponent_TisText_t356221433_m1342661039(__this, method) ((  Text_t356221433 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AdMob_Manager::.ctor()
extern "C"  void AdMob_Manager__ctor_m816106580 (AdMob_Manager_t3979489515 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdMob_Manager::set_Instance(AdMob_Manager)
extern Il2CppClass* AdMob_Manager_t3979489515_il2cpp_TypeInfo_var;
extern const uint32_t AdMob_Manager_set_Instance_m2033218227_MetadataUsageId;
extern "C"  void AdMob_Manager_set_Instance_m2033218227 (Il2CppObject * __this /* static, unused */, AdMob_Manager_t3979489515 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AdMob_Manager_set_Instance_m2033218227_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AdMob_Manager_t3979489515 * L_0 = ___value0;
		((AdMob_Manager_t3979489515_StaticFields*)AdMob_Manager_t3979489515_il2cpp_TypeInfo_var->static_fields)->set_U3CInstanceU3Ek__BackingField_6(L_0);
		return;
	}
}
// AdMob_Manager AdMob_Manager::get_Instance()
extern Il2CppClass* AdMob_Manager_t3979489515_il2cpp_TypeInfo_var;
extern const uint32_t AdMob_Manager_get_Instance_m2706670290_MetadataUsageId;
extern "C"  AdMob_Manager_t3979489515 * AdMob_Manager_get_Instance_m2706670290 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AdMob_Manager_get_Instance_m2706670290_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AdMob_Manager_t3979489515 * L_0 = ((AdMob_Manager_t3979489515_StaticFields*)AdMob_Manager_t3979489515_il2cpp_TypeInfo_var->static_fields)->get_U3CInstanceU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void AdMob_Manager::Start()
extern "C"  void AdMob_Manager_Start_m3573778096 (AdMob_Manager_t3979489515 * __this, const MethodInfo* method)
{
	{
		AdMob_Manager_set_Instance_m2033218227(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		Admob_t546240967 * L_0 = Admob_Instance_m352023099(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = __this->get_iosBanner_5();
		String_t* L_2 = __this->get_iosInterstitial_4();
		NullCheck(L_0);
		Admob_initAdmob_m1941857206(L_0, L_1, L_2, /*hidden argument*/NULL);
		Admob_t546240967 * L_3 = Admob_Instance_m352023099(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		Admob_loadInterstitial_m2602394663(L_3, /*hidden argument*/NULL);
		bool L_4 = __this->get_showBannerAtStart_7();
		if (!L_4)
		{
			goto IL_0037;
		}
	}
	{
		AdMob_Manager_ShowBannerAd_m1706154128(__this, /*hidden argument*/NULL);
	}

IL_0037:
	{
		return;
	}
}
// System.Void AdMob_Manager::ShowImageAd()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral17049343;
extern const uint32_t AdMob_Manager_ShowImageAd_m789447395_MetadataUsageId;
extern "C"  void AdMob_Manager_ShowImageAd_m789447395 (AdMob_Manager_t3979489515 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AdMob_Manager_ShowImageAd_m789447395_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Admob_t546240967 * L_0 = Admob_Instance_m352023099(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = Admob_isInterstitialReady_m3353041644(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		Admob_t546240967 * L_2 = Admob_Instance_m352023099(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Admob_showInterstitial_m2032088296(L_2, /*hidden argument*/NULL);
		goto IL_0028;
	}

IL_001e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral17049343, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void AdMob_Manager::ShowBannerAd()
extern Il2CppClass* AdSize_t3770813302_il2cpp_TypeInfo_var;
extern Il2CppClass* AdPosition_t1947228246_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1191327663;
extern const uint32_t AdMob_Manager_ShowBannerAd_m1706154128_MetadataUsageId;
extern "C"  void AdMob_Manager_ShowBannerAd_m1706154128 (AdMob_Manager_t3979489515 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AdMob_Manager_ShowBannerAd_m1706154128_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Admob_t546240967 * L_0 = Admob_Instance_m352023099(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = __this->get_iosBanner_5();
		String_t* L_2 = __this->get_iosInterstitial_4();
		NullCheck(L_0);
		Admob_initAdmob_m1941857206(L_0, L_1, L_2, /*hidden argument*/NULL);
		Admob_t546240967 * L_3 = Admob_Instance_m352023099(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AdSize_t3770813302_il2cpp_TypeInfo_var);
		AdSize_t3770813302 * L_4 = ((AdSize_t3770813302_StaticFields*)AdSize_t3770813302_il2cpp_TypeInfo_var->static_fields)->get_Banner_2();
		IL2CPP_RUNTIME_CLASS_INIT(AdPosition_t1947228246_il2cpp_TypeInfo_var);
		int32_t L_5 = ((AdPosition_t1947228246_StaticFields*)AdPosition_t1947228246_il2cpp_TypeInfo_var->static_fields)->get_BOTTOM_LEFT_7();
		NullCheck(L_3);
		Admob_showBannerRelative_m3640333681(L_3, L_4, L_5, 0, _stringLiteral1191327663, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdMob_Manager::ReGetInterstitial()
extern "C"  void AdMob_Manager_ReGetInterstitial_m990499913 (AdMob_Manager_t3979489515 * __this, const MethodInfo* method)
{
	{
		Admob_t546240967 * L_0 = Admob_Instance_m352023099(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Admob_loadInterstitial_m2602394663(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void admobdemo::.ctor()
extern "C"  void admobdemo__ctor_m2338502621 (admobdemo_t4253778382 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void admobdemo::Start()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3648873761;
extern const uint32_t admobdemo_Start_m1355362069_MetadataUsageId;
extern "C"  void admobdemo_Start_m1355362069 (admobdemo_t4253778382 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (admobdemo_Start_m1355362069_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3648873761, /*hidden argument*/NULL);
		admobdemo_initAdmob_m4197987538(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void admobdemo::Update()
extern "C"  void admobdemo_Update_m435938768 (admobdemo_t4253778382 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void admobdemo::initAdmob()
extern Il2CppClass* AdmobEventHandler_t2983421020_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* admobdemo_onBannerEvent_m3861622086_MethodInfo_var;
extern const MethodInfo* admobdemo_onInterstitialEvent_m100663414_MethodInfo_var;
extern const MethodInfo* admobdemo_onRewardedVideoEvent_m3014261907_MethodInfo_var;
extern const MethodInfo* admobdemo_onNativeBannerEvent_m2650432589_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral123592310;
extern Il2CppCodeGenString* _stringLiteral888516073;
extern Il2CppCodeGenString* _stringLiteral2594436863;
extern const uint32_t admobdemo_initAdmob_m4197987538_MetadataUsageId;
extern "C"  void admobdemo_initAdmob_m4197987538 (admobdemo_t4253778382 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (admobdemo_initAdmob_m4197987538_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Admob_t546240967 * L_0 = Admob_Instance_m352023099(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_ad_2(L_0);
		Admob_t546240967 * L_1 = __this->get_ad_2();
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)admobdemo_onBannerEvent_m3861622086_MethodInfo_var);
		AdmobEventHandler_t2983421020 * L_3 = (AdmobEventHandler_t2983421020 *)il2cpp_codegen_object_new(AdmobEventHandler_t2983421020_il2cpp_TypeInfo_var);
		AdmobEventHandler__ctor_m943312861(L_3, __this, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		Admob_add_bannerEventHandler_m1557830709(L_1, L_3, /*hidden argument*/NULL);
		Admob_t546240967 * L_4 = __this->get_ad_2();
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)admobdemo_onInterstitialEvent_m100663414_MethodInfo_var);
		AdmobEventHandler_t2983421020 * L_6 = (AdmobEventHandler_t2983421020 *)il2cpp_codegen_object_new(AdmobEventHandler_t2983421020_il2cpp_TypeInfo_var);
		AdmobEventHandler__ctor_m943312861(L_6, __this, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		Admob_add_interstitialEventHandler_m528202391(L_4, L_6, /*hidden argument*/NULL);
		Admob_t546240967 * L_7 = __this->get_ad_2();
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)admobdemo_onRewardedVideoEvent_m3014261907_MethodInfo_var);
		AdmobEventHandler_t2983421020 * L_9 = (AdmobEventHandler_t2983421020 *)il2cpp_codegen_object_new(AdmobEventHandler_t2983421020_il2cpp_TypeInfo_var);
		AdmobEventHandler__ctor_m943312861(L_9, __this, L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Admob_add_rewardedVideoEventHandler_m698753172(L_7, L_9, /*hidden argument*/NULL);
		Admob_t546240967 * L_10 = __this->get_ad_2();
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)(void*)admobdemo_onNativeBannerEvent_m2650432589_MethodInfo_var);
		AdmobEventHandler_t2983421020 * L_12 = (AdmobEventHandler_t2983421020 *)il2cpp_codegen_object_new(AdmobEventHandler_t2983421020_il2cpp_TypeInfo_var);
		AdmobEventHandler__ctor_m943312861(L_12, __this, L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		Admob_add_nativeBannerEventHandler_m2314856020(L_10, L_12, /*hidden argument*/NULL);
		Admob_t546240967 * L_13 = __this->get_ad_2();
		NullCheck(L_13);
		Admob_initAdmob_m1941857206(L_13, _stringLiteral123592310, _stringLiteral888516073, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2594436863, /*hidden argument*/NULL);
		return;
	}
}
// System.Void admobdemo::OnGUI()
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppClass* AdSize_t3770813302_il2cpp_TypeInfo_var;
extern Il2CppClass* AdPosition_t1947228246_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral812671165;
extern Il2CppCodeGenString* _stringLiteral390248577;
extern Il2CppCodeGenString* _stringLiteral2105427516;
extern Il2CppCodeGenString* _stringLiteral988059219;
extern Il2CppCodeGenString* _stringLiteral1191327663;
extern Il2CppCodeGenString* _stringLiteral2997813051;
extern Il2CppCodeGenString* _stringLiteral1590035544;
extern Il2CppCodeGenString* _stringLiteral1757247056;
extern Il2CppCodeGenString* _stringLiteral3105200936;
extern Il2CppCodeGenString* _stringLiteral2958410622;
extern Il2CppCodeGenString* _stringLiteral2699725072;
extern Il2CppCodeGenString* _stringLiteral2322719939;
extern const uint32_t admobdemo_OnGUI_m3236244459_MetadataUsageId;
extern "C"  void admobdemo_OnGUI_m3236244459 (admobdemo_t4253778382 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (admobdemo_OnGUI_m3236244459_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		Rect_t3681755626  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Rect__ctor_m1220545469(&L_0, (120.0f), (0.0f), (100.0f), (60.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_1 = GUI_Button_m3054448581(NULL /*static, unused*/, L_0, _stringLiteral812671165, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0053;
		}
	}
	{
		Admob_t546240967 * L_2 = __this->get_ad_2();
		NullCheck(L_2);
		bool L_3 = Admob_isInterstitialReady_m3353041644(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0048;
		}
	}
	{
		Admob_t546240967 * L_4 = __this->get_ad_2();
		NullCheck(L_4);
		Admob_showInterstitial_m2032088296(L_4, /*hidden argument*/NULL);
		goto IL_0053;
	}

IL_0048:
	{
		Admob_t546240967 * L_5 = __this->get_ad_2();
		NullCheck(L_5);
		Admob_loadInterstitial_m2602394663(L_5, /*hidden argument*/NULL);
	}

IL_0053:
	{
		Rect_t3681755626  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Rect__ctor_m1220545469(&L_6, (240.0f), (0.0f), (100.0f), (60.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_7 = GUI_Button_m3054448581(NULL /*static, unused*/, L_6, _stringLiteral390248577, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_00ab;
		}
	}
	{
		Admob_t546240967 * L_8 = __this->get_ad_2();
		NullCheck(L_8);
		bool L_9 = Admob_isRewardedVideoReady_m89423453(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_009b;
		}
	}
	{
		Admob_t546240967 * L_10 = __this->get_ad_2();
		NullCheck(L_10);
		Admob_showRewardedVideo_m908495731(L_10, /*hidden argument*/NULL);
		goto IL_00ab;
	}

IL_009b:
	{
		Admob_t546240967 * L_11 = __this->get_ad_2();
		NullCheck(L_11);
		Admob_loadRewardedVideo_m4191770420(L_11, _stringLiteral2105427516, /*hidden argument*/NULL);
	}

IL_00ab:
	{
		Rect_t3681755626  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Rect__ctor_m1220545469(&L_12, (0.0f), (100.0f), (100.0f), (60.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_13 = GUI_Button_m3054448581(NULL /*static, unused*/, L_12, _stringLiteral988059219, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00ed;
		}
	}
	{
		Admob_t546240967 * L_14 = Admob_Instance_m352023099(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AdSize_t3770813302_il2cpp_TypeInfo_var);
		AdSize_t3770813302 * L_15 = ((AdSize_t3770813302_StaticFields*)AdSize_t3770813302_il2cpp_TypeInfo_var->static_fields)->get_Banner_2();
		IL2CPP_RUNTIME_CLASS_INIT(AdPosition_t1947228246_il2cpp_TypeInfo_var);
		int32_t L_16 = ((AdPosition_t1947228246_StaticFields*)AdPosition_t1947228246_il2cpp_TypeInfo_var->static_fields)->get_BOTTOM_CENTER_8();
		NullCheck(L_14);
		Admob_showBannerRelative_m3640333681(L_14, L_15, L_16, 0, _stringLiteral1191327663, /*hidden argument*/NULL);
	}

IL_00ed:
	{
		Rect_t3681755626  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Rect__ctor_m1220545469(&L_17, (120.0f), (100.0f), (100.0f), (60.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_18 = GUI_Button_m3054448581(NULL /*static, unused*/, L_17, _stringLiteral2997813051, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_012f;
		}
	}
	{
		Admob_t546240967 * L_19 = Admob_Instance_m352023099(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AdSize_t3770813302_il2cpp_TypeInfo_var);
		AdSize_t3770813302 * L_20 = ((AdSize_t3770813302_StaticFields*)AdSize_t3770813302_il2cpp_TypeInfo_var->static_fields)->get_Banner_2();
		NullCheck(L_19);
		Admob_showBannerAbsolute_m450906656(L_19, L_20, 0, ((int32_t)300), _stringLiteral1191327663, /*hidden argument*/NULL);
	}

IL_012f:
	{
		Rect_t3681755626  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Rect__ctor_m1220545469(&L_21, (240.0f), (100.0f), (100.0f), (60.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_22 = GUI_Button_m3054448581(NULL /*static, unused*/, L_21, _stringLiteral1590035544, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0166;
		}
	}
	{
		Admob_t546240967 * L_23 = Admob_Instance_m352023099(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_23);
		Admob_removeBanner_m2206939851(L_23, _stringLiteral1191327663, /*hidden argument*/NULL);
	}

IL_0166:
	{
		V_0 = _stringLiteral1757247056;
		Rect_t3681755626  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Rect__ctor_m1220545469(&L_24, (0.0f), (200.0f), (100.0f), (60.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_25 = GUI_Button_m3054448581(NULL /*static, unused*/, L_24, _stringLiteral3105200936, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_01b6;
		}
	}
	{
		Admob_t546240967 * L_26 = Admob_Instance_m352023099(NULL /*static, unused*/, /*hidden argument*/NULL);
		AdSize_t3770813302 * L_27 = (AdSize_t3770813302 *)il2cpp_codegen_object_new(AdSize_t3770813302_il2cpp_TypeInfo_var);
		AdSize__ctor_m3068073238(L_27, ((int32_t)320), ((int32_t)120), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AdPosition_t1947228246_il2cpp_TypeInfo_var);
		int32_t L_28 = ((AdPosition_t1947228246_StaticFields*)AdPosition_t1947228246_il2cpp_TypeInfo_var->static_fields)->get_BOTTOM_CENTER_8();
		String_t* L_29 = V_0;
		NullCheck(L_26);
		Admob_showNativeBannerRelative_m4052082498(L_26, L_27, L_28, 0, L_29, _stringLiteral2958410622, /*hidden argument*/NULL);
	}

IL_01b6:
	{
		Rect_t3681755626  L_30;
		memset(&L_30, 0, sizeof(L_30));
		Rect__ctor_m1220545469(&L_30, (120.0f), (200.0f), (100.0f), (60.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_31 = GUI_Button_m3054448581(NULL /*static, unused*/, L_30, _stringLiteral2699725072, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_0200;
		}
	}
	{
		Admob_t546240967 * L_32 = Admob_Instance_m352023099(NULL /*static, unused*/, /*hidden argument*/NULL);
		AdSize_t3770813302 * L_33 = (AdSize_t3770813302 *)il2cpp_codegen_object_new(AdSize_t3770813302_il2cpp_TypeInfo_var);
		AdSize__ctor_m3068073238(L_33, ((int32_t)320), ((int32_t)120), /*hidden argument*/NULL);
		String_t* L_34 = V_0;
		NullCheck(L_32);
		Admob_showNativeBannerAbsolute_m2200024897(L_32, L_33, 0, ((int32_t)300), L_34, _stringLiteral2958410622, /*hidden argument*/NULL);
	}

IL_0200:
	{
		Rect_t3681755626  L_35;
		memset(&L_35, 0, sizeof(L_35));
		Rect__ctor_m1220545469(&L_35, (240.0f), (200.0f), (100.0f), (60.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_36 = GUI_Button_m3054448581(NULL /*static, unused*/, L_35, _stringLiteral2322719939, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_0237;
		}
	}
	{
		Admob_t546240967 * L_37 = Admob_Instance_m352023099(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_37);
		Admob_removeNativeBanner_m348786124(L_37, _stringLiteral2958410622, /*hidden argument*/NULL);
	}

IL_0237:
	{
		return;
	}
}
// System.Void admobdemo::onInterstitialEvent(System.String,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* AdmobEvent_t4123996035_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral845942689;
extern Il2CppCodeGenString* _stringLiteral816986750;
extern const uint32_t admobdemo_onInterstitialEvent_m100663414_MetadataUsageId;
extern "C"  void admobdemo_onInterstitialEvent_m100663414 (admobdemo_t4253778382 * __this, String_t* ___eventName0, String_t* ___msg1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (admobdemo_onInterstitialEvent_m100663414_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___eventName0;
		String_t* L_1 = ___msg1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m1561703559(NULL /*static, unused*/, _stringLiteral845942689, L_0, _stringLiteral816986750, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		String_t* L_3 = ___eventName0;
		IL2CPP_RUNTIME_CLASS_INIT(AdmobEvent_t4123996035_il2cpp_TypeInfo_var);
		String_t* L_4 = ((AdmobEvent_t4123996035_StaticFields*)AdmobEvent_t4123996035_il2cpp_TypeInfo_var->static_fields)->get_onAdLoaded_0();
		bool L_5 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0030;
		}
	}
	{
		Admob_t546240967 * L_6 = Admob_Instance_m352023099(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		Admob_showInterstitial_m2032088296(L_6, /*hidden argument*/NULL);
	}

IL_0030:
	{
		return;
	}
}
// System.Void admobdemo::onBannerEvent(System.String,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral145830601;
extern Il2CppCodeGenString* _stringLiteral816986750;
extern const uint32_t admobdemo_onBannerEvent_m3861622086_MetadataUsageId;
extern "C"  void admobdemo_onBannerEvent_m3861622086 (admobdemo_t4253778382 * __this, String_t* ___eventName0, String_t* ___msg1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (admobdemo_onBannerEvent_m3861622086_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___eventName0;
		String_t* L_1 = ___msg1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m1561703559(NULL /*static, unused*/, _stringLiteral145830601, L_0, _stringLiteral816986750, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void admobdemo::onRewardedVideoEvent(System.String,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2508308295;
extern Il2CppCodeGenString* _stringLiteral816986750;
extern const uint32_t admobdemo_onRewardedVideoEvent_m3014261907_MetadataUsageId;
extern "C"  void admobdemo_onRewardedVideoEvent_m3014261907 (admobdemo_t4253778382 * __this, String_t* ___eventName0, String_t* ___msg1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (admobdemo_onRewardedVideoEvent_m3014261907_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___eventName0;
		String_t* L_1 = ___msg1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m1561703559(NULL /*static, unused*/, _stringLiteral2508308295, L_0, _stringLiteral816986750, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void admobdemo::onNativeBannerEvent(System.String,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1070140576;
extern Il2CppCodeGenString* _stringLiteral816986750;
extern const uint32_t admobdemo_onNativeBannerEvent_m2650432589_MetadataUsageId;
extern "C"  void admobdemo_onNativeBannerEvent_m2650432589 (admobdemo_t4253778382 * __this, String_t* ___eventName0, String_t* ___msg1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (admobdemo_onNativeBannerEvent_m2650432589_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___eventName0;
		String_t* L_1 = ___msg1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m1561703559(NULL /*static, unused*/, _stringLiteral1070140576, L_0, _stringLiteral816986750, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Arena_Manager::.ctor()
extern "C"  void Arena_Manager__ctor_m3163061250 (Arena_Manager_t4218382287 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Arena_Manager::Start()
extern "C"  void Arena_Manager_Start_m3574877078 (Arena_Manager_t4218382287 * __this, const MethodInfo* method)
{
	GameObject_t1756533147 * V_0 = NULL;
	GameObjectU5BU5D_t3057952154* V_1 = NULL;
	int32_t V_2 = 0;
	{
		GameObjectU5BU5D_t3057952154* L_0 = __this->get_arenas_2();
		V_1 = L_0;
		V_2 = 0;
		goto IL_001d;
	}

IL_000e:
	{
		GameObjectU5BU5D_t3057952154* L_1 = V_1;
		int32_t L_2 = V_2;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		GameObject_t1756533147 * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = L_4;
		GameObject_t1756533147 * L_5 = V_0;
		NullCheck(L_5);
		GameObject_SetActive_m2887581199(L_5, (bool)0, /*hidden argument*/NULL);
		int32_t L_6 = V_2;
		V_2 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_001d:
	{
		int32_t L_7 = V_2;
		GameObjectU5BU5D_t3057952154* L_8 = V_1;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_000e;
		}
	}
	{
		GameObjectU5BU5D_t3057952154* L_9 = __this->get_arenas_2();
		Game_Manager_t4105539090 * L_10 = Game_Manager_get__gminstance_m2308798245(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		int32_t L_11 = L_10->get_arenaIndex_4();
		NullCheck(L_9);
		int32_t L_12 = L_11;
		GameObject_t1756533147 * L_13 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		NullCheck(L_13);
		GameObject_SetActive_m2887581199(L_13, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Color_Manager_Script::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObjectU5BU5D_t3057952154_il2cpp_TypeInfo_var;
extern const uint32_t Color_Manager_Script__ctor_m3095357310_MetadataUsageId;
extern "C"  void Color_Manager_Script__ctor_m3095357310 (Color_Manager_Script_t4053987423 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Color_Manager_Script__ctor_m3095357310_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_myName_2(L_0);
		__this->set_meriGotiya_4(((GameObjectU5BU5D_t3057952154*)SZArrayNew(GameObjectU5BU5D_t3057952154_il2cpp_TypeInfo_var, (uint32_t)4)));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Color_Manager_Script::Start()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3510846499;
extern Il2CppCodeGenString* _stringLiteral2395476974;
extern Il2CppCodeGenString* _stringLiteral777220966;
extern Il2CppCodeGenString* _stringLiteral3021629811;
extern const uint32_t Color_Manager_Script_Start_m3824912202_MetadataUsageId;
extern "C"  void Color_Manager_Script_Start_m3824912202 (Color_Manager_Script_t4053987423 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Color_Manager_Script_Start_m3824912202_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	GameObjectU5BU5D_t3057952154* V_1 = NULL;
	int32_t V_2 = 0;
	{
		GameObject_t1756533147 * L_0 = __this->get_OnWinObj_15();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		Game_Manager_t4105539090 * L_1 = Game_Manager_get__gminstance_m2308798245(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = Game_Manager_GetPlayers_m971560877(L_1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_00a2;
		}
	}
	{
		String_t* L_3 = __this->get_myName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_3, _stringLiteral3510846499, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003d;
		}
	}
	{
		__this->set_amIPlayin_3((bool)1);
		goto IL_0081;
	}

IL_003d:
	{
		String_t* L_5 = __this->get_myName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_5, _stringLiteral2395476974, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0065;
		}
	}
	{
		__this->set_amIPlayin_3((bool)1);
		__this->set_CPU_13((bool)1);
		goto IL_0081;
	}

IL_0065:
	{
		String_t* L_7 = __this->get_myName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_8 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_7, _stringLiteral777220966, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0081;
		}
	}
	{
		__this->set_amIPlayin_3((bool)0);
	}

IL_0081:
	{
		String_t* L_9 = __this->get_myName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_9, _stringLiteral3021629811, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_009d;
		}
	}
	{
		__this->set_amIPlayin_3((bool)0);
	}

IL_009d:
	{
		goto IL_024a;
	}

IL_00a2:
	{
		Game_Manager_t4105539090 * L_11 = Game_Manager_get__gminstance_m2308798245(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		int32_t L_12 = Game_Manager_GetPlayers_m971560877(L_11, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_12) == ((uint32_t)2))))
		{
			goto IL_0131;
		}
	}
	{
		String_t* L_13 = __this->get_myName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_14 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_13, _stringLiteral3510846499, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00d3;
		}
	}
	{
		__this->set_amIPlayin_3((bool)1);
		goto IL_0110;
	}

IL_00d3:
	{
		String_t* L_15 = __this->get_myName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_16 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_15, _stringLiteral2395476974, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00f4;
		}
	}
	{
		__this->set_amIPlayin_3((bool)1);
		goto IL_0110;
	}

IL_00f4:
	{
		String_t* L_17 = __this->get_myName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_18 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_17, _stringLiteral777220966, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0110;
		}
	}
	{
		__this->set_amIPlayin_3((bool)0);
	}

IL_0110:
	{
		String_t* L_19 = __this->get_myName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_20 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_19, _stringLiteral3021629811, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_012c;
		}
	}
	{
		__this->set_amIPlayin_3((bool)0);
	}

IL_012c:
	{
		goto IL_024a;
	}

IL_0131:
	{
		Game_Manager_t4105539090 * L_21 = Game_Manager_get__gminstance_m2308798245(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_21);
		int32_t L_22 = Game_Manager_GetPlayers_m971560877(L_21, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_22) == ((uint32_t)3))))
		{
			goto IL_01c0;
		}
	}
	{
		String_t* L_23 = __this->get_myName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_24 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_23, _stringLiteral3510846499, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_0162;
		}
	}
	{
		__this->set_amIPlayin_3((bool)1);
		goto IL_019f;
	}

IL_0162:
	{
		String_t* L_25 = __this->get_myName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_26 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_25, _stringLiteral2395476974, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_0183;
		}
	}
	{
		__this->set_amIPlayin_3((bool)1);
		goto IL_019f;
	}

IL_0183:
	{
		String_t* L_27 = __this->get_myName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_28 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_27, _stringLiteral777220966, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_019f;
		}
	}
	{
		__this->set_amIPlayin_3((bool)1);
	}

IL_019f:
	{
		String_t* L_29 = __this->get_myName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_30 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_29, _stringLiteral3021629811, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_01bb;
		}
	}
	{
		__this->set_amIPlayin_3((bool)0);
	}

IL_01bb:
	{
		goto IL_024a;
	}

IL_01c0:
	{
		Game_Manager_t4105539090 * L_31 = Game_Manager_get__gminstance_m2308798245(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_31);
		int32_t L_32 = Game_Manager_GetPlayers_m971560877(L_31, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_32) == ((uint32_t)4))))
		{
			goto IL_024a;
		}
	}
	{
		String_t* L_33 = __this->get_myName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_34 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_33, _stringLiteral3510846499, /*hidden argument*/NULL);
		if (!L_34)
		{
			goto IL_01f1;
		}
	}
	{
		__this->set_amIPlayin_3((bool)1);
		goto IL_022e;
	}

IL_01f1:
	{
		String_t* L_35 = __this->get_myName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_36 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_35, _stringLiteral2395476974, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_0212;
		}
	}
	{
		__this->set_amIPlayin_3((bool)1);
		goto IL_022e;
	}

IL_0212:
	{
		String_t* L_37 = __this->get_myName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_38 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_37, _stringLiteral777220966, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_022e;
		}
	}
	{
		__this->set_amIPlayin_3((bool)1);
	}

IL_022e:
	{
		String_t* L_39 = __this->get_myName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_40 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_39, _stringLiteral3021629811, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_024a;
		}
	}
	{
		__this->set_amIPlayin_3((bool)1);
	}

IL_024a:
	{
		GameObjectU5BU5D_t3057952154* L_41 = __this->get_meriGotiya_4();
		V_1 = L_41;
		V_2 = 0;
		goto IL_026c;
	}

IL_0258:
	{
		GameObjectU5BU5D_t3057952154* L_42 = V_1;
		int32_t L_43 = V_2;
		NullCheck(L_42);
		int32_t L_44 = L_43;
		GameObject_t1756533147 * L_45 = (L_42)->GetAt(static_cast<il2cpp_array_size_t>(L_44));
		V_0 = L_45;
		GameObject_t1756533147 * L_46 = V_0;
		bool L_47 = __this->get_amIPlayin_3();
		NullCheck(L_46);
		GameObject_SetActive_m2887581199(L_46, L_47, /*hidden argument*/NULL);
		int32_t L_48 = V_2;
		V_2 = ((int32_t)((int32_t)L_48+(int32_t)1));
	}

IL_026c:
	{
		int32_t L_49 = V_2;
		GameObjectU5BU5D_t3057952154* L_50 = V_1;
		NullCheck(L_50);
		if ((((int32_t)L_49) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_50)->max_length)))))))
		{
			goto IL_0258;
		}
	}
	{
		Color_Manager_Script_CalculatePlayables_m3860738715(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Color_Manager_Script::Update()
extern const MethodInfo* GameObject_GetComponent_TisGoti_Status_t1473870168_m3243051065_MethodInfo_var;
extern const uint32_t Color_Manager_Script_Update_m1168648999_MetadataUsageId;
extern "C"  void Color_Manager_Script_Update_m1168648999 (Color_Manager_Script_t4053987423 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Color_Manager_Script_Update_m1168648999_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_Manager_Script_t4053987423 * G_B4_0 = NULL;
	Color_Manager_Script_t4053987423 * G_B1_0 = NULL;
	Color_Manager_Script_t4053987423 * G_B2_0 = NULL;
	Color_Manager_Script_t4053987423 * G_B3_0 = NULL;
	int32_t G_B5_0 = 0;
	Color_Manager_Script_t4053987423 * G_B5_1 = NULL;
	{
		GameObjectU5BU5D_t3057952154* L_0 = __this->get_meriGotiya_4();
		NullCheck(L_0);
		int32_t L_1 = 0;
		GameObject_t1756533147 * L_2 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_1));
		NullCheck(L_2);
		Goti_Status_t1473870168 * L_3 = GameObject_GetComponent_TisGoti_Status_t1473870168_m3243051065(L_2, /*hidden argument*/GameObject_GetComponent_TisGoti_Status_t1473870168_m3243051065_MethodInfo_var);
		NullCheck(L_3);
		bool L_4 = L_3->get_canBePlayed_3();
		G_B1_0 = __this;
		if (L_4)
		{
			G_B4_0 = __this;
			goto IL_005a;
		}
	}
	{
		GameObjectU5BU5D_t3057952154* L_5 = __this->get_meriGotiya_4();
		NullCheck(L_5);
		int32_t L_6 = 1;
		GameObject_t1756533147 * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck(L_7);
		Goti_Status_t1473870168 * L_8 = GameObject_GetComponent_TisGoti_Status_t1473870168_m3243051065(L_7, /*hidden argument*/GameObject_GetComponent_TisGoti_Status_t1473870168_m3243051065_MethodInfo_var);
		NullCheck(L_8);
		bool L_9 = L_8->get_canBePlayed_3();
		G_B2_0 = G_B1_0;
		if (L_9)
		{
			G_B4_0 = G_B1_0;
			goto IL_005a;
		}
	}
	{
		GameObjectU5BU5D_t3057952154* L_10 = __this->get_meriGotiya_4();
		NullCheck(L_10);
		int32_t L_11 = 2;
		GameObject_t1756533147 * L_12 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_12);
		Goti_Status_t1473870168 * L_13 = GameObject_GetComponent_TisGoti_Status_t1473870168_m3243051065(L_12, /*hidden argument*/GameObject_GetComponent_TisGoti_Status_t1473870168_m3243051065_MethodInfo_var);
		NullCheck(L_13);
		bool L_14 = L_13->get_canBePlayed_3();
		G_B3_0 = G_B2_0;
		if (L_14)
		{
			G_B4_0 = G_B2_0;
			goto IL_005a;
		}
	}
	{
		GameObjectU5BU5D_t3057952154* L_15 = __this->get_meriGotiya_4();
		NullCheck(L_15);
		int32_t L_16 = 3;
		GameObject_t1756533147 * L_17 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		NullCheck(L_17);
		Goti_Status_t1473870168 * L_18 = GameObject_GetComponent_TisGoti_Status_t1473870168_m3243051065(L_17, /*hidden argument*/GameObject_GetComponent_TisGoti_Status_t1473870168_m3243051065_MethodInfo_var);
		NullCheck(L_18);
		bool L_19 = L_18->get_canBePlayed_3();
		G_B5_0 = ((int32_t)(L_19));
		G_B5_1 = G_B3_0;
		goto IL_005b;
	}

IL_005a:
	{
		G_B5_0 = 1;
		G_B5_1 = G_B4_0;
	}

IL_005b:
	{
		NullCheck(G_B5_1);
		G_B5_1->set_canIPlay_8((bool)G_B5_0);
		return;
	}
}
// System.Void Color_Manager_Script::StopTheDice()
extern Il2CppCodeGenString* _stringLiteral3678162929;
extern const uint32_t Color_Manager_Script_StopTheDice_m250861038_MetadataUsageId;
extern "C"  void Color_Manager_Script_StopTheDice_m250861038 (Color_Manager_Script_t4053987423 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Color_Manager_Script_StopTheDice_m250861038_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dice_Script_t640145349 * L_0 = __this->get_dc_5();
		NullCheck(L_0);
		int32_t L_1 = L_0->get_dInp_4();
		__this->set_diceInput_7(L_1);
		MonoBehaviour_Invoke_m666563676(__this, _stringLiteral3678162929, (0.5f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Color_Manager_Script::AfterDiceAction()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* Goti_Status_t1473870168_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisEnemy_AI_t244483799_m3521222472_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisGoti_Status_t1473870168_m3243051065_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4004194831;
extern Il2CppCodeGenString* _stringLiteral1105815766;
extern Il2CppCodeGenString* _stringLiteral1174380009;
extern const uint32_t Color_Manager_Script_AfterDiceAction_m3710082315_MetadataUsageId;
extern "C"  void Color_Manager_Script_AfterDiceAction_m3710082315 (Color_Manager_Script_t4053987423 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Color_Manager_Script_AfterDiceAction_m3710082315_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Goti_Status_t1473870168 * V_0 = NULL;
	GameObject_t1756533147 * V_1 = NULL;
	GameObjectU5BU5D_t3057952154* V_2 = NULL;
	int32_t V_3 = 0;
	GameObject_t1756533147 * V_4 = NULL;
	GameObjectU5BU5D_t3057952154* V_5 = NULL;
	int32_t V_6 = 0;
	{
		bool L_0 = __this->get_haveLost_17();
		if (L_0)
		{
			goto IL_012b;
		}
	}
	{
		bool L_1 = __this->get_canIPlay_8();
		if (L_1)
		{
			goto IL_003b;
		}
	}
	{
		String_t* L_2 = __this->get_myName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m2596409543(NULL /*static, unused*/, L_2, _stringLiteral4004194831, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Turn_Manager_t2388373579 * L_4 = __this->get_tm_6();
		NullCheck(L_4);
		Turn_Manager_PerformNextTurn_m2187029365(L_4, /*hidden argument*/NULL);
		goto IL_012b;
	}

IL_003b:
	{
		String_t* L_5 = __this->get_myName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral1105815766, L_5, _stringLiteral1174380009, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		Color_Manager_Script_CalculatePlayables_m3860738715(__this, /*hidden argument*/NULL);
		bool L_7 = __this->get_CPU_13();
		if (L_7)
		{
			goto IL_007c;
		}
	}
	{
		Raycast_Manager_t4268834365 * L_8 = __this->get_rm_9();
		NullCheck(L_8);
		GameObject_t1756533147 * L_9 = Component_get_gameObject_m3105766835(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		GameObject_SetActive_m2887581199(L_9, (bool)1, /*hidden argument*/NULL);
		goto IL_0098;
	}

IL_007c:
	{
		Raycast_Manager_t4268834365 * L_10 = __this->get_rm_9();
		NullCheck(L_10);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		GameObject_SetActive_m2887581199(L_11, (bool)0, /*hidden argument*/NULL);
		Enemy_AI_t244483799 * L_12 = Component_GetComponent_TisEnemy_AI_t244483799_m3521222472(__this, /*hidden argument*/Component_GetComponent_TisEnemy_AI_t244483799_m3521222472_MethodInfo_var);
		NullCheck(L_12);
		Enemy_AI_MovePiece_m1145793173(L_12, /*hidden argument*/NULL);
	}

IL_0098:
	{
		int32_t L_13 = __this->get_noOfPlaying_12();
		if ((!(((uint32_t)L_13) == ((uint32_t)1))))
		{
			goto IL_00e6;
		}
	}
	{
		Goti_Status_t1473870168 * L_14 = (Goti_Status_t1473870168 *)il2cpp_codegen_object_new(Goti_Status_t1473870168_il2cpp_TypeInfo_var);
		Goti_Status__ctor_m4251937469(L_14, /*hidden argument*/NULL);
		V_0 = L_14;
		GameObjectU5BU5D_t3057952154* L_15 = __this->get_meriGotiya_4();
		V_2 = L_15;
		V_3 = 0;
		goto IL_00d7;
	}

IL_00b8:
	{
		GameObjectU5BU5D_t3057952154* L_16 = V_2;
		int32_t L_17 = V_3;
		NullCheck(L_16);
		int32_t L_18 = L_17;
		GameObject_t1756533147 * L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		V_1 = L_19;
		GameObject_t1756533147 * L_20 = V_1;
		NullCheck(L_20);
		Goti_Status_t1473870168 * L_21 = GameObject_GetComponent_TisGoti_Status_t1473870168_m3243051065(L_20, /*hidden argument*/GameObject_GetComponent_TisGoti_Status_t1473870168_m3243051065_MethodInfo_var);
		NullCheck(L_21);
		bool L_22 = L_21->get_canBePlayed_3();
		if (!L_22)
		{
			goto IL_00d3;
		}
	}
	{
		GameObject_t1756533147 * L_23 = V_1;
		NullCheck(L_23);
		Goti_Status_t1473870168 * L_24 = GameObject_GetComponent_TisGoti_Status_t1473870168_m3243051065(L_23, /*hidden argument*/GameObject_GetComponent_TisGoti_Status_t1473870168_m3243051065_MethodInfo_var);
		V_0 = L_24;
	}

IL_00d3:
	{
		int32_t L_25 = V_3;
		V_3 = ((int32_t)((int32_t)L_25+(int32_t)1));
	}

IL_00d7:
	{
		int32_t L_26 = V_3;
		GameObjectU5BU5D_t3057952154* L_27 = V_2;
		NullCheck(L_27);
		if ((((int32_t)L_26) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_27)->max_length)))))))
		{
			goto IL_00b8;
		}
	}
	{
		Goti_Status_t1473870168 * L_28 = V_0;
		NullCheck(L_28);
		Goti_Status_PerformMovement_m702764983(L_28, /*hidden argument*/NULL);
	}

IL_00e6:
	{
		GameObjectU5BU5D_t3057952154* L_29 = __this->get_meriGotiya_4();
		V_5 = L_29;
		V_6 = 0;
		goto IL_0120;
	}

IL_00f6:
	{
		GameObjectU5BU5D_t3057952154* L_30 = V_5;
		int32_t L_31 = V_6;
		NullCheck(L_30);
		int32_t L_32 = L_31;
		GameObject_t1756533147 * L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		V_4 = L_33;
		GameObject_t1756533147 * L_34 = V_4;
		NullCheck(L_34);
		Goti_Status_t1473870168 * L_35 = GameObject_GetComponent_TisGoti_Status_t1473870168_m3243051065(L_34, /*hidden argument*/GameObject_GetComponent_TisGoti_Status_t1473870168_m3243051065_MethodInfo_var);
		NullCheck(L_35);
		bool L_36 = L_35->get_canBePlayed_3();
		if (!L_36)
		{
			goto IL_011a;
		}
	}
	{
		GameObject_t1756533147 * L_37 = V_4;
		NullCheck(L_37);
		Goti_Status_t1473870168 * L_38 = GameObject_GetComponent_TisGoti_Status_t1473870168_m3243051065(L_37, /*hidden argument*/GameObject_GetComponent_TisGoti_Status_t1473870168_m3243051065_MethodInfo_var);
		NullCheck(L_38);
		Goti_Status_ShowICanPlay_m3291303315(L_38, /*hidden argument*/NULL);
	}

IL_011a:
	{
		int32_t L_39 = V_6;
		V_6 = ((int32_t)((int32_t)L_39+(int32_t)1));
	}

IL_0120:
	{
		int32_t L_40 = V_6;
		GameObjectU5BU5D_t3057952154* L_41 = V_5;
		NullCheck(L_41);
		if ((((int32_t)L_40) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_41)->max_length)))))))
		{
			goto IL_00f6;
		}
	}

IL_012b:
	{
		return;
	}
}
// System.Void Color_Manager_Script::StopShowingAll()
extern const MethodInfo* GameObject_GetComponent_TisGoti_Status_t1473870168_m3243051065_MethodInfo_var;
extern const uint32_t Color_Manager_Script_StopShowingAll_m978555850_MetadataUsageId;
extern "C"  void Color_Manager_Script_StopShowingAll_m978555850 (Color_Manager_Script_t4053987423 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Color_Manager_Script_StopShowingAll_m978555850_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	GameObjectU5BU5D_t3057952154* V_1 = NULL;
	int32_t V_2 = 0;
	{
		GameObjectU5BU5D_t3057952154* L_0 = __this->get_meriGotiya_4();
		V_1 = L_0;
		V_2 = 0;
		goto IL_0021;
	}

IL_000e:
	{
		GameObjectU5BU5D_t3057952154* L_1 = V_1;
		int32_t L_2 = V_2;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		GameObject_t1756533147 * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = L_4;
		GameObject_t1756533147 * L_5 = V_0;
		NullCheck(L_5);
		Goti_Status_t1473870168 * L_6 = GameObject_GetComponent_TisGoti_Status_t1473870168_m3243051065(L_5, /*hidden argument*/GameObject_GetComponent_TisGoti_Status_t1473870168_m3243051065_MethodInfo_var);
		NullCheck(L_6);
		Goti_Status_StopShowing_m4087610708(L_6, /*hidden argument*/NULL);
		int32_t L_7 = V_2;
		V_2 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0021:
	{
		int32_t L_8 = V_2;
		GameObjectU5BU5D_t3057952154* L_9 = V_1;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_000e;
		}
	}
	{
		return;
	}
}
// System.Void Color_Manager_Script::CalculatePlayables()
extern const MethodInfo* GameObject_GetComponent_TisGoti_Status_t1473870168_m3243051065_MethodInfo_var;
extern const uint32_t Color_Manager_Script_CalculatePlayables_m3860738715_MetadataUsageId;
extern "C"  void Color_Manager_Script_CalculatePlayables_m3860738715 (Color_Manager_Script_t4053987423 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Color_Manager_Script_CalculatePlayables_m3860738715_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	GameObjectU5BU5D_t3057952154* V_1 = NULL;
	int32_t V_2 = 0;
	GameObject_t1756533147 * V_3 = NULL;
	GameObjectU5BU5D_t3057952154* V_4 = NULL;
	int32_t V_5 = 0;
	{
		__this->set_noOfPlaying_12(0);
		GameObjectU5BU5D_t3057952154* L_0 = __this->get_meriGotiya_4();
		V_1 = L_0;
		V_2 = 0;
		goto IL_003b;
	}

IL_0015:
	{
		GameObjectU5BU5D_t3057952154* L_1 = V_1;
		int32_t L_2 = V_2;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		GameObject_t1756533147 * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = L_4;
		GameObject_t1756533147 * L_5 = V_0;
		NullCheck(L_5);
		Goti_Status_t1473870168 * L_6 = GameObject_GetComponent_TisGoti_Status_t1473870168_m3243051065(L_5, /*hidden argument*/GameObject_GetComponent_TisGoti_Status_t1473870168_m3243051065_MethodInfo_var);
		NullCheck(L_6);
		bool L_7 = L_6->get_canBePlayed_3();
		if (!L_7)
		{
			goto IL_0037;
		}
	}
	{
		int32_t L_8 = __this->get_noOfPlaying_12();
		__this->set_noOfPlaying_12(((int32_t)((int32_t)L_8+(int32_t)1)));
	}

IL_0037:
	{
		int32_t L_9 = V_2;
		V_2 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_003b:
	{
		int32_t L_10 = V_2;
		GameObjectU5BU5D_t3057952154* L_11 = V_1;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0015;
		}
	}
	{
		__this->set_noOfGotiyaWin_14(0);
		GameObjectU5BU5D_t3057952154* L_12 = __this->get_meriGotiya_4();
		V_4 = L_12;
		V_5 = 0;
		goto IL_0086;
	}

IL_005b:
	{
		GameObjectU5BU5D_t3057952154* L_13 = V_4;
		int32_t L_14 = V_5;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		GameObject_t1756533147 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_3 = L_16;
		GameObject_t1756533147 * L_17 = V_3;
		NullCheck(L_17);
		Goti_Status_t1473870168 * L_18 = GameObject_GetComponent_TisGoti_Status_t1473870168_m3243051065(L_17, /*hidden argument*/GameObject_GetComponent_TisGoti_Status_t1473870168_m3243051065_MethodInfo_var);
		NullCheck(L_18);
		int32_t L_19 = L_18->get_myState_2();
		if ((!(((uint32_t)L_19) == ((uint32_t)3))))
		{
			goto IL_0080;
		}
	}
	{
		int32_t L_20 = __this->get_noOfGotiyaWin_14();
		__this->set_noOfGotiyaWin_14(((int32_t)((int32_t)L_20+(int32_t)1)));
	}

IL_0080:
	{
		int32_t L_21 = V_5;
		V_5 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_0086:
	{
		int32_t L_22 = V_5;
		GameObjectU5BU5D_t3057952154* L_23 = V_4;
		NullCheck(L_23);
		if ((((int32_t)L_22) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_23)->max_length)))))))
		{
			goto IL_005b;
		}
	}
	{
		Color_Manager_Script_CalculateIfIWin_m481506854(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Color_Manager_Script::CalculateIfIWin()
extern "C"  void Color_Manager_Script_CalculateIfIWin_m481506854 (Color_Manager_Script_t4053987423 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_amIPlayin_3();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_1 = __this->get_noOfGotiyaWin_14();
		if ((!(((uint32_t)L_1) == ((uint32_t)4))))
		{
			goto IL_001d;
		}
	}
	{
		Color_Manager_Script_IWIN_m536760483(__this, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Void Color_Manager_Script::IWIN()
extern "C"  void Color_Manager_Script_IWIN_m536760483 (Color_Manager_Script_t4053987423 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_OnWinObj_15();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)1, /*hidden argument*/NULL);
		__this->set_haveWon_16((bool)1);
		Turn_Manager_t2388373579 * L_1 = __this->get_tm_6();
		Turn_Manager_t2388373579 * L_2 = L_1;
		NullCheck(L_2);
		int32_t L_3 = L_2->get_wonPos_15();
		NullCheck(L_2);
		L_2->set_wonPos_15(((int32_t)((int32_t)L_3+(int32_t)1)));
		Turn_Manager_t2388373579 * L_4 = __this->get_tm_6();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_wonPos_15();
		if ((!(((uint32_t)L_5) == ((uint32_t)1))))
		{
			goto IL_004d;
		}
	}
	{
		Turn_Manager_t2388373579 * L_6 = __this->get_tm_6();
		String_t* L_7 = __this->get_myName_2();
		NullCheck(L_6);
		L_6->set_first_20(L_7);
		goto IL_0096;
	}

IL_004d:
	{
		Turn_Manager_t2388373579 * L_8 = __this->get_tm_6();
		NullCheck(L_8);
		int32_t L_9 = L_8->get_wonPos_15();
		if ((!(((uint32_t)L_9) == ((uint32_t)2))))
		{
			goto IL_0074;
		}
	}
	{
		Turn_Manager_t2388373579 * L_10 = __this->get_tm_6();
		String_t* L_11 = __this->get_myName_2();
		NullCheck(L_10);
		L_10->set_second_21(L_11);
		goto IL_0096;
	}

IL_0074:
	{
		Turn_Manager_t2388373579 * L_12 = __this->get_tm_6();
		NullCheck(L_12);
		int32_t L_13 = L_12->get_wonPos_15();
		if ((!(((uint32_t)L_13) == ((uint32_t)3))))
		{
			goto IL_0096;
		}
	}
	{
		Turn_Manager_t2388373579 * L_14 = __this->get_tm_6();
		String_t* L_15 = __this->get_myName_2();
		NullCheck(L_14);
		L_14->set_third_22(L_15);
	}

IL_0096:
	{
		GameObject_t1756533147 * L_16 = __this->get_OnWinObj_15();
		NullCheck(L_16);
		Transform_t3275118058 * L_17 = GameObject_get_transform_m909382139(L_16, /*hidden argument*/NULL);
		Turn_Manager_t2388373579 * L_18 = __this->get_tm_6();
		NullCheck(L_18);
		int32_t L_19 = L_18->get_wonPos_15();
		NullCheck(L_17);
		Transform_t3275118058 * L_20 = Transform_GetChild_m3838588184(L_17, ((int32_t)((int32_t)L_19-(int32_t)1)), /*hidden argument*/NULL);
		NullCheck(L_20);
		GameObject_t1756533147 * L_21 = Component_get_gameObject_m3105766835(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		GameObject_SetActive_m2887581199(L_21, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Color_Manager_Script::ILOST()
extern "C"  void Color_Manager_Script_ILOST_m326932739 (Color_Manager_Script_t4053987423 * __this, const MethodInfo* method)
{
	{
		__this->set_haveLost_17((bool)1);
		Turn_Manager_t2388373579 * L_0 = __this->get_tm_6();
		NullCheck(L_0);
		int32_t L_1 = L_0->get_wonPos_15();
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_002e;
		}
	}
	{
		Turn_Manager_t2388373579 * L_2 = __this->get_tm_6();
		String_t* L_3 = __this->get_myName_2();
		NullCheck(L_2);
		L_2->set_second_21(L_3);
		goto IL_0077;
	}

IL_002e:
	{
		Turn_Manager_t2388373579 * L_4 = __this->get_tm_6();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_wonPos_15();
		if ((!(((uint32_t)L_5) == ((uint32_t)2))))
		{
			goto IL_0055;
		}
	}
	{
		Turn_Manager_t2388373579 * L_6 = __this->get_tm_6();
		String_t* L_7 = __this->get_myName_2();
		NullCheck(L_6);
		L_6->set_third_22(L_7);
		goto IL_0077;
	}

IL_0055:
	{
		Turn_Manager_t2388373579 * L_8 = __this->get_tm_6();
		NullCheck(L_8);
		int32_t L_9 = L_8->get_wonPos_15();
		if ((!(((uint32_t)L_9) == ((uint32_t)3))))
		{
			goto IL_0077;
		}
	}
	{
		Turn_Manager_t2388373579 * L_10 = __this->get_tm_6();
		String_t* L_11 = __this->get_myName_2();
		NullCheck(L_10);
		L_10->set_fourth_23(L_11);
	}

IL_0077:
	{
		return;
	}
}
// System.Void Color_Manager_Script::DisableAllCameras()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3776056063;
extern const uint32_t Color_Manager_Script_DisableAllCameras_m2295878905_MetadataUsageId;
extern "C"  void Color_Manager_Script_DisableAllCameras_m2295878905 (Color_Manager_Script_t4053987423 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Color_Manager_Script_DisableAllCameras_m2295878905_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Camera_t189460977 * L_0 = __this->get_greenLane_19();
		NullCheck(L_0);
		Behaviour_set_enabled_m1796096907(L_0, (bool)0, /*hidden argument*/NULL);
		Camera_t189460977 * L_1 = __this->get_blueLane_22();
		NullCheck(L_1);
		Behaviour_set_enabled_m1796096907(L_1, (bool)0, /*hidden argument*/NULL);
		Camera_t189460977 * L_2 = __this->get_redLane_20();
		NullCheck(L_2);
		Behaviour_set_enabled_m1796096907(L_2, (bool)0, /*hidden argument*/NULL);
		Camera_t189460977 * L_3 = __this->get_yellowLane_21();
		NullCheck(L_3);
		Behaviour_set_enabled_m1796096907(L_3, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3776056063, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Color_Manager_Script::EnableMainCamera(System.Boolean)
extern "C"  void Color_Manager_Script_EnableMainCamera_m3356488996 (Color_Manager_Script_t4053987423 * __this, bool ___flag0, const MethodInfo* method)
{
	{
		Camera_t189460977 * L_0 = __this->get_myMainCamera_23();
		bool L_1 = ___flag0;
		NullCheck(L_0);
		Behaviour_set_enabled_m1796096907(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void dance::.ctor()
extern "C"  void dance__ctor_m2809252018 (dance_t2594252983 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void dance::Start()
extern "C"  void dance_Start_m1515533566 (dance_t2594252983 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void dance::Update()
extern "C"  void dance_Update_m3558283735 (dance_t2594252983 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Dice_Script::.ctor()
extern "C"  void Dice_Script__ctor_m3517106066 (Dice_Script_t640145349 * __this, const MethodInfo* method)
{
	{
		__this->set_diceRollSpeed_3((10.0f));
		__this->set_arrowMovementRange_10((10.0f));
		__this->set_arrowMovementSpeed_11((10.0f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Dice_Script::Start()
extern "C"  void Dice_Script_Start_m2194034762 (Dice_Script_t640145349 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		__this->set_thisTransform_2(L_0);
		Image_t2042527209 * L_1 = __this->get_diceArrow_7();
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_SetActive_m2887581199(L_2, (bool)0, /*hidden argument*/NULL);
		Image_t2042527209 * L_3 = __this->get_diceArrow_7();
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t2243707580  L_5 = Transform_get_position_m1104419803(L_4, /*hidden argument*/NULL);
		__this->set_initArrow_9(L_5);
		return;
	}
}
// System.Void Dice_Script::Update()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Dice_Script_Update_m4037671213_MetadataUsageId;
extern "C"  void Dice_Script_Update_m4037671213 (Dice_Script_t640145349 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dice_Script_Update_m4037671213_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKey_m3849524999(NULL /*static, unused*/, ((int32_t)114), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		Dice_Script_RollDice_m839356228(__this, /*hidden argument*/NULL);
	}

IL_0012:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKeyUp_m1008512962(NULL /*static, unused*/, ((int32_t)114), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		Dice_Script_StopDice_m3232255983(__this, /*hidden argument*/NULL);
	}

IL_0024:
	{
		bool L_2 = __this->get_startArrowAnim_8();
		if (!L_2)
		{
			goto IL_007f;
		}
	}
	{
		float L_3 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_4 = __this->get_arrowMovementSpeed_11();
		float L_5 = __this->get_arrowMovementRange_10();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_6 = Mathf_PingPong_m2539699755(NULL /*static, unused*/, ((float)((float)L_3*(float)L_4)), L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		Image_t2042527209 * L_7 = __this->get_diceArrow_7();
		NullCheck(L_7);
		Transform_t3275118058 * L_8 = Component_get_transform_m2697483695(L_7, /*hidden argument*/NULL);
		Vector3_t2243707580 * L_9 = __this->get_address_of_initArrow_9();
		float L_10 = L_9->get_x_1();
		Vector3_t2243707580 * L_11 = __this->get_address_of_initArrow_9();
		float L_12 = L_11->get_y_2();
		float L_13 = V_0;
		Vector3_t2243707580 * L_14 = __this->get_address_of_initArrow_9();
		float L_15 = L_14->get_z_3();
		Vector3_t2243707580  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Vector3__ctor_m2638739322(&L_16, L_10, ((float)((float)L_12+(float)L_13)), L_15, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_set_position_m2469242620(L_8, L_16, /*hidden argument*/NULL);
	}

IL_007f:
	{
		return;
	}
}
// System.Void Dice_Script::RollDice()
extern "C"  void Dice_Script_RollDice_m839356228 (Dice_Script_t640145349 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		int32_t L_0 = Random_Range_m694320887(NULL /*static, unused*/, 0, 2, /*hidden argument*/NULL);
		V_0 = (((float)((float)L_0)));
		float L_1 = V_0;
		if ((!(((float)L_1) == ((float)(0.0f)))))
		{
			goto IL_001f;
		}
	}
	{
		V_0 = (-1.0f);
		goto IL_0030;
	}

IL_001f:
	{
		float L_2 = V_0;
		if ((!(((float)L_2) == ((float)(1.0f)))))
		{
			goto IL_0030;
		}
	}
	{
		V_0 = (1.0f);
	}

IL_0030:
	{
		Transform_t3275118058 * L_3 = __this->get_thisTransform_2();
		float L_4 = __this->get_diceRollSpeed_3();
		float L_5 = V_0;
		float L_6 = __this->get_diceRollSpeed_3();
		float L_7 = V_0;
		float L_8 = __this->get_diceRollSpeed_3();
		float L_9 = V_0;
		NullCheck(L_3);
		Transform_Rotate_m4255273365(L_3, ((float)((float)((float)((float)(1.0f)*(float)L_4))*(float)L_5)), ((float)((float)((float)((float)(1.0f)*(float)L_6))*(float)L_7)), ((float)((float)((float)((float)(1.0f)*(float)L_8))*(float)L_9)), /*hidden argument*/NULL);
		Dice_Script_DisableArrow_m36271865(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Dice_Script::StopDice()
extern "C"  void Dice_Script_StopDice_m3232255983 (Dice_Script_t640145349 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Turn_Manager_t2388373579 * L_0 = __this->get_tm_5();
		NullCheck(L_0);
		Turn_Manager_DeactivateButton_m2793329282(L_0, /*hidden argument*/NULL);
		int32_t L_1 = Random_Range_m694320887(NULL /*static, unused*/, 1, 7, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		__this->set_dInp_4(L_2);
		int32_t L_3 = V_0;
		if ((!(((uint32_t)L_3) == ((uint32_t)2))))
		{
			goto IL_0045;
		}
	}
	{
		Transform_t3275118058 * L_4 = __this->get_thisTransform_2();
		Vector3_t2243707580  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector3__ctor_m2638739322(&L_5, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_localEulerAngles_m2927195985(L_4, L_5, /*hidden argument*/NULL);
		goto IL_0117;
	}

IL_0045:
	{
		int32_t L_6 = V_0;
		if ((!(((uint32_t)L_6) == ((uint32_t)6))))
		{
			goto IL_0070;
		}
	}
	{
		Transform_t3275118058 * L_7 = __this->get_thisTransform_2();
		Vector3_t2243707580  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector3__ctor_m2638739322(&L_8, (0.0f), (0.0f), (-90.0f), /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_set_localEulerAngles_m2927195985(L_7, L_8, /*hidden argument*/NULL);
		goto IL_0117;
	}

IL_0070:
	{
		int32_t L_9 = V_0;
		if ((!(((uint32_t)L_9) == ((uint32_t)5))))
		{
			goto IL_009b;
		}
	}
	{
		Transform_t3275118058 * L_10 = __this->get_thisTransform_2();
		Vector3_t2243707580  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Vector3__ctor_m2638739322(&L_11, (90.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_set_localEulerAngles_m2927195985(L_10, L_11, /*hidden argument*/NULL);
		goto IL_0117;
	}

IL_009b:
	{
		int32_t L_12 = V_0;
		if ((!(((uint32_t)L_12) == ((uint32_t)3))))
		{
			goto IL_00c6;
		}
	}
	{
		Transform_t3275118058 * L_13 = __this->get_thisTransform_2();
		Vector3_t2243707580  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m2638739322(&L_14, (-90.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_set_localEulerAngles_m2927195985(L_13, L_14, /*hidden argument*/NULL);
		goto IL_0117;
	}

IL_00c6:
	{
		int32_t L_15 = V_0;
		if ((!(((uint32_t)L_15) == ((uint32_t)4))))
		{
			goto IL_00f1;
		}
	}
	{
		Transform_t3275118058 * L_16 = __this->get_thisTransform_2();
		Vector3_t2243707580  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Vector3__ctor_m2638739322(&L_17, (0.0f), (0.0f), (90.0f), /*hidden argument*/NULL);
		NullCheck(L_16);
		Transform_set_localEulerAngles_m2927195985(L_16, L_17, /*hidden argument*/NULL);
		goto IL_0117;
	}

IL_00f1:
	{
		int32_t L_18 = V_0;
		if ((!(((uint32_t)L_18) == ((uint32_t)1))))
		{
			goto IL_0117;
		}
	}
	{
		Transform_t3275118058 * L_19 = __this->get_thisTransform_2();
		Vector3_t2243707580  L_20;
		memset(&L_20, 0, sizeof(L_20));
		Vector3__ctor_m2638739322(&L_20, (-180.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_19);
		Transform_set_localEulerAngles_m2927195985(L_19, L_20, /*hidden argument*/NULL);
	}

IL_0117:
	{
		return;
	}
}
// System.Void Dice_Script::EnableArrow()
extern "C"  void Dice_Script_EnableArrow_m2954576550 (Dice_Script_t640145349 * __this, const MethodInfo* method)
{
	{
		Image_t2042527209 * L_0 = __this->get_diceArrow_7();
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m2887581199(L_1, (bool)1, /*hidden argument*/NULL);
		__this->set_startArrowAnim_8((bool)1);
		return;
	}
}
// System.Void Dice_Script::DisableArrow()
extern "C"  void Dice_Script_DisableArrow_m36271865 (Dice_Script_t640145349 * __this, const MethodInfo* method)
{
	{
		Image_t2042527209 * L_0 = __this->get_diceArrow_7();
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m2887581199(L_1, (bool)0, /*hidden argument*/NULL);
		__this->set_startArrowAnim_8((bool)0);
		return;
	}
}
// System.Void Enemy_AI::.ctor()
extern "C"  void Enemy_AI__ctor_m3907679114 (Enemy_AI_t244483799 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Enemy_AI::Start()
extern const MethodInfo* Component_GetComponent_TisColor_Manager_Script_t4053987423_m2545735956_MethodInfo_var;
extern const uint32_t Enemy_AI_Start_m1919036822_MetadataUsageId;
extern "C"  void Enemy_AI_Start_m1919036822 (Enemy_AI_t244483799 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enemy_AI_Start_m1919036822_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Color_Manager_Script_t4053987423 * L_0 = Component_GetComponent_TisColor_Manager_Script_t4053987423_m2545735956(__this, /*hidden argument*/Component_GetComponent_TisColor_Manager_Script_t4053987423_m2545735956_MethodInfo_var);
		__this->set_cms_4(L_0);
		return;
	}
}
// System.Void Enemy_AI::Update()
extern Il2CppCodeGenString* _stringLiteral850233221;
extern const uint32_t Enemy_AI_Update_m1237139619_MetadataUsageId;
extern "C"  void Enemy_AI_Update_m1237139619 (Enemy_AI_t244483799 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enemy_AI_Update_m1237139619_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		bool L_0 = __this->get_rollDice_2();
		if (!L_0)
		{
			goto IL_0039;
		}
	}
	{
		Turn_Manager_t2388373579 * L_1 = __this->get_tm_3();
		NullCheck(L_1);
		Turn_Manager_AIRoll_m382572419(L_1, /*hidden argument*/NULL);
		float L_2 = Random_Range_m2884721203(NULL /*static, unused*/, (0.3f), (2.1f), /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = V_0;
		MonoBehaviour_Invoke_m666563676(__this, _stringLiteral850233221, L_3, /*hidden argument*/NULL);
		__this->set_rollDice_2((bool)0);
	}

IL_0039:
	{
		return;
	}
}
// System.Void Enemy_AI::StopRolling()
extern "C"  void Enemy_AI_StopRolling_m1450032615 (Enemy_AI_t244483799 * __this, const MethodInfo* method)
{
	Goti_Status_t1473870168 * V_0 = NULL;
	Goti_StatusU5BU5D_t3854859849* V_1 = NULL;
	int32_t V_2 = 0;
	{
		Turn_Manager_t2388373579 * L_0 = __this->get_tm_3();
		NullCheck(L_0);
		Turn_Manager_StopDice_m2156974429(L_0, /*hidden argument*/NULL);
		Goti_StatusU5BU5D_t3854859849* L_1 = __this->get_myPieces_5();
		V_1 = L_1;
		V_2 = 0;
		goto IL_0033;
	}

IL_0019:
	{
		Goti_StatusU5BU5D_t3854859849* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		Goti_Status_t1473870168 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_0 = L_5;
		Goti_Status_t1473870168 * L_6 = V_0;
		NullCheck(L_6);
		int32_t L_7 = L_6->get_myState_2();
		if ((((int32_t)L_7) == ((int32_t)3)))
		{
			goto IL_002f;
		}
	}
	{
		Goti_Status_t1473870168 * L_8 = V_0;
		NullCheck(L_8);
		Goti_Status_FlagLogics_m2683927470(L_8, /*hidden argument*/NULL);
	}

IL_002f:
	{
		int32_t L_9 = V_2;
		V_2 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0033:
	{
		int32_t L_10 = V_2;
		Goti_StatusU5BU5D_t3854859849* L_11 = V_1;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0019;
		}
	}
	{
		return;
	}
}
// System.Void Enemy_AI::MovePiece()
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Clear_m3429729458_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2587217423_MethodInfo_var;
extern const MethodInfo* List_1_set_Item_m1015326455_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m690040376_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m114142811_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1086517090;
extern Il2CppCodeGenString* _stringLiteral3308605598;
extern Il2CppCodeGenString* _stringLiteral2070568808;
extern Il2CppCodeGenString* _stringLiteral3531446417;
extern Il2CppCodeGenString* _stringLiteral2800033169;
extern const uint32_t Enemy_AI_MovePiece_m1145793173_MetadataUsageId;
extern "C"  void Enemy_AI_MovePiece_m1145793173 (Enemy_AI_t244483799 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enemy_AI_MovePiece_m1145793173_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Goti_Status_t1473870168 * V_3 = NULL;
	Goti_StatusU5BU5D_t3854859849* V_4 = NULL;
	int32_t V_5 = 0;
	Goti_Status_t1473870168 * V_6 = NULL;
	Goti_StatusU5BU5D_t3854859849* V_7 = NULL;
	int32_t V_8 = 0;
	Goti_Status_t1473870168 * V_9 = NULL;
	Goti_StatusU5BU5D_t3854859849* V_10 = NULL;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	int32_t V_14 = 0;
	{
		Color_Manager_Script_t4053987423 * L_0 = __this->get_cms_4();
		NullCheck(L_0);
		bool L_1 = L_0->get_haveLost_17();
		if (L_1)
		{
			goto IL_029c;
		}
	}
	{
		V_0 = 0;
		List_1_t842991300 * L_2 = __this->get_closedPieces_8();
		NullCheck(L_2);
		List_1_Clear_m3429729458(L_2, /*hidden argument*/List_1_Clear_m3429729458_MethodInfo_var);
		Color_Manager_Script_t4053987423 * L_3 = __this->get_cms_4();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_diceInput_7();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1086517090, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		V_1 = 0;
		List_1_t842991300 * L_8 = __this->get_playAblePieces_7();
		NullCheck(L_8);
		List_1_Clear_m3429729458(L_8, /*hidden argument*/List_1_Clear_m3429729458_MethodInfo_var);
		V_2 = 0;
		List_1_t842991300 * L_9 = __this->get_wonPieces_9();
		NullCheck(L_9);
		List_1_Clear_m3429729458(L_9, /*hidden argument*/List_1_Clear_m3429729458_MethodInfo_var);
		Goti_StatusU5BU5D_t3854859849* L_10 = __this->get_myPieces_5();
		V_4 = L_10;
		V_5 = 0;
		goto IL_009a;
	}

IL_0066:
	{
		Goti_StatusU5BU5D_t3854859849* L_11 = V_4;
		int32_t L_12 = V_5;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		Goti_Status_t1473870168 * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		V_3 = L_14;
		Goti_Status_t1473870168 * L_15 = V_3;
		NullCheck(L_15);
		int32_t L_16 = L_15->get_myState_2();
		if (L_16)
		{
			goto IL_0094;
		}
	}
	{
		List_1_t842991300 * L_17 = __this->get_playAblePieces_7();
		Goti_Status_t1473870168 * L_18 = V_3;
		NullCheck(L_17);
		List_1_Add_m2587217423(L_17, L_18, /*hidden argument*/List_1_Add_m2587217423_MethodInfo_var);
		List_1_t842991300 * L_19 = __this->get_playAblePieces_7();
		int32_t L_20 = V_1;
		Goti_Status_t1473870168 * L_21 = V_3;
		NullCheck(L_19);
		List_1_set_Item_m1015326455(L_19, L_20, L_21, /*hidden argument*/List_1_set_Item_m1015326455_MethodInfo_var);
		int32_t L_22 = V_1;
		V_1 = ((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_0094:
	{
		int32_t L_23 = V_5;
		V_5 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_009a:
	{
		int32_t L_24 = V_5;
		Goti_StatusU5BU5D_t3854859849* L_25 = V_4;
		NullCheck(L_25);
		if ((((int32_t)L_24) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_25)->max_length)))))))
		{
			goto IL_0066;
		}
	}
	{
		Goti_StatusU5BU5D_t3854859849* L_26 = __this->get_myPieces_5();
		V_7 = L_26;
		V_8 = 0;
		goto IL_00ee;
	}

IL_00b5:
	{
		Goti_StatusU5BU5D_t3854859849* L_27 = V_7;
		int32_t L_28 = V_8;
		NullCheck(L_27);
		int32_t L_29 = L_28;
		Goti_Status_t1473870168 * L_30 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_29));
		V_6 = L_30;
		Goti_Status_t1473870168 * L_31 = V_6;
		NullCheck(L_31);
		int32_t L_32 = L_31->get_myState_2();
		if ((!(((uint32_t)L_32) == ((uint32_t)3))))
		{
			goto IL_00e8;
		}
	}
	{
		List_1_t842991300 * L_33 = __this->get_wonPieces_9();
		Goti_Status_t1473870168 * L_34 = V_6;
		NullCheck(L_33);
		List_1_Add_m2587217423(L_33, L_34, /*hidden argument*/List_1_Add_m2587217423_MethodInfo_var);
		List_1_t842991300 * L_35 = __this->get_wonPieces_9();
		int32_t L_36 = V_2;
		Goti_Status_t1473870168 * L_37 = V_6;
		NullCheck(L_35);
		List_1_set_Item_m1015326455(L_35, L_36, L_37, /*hidden argument*/List_1_set_Item_m1015326455_MethodInfo_var);
		int32_t L_38 = V_2;
		V_2 = ((int32_t)((int32_t)L_38+(int32_t)1));
	}

IL_00e8:
	{
		int32_t L_39 = V_8;
		V_8 = ((int32_t)((int32_t)L_39+(int32_t)1));
	}

IL_00ee:
	{
		int32_t L_40 = V_8;
		Goti_StatusU5BU5D_t3854859849* L_41 = V_7;
		NullCheck(L_41);
		if ((((int32_t)L_40) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_41)->max_length)))))))
		{
			goto IL_00b5;
		}
	}
	{
		Goti_StatusU5BU5D_t3854859849* L_42 = __this->get_myPieces_5();
		V_10 = L_42;
		V_11 = 0;
		goto IL_0148;
	}

IL_0109:
	{
		Goti_StatusU5BU5D_t3854859849* L_43 = V_10;
		int32_t L_44 = V_11;
		NullCheck(L_43);
		int32_t L_45 = L_44;
		Goti_Status_t1473870168 * L_46 = (L_43)->GetAt(static_cast<il2cpp_array_size_t>(L_45));
		V_9 = L_46;
		Goti_Status_t1473870168 * L_47 = V_9;
		NullCheck(L_47);
		int32_t L_48 = L_47->get_myState_2();
		if ((!(((uint32_t)L_48) == ((uint32_t)1))))
		{
			goto IL_0142;
		}
	}
	{
		List_1_t842991300 * L_49 = __this->get_closedPieces_8();
		Goti_StatusU5BU5D_t3854859849* L_50 = __this->get_myPieces_5();
		NullCheck(L_50);
		int32_t L_51 = 0;
		Goti_Status_t1473870168 * L_52 = (L_50)->GetAt(static_cast<il2cpp_array_size_t>(L_51));
		NullCheck(L_49);
		List_1_Add_m2587217423(L_49, L_52, /*hidden argument*/List_1_Add_m2587217423_MethodInfo_var);
		List_1_t842991300 * L_53 = __this->get_closedPieces_8();
		int32_t L_54 = V_0;
		Goti_Status_t1473870168 * L_55 = V_9;
		NullCheck(L_53);
		List_1_set_Item_m1015326455(L_53, L_54, L_55, /*hidden argument*/List_1_set_Item_m1015326455_MethodInfo_var);
		int32_t L_56 = V_0;
		V_0 = ((int32_t)((int32_t)L_56+(int32_t)1));
	}

IL_0142:
	{
		int32_t L_57 = V_11;
		V_11 = ((int32_t)((int32_t)L_57+(int32_t)1));
	}

IL_0148:
	{
		int32_t L_58 = V_11;
		Goti_StatusU5BU5D_t3854859849* L_59 = V_10;
		NullCheck(L_59);
		if ((((int32_t)L_58) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_59)->max_length)))))))
		{
			goto IL_0109;
		}
	}
	{
		Color_Manager_Script_t4053987423 * L_60 = __this->get_cms_4();
		NullCheck(L_60);
		int32_t L_61 = L_60->get_diceInput_7();
		if ((!(((uint32_t)L_61) == ((uint32_t)6))))
		{
			goto IL_027e;
		}
	}
	{
		int32_t L_62 = V_0;
		int32_t L_63 = V_2;
		if ((!(((uint32_t)((int32_t)((int32_t)L_62+(int32_t)L_63))) == ((uint32_t)4))))
		{
			goto IL_0193;
		}
	}
	{
		int32_t L_64 = V_0;
		if (!L_64)
		{
			goto IL_0193;
		}
	}
	{
		List_1_t842991300 * L_65 = __this->get_closedPieces_8();
		NullCheck(L_65);
		Goti_Status_t1473870168 * L_66 = List_1_get_Item_m690040376(L_65, 0, /*hidden argument*/List_1_get_Item_m690040376_MethodInfo_var);
		NullCheck(L_66);
		Goti_Status_PerformMovement_m702764983(L_66, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3308605598, /*hidden argument*/NULL);
		goto IL_0279;
	}

IL_0193:
	{
		int32_t L_67 = V_0;
		int32_t L_68 = V_2;
		if ((!(((uint32_t)((int32_t)((int32_t)L_67+(int32_t)L_68))) == ((uint32_t)3))))
		{
			goto IL_01c2;
		}
	}
	{
		int32_t L_69 = V_0;
		if (!L_69)
		{
			goto IL_01c2;
		}
	}
	{
		List_1_t842991300 * L_70 = __this->get_closedPieces_8();
		NullCheck(L_70);
		Goti_Status_t1473870168 * L_71 = List_1_get_Item_m690040376(L_70, 0, /*hidden argument*/List_1_get_Item_m690040376_MethodInfo_var);
		NullCheck(L_71);
		Goti_Status_PerformMovement_m702764983(L_71, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2070568808, /*hidden argument*/NULL);
		goto IL_0279;
	}

IL_01c2:
	{
		int32_t L_72 = V_0;
		if ((((int32_t)L_72) > ((int32_t)2)))
		{
			goto IL_0273;
		}
	}
	{
		int32_t L_73 = V_0;
		if (!L_73)
		{
			goto IL_0273;
		}
	}
	{
		int32_t L_74 = Random_Range_m694320887(NULL /*static, unused*/, 1, ((int32_t)11), /*hidden argument*/NULL);
		V_12 = L_74;
		bool L_75 = Enemy_AI_isAnyOneInVicinity_m306585268(__this, /*hidden argument*/NULL);
		if (!L_75)
		{
			goto IL_023f;
		}
	}
	{
		List_1_t842991300 * L_76 = __this->get_closedPieces_8();
		NullCheck(L_76);
		int32_t L_77 = List_1_get_Count_m114142811(L_76, /*hidden argument*/List_1_get_Count_m114142811_MethodInfo_var);
		int32_t L_78 = Random_Range_m694320887(NULL /*static, unused*/, 0, L_77, /*hidden argument*/NULL);
		V_13 = L_78;
		int32_t L_79 = Random_Range_m694320887(NULL /*static, unused*/, 1, ((int32_t)11), /*hidden argument*/NULL);
		V_14 = L_79;
		int32_t L_80 = V_14;
		if ((((int32_t)L_80) > ((int32_t)((int32_t)9))))
		{
			goto IL_022b;
		}
	}
	{
		List_1_t842991300 * L_81 = __this->get_closedPieces_8();
		int32_t L_82 = V_13;
		NullCheck(L_81);
		Goti_Status_t1473870168 * L_83 = List_1_get_Item_m690040376(L_81, L_82, /*hidden argument*/List_1_get_Item_m690040376_MethodInfo_var);
		NullCheck(L_83);
		Goti_Status_PerformMovement_m702764983(L_83, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3531446417, /*hidden argument*/NULL);
		goto IL_023a;
	}

IL_022b:
	{
		int32_t L_84 = V_14;
		if ((((int32_t)L_84) <= ((int32_t)((int32_t)9))))
		{
			goto IL_023a;
		}
	}
	{
		Enemy_AI_TurnNotOn6_m871805665(__this, /*hidden argument*/NULL);
	}

IL_023a:
	{
		goto IL_026e;
	}

IL_023f:
	{
		bool L_85 = Enemy_AI_isAnyOneInVicinity_m306585268(__this, /*hidden argument*/NULL);
		if (L_85)
		{
			goto IL_026e;
		}
	}
	{
		int32_t L_86 = V_12;
		if ((((int32_t)L_86) > ((int32_t)3)))
		{
			goto IL_0268;
		}
	}
	{
		List_1_t842991300 * L_87 = __this->get_closedPieces_8();
		NullCheck(L_87);
		Goti_Status_t1473870168 * L_88 = List_1_get_Item_m690040376(L_87, 0, /*hidden argument*/List_1_get_Item_m690040376_MethodInfo_var);
		NullCheck(L_88);
		Goti_Status_PerformMovement_m702764983(L_88, /*hidden argument*/NULL);
		goto IL_026e;
	}

IL_0268:
	{
		Enemy_AI_TurnNotOn6_m871805665(__this, /*hidden argument*/NULL);
	}

IL_026e:
	{
		goto IL_0279;
	}

IL_0273:
	{
		Enemy_AI_TurnNotOn6_m871805665(__this, /*hidden argument*/NULL);
	}

IL_0279:
	{
		goto IL_029c;
	}

IL_027e:
	{
		int32_t L_89 = V_1;
		if ((!(((uint32_t)L_89) == ((uint32_t)1))))
		{
			goto IL_028f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2800033169, /*hidden argument*/NULL);
	}

IL_028f:
	{
		int32_t L_90 = V_1;
		if ((((int32_t)L_90) <= ((int32_t)1)))
		{
			goto IL_029c;
		}
	}
	{
		Enemy_AI_TurnNotOn6_m871805665(__this, /*hidden argument*/NULL);
	}

IL_029c:
	{
		return;
	}
}
// System.Boolean Enemy_AI::isAnyOneInVicinity()
extern "C"  bool Enemy_AI_isAnyOneInVicinity_m306585268 (Enemy_AI_t244483799 * __this, const MethodInfo* method)
{
	Goti_Status_t1473870168 * V_0 = NULL;
	Goti_StatusU5BU5D_t3854859849* V_1 = NULL;
	int32_t V_2 = 0;
	Vector2_t2243707579  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Transform_t3275118058 * V_6 = NULL;
	TransformU5BU5D_t3764228911* V_7 = NULL;
	int32_t V_8 = 0;
	Vector2_t2243707579  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3_t2243707580  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	{
		Goti_StatusU5BU5D_t3854859849* L_0 = __this->get_enemyPieces_6();
		V_1 = L_0;
		V_2 = 0;
		goto IL_00a5;
	}

IL_000e:
	{
		Goti_StatusU5BU5D_t3854859849* L_1 = V_1;
		int32_t L_2 = V_2;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		Goti_Status_t1473870168 * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = L_4;
		Goti_Status_t1473870168 * L_5 = V_0;
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = Component_get_transform_m2697483695(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		V_4 = L_7;
		float L_8 = (&V_4)->get_x_1();
		Goti_Status_t1473870168 * L_9 = V_0;
		NullCheck(L_9);
		Transform_t3275118058 * L_10 = Component_get_transform_m2697483695(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t2243707580  L_11 = Transform_get_position_m1104419803(L_10, /*hidden argument*/NULL);
		V_5 = L_11;
		float L_12 = (&V_5)->get_z_3();
		Vector2__ctor_m3067419446((&V_3), L_8, L_12, /*hidden argument*/NULL);
		TransformU5BU5D_t3764228911* L_13 = __this->get_vicinityPath_10();
		V_7 = L_13;
		V_8 = 0;
		goto IL_0096;
	}

IL_0051:
	{
		TransformU5BU5D_t3764228911* L_14 = V_7;
		int32_t L_15 = V_8;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		Transform_t3275118058 * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		V_6 = L_17;
		Transform_t3275118058 * L_18 = V_6;
		NullCheck(L_18);
		Vector3_t2243707580  L_19 = Transform_get_position_m1104419803(L_18, /*hidden argument*/NULL);
		V_10 = L_19;
		float L_20 = (&V_10)->get_x_1();
		Transform_t3275118058 * L_21 = V_6;
		NullCheck(L_21);
		Vector3_t2243707580  L_22 = Transform_get_position_m1104419803(L_21, /*hidden argument*/NULL);
		V_11 = L_22;
		float L_23 = (&V_11)->get_z_3();
		Vector2__ctor_m3067419446((&V_9), L_20, L_23, /*hidden argument*/NULL);
		Vector2_t2243707579  L_24 = V_9;
		Vector2_t2243707579  L_25 = V_3;
		bool L_26 = Vector2_op_Equality_m4168854394(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_008e;
		}
	}
	{
		return (bool)1;
	}

IL_008e:
	{
		return (bool)0;
	}
	// Dead block : IL_0090: ldloc.s V_8

IL_0096:
	{
		int32_t L_27 = V_8;
		TransformU5BU5D_t3764228911* L_28 = V_7;
		NullCheck(L_28);
		if ((((int32_t)L_27) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_28)->max_length)))))))
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_29 = V_2;
		V_2 = ((int32_t)((int32_t)L_29+(int32_t)1));
	}

IL_00a5:
	{
		int32_t L_30 = V_2;
		Goti_StatusU5BU5D_t3854859849* L_31 = V_1;
		NullCheck(L_31);
		if ((((int32_t)L_30) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_31)->max_length)))))))
		{
			goto IL_000e;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void Enemy_AI::TurnNotOn6()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t842991300_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m832443379_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1796027824_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3451884462_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4249432516_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m3497878668_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m114142811_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m690040376_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1780901677;
extern Il2CppCodeGenString* _stringLiteral698929115;
extern Il2CppCodeGenString* _stringLiteral1018270324;
extern Il2CppCodeGenString* _stringLiteral3181633511;
extern Il2CppCodeGenString* _stringLiteral2619988042;
extern Il2CppCodeGenString* _stringLiteral1014458620;
extern Il2CppCodeGenString* _stringLiteral2141327708;
extern Il2CppCodeGenString* _stringLiteral2838835724;
extern Il2CppCodeGenString* _stringLiteral2842185457;
extern Il2CppCodeGenString* _stringLiteral3533501253;
extern Il2CppCodeGenString* _stringLiteral3694841918;
extern Il2CppCodeGenString* _stringLiteral3015111838;
extern Il2CppCodeGenString* _stringLiteral1088547084;
extern Il2CppCodeGenString* _stringLiteral2919295846;
extern const uint32_t Enemy_AI_TurnNotOn6_m871805665_MetadataUsageId;
extern "C"  void Enemy_AI_TurnNotOn6_m871805665 (Enemy_AI_t244483799 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enemy_AI_TurnNotOn6_m871805665_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t842991300 * V_0 = NULL;
	bool V_1 = false;
	Goti_Status_t1473870168 * V_2 = NULL;
	Enumerator_t377720974  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Goti_Status_t1473870168 * V_4 = NULL;
	Enumerator_t377720974  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Goti_Status_t1473870168 * V_6 = NULL;
	Enumerator_t377720974  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Goti_Status_t1473870168 * V_8 = NULL;
	Enumerator_t377720974  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Goti_Status_t1473870168 * V_10 = NULL;
	Enumerator_t377720974  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Goti_Status_t1473870168 * V_12 = NULL;
	Enumerator_t377720974  V_13;
	memset(&V_13, 0, sizeof(V_13));
	Goti_Status_t1473870168 * V_14 = NULL;
	Enumerator_t377720974  V_15;
	memset(&V_15, 0, sizeof(V_15));
	Goti_Status_t1473870168 * V_16 = NULL;
	Enumerator_t377720974  V_17;
	memset(&V_17, 0, sizeof(V_17));
	Goti_Status_t1473870168 * V_18 = NULL;
	Enumerator_t377720974  V_19;
	memset(&V_19, 0, sizeof(V_19));
	Goti_Status_t1473870168 * V_20 = NULL;
	Enumerator_t377720974  V_21;
	memset(&V_21, 0, sizeof(V_21));
	Goti_Status_t1473870168 * V_22 = NULL;
	Enumerator_t377720974  V_23;
	memset(&V_23, 0, sizeof(V_23));
	Goti_Status_t1473870168 * V_24 = NULL;
	Enumerator_t377720974  V_25;
	memset(&V_25, 0, sizeof(V_25));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1780901677, /*hidden argument*/NULL);
		List_1_t842991300 * L_0 = (List_1_t842991300 *)il2cpp_codegen_object_new(List_1_t842991300_il2cpp_TypeInfo_var);
		List_1__ctor_m832443379(L_0, /*hidden argument*/List_1__ctor_m832443379_MethodInfo_var);
		V_0 = L_0;
		V_1 = (bool)1;
		bool L_1 = V_1;
		if (!L_1)
		{
			goto IL_008e;
		}
	}
	{
		List_1_t842991300 * L_2 = __this->get_playAblePieces_7();
		NullCheck(L_2);
		Enumerator_t377720974  L_3 = List_1_GetEnumerator_m1796027824(L_2, /*hidden argument*/List_1_GetEnumerator_m1796027824_MethodInfo_var);
		V_3 = L_3;
	}

IL_0024:
	try
	{ // begin try (depth: 1)
		{
			goto IL_006f;
		}

IL_0029:
		{
			Goti_Status_t1473870168 * L_4 = Enumerator_get_Current_m3451884462((&V_3), /*hidden argument*/Enumerator_get_Current_m3451884462_MethodInfo_var);
			V_2 = L_4;
			Goti_Status_t1473870168 * L_5 = V_2;
			NullCheck(L_5);
			bool L_6 = L_5->get_isSafe_19();
			if (L_6)
			{
				goto IL_006f;
			}
		}

IL_003c:
		{
			Goti_Status_t1473870168 * L_7 = V_2;
			NullCheck(L_7);
			bool L_8 = L_7->get_canCutSomeOne_34();
			if (!L_8)
			{
				goto IL_006f;
			}
		}

IL_0047:
		{
			bool L_9 = V_1;
			if (!L_9)
			{
				goto IL_006f;
			}
		}

IL_004d:
		{
			Goti_Status_t1473870168 * L_10 = V_2;
			NullCheck(L_10);
			Goti_Status_PerformMovement_m702764983(L_10, /*hidden argument*/NULL);
			Goti_Status_t1473870168 * L_11 = V_2;
			NullCheck(L_11);
			String_t* L_12 = Object_get_name_m2079638459(L_11, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_13 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral698929115, L_12, _stringLiteral1018270324, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
			Debug_Log_m920475918(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
			V_1 = (bool)0;
		}

IL_006f:
		{
			bool L_14 = Enumerator_MoveNext_m4249432516((&V_3), /*hidden argument*/Enumerator_MoveNext_m4249432516_MethodInfo_var);
			if (L_14)
			{
				goto IL_0029;
			}
		}

IL_007b:
		{
			IL2CPP_LEAVE(0x8E, FINALLY_0080);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0080;
	}

FINALLY_0080:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3497878668((&V_3), /*hidden argument*/Enumerator_Dispose_m3497878668_MethodInfo_var);
		IL2CPP_END_FINALLY(128)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(128)
	{
		IL2CPP_JUMP_TBL(0x8E, IL_008e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_008e:
	{
		bool L_15 = V_1;
		if (!L_15)
		{
			goto IL_0110;
		}
	}
	{
		List_1_t842991300 * L_16 = __this->get_playAblePieces_7();
		NullCheck(L_16);
		Enumerator_t377720974  L_17 = List_1_GetEnumerator_m1796027824(L_16, /*hidden argument*/List_1_GetEnumerator_m1796027824_MethodInfo_var);
		V_5 = L_17;
	}

IL_00a1:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00f1;
		}

IL_00a6:
		{
			Goti_Status_t1473870168 * L_18 = Enumerator_get_Current_m3451884462((&V_5), /*hidden argument*/Enumerator_get_Current_m3451884462_MethodInfo_var);
			V_4 = L_18;
			Goti_Status_t1473870168 * L_19 = V_4;
			NullCheck(L_19);
			bool L_20 = L_19->get_isSafe_19();
			if (!L_20)
			{
				goto IL_00f1;
			}
		}

IL_00bb:
		{
			Goti_Status_t1473870168 * L_21 = V_4;
			NullCheck(L_21);
			bool L_22 = L_21->get_canCutSomeOne_34();
			if (!L_22)
			{
				goto IL_00f1;
			}
		}

IL_00c7:
		{
			bool L_23 = V_1;
			if (!L_23)
			{
				goto IL_00f1;
			}
		}

IL_00cd:
		{
			Goti_Status_t1473870168 * L_24 = V_4;
			NullCheck(L_24);
			Goti_Status_PerformMovement_m702764983(L_24, /*hidden argument*/NULL);
			Goti_Status_t1473870168 * L_25 = V_4;
			NullCheck(L_25);
			String_t* L_26 = Object_get_name_m2079638459(L_25, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_27 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral698929115, L_26, _stringLiteral1018270324, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
			Debug_Log_m920475918(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
			V_1 = (bool)0;
		}

IL_00f1:
		{
			bool L_28 = Enumerator_MoveNext_m4249432516((&V_5), /*hidden argument*/Enumerator_MoveNext_m4249432516_MethodInfo_var);
			if (L_28)
			{
				goto IL_00a6;
			}
		}

IL_00fd:
		{
			IL2CPP_LEAVE(0x110, FINALLY_0102);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0102;
	}

FINALLY_0102:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3497878668((&V_5), /*hidden argument*/Enumerator_Dispose_m3497878668_MethodInfo_var);
		IL2CPP_END_FINALLY(258)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(258)
	{
		IL2CPP_JUMP_TBL(0x110, IL_0110)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0110:
	{
		bool L_29 = V_1;
		if (!L_29)
		{
			goto IL_0223;
		}
	}
	{
		List_1_t842991300 * L_30 = __this->get_playAblePieces_7();
		NullCheck(L_30);
		Enumerator_t377720974  L_31 = List_1_GetEnumerator_m1796027824(L_30, /*hidden argument*/List_1_GetEnumerator_m1796027824_MethodInfo_var);
		V_7 = L_31;
	}

IL_0123:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0204;
		}

IL_0128:
		{
			Goti_Status_t1473870168 * L_32 = Enumerator_get_Current_m3451884462((&V_7), /*hidden argument*/Enumerator_get_Current_m3451884462_MethodInfo_var);
			V_6 = L_32;
			Goti_Status_t1473870168 * L_33 = V_6;
			NullCheck(L_33);
			bool L_34 = L_33->get_isSafe_19();
			if (!L_34)
			{
				goto IL_0204;
			}
		}

IL_013d:
		{
			List_1_t842991300 * L_35 = V_0;
			NullCheck(L_35);
			int32_t L_36 = List_1_get_Count_m114142811(L_35, /*hidden argument*/List_1_get_Count_m114142811_MethodInfo_var);
			List_1_t842991300 * L_37 = __this->get_playAblePieces_7();
			NullCheck(L_37);
			int32_t L_38 = List_1_get_Count_m114142811(L_37, /*hidden argument*/List_1_get_Count_m114142811_MethodInfo_var);
			if ((!(((uint32_t)L_36) == ((uint32_t)L_38))))
			{
				goto IL_0204;
			}
		}

IL_0153:
		{
			bool L_39 = V_1;
			if (!L_39)
			{
				goto IL_0204;
			}
		}

IL_0159:
		{
			List_1_t842991300 * L_40 = V_0;
			NullCheck(L_40);
			Enumerator_t377720974  L_41 = List_1_GetEnumerator_m1796027824(L_40, /*hidden argument*/List_1_GetEnumerator_m1796027824_MethodInfo_var);
			V_9 = L_41;
		}

IL_0161:
		try
		{ // begin try (depth: 2)
			{
				goto IL_01a5;
			}

IL_0166:
			{
				Goti_Status_t1473870168 * L_42 = Enumerator_get_Current_m3451884462((&V_9), /*hidden argument*/Enumerator_get_Current_m3451884462_MethodInfo_var);
				V_8 = L_42;
				Goti_Status_t1473870168 * L_43 = V_8;
				NullCheck(L_43);
				bool L_44 = L_43->get_isChaser_40();
				if (L_44)
				{
					goto IL_01a5;
				}
			}

IL_017b:
			{
				bool L_45 = V_1;
				if (!L_45)
				{
					goto IL_01a5;
				}
			}

IL_0181:
			{
				Goti_Status_t1473870168 * L_46 = V_6;
				NullCheck(L_46);
				String_t* L_47 = Object_get_name_m2079638459(L_46, /*hidden argument*/NULL);
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_48 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral3181633511, L_47, _stringLiteral2619988042, /*hidden argument*/NULL);
				IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
				Debug_Log_m920475918(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
				V_1 = (bool)0;
				Goti_Status_t1473870168 * L_49 = V_8;
				NullCheck(L_49);
				Goti_Status_PerformMovement_m702764983(L_49, /*hidden argument*/NULL);
			}

IL_01a5:
			{
				bool L_50 = Enumerator_MoveNext_m4249432516((&V_9), /*hidden argument*/Enumerator_MoveNext_m4249432516_MethodInfo_var);
				if (L_50)
				{
					goto IL_0166;
				}
			}

IL_01b1:
			{
				IL2CPP_LEAVE(0x1C4, FINALLY_01b6);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
			goto FINALLY_01b6;
		}

FINALLY_01b6:
		{ // begin finally (depth: 2)
			Enumerator_Dispose_m3497878668((&V_9), /*hidden argument*/Enumerator_Dispose_m3497878668_MethodInfo_var);
			IL2CPP_END_FINALLY(438)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(438)
		{
			IL2CPP_JUMP_TBL(0x1C4, IL_01c4)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
		}

IL_01c4:
		{
			bool L_51 = V_1;
			if (!L_51)
			{
				goto IL_0204;
			}
		}

IL_01ca:
		{
			List_1_t842991300 * L_52 = __this->get_playAblePieces_7();
			List_1_t842991300 * L_53 = __this->get_playAblePieces_7();
			NullCheck(L_53);
			int32_t L_54 = List_1_get_Count_m114142811(L_53, /*hidden argument*/List_1_get_Count_m114142811_MethodInfo_var);
			NullCheck(L_52);
			Goti_Status_t1473870168 * L_55 = List_1_get_Item_m690040376(L_52, ((int32_t)((int32_t)L_54-(int32_t)1)), /*hidden argument*/List_1_get_Item_m690040376_MethodInfo_var);
			NullCheck(L_55);
			Goti_Status_PerformMovement_m702764983(L_55, /*hidden argument*/NULL);
			Goti_Status_t1473870168 * L_56 = V_6;
			NullCheck(L_56);
			String_t* L_57 = Object_get_name_m2079638459(L_56, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_58 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral3181633511, L_57, _stringLiteral1014458620, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
			Debug_Log_m920475918(NULL /*static, unused*/, L_58, /*hidden argument*/NULL);
			V_1 = (bool)0;
		}

IL_0204:
		{
			bool L_59 = Enumerator_MoveNext_m4249432516((&V_7), /*hidden argument*/Enumerator_MoveNext_m4249432516_MethodInfo_var);
			if (L_59)
			{
				goto IL_0128;
			}
		}

IL_0210:
		{
			IL2CPP_LEAVE(0x223, FINALLY_0215);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0215;
	}

FINALLY_0215:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3497878668((&V_7), /*hidden argument*/Enumerator_Dispose_m3497878668_MethodInfo_var);
		IL2CPP_END_FINALLY(533)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(533)
	{
		IL2CPP_JUMP_TBL(0x223, IL_0223)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0223:
	{
		bool L_60 = V_1;
		if (!L_60)
		{
			goto IL_02a5;
		}
	}
	{
		List_1_t842991300 * L_61 = __this->get_playAblePieces_7();
		NullCheck(L_61);
		Enumerator_t377720974  L_62 = List_1_GetEnumerator_m1796027824(L_61, /*hidden argument*/List_1_GetEnumerator_m1796027824_MethodInfo_var);
		V_11 = L_62;
	}

IL_0236:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0286;
		}

IL_023b:
		{
			Goti_Status_t1473870168 * L_63 = Enumerator_get_Current_m3451884462((&V_11), /*hidden argument*/Enumerator_get_Current_m3451884462_MethodInfo_var);
			V_10 = L_63;
			Goti_Status_t1473870168 * L_64 = V_10;
			NullCheck(L_64);
			bool L_65 = L_64->get_isSafe_19();
			if (L_65)
			{
				goto IL_0286;
			}
		}

IL_0250:
		{
			Goti_Status_t1473870168 * L_66 = V_10;
			NullCheck(L_66);
			bool L_67 = L_66->get_becomingSafe_35();
			if (!L_67)
			{
				goto IL_0286;
			}
		}

IL_025c:
		{
			bool L_68 = V_1;
			if (!L_68)
			{
				goto IL_0286;
			}
		}

IL_0262:
		{
			Goti_Status_t1473870168 * L_69 = V_10;
			NullCheck(L_69);
			Goti_Status_PerformMovement_m702764983(L_69, /*hidden argument*/NULL);
			Goti_Status_t1473870168 * L_70 = V_10;
			NullCheck(L_70);
			String_t* L_71 = Object_get_name_m2079638459(L_70, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_72 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral698929115, L_71, _stringLiteral2141327708, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
			Debug_Log_m920475918(NULL /*static, unused*/, L_72, /*hidden argument*/NULL);
			V_1 = (bool)0;
		}

IL_0286:
		{
			bool L_73 = Enumerator_MoveNext_m4249432516((&V_11), /*hidden argument*/Enumerator_MoveNext_m4249432516_MethodInfo_var);
			if (L_73)
			{
				goto IL_023b;
			}
		}

IL_0292:
		{
			IL2CPP_LEAVE(0x2A5, FINALLY_0297);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0297;
	}

FINALLY_0297:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3497878668((&V_11), /*hidden argument*/Enumerator_Dispose_m3497878668_MethodInfo_var);
		IL2CPP_END_FINALLY(663)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(663)
	{
		IL2CPP_JUMP_TBL(0x2A5, IL_02a5)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_02a5:
	{
		bool L_74 = V_1;
		if (!L_74)
		{
			goto IL_0327;
		}
	}
	{
		List_1_t842991300 * L_75 = __this->get_playAblePieces_7();
		NullCheck(L_75);
		Enumerator_t377720974  L_76 = List_1_GetEnumerator_m1796027824(L_75, /*hidden argument*/List_1_GetEnumerator_m1796027824_MethodInfo_var);
		V_13 = L_76;
	}

IL_02b8:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0308;
		}

IL_02bd:
		{
			Goti_Status_t1473870168 * L_77 = Enumerator_get_Current_m3451884462((&V_13), /*hidden argument*/Enumerator_get_Current_m3451884462_MethodInfo_var);
			V_12 = L_77;
			Goti_Status_t1473870168 * L_78 = V_12;
			NullCheck(L_78);
			bool L_79 = L_78->get_isSafe_19();
			if (L_79)
			{
				goto IL_0308;
			}
		}

IL_02d2:
		{
			Goti_Status_t1473870168 * L_80 = V_12;
			NullCheck(L_80);
			bool L_81 = L_80->get_mightGetAttacked_38();
			if (L_81)
			{
				goto IL_0308;
			}
		}

IL_02de:
		{
			bool L_82 = V_1;
			if (!L_82)
			{
				goto IL_0308;
			}
		}

IL_02e4:
		{
			Goti_Status_t1473870168 * L_83 = V_12;
			NullCheck(L_83);
			Goti_Status_PerformMovement_m702764983(L_83, /*hidden argument*/NULL);
			Goti_Status_t1473870168 * L_84 = V_12;
			NullCheck(L_84);
			String_t* L_85 = Object_get_name_m2079638459(L_84, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_86 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral698929115, L_85, _stringLiteral2838835724, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
			Debug_Log_m920475918(NULL /*static, unused*/, L_86, /*hidden argument*/NULL);
			V_1 = (bool)0;
		}

IL_0308:
		{
			bool L_87 = Enumerator_MoveNext_m4249432516((&V_13), /*hidden argument*/Enumerator_MoveNext_m4249432516_MethodInfo_var);
			if (L_87)
			{
				goto IL_02bd;
			}
		}

IL_0314:
		{
			IL2CPP_LEAVE(0x327, FINALLY_0319);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0319;
	}

FINALLY_0319:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3497878668((&V_13), /*hidden argument*/Enumerator_Dispose_m3497878668_MethodInfo_var);
		IL2CPP_END_FINALLY(793)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(793)
	{
		IL2CPP_JUMP_TBL(0x327, IL_0327)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0327:
	{
		bool L_88 = V_1;
		if (!L_88)
		{
			goto IL_03c0;
		}
	}
	{
		List_1_t842991300 * L_89 = __this->get_playAblePieces_7();
		NullCheck(L_89);
		Enumerator_t377720974  L_90 = List_1_GetEnumerator_m1796027824(L_89, /*hidden argument*/List_1_GetEnumerator_m1796027824_MethodInfo_var);
		V_15 = L_90;
	}

IL_033a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_03a1;
		}

IL_033f:
		{
			Goti_Status_t1473870168 * L_91 = Enumerator_get_Current_m3451884462((&V_15), /*hidden argument*/Enumerator_get_Current_m3451884462_MethodInfo_var);
			V_14 = L_91;
			Goti_Status_t1473870168 * L_92 = V_14;
			NullCheck(L_92);
			bool L_93 = L_92->get_isSafe_19();
			if (L_93)
			{
				goto IL_03a1;
			}
		}

IL_0354:
		{
			Goti_Status_t1473870168 * L_94 = V_14;
			NullCheck(L_94);
			bool L_95 = L_94->get_canChase_37();
			if (!L_95)
			{
				goto IL_03a1;
			}
		}

IL_0360:
		{
			bool L_96 = V_1;
			if (!L_96)
			{
				goto IL_03a1;
			}
		}

IL_0366:
		{
			Color_Manager_Script_t4053987423 * L_97 = __this->get_cms_4();
			NullCheck(L_97);
			int32_t L_98 = L_97->get_diceInput_7();
			Goti_Status_t1473870168 * L_99 = V_14;
			NullCheck(L_99);
			int32_t L_100 = L_99->get_chaseGap_41();
			if ((((int32_t)L_98) >= ((int32_t)L_100)))
			{
				goto IL_03a1;
			}
		}

IL_037d:
		{
			Goti_Status_t1473870168 * L_101 = V_14;
			NullCheck(L_101);
			Goti_Status_PerformMovement_m702764983(L_101, /*hidden argument*/NULL);
			Goti_Status_t1473870168 * L_102 = V_14;
			NullCheck(L_102);
			String_t* L_103 = Object_get_name_m2079638459(L_102, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_104 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral698929115, L_103, _stringLiteral2842185457, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
			Debug_Log_m920475918(NULL /*static, unused*/, L_104, /*hidden argument*/NULL);
			V_1 = (bool)0;
		}

IL_03a1:
		{
			bool L_105 = Enumerator_MoveNext_m4249432516((&V_15), /*hidden argument*/Enumerator_MoveNext_m4249432516_MethodInfo_var);
			if (L_105)
			{
				goto IL_033f;
			}
		}

IL_03ad:
		{
			IL2CPP_LEAVE(0x3C0, FINALLY_03b2);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_03b2;
	}

FINALLY_03b2:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3497878668((&V_15), /*hidden argument*/Enumerator_Dispose_m3497878668_MethodInfo_var);
		IL2CPP_END_FINALLY(946)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(946)
	{
		IL2CPP_JUMP_TBL(0x3C0, IL_03c0)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_03c0:
	{
		bool L_106 = V_1;
		if (!L_106)
		{
			goto IL_0459;
		}
	}
	{
		List_1_t842991300 * L_107 = __this->get_playAblePieces_7();
		NullCheck(L_107);
		Enumerator_t377720974  L_108 = List_1_GetEnumerator_m1796027824(L_107, /*hidden argument*/List_1_GetEnumerator_m1796027824_MethodInfo_var);
		V_17 = L_108;
	}

IL_03d3:
	try
	{ // begin try (depth: 1)
		{
			goto IL_043a;
		}

IL_03d8:
		{
			Goti_Status_t1473870168 * L_109 = Enumerator_get_Current_m3451884462((&V_17), /*hidden argument*/Enumerator_get_Current_m3451884462_MethodInfo_var);
			V_16 = L_109;
			Goti_Status_t1473870168 * L_110 = V_16;
			NullCheck(L_110);
			bool L_111 = L_110->get_isSafe_19();
			if (!L_111)
			{
				goto IL_043a;
			}
		}

IL_03ed:
		{
			Goti_Status_t1473870168 * L_112 = V_16;
			NullCheck(L_112);
			bool L_113 = L_112->get_canChase_37();
			if (!L_113)
			{
				goto IL_043a;
			}
		}

IL_03f9:
		{
			bool L_114 = V_1;
			if (!L_114)
			{
				goto IL_043a;
			}
		}

IL_03ff:
		{
			Color_Manager_Script_t4053987423 * L_115 = __this->get_cms_4();
			NullCheck(L_115);
			int32_t L_116 = L_115->get_diceInput_7();
			Goti_Status_t1473870168 * L_117 = V_16;
			NullCheck(L_117);
			int32_t L_118 = L_117->get_chaseGap_41();
			if ((((int32_t)L_116) >= ((int32_t)L_118)))
			{
				goto IL_043a;
			}
		}

IL_0416:
		{
			Goti_Status_t1473870168 * L_119 = V_16;
			NullCheck(L_119);
			Goti_Status_PerformMovement_m702764983(L_119, /*hidden argument*/NULL);
			Goti_Status_t1473870168 * L_120 = V_16;
			NullCheck(L_120);
			String_t* L_121 = Object_get_name_m2079638459(L_120, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_122 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral698929115, L_121, _stringLiteral2842185457, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
			Debug_Log_m920475918(NULL /*static, unused*/, L_122, /*hidden argument*/NULL);
			V_1 = (bool)0;
		}

IL_043a:
		{
			bool L_123 = Enumerator_MoveNext_m4249432516((&V_17), /*hidden argument*/Enumerator_MoveNext_m4249432516_MethodInfo_var);
			if (L_123)
			{
				goto IL_03d8;
			}
		}

IL_0446:
		{
			IL2CPP_LEAVE(0x459, FINALLY_044b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_044b;
	}

FINALLY_044b:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3497878668((&V_17), /*hidden argument*/Enumerator_Dispose_m3497878668_MethodInfo_var);
		IL2CPP_END_FINALLY(1099)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(1099)
	{
		IL2CPP_JUMP_TBL(0x459, IL_0459)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0459:
	{
		bool L_124 = V_1;
		if (!L_124)
		{
			goto IL_04db;
		}
	}
	{
		List_1_t842991300 * L_125 = __this->get_playAblePieces_7();
		NullCheck(L_125);
		Enumerator_t377720974  L_126 = List_1_GetEnumerator_m1796027824(L_125, /*hidden argument*/List_1_GetEnumerator_m1796027824_MethodInfo_var);
		V_19 = L_126;
	}

IL_046c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_04bc;
		}

IL_0471:
		{
			Goti_Status_t1473870168 * L_127 = Enumerator_get_Current_m3451884462((&V_19), /*hidden argument*/Enumerator_get_Current_m3451884462_MethodInfo_var);
			V_18 = L_127;
			Goti_Status_t1473870168 * L_128 = V_18;
			NullCheck(L_128);
			bool L_129 = L_128->get_isSafe_19();
			if (!L_129)
			{
				goto IL_04bc;
			}
		}

IL_0486:
		{
			Goti_Status_t1473870168 * L_130 = V_18;
			NullCheck(L_130);
			bool L_131 = L_130->get_becomingSafe_35();
			if (!L_131)
			{
				goto IL_04bc;
			}
		}

IL_0492:
		{
			bool L_132 = V_1;
			if (!L_132)
			{
				goto IL_04bc;
			}
		}

IL_0498:
		{
			Goti_Status_t1473870168 * L_133 = V_18;
			NullCheck(L_133);
			Goti_Status_PerformMovement_m702764983(L_133, /*hidden argument*/NULL);
			Goti_Status_t1473870168 * L_134 = V_18;
			NullCheck(L_134);
			String_t* L_135 = Object_get_name_m2079638459(L_134, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_136 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral698929115, L_135, _stringLiteral3533501253, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
			Debug_Log_m920475918(NULL /*static, unused*/, L_136, /*hidden argument*/NULL);
			V_1 = (bool)0;
		}

IL_04bc:
		{
			bool L_137 = Enumerator_MoveNext_m4249432516((&V_19), /*hidden argument*/Enumerator_MoveNext_m4249432516_MethodInfo_var);
			if (L_137)
			{
				goto IL_0471;
			}
		}

IL_04c8:
		{
			IL2CPP_LEAVE(0x4DB, FINALLY_04cd);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_04cd;
	}

FINALLY_04cd:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3497878668((&V_19), /*hidden argument*/Enumerator_Dispose_m3497878668_MethodInfo_var);
		IL2CPP_END_FINALLY(1229)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(1229)
	{
		IL2CPP_JUMP_TBL(0x4DB, IL_04db)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_04db:
	{
		bool L_138 = V_1;
		if (!L_138)
		{
			goto IL_055d;
		}
	}
	{
		List_1_t842991300 * L_139 = __this->get_playAblePieces_7();
		NullCheck(L_139);
		Enumerator_t377720974  L_140 = List_1_GetEnumerator_m1796027824(L_139, /*hidden argument*/List_1_GetEnumerator_m1796027824_MethodInfo_var);
		V_21 = L_140;
	}

IL_04ee:
	try
	{ // begin try (depth: 1)
		{
			goto IL_053e;
		}

IL_04f3:
		{
			Goti_Status_t1473870168 * L_141 = Enumerator_get_Current_m3451884462((&V_21), /*hidden argument*/Enumerator_get_Current_m3451884462_MethodInfo_var);
			V_20 = L_141;
			Goti_Status_t1473870168 * L_142 = V_20;
			NullCheck(L_142);
			bool L_143 = L_142->get_isSafe_19();
			if (!L_143)
			{
				goto IL_053e;
			}
		}

IL_0508:
		{
			Goti_Status_t1473870168 * L_144 = V_20;
			NullCheck(L_144);
			bool L_145 = L_144->get_mightGetAttacked_38();
			if (L_145)
			{
				goto IL_053e;
			}
		}

IL_0514:
		{
			bool L_146 = V_1;
			if (!L_146)
			{
				goto IL_053e;
			}
		}

IL_051a:
		{
			Goti_Status_t1473870168 * L_147 = V_20;
			NullCheck(L_147);
			Goti_Status_PerformMovement_m702764983(L_147, /*hidden argument*/NULL);
			Goti_Status_t1473870168 * L_148 = V_20;
			NullCheck(L_148);
			String_t* L_149 = Object_get_name_m2079638459(L_148, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_150 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral698929115, L_149, _stringLiteral3694841918, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
			Debug_Log_m920475918(NULL /*static, unused*/, L_150, /*hidden argument*/NULL);
			V_1 = (bool)0;
		}

IL_053e:
		{
			bool L_151 = Enumerator_MoveNext_m4249432516((&V_21), /*hidden argument*/Enumerator_MoveNext_m4249432516_MethodInfo_var);
			if (L_151)
			{
				goto IL_04f3;
			}
		}

IL_054a:
		{
			IL2CPP_LEAVE(0x55D, FINALLY_054f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_054f;
	}

FINALLY_054f:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3497878668((&V_21), /*hidden argument*/Enumerator_Dispose_m3497878668_MethodInfo_var);
		IL2CPP_END_FINALLY(1359)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(1359)
	{
		IL2CPP_JUMP_TBL(0x55D, IL_055d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_055d:
	{
		bool L_152 = V_1;
		if (!L_152)
		{
			goto IL_05df;
		}
	}
	{
		List_1_t842991300 * L_153 = __this->get_playAblePieces_7();
		NullCheck(L_153);
		Enumerator_t377720974  L_154 = List_1_GetEnumerator_m1796027824(L_153, /*hidden argument*/List_1_GetEnumerator_m1796027824_MethodInfo_var);
		V_23 = L_154;
	}

IL_0570:
	try
	{ // begin try (depth: 1)
		{
			goto IL_05c0;
		}

IL_0575:
		{
			Goti_Status_t1473870168 * L_155 = Enumerator_get_Current_m3451884462((&V_23), /*hidden argument*/Enumerator_get_Current_m3451884462_MethodInfo_var);
			V_22 = L_155;
			Goti_Status_t1473870168 * L_156 = V_22;
			NullCheck(L_156);
			bool L_157 = L_156->get_isSafe_19();
			if (L_157)
			{
				goto IL_05c0;
			}
		}

IL_058a:
		{
			Goti_Status_t1473870168 * L_158 = V_22;
			NullCheck(L_158);
			bool L_159 = L_158->get_winning_36();
			if (!L_159)
			{
				goto IL_05c0;
			}
		}

IL_0596:
		{
			bool L_160 = V_1;
			if (!L_160)
			{
				goto IL_05c0;
			}
		}

IL_059c:
		{
			Goti_Status_t1473870168 * L_161 = V_22;
			NullCheck(L_161);
			Goti_Status_PerformMovement_m702764983(L_161, /*hidden argument*/NULL);
			Goti_Status_t1473870168 * L_162 = V_22;
			NullCheck(L_162);
			String_t* L_163 = Object_get_name_m2079638459(L_162, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_164 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral698929115, L_163, _stringLiteral3015111838, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
			Debug_Log_m920475918(NULL /*static, unused*/, L_164, /*hidden argument*/NULL);
			V_1 = (bool)0;
		}

IL_05c0:
		{
			bool L_165 = Enumerator_MoveNext_m4249432516((&V_23), /*hidden argument*/Enumerator_MoveNext_m4249432516_MethodInfo_var);
			if (L_165)
			{
				goto IL_0575;
			}
		}

IL_05cc:
		{
			IL2CPP_LEAVE(0x5DF, FINALLY_05d1);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_05d1;
	}

FINALLY_05d1:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3497878668((&V_23), /*hidden argument*/Enumerator_Dispose_m3497878668_MethodInfo_var);
		IL2CPP_END_FINALLY(1489)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(1489)
	{
		IL2CPP_JUMP_TBL(0x5DF, IL_05df)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_05df:
	{
		bool L_166 = V_1;
		if (!L_166)
		{
			goto IL_0638;
		}
	}
	{
		List_1_t842991300 * L_167 = __this->get_playAblePieces_7();
		NullCheck(L_167);
		Enumerator_t377720974  L_168 = List_1_GetEnumerator_m1796027824(L_167, /*hidden argument*/List_1_GetEnumerator_m1796027824_MethodInfo_var);
		V_25 = L_168;
	}

IL_05f2:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0619;
		}

IL_05f7:
		{
			Goti_Status_t1473870168 * L_169 = Enumerator_get_Current_m3451884462((&V_25), /*hidden argument*/Enumerator_get_Current_m3451884462_MethodInfo_var);
			V_24 = L_169;
			bool L_170 = V_1;
			if (!L_170)
			{
				goto IL_0617;
			}
		}

IL_0606:
		{
			Goti_Status_t1473870168 * L_171 = V_24;
			NullCheck(L_171);
			Goti_Status_PerformMovement_m702764983(L_171, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
			Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1088547084, /*hidden argument*/NULL);
		}

IL_0617:
		{
			V_1 = (bool)0;
		}

IL_0619:
		{
			bool L_172 = Enumerator_MoveNext_m4249432516((&V_25), /*hidden argument*/Enumerator_MoveNext_m4249432516_MethodInfo_var);
			if (L_172)
			{
				goto IL_05f7;
			}
		}

IL_0625:
		{
			IL2CPP_LEAVE(0x638, FINALLY_062a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_062a;
	}

FINALLY_062a:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3497878668((&V_25), /*hidden argument*/Enumerator_Dispose_m3497878668_MethodInfo_var);
		IL2CPP_END_FINALLY(1578)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(1578)
	{
		IL2CPP_JUMP_TBL(0x638, IL_0638)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0638:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2919295846, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.AccessTokenMenu::.ctor()
extern "C"  void AccessTokenMenu__ctor_m3177668320 (AccessTokenMenu_t400501458 * __this, const MethodInfo* method)
{
	{
		MenuBase__ctor_m2359507012(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.AccessTokenMenu::GetGui()
extern Il2CppClass* FacebookDelegate_1_t1759649537_il2cpp_TypeInfo_var;
extern const MethodInfo* MenuBase_HandleResult_m2705601712_MethodInfo_var;
extern const MethodInfo* FacebookDelegate_1__ctor_m3953166441_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2954619648;
extern const uint32_t AccessTokenMenu_GetGui_m3309168615_MetadataUsageId;
extern "C"  void AccessTokenMenu_GetGui_m3309168615 (AccessTokenMenu_t400501458 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AccessTokenMenu_GetGui_m3309168615_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ConsoleBase_Button_m1033918234(__this, _stringLiteral2954619648, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)MenuBase_HandleResult_m2705601712_MethodInfo_var);
		FacebookDelegate_1_t1759649537 * L_2 = (FacebookDelegate_1_t1759649537 *)il2cpp_codegen_object_new(FacebookDelegate_1_t1759649537_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m3953166441(L_2, __this, L_1, /*hidden argument*/FacebookDelegate_1__ctor_m3953166441_MethodInfo_var);
		Mobile_RefreshCurrentAccessToken_m372021007(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0021:
	{
		return;
	}
}
// System.Void Facebook.Unity.Example.AppEvents::.ctor()
extern "C"  void AppEvents__ctor_m1654495994 (AppEvents_t3685765292 * __this, const MethodInfo* method)
{
	{
		MenuBase__ctor_m2359507012(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.AppEvents::GetGui()
extern Il2CppClass* Nullable_1_t339576247_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t309261261_il2cpp_TypeInfo_var;
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* LogView_t3192394209_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1755215597_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m703837045_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral821193345;
extern Il2CppCodeGenString* _stringLiteral1481037337;
extern Il2CppCodeGenString* _stringLiteral1083002369;
extern Il2CppCodeGenString* _stringLiteral2413555089;
extern Il2CppCodeGenString* _stringLiteral1611384706;
extern Il2CppCodeGenString* _stringLiteral908002437;
extern const uint32_t AppEvents_GetGui_m3257034077_MetadataUsageId;
extern "C"  void AppEvents_GetGui_m3257034077 (AppEvents_t3685765292 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppEvents_GetGui_m3257034077_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Nullable_1_t339576247  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Dictionary_2_t309261261 * V_1 = NULL;
	{
		bool L_0 = ConsoleBase_Button_m1033918234(__this, _stringLiteral821193345, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0059;
		}
	}
	{
		ConsoleBase_set_Status_m2578665855(__this, _stringLiteral1481037337, /*hidden argument*/NULL);
		Initobj (Nullable_1_t339576247_il2cpp_TypeInfo_var, (&V_0));
		Nullable_1_t339576247  L_1 = V_0;
		Dictionary_2_t309261261 * L_2 = (Dictionary_2_t309261261 *)il2cpp_codegen_object_new(Dictionary_2_t309261261_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1755215597(L_2, /*hidden argument*/Dictionary_2__ctor_m1755215597_MethodInfo_var);
		V_1 = L_2;
		Dictionary_2_t309261261 * L_3 = V_1;
		NullCheck(L_3);
		Dictionary_2_Add_m703837045(L_3, _stringLiteral2413555089, _stringLiteral1611384706, /*hidden argument*/Dictionary_2_Add_m703837045_MethodInfo_var);
		Dictionary_2_t309261261 * L_4 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_LogAppEvent_m3186815041(NULL /*static, unused*/, _stringLiteral1083002369, L_1, L_4, /*hidden argument*/NULL);
		String_t* L_5 = FB_get_AppId_m4024040373(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral908002437, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LogView_t3192394209_il2cpp_TypeInfo_var);
		LogView_AddLog_m3109927044(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
	}

IL_0059:
	{
		return;
	}
}
// System.Void Facebook.Unity.Example.AppInvites::.ctor()
extern "C"  void AppInvites__ctor_m1715532141 (AppInvites_t3031115107 * __this, const MethodInfo* method)
{
	{
		MenuBase__ctor_m2359507012(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.AppInvites::GetGui()
extern Il2CppClass* Uri_t19570940_il2cpp_TypeInfo_var;
extern Il2CppClass* FacebookDelegate_1_t2564900615_il2cpp_TypeInfo_var;
extern const MethodInfo* MenuBase_HandleResult_m2705601712_MethodInfo_var;
extern const MethodInfo* FacebookDelegate_1__ctor_m3796675595_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2844841786;
extern Il2CppCodeGenString* _stringLiteral1481037337;
extern Il2CppCodeGenString* _stringLiteral1386724099;
extern Il2CppCodeGenString* _stringLiteral1363139268;
extern Il2CppCodeGenString* _stringLiteral174124874;
extern Il2CppCodeGenString* _stringLiteral3210682012;
extern Il2CppCodeGenString* _stringLiteral1082876766;
extern Il2CppCodeGenString* _stringLiteral3131515922;
extern const uint32_t AppInvites_GetGui_m614692740_MetadataUsageId;
extern "C"  void AppInvites_GetGui_m614692740 (AppInvites_t3031115107 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppInvites_GetGui_m614692740_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ConsoleBase_Button_m1033918234(__this, _stringLiteral2844841786, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0037;
		}
	}
	{
		ConsoleBase_set_Status_m2578665855(__this, _stringLiteral1481037337, /*hidden argument*/NULL);
		Uri_t19570940 * L_1 = (Uri_t19570940 *)il2cpp_codegen_object_new(Uri_t19570940_il2cpp_TypeInfo_var);
		Uri__ctor_m3927533881(L_1, _stringLiteral1386724099, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)MenuBase_HandleResult_m2705601712_MethodInfo_var);
		FacebookDelegate_1_t2564900615 * L_3 = (FacebookDelegate_1_t2564900615 *)il2cpp_codegen_object_new(FacebookDelegate_1_t2564900615_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m3796675595(L_3, __this, L_2, /*hidden argument*/FacebookDelegate_1__ctor_m3796675595_MethodInfo_var);
		Mobile_AppInvite_m3081691722(NULL /*static, unused*/, L_1, (Uri_t19570940 *)NULL, L_3, /*hidden argument*/NULL);
	}

IL_0037:
	{
		bool L_4 = ConsoleBase_Button_m1033918234(__this, _stringLiteral1363139268, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0077;
		}
	}
	{
		ConsoleBase_set_Status_m2578665855(__this, _stringLiteral1481037337, /*hidden argument*/NULL);
		Uri_t19570940 * L_5 = (Uri_t19570940 *)il2cpp_codegen_object_new(Uri_t19570940_il2cpp_TypeInfo_var);
		Uri__ctor_m3927533881(L_5, _stringLiteral1386724099, /*hidden argument*/NULL);
		Uri_t19570940 * L_6 = (Uri_t19570940 *)il2cpp_codegen_object_new(Uri_t19570940_il2cpp_TypeInfo_var);
		Uri__ctor_m3927533881(L_6, _stringLiteral174124874, /*hidden argument*/NULL);
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)MenuBase_HandleResult_m2705601712_MethodInfo_var);
		FacebookDelegate_1_t2564900615 * L_8 = (FacebookDelegate_1_t2564900615 *)il2cpp_codegen_object_new(FacebookDelegate_1_t2564900615_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m3796675595(L_8, __this, L_7, /*hidden argument*/FacebookDelegate_1__ctor_m3796675595_MethodInfo_var);
		Mobile_AppInvite_m3081691722(NULL /*static, unused*/, L_5, L_6, L_8, /*hidden argument*/NULL);
	}

IL_0077:
	{
		bool L_9 = ConsoleBase_Button_m1033918234(__this, _stringLiteral3210682012, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_00ae;
		}
	}
	{
		ConsoleBase_set_Status_m2578665855(__this, _stringLiteral1481037337, /*hidden argument*/NULL);
		Uri_t19570940 * L_10 = (Uri_t19570940 *)il2cpp_codegen_object_new(Uri_t19570940_il2cpp_TypeInfo_var);
		Uri__ctor_m3927533881(L_10, _stringLiteral1082876766, /*hidden argument*/NULL);
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)(void*)MenuBase_HandleResult_m2705601712_MethodInfo_var);
		FacebookDelegate_1_t2564900615 * L_12 = (FacebookDelegate_1_t2564900615 *)il2cpp_codegen_object_new(FacebookDelegate_1_t2564900615_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m3796675595(L_12, __this, L_11, /*hidden argument*/FacebookDelegate_1__ctor_m3796675595_MethodInfo_var);
		Mobile_AppInvite_m3081691722(NULL /*static, unused*/, L_10, (Uri_t19570940 *)NULL, L_12, /*hidden argument*/NULL);
	}

IL_00ae:
	{
		bool L_13 = ConsoleBase_Button_m1033918234(__this, _stringLiteral3131515922, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00ee;
		}
	}
	{
		ConsoleBase_set_Status_m2578665855(__this, _stringLiteral1481037337, /*hidden argument*/NULL);
		Uri_t19570940 * L_14 = (Uri_t19570940 *)il2cpp_codegen_object_new(Uri_t19570940_il2cpp_TypeInfo_var);
		Uri__ctor_m3927533881(L_14, _stringLiteral1082876766, /*hidden argument*/NULL);
		Uri_t19570940 * L_15 = (Uri_t19570940 *)il2cpp_codegen_object_new(Uri_t19570940_il2cpp_TypeInfo_var);
		Uri__ctor_m3927533881(L_15, _stringLiteral174124874, /*hidden argument*/NULL);
		IntPtr_t L_16;
		L_16.set_m_value_0((void*)(void*)MenuBase_HandleResult_m2705601712_MethodInfo_var);
		FacebookDelegate_1_t2564900615 * L_17 = (FacebookDelegate_1_t2564900615 *)il2cpp_codegen_object_new(FacebookDelegate_1_t2564900615_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m3796675595(L_17, __this, L_16, /*hidden argument*/FacebookDelegate_1__ctor_m3796675595_MethodInfo_var);
		Mobile_AppInvite_m3081691722(NULL /*static, unused*/, L_14, L_15, L_17, /*hidden argument*/NULL);
	}

IL_00ee:
	{
		return;
	}
}
// System.Void Facebook.Unity.Example.AppLinks::.ctor()
extern "C"  void AppLinks__ctor_m2376835670 (AppLinks_t3144027900 * __this, const MethodInfo* method)
{
	{
		MenuBase__ctor_m2359507012(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.AppLinks::GetGui()
extern Il2CppClass* FacebookDelegate_1_t2578089594_il2cpp_TypeInfo_var;
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern const MethodInfo* MenuBase_HandleResult_m2705601712_MethodInfo_var;
extern const MethodInfo* FacebookDelegate_1__ctor_m2369388222_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral712825407;
extern Il2CppCodeGenString* _stringLiteral2177456342;
extern const uint32_t AppLinks_GetGui_m998202125_MetadataUsageId;
extern "C"  void AppLinks_GetGui_m998202125 (AppLinks_t3144027900 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppLinks_GetGui_m998202125_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ConsoleBase_Button_m1033918234(__this, _stringLiteral712825407, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)MenuBase_HandleResult_m2705601712_MethodInfo_var);
		FacebookDelegate_1_t2578089594 * L_2 = (FacebookDelegate_1_t2578089594 *)il2cpp_codegen_object_new(FacebookDelegate_1_t2578089594_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m2369388222(L_2, __this, L_1, /*hidden argument*/FacebookDelegate_1__ctor_m2369388222_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_GetAppLink_m1904410119(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0021:
	{
		bool L_3 = Constants_get_IsMobile_m868982319(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_004c;
		}
	}
	{
		bool L_4 = ConsoleBase_Button_m1033918234(__this, _stringLiteral2177456342, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_004c;
		}
	}
	{
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)MenuBase_HandleResult_m2705601712_MethodInfo_var);
		FacebookDelegate_1_t2578089594 * L_6 = (FacebookDelegate_1_t2578089594 *)il2cpp_codegen_object_new(FacebookDelegate_1_t2578089594_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m2369388222(L_6, __this, L_5, /*hidden argument*/FacebookDelegate_1__ctor_m2369388222_MethodInfo_var);
		Mobile_FetchDeferredAppLinkData_m2117344005(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
	}

IL_004c:
	{
		return;
	}
}
// System.Void Facebook.Unity.Example.AppRequests::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* OGActionType_t1978093408_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3061400272;
extern const uint32_t AppRequests__ctor_m2641880517_MetadataUsageId;
extern "C"  void AppRequests__ctor_m2641880517 (AppRequests_t1683098915 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppRequests__ctor_m2641880517_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_requestMessage_14(L_0);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_requestTo_15(L_1);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_requestFilter_16(L_2);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_requestExcludes_17(L_3);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_requestMax_18(L_4);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_requestData_19(L_5);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_requestTitle_20(L_6);
		String_t* L_7 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_requestObjectID_21(L_7);
		StringU5BU5D_t1642385972* L_8 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, _stringLiteral3061400272);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3061400272);
		StringU5BU5D_t1642385972* L_9 = L_8;
		V_0 = 0;
		Il2CppObject * L_10 = Box(OGActionType_t1978093408_il2cpp_TypeInfo_var, (&V_0));
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_11);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_11);
		StringU5BU5D_t1642385972* L_12 = L_9;
		V_1 = 1;
		Il2CppObject * L_13 = Box(OGActionType_t1978093408_il2cpp_TypeInfo_var, (&V_1));
		NullCheck(L_13);
		String_t* L_14 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_13);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_14);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_14);
		StringU5BU5D_t1642385972* L_15 = L_12;
		V_2 = 2;
		Il2CppObject * L_16 = Box(OGActionType_t1978093408_il2cpp_TypeInfo_var, (&V_2));
		NullCheck(L_16);
		String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_16);
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, L_17);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_17);
		__this->set_actionTypeStrings_23(L_15);
		MenuBase__ctor_m2359507012(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.AppRequests::GetGui()
extern Il2CppClass* Nullable_1_t334943763_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* FacebookDelegate_1_t909463455_il2cpp_TypeInfo_var;
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t2058570427_il2cpp_TypeInfo_var;
extern Il2CppClass* GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var;
extern Il2CppClass* ConsoleBase_t4290192428_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern const MethodInfo* MenuBase_HandleResult_m2705601712_MethodInfo_var;
extern const MethodInfo* FacebookDelegate_1__ctor_m113473383_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m2362181390_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2043351842_MethodInfo_var;
extern const MethodInfo* Nullable_1__ctor_m255206972_MethodInfo_var;
extern const MethodInfo* Nullable_1_get_HasValue_m3034365232_MethodInfo_var;
extern const MethodInfo* Nullable_1_get_Value_m1809768199_MethodInfo_var;
extern const MethodInfo* Enumerable_OfType_TisIl2CppObject_m32700024_MethodInfo_var;
extern const MethodInfo* Enumerable_ToList_TisIl2CppObject_m463720312_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1791455987;
extern Il2CppCodeGenString* _stringLiteral1658360153;
extern Il2CppCodeGenString* _stringLiteral513324085;
extern Il2CppCodeGenString* _stringLiteral3703024654;
extern Il2CppCodeGenString* _stringLiteral1643770149;
extern Il2CppCodeGenString* _stringLiteral3427463390;
extern Il2CppCodeGenString* _stringLiteral3925014021;
extern Il2CppCodeGenString* _stringLiteral186626780;
extern Il2CppCodeGenString* _stringLiteral327073669;
extern Il2CppCodeGenString* _stringLiteral2007600385;
extern Il2CppCodeGenString* _stringLiteral4044525897;
extern Il2CppCodeGenString* _stringLiteral2246073445;
extern Il2CppCodeGenString* _stringLiteral2784896389;
extern Il2CppCodeGenString* _stringLiteral257063285;
extern Il2CppCodeGenString* _stringLiteral2702913032;
extern Il2CppCodeGenString* _stringLiteral1498664412;
extern Il2CppCodeGenString* _stringLiteral3986508123;
extern const uint32_t AppRequests_GetGui_m1295348440_MetadataUsageId;
extern "C"  void AppRequests_GetGui_m1295348440 (AppRequests_t1683098915 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppRequests_GetGui_m1295348440_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Nullable_1_t334943763  V_0;
	memset(&V_0, 0, sizeof(V_0));
	List_1_t2058570427 * V_1 = NULL;
	List_1_t2058570427 * V_2 = NULL;
	List_1_t2058570427 * V_3 = NULL;
	Nullable_1_t241159723  V_4;
	memset(&V_4, 0, sizeof(V_4));
	String_t* G_B10_0 = NULL;
	int32_t G_B10_1 = 0;
	String_t* G_B10_2 = NULL;
	String_t* G_B9_0 = NULL;
	int32_t G_B9_1 = 0;
	String_t* G_B9_2 = NULL;
	StringU5BU5D_t1642385972* G_B11_0 = NULL;
	String_t* G_B11_1 = NULL;
	int32_t G_B11_2 = 0;
	String_t* G_B11_3 = NULL;
	String_t* G_B14_0 = NULL;
	String_t* G_B13_0 = NULL;
	StringU5BU5D_t1642385972* G_B15_0 = NULL;
	String_t* G_B15_1 = NULL;
	StringU5BU5D_t1642385972* G_B17_0 = NULL;
	String_t* G_B17_1 = NULL;
	StringU5BU5D_t1642385972* G_B16_0 = NULL;
	String_t* G_B16_1 = NULL;
	List_1_t2058570427 * G_B18_0 = NULL;
	StringU5BU5D_t1642385972* G_B18_1 = NULL;
	String_t* G_B18_2 = NULL;
	List_1_t2058570427 * G_B20_0 = NULL;
	StringU5BU5D_t1642385972* G_B20_1 = NULL;
	String_t* G_B20_2 = NULL;
	List_1_t2058570427 * G_B19_0 = NULL;
	StringU5BU5D_t1642385972* G_B19_1 = NULL;
	String_t* G_B19_2 = NULL;
	StringU5BU5D_t1642385972* G_B21_0 = NULL;
	List_1_t2058570427 * G_B21_1 = NULL;
	StringU5BU5D_t1642385972* G_B21_2 = NULL;
	String_t* G_B21_3 = NULL;
	StringU5BU5D_t1642385972* G_B23_0 = NULL;
	List_1_t2058570427 * G_B23_1 = NULL;
	StringU5BU5D_t1642385972* G_B23_2 = NULL;
	String_t* G_B23_3 = NULL;
	StringU5BU5D_t1642385972* G_B22_0 = NULL;
	List_1_t2058570427 * G_B22_1 = NULL;
	StringU5BU5D_t1642385972* G_B22_2 = NULL;
	String_t* G_B22_3 = NULL;
	int32_t G_B24_0 = 0;
	StringU5BU5D_t1642385972* G_B24_1 = NULL;
	List_1_t2058570427 * G_B24_2 = NULL;
	StringU5BU5D_t1642385972* G_B24_3 = NULL;
	String_t* G_B24_4 = NULL;
	{
		bool L_0 = ConsoleBase_Button_m1033918234(__this, _stringLiteral1791455987, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_003c;
		}
	}
	{
		Initobj (Nullable_1_t334943763_il2cpp_TypeInfo_var, (&V_0));
		Nullable_1_t334943763  L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)MenuBase_HandleResult_m2705601712_MethodInfo_var);
		FacebookDelegate_1_t909463455 * L_5 = (FacebookDelegate_1_t909463455 *)il2cpp_codegen_object_new(FacebookDelegate_1_t909463455_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m113473383(L_5, __this, L_4, /*hidden argument*/FacebookDelegate_1__ctor_m113473383_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_AppRequest_m1975988946(NULL /*static, unused*/, _stringLiteral1658360153, (Il2CppObject*)NULL, (Il2CppObject*)NULL, (Il2CppObject*)NULL, L_1, L_2, L_3, L_5, /*hidden argument*/NULL);
	}

IL_003c:
	{
		bool L_6 = ConsoleBase_Button_m1033918234(__this, _stringLiteral513324085, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0088;
		}
	}
	{
		List_1_t2058570427 * L_7 = (List_1_t2058570427 *)il2cpp_codegen_object_new(List_1_t2058570427_il2cpp_TypeInfo_var);
		List_1__ctor_m2362181390(L_7, /*hidden argument*/List_1__ctor_m2362181390_MethodInfo_var);
		V_2 = L_7;
		List_1_t2058570427 * L_8 = V_2;
		NullCheck(L_8);
		List_1_Add_m2043351842(L_8, _stringLiteral3703024654, /*hidden argument*/List_1_Add_m2043351842_MethodInfo_var);
		List_1_t2058570427 * L_9 = V_2;
		V_1 = L_9;
		List_1_t2058570427 * L_10 = V_1;
		Nullable_1_t334943763  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Nullable_1__ctor_m255206972(&L_11, 0, /*hidden argument*/Nullable_1__ctor_m255206972_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_13 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)(void*)MenuBase_HandleResult_m2705601712_MethodInfo_var);
		FacebookDelegate_1_t909463455 * L_15 = (FacebookDelegate_1_t909463455 *)il2cpp_codegen_object_new(FacebookDelegate_1_t909463455_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m113473383(L_15, __this, L_14, /*hidden argument*/FacebookDelegate_1__ctor_m113473383_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_AppRequest_m1975988946(NULL /*static, unused*/, _stringLiteral1658360153, (Il2CppObject*)NULL, L_10, (Il2CppObject*)NULL, L_11, L_12, L_13, L_15, /*hidden argument*/NULL);
	}

IL_0088:
	{
		bool L_16 = ConsoleBase_Button_m1033918234(__this, _stringLiteral1643770149, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00d4;
		}
	}
	{
		List_1_t2058570427 * L_17 = (List_1_t2058570427 *)il2cpp_codegen_object_new(List_1_t2058570427_il2cpp_TypeInfo_var);
		List_1__ctor_m2362181390(L_17, /*hidden argument*/List_1__ctor_m2362181390_MethodInfo_var);
		V_2 = L_17;
		List_1_t2058570427 * L_18 = V_2;
		NullCheck(L_18);
		List_1_Add_m2043351842(L_18, _stringLiteral3427463390, /*hidden argument*/List_1_Add_m2043351842_MethodInfo_var);
		List_1_t2058570427 * L_19 = V_2;
		V_3 = L_19;
		List_1_t2058570427 * L_20 = V_3;
		Nullable_1_t334943763  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Nullable_1__ctor_m255206972(&L_21, 0, /*hidden argument*/Nullable_1__ctor_m255206972_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_23 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		IntPtr_t L_24;
		L_24.set_m_value_0((void*)(void*)MenuBase_HandleResult_m2705601712_MethodInfo_var);
		FacebookDelegate_1_t909463455 * L_25 = (FacebookDelegate_1_t909463455 *)il2cpp_codegen_object_new(FacebookDelegate_1_t909463455_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m113473383(L_25, __this, L_24, /*hidden argument*/FacebookDelegate_1__ctor_m113473383_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_AppRequest_m1975988946(NULL /*static, unused*/, _stringLiteral1658360153, (Il2CppObject*)NULL, L_20, (Il2CppObject*)NULL, L_21, L_22, L_23, L_25, /*hidden argument*/NULL);
	}

IL_00d4:
	{
		String_t** L_26 = __this->get_address_of_requestMessage_14();
		ConsoleBase_LabelAndTextField_m425453358(__this, _stringLiteral3925014021, L_26, /*hidden argument*/NULL);
		String_t** L_27 = __this->get_address_of_requestTo_15();
		ConsoleBase_LabelAndTextField_m425453358(__this, _stringLiteral186626780, L_27, /*hidden argument*/NULL);
		String_t** L_28 = __this->get_address_of_requestFilter_16();
		ConsoleBase_LabelAndTextField_m425453358(__this, _stringLiteral327073669, L_28, /*hidden argument*/NULL);
		String_t** L_29 = __this->get_address_of_requestExcludes_17();
		ConsoleBase_LabelAndTextField_m425453358(__this, _stringLiteral2007600385, L_29, /*hidden argument*/NULL);
		String_t** L_30 = __this->get_address_of_requestExcludes_17();
		ConsoleBase_LabelAndTextField_m425453358(__this, _stringLiteral4044525897, L_30, /*hidden argument*/NULL);
		String_t** L_31 = __this->get_address_of_requestMax_18();
		ConsoleBase_LabelAndTextField_m425453358(__this, _stringLiteral2246073445, L_31, /*hidden argument*/NULL);
		String_t** L_32 = __this->get_address_of_requestData_19();
		ConsoleBase_LabelAndTextField_m425453358(__this, _stringLiteral2784896389, L_32, /*hidden argument*/NULL);
		String_t** L_33 = __this->get_address_of_requestTitle_20();
		ConsoleBase_LabelAndTextField_m425453358(__this, _stringLiteral257063285, L_33, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m212592284(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_34 = ConsoleBase_get_LabelStyle_m3739078378(__this, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2108882777* L_35 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)1));
		float L_36 = ConsoleBase_get_ScaleFactor_m2222574120(__this, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_37 = GUILayout_MaxWidth_m3473061269(NULL /*static, unused*/, ((float)((float)(200.0f)*(float)L_36)), /*hidden argument*/NULL);
		NullCheck(L_35);
		ArrayElementTypeCheck (L_35, L_37);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_37);
		GUILayout_Label_m616748838(NULL /*static, unused*/, _stringLiteral2702913032, L_34, L_35, /*hidden argument*/NULL);
		int32_t L_38 = __this->get_selectedAction_22();
		StringU5BU5D_t1642385972* L_39 = __this->get_actionTypeStrings_23();
		GUIStyle_t1799908754 * L_40 = ConsoleBase_get_ButtonStyle_m304276404(__this, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2108882777* L_41 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		IL2CPP_RUNTIME_CLASS_INIT(ConsoleBase_t4290192428_il2cpp_TypeInfo_var);
		int32_t L_42 = ConsoleBase_get_ButtonHeight_m504918286(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_43 = ConsoleBase_get_ScaleFactor_m2222574120(__this, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_44 = GUILayout_MinHeight_m1829895296(NULL /*static, unused*/, ((float)((float)(((float)((float)L_42)))*(float)L_43)), /*hidden argument*/NULL);
		NullCheck(L_41);
		ArrayElementTypeCheck (L_41, L_44);
		(L_41)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_44);
		GUILayoutOptionU5BU5D_t2108882777* L_45 = L_41;
		int32_t L_46 = ConsoleBase_get_MainWindowWidth_m4208023734(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_47 = GUILayout_MaxWidth_m3473061269(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_46-(int32_t)((int32_t)150)))))), /*hidden argument*/NULL);
		NullCheck(L_45);
		ArrayElementTypeCheck (L_45, L_47);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_47);
		int32_t L_48 = GUILayout_Toolbar_m1679488300(NULL /*static, unused*/, L_38, L_39, L_40, L_45, /*hidden argument*/NULL);
		__this->set_selectedAction_22(L_48);
		GUILayout_EndHorizontal_m4258536965(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t** L_49 = __this->get_address_of_requestObjectID_21();
		ConsoleBase_LabelAndTextField_m425453358(__this, _stringLiteral1498664412, L_49, /*hidden argument*/NULL);
		bool L_50 = ConsoleBase_Button_m1033918234(__this, _stringLiteral3986508123, /*hidden argument*/NULL);
		if (!L_50)
		{
			goto IL_034f;
		}
	}
	{
		Nullable_1_t241159723  L_51 = AppRequests_GetSelectedOGActionType_m218093443(__this, /*hidden argument*/NULL);
		V_4 = L_51;
		bool L_52 = Nullable_1_get_HasValue_m3034365232((&V_4), /*hidden argument*/Nullable_1_get_HasValue_m3034365232_MethodInfo_var);
		if (!L_52)
		{
			goto IL_0278;
		}
	}
	{
		String_t* L_53 = __this->get_requestMessage_14();
		int32_t L_54 = Nullable_1_get_Value_m1809768199((&V_4), /*hidden argument*/Nullable_1_get_Value_m1809768199_MethodInfo_var);
		String_t* L_55 = __this->get_requestObjectID_21();
		String_t* L_56 = __this->get_requestTo_15();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_57 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_56, /*hidden argument*/NULL);
		G_B9_0 = L_55;
		G_B9_1 = L_54;
		G_B9_2 = L_53;
		if (!L_57)
		{
			G_B10_0 = L_55;
			G_B10_1 = L_54;
			G_B10_2 = L_53;
			goto IL_0240;
		}
	}
	{
		G_B11_0 = ((StringU5BU5D_t1642385972*)(NULL));
		G_B11_1 = G_B9_0;
		G_B11_2 = G_B9_1;
		G_B11_3 = G_B9_2;
		goto IL_0256;
	}

IL_0240:
	{
		String_t* L_58 = __this->get_requestTo_15();
		CharU5BU5D_t1328083999* L_59 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_59);
		(L_59)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)44));
		NullCheck(L_58);
		StringU5BU5D_t1642385972* L_60 = String_Split_m3326265864(L_58, L_59, /*hidden argument*/NULL);
		G_B11_0 = L_60;
		G_B11_1 = G_B10_0;
		G_B11_2 = G_B10_1;
		G_B11_3 = G_B10_2;
	}

IL_0256:
	{
		String_t* L_61 = __this->get_requestData_19();
		String_t* L_62 = __this->get_requestTitle_20();
		IntPtr_t L_63;
		L_63.set_m_value_0((void*)(void*)MenuBase_HandleResult_m2705601712_MethodInfo_var);
		FacebookDelegate_1_t909463455 * L_64 = (FacebookDelegate_1_t909463455 *)il2cpp_codegen_object_new(FacebookDelegate_1_t909463455_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m113473383(L_64, __this, L_63, /*hidden argument*/FacebookDelegate_1__ctor_m113473383_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_AppRequest_m380848205(NULL /*static, unused*/, G_B11_3, G_B11_2, G_B11_1, (Il2CppObject*)(Il2CppObject*)G_B11_0, L_61, L_62, L_64, /*hidden argument*/NULL);
		goto IL_034f;
	}

IL_0278:
	{
		String_t* L_65 = __this->get_requestMessage_14();
		String_t* L_66 = __this->get_requestTo_15();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_67 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_66, /*hidden argument*/NULL);
		G_B13_0 = L_65;
		if (!L_67)
		{
			G_B14_0 = L_65;
			goto IL_0294;
		}
	}
	{
		G_B15_0 = ((StringU5BU5D_t1642385972*)(NULL));
		G_B15_1 = G_B13_0;
		goto IL_02aa;
	}

IL_0294:
	{
		String_t* L_68 = __this->get_requestTo_15();
		CharU5BU5D_t1328083999* L_69 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_69);
		(L_69)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)44));
		NullCheck(L_68);
		StringU5BU5D_t1642385972* L_70 = String_Split_m3326265864(L_68, L_69, /*hidden argument*/NULL);
		G_B15_0 = L_70;
		G_B15_1 = G_B14_0;
	}

IL_02aa:
	{
		String_t* L_71 = __this->get_requestFilter_16();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_72 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_71, /*hidden argument*/NULL);
		G_B16_0 = G_B15_0;
		G_B16_1 = G_B15_1;
		if (!L_72)
		{
			G_B17_0 = G_B15_0;
			G_B17_1 = G_B15_1;
			goto IL_02c0;
		}
	}
	{
		G_B18_0 = ((List_1_t2058570427 *)(NULL));
		G_B18_1 = G_B16_0;
		G_B18_2 = G_B16_1;
		goto IL_02e0;
	}

IL_02c0:
	{
		String_t* L_73 = __this->get_requestFilter_16();
		CharU5BU5D_t1328083999* L_74 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_74);
		(L_74)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)44));
		NullCheck(L_73);
		StringU5BU5D_t1642385972* L_75 = String_Split_m3326265864(L_73, L_74, /*hidden argument*/NULL);
		Il2CppObject* L_76 = Enumerable_OfType_TisIl2CppObject_m32700024(NULL /*static, unused*/, (Il2CppObject *)(Il2CppObject *)L_75, /*hidden argument*/Enumerable_OfType_TisIl2CppObject_m32700024_MethodInfo_var);
		List_1_t2058570427 * L_77 = Enumerable_ToList_TisIl2CppObject_m463720312(NULL /*static, unused*/, L_76, /*hidden argument*/Enumerable_ToList_TisIl2CppObject_m463720312_MethodInfo_var);
		G_B18_0 = L_77;
		G_B18_1 = G_B17_0;
		G_B18_2 = G_B17_1;
	}

IL_02e0:
	{
		String_t* L_78 = __this->get_requestExcludes_17();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_79 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_78, /*hidden argument*/NULL);
		G_B19_0 = G_B18_0;
		G_B19_1 = G_B18_1;
		G_B19_2 = G_B18_2;
		if (!L_79)
		{
			G_B20_0 = G_B18_0;
			G_B20_1 = G_B18_1;
			G_B20_2 = G_B18_2;
			goto IL_02f6;
		}
	}
	{
		G_B21_0 = ((StringU5BU5D_t1642385972*)(NULL));
		G_B21_1 = G_B19_0;
		G_B21_2 = G_B19_1;
		G_B21_3 = G_B19_2;
		goto IL_030c;
	}

IL_02f6:
	{
		String_t* L_80 = __this->get_requestExcludes_17();
		CharU5BU5D_t1328083999* L_81 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_81);
		(L_81)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)44));
		NullCheck(L_80);
		StringU5BU5D_t1642385972* L_82 = String_Split_m3326265864(L_80, L_81, /*hidden argument*/NULL);
		G_B21_0 = L_82;
		G_B21_1 = G_B20_0;
		G_B21_2 = G_B20_1;
		G_B21_3 = G_B20_2;
	}

IL_030c:
	{
		String_t* L_83 = __this->get_requestMax_18();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_84 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_83, /*hidden argument*/NULL);
		G_B22_0 = G_B21_0;
		G_B22_1 = G_B21_1;
		G_B22_2 = G_B21_2;
		G_B22_3 = G_B21_3;
		if (!L_84)
		{
			G_B23_0 = G_B21_0;
			G_B23_1 = G_B21_1;
			G_B23_2 = G_B21_2;
			G_B23_3 = G_B21_3;
			goto IL_0322;
		}
	}
	{
		G_B24_0 = 0;
		G_B24_1 = G_B22_0;
		G_B24_2 = G_B22_1;
		G_B24_3 = G_B22_2;
		G_B24_4 = G_B22_3;
		goto IL_032d;
	}

IL_0322:
	{
		String_t* L_85 = __this->get_requestMax_18();
		int32_t L_86 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_85, /*hidden argument*/NULL);
		G_B24_0 = L_86;
		G_B24_1 = G_B23_0;
		G_B24_2 = G_B23_1;
		G_B24_3 = G_B23_2;
		G_B24_4 = G_B23_3;
	}

IL_032d:
	{
		Nullable_1_t334943763  L_87;
		memset(&L_87, 0, sizeof(L_87));
		Nullable_1__ctor_m255206972(&L_87, G_B24_0, /*hidden argument*/Nullable_1__ctor_m255206972_MethodInfo_var);
		String_t* L_88 = __this->get_requestData_19();
		String_t* L_89 = __this->get_requestTitle_20();
		IntPtr_t L_90;
		L_90.set_m_value_0((void*)(void*)MenuBase_HandleResult_m2705601712_MethodInfo_var);
		FacebookDelegate_1_t909463455 * L_91 = (FacebookDelegate_1_t909463455 *)il2cpp_codegen_object_new(FacebookDelegate_1_t909463455_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m113473383(L_91, __this, L_90, /*hidden argument*/FacebookDelegate_1__ctor_m113473383_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_AppRequest_m1975988946(NULL /*static, unused*/, G_B24_4, (Il2CppObject*)(Il2CppObject*)G_B24_3, G_B24_2, (Il2CppObject*)(Il2CppObject*)G_B24_1, L_87, L_88, L_89, L_91, /*hidden argument*/NULL);
	}

IL_034f:
	{
		return;
	}
}
// System.Nullable`1<Facebook.Unity.OGActionType> Facebook.Unity.Example.AppRequests::GetSelectedOGActionType()
extern Il2CppClass* OGActionType_t1978093408_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Nullable_1_t241159723_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1__ctor_m2425814835_MethodInfo_var;
extern const uint32_t AppRequests_GetSelectedOGActionType_m218093443_MetadataUsageId;
extern "C"  Nullable_1_t241159723  AppRequests_GetSelectedOGActionType_m218093443 (AppRequests_t1683098915 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppRequests_GetSelectedOGActionType_m218093443_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Nullable_1_t241159723  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		StringU5BU5D_t1642385972* L_0 = __this->get_actionTypeStrings_23();
		int32_t L_1 = __this->get_selectedAction_22();
		NullCheck(L_0);
		int32_t L_2 = L_1;
		String_t* L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_0 = L_3;
		String_t* L_4 = V_0;
		V_1 = 0;
		Il2CppObject * L_5 = Box(OGActionType_t1978093408_il2cpp_TypeInfo_var, (&V_1));
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_002f;
		}
	}
	{
		Nullable_1_t241159723  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Nullable_1__ctor_m2425814835(&L_8, 0, /*hidden argument*/Nullable_1__ctor_m2425814835_MethodInfo_var);
		return L_8;
	}

IL_002f:
	{
		String_t* L_9 = V_0;
		V_2 = 1;
		Il2CppObject * L_10 = Box(OGActionType_t1978093408_il2cpp_TypeInfo_var, (&V_2));
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0050;
		}
	}
	{
		Nullable_1_t241159723  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Nullable_1__ctor_m2425814835(&L_13, 1, /*hidden argument*/Nullable_1__ctor_m2425814835_MethodInfo_var);
		return L_13;
	}

IL_0050:
	{
		String_t* L_14 = V_0;
		V_3 = 2;
		Il2CppObject * L_15 = Box(OGActionType_t1978093408_il2cpp_TypeInfo_var, (&V_3));
		NullCheck(L_15);
		String_t* L_16 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_15);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_17 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_14, L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0071;
		}
	}
	{
		Nullable_1_t241159723  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Nullable_1__ctor_m2425814835(&L_18, 2, /*hidden argument*/Nullable_1__ctor_m2425814835_MethodInfo_var);
		return L_18;
	}

IL_0071:
	{
		Initobj (Nullable_1_t241159723_il2cpp_TypeInfo_var, (&V_4));
		Nullable_1_t241159723  L_19 = V_4;
		return L_19;
	}
}
// System.Void Facebook.Unity.Example.ConsoleBase::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral28858563;
extern const uint32_t ConsoleBase__ctor_m2763526896_MetadataUsageId;
extern "C"  void ConsoleBase__ctor_m2763526896 (ConsoleBase_t4290192428 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConsoleBase__ctor_m2763526896_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_status_4(_stringLiteral28858563);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_lastResponse_5(L_0);
		Vector2_t2243707579  L_1 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_scrollPosition_6(L_1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Facebook.Unity.Example.ConsoleBase::get_ButtonHeight()
extern "C"  int32_t ConsoleBase_get_ButtonHeight_m504918286 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		bool L_0 = Constants_get_IsMobile_m868982319(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = ((int32_t)60);
		goto IL_0013;
	}

IL_0011:
	{
		G_B3_0 = ((int32_t)24);
	}

IL_0013:
	{
		return G_B3_0;
	}
}
// System.Int32 Facebook.Unity.Example.ConsoleBase::get_MainWindowWidth()
extern "C"  int32_t ConsoleBase_get_MainWindowWidth_m4208023734 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		bool L_0 = Constants_get_IsMobile_m868982319(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_1 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)((int32_t)L_1-(int32_t)((int32_t)30)));
		goto IL_001c;
	}

IL_0017:
	{
		G_B3_0 = ((int32_t)700);
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// System.Int32 Facebook.Unity.Example.ConsoleBase::get_MainWindowFullWidth()
extern "C"  int32_t ConsoleBase_get_MainWindowFullWidth_m1784023901 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		bool L_0 = Constants_get_IsMobile_m868982319(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_1 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_0014:
	{
		G_B3_0 = ((int32_t)760);
	}

IL_0019:
	{
		return G_B3_0;
	}
}
// System.Int32 Facebook.Unity.Example.ConsoleBase::get_MarginFix()
extern "C"  int32_t ConsoleBase_get_MarginFix_m1996802084 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		bool L_0 = Constants_get_IsMobile_m868982319(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_0012;
	}

IL_0010:
	{
		G_B3_0 = ((int32_t)48);
	}

IL_0012:
	{
		return G_B3_0;
	}
}
// System.Collections.Generic.Stack`1<System.String> Facebook.Unity.Example.ConsoleBase::get_MenuStack()
extern Il2CppClass* ConsoleBase_t4290192428_il2cpp_TypeInfo_var;
extern const uint32_t ConsoleBase_get_MenuStack_m2022459395_MetadataUsageId;
extern "C"  Stack_1_t3116948387 * ConsoleBase_get_MenuStack_m2022459395 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConsoleBase_get_MenuStack_m2022459395_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ConsoleBase_t4290192428_il2cpp_TypeInfo_var);
		Stack_1_t3116948387 * L_0 = ((ConsoleBase_t4290192428_StaticFields*)ConsoleBase_t4290192428_il2cpp_TypeInfo_var->static_fields)->get_menuStack_3();
		return L_0;
	}
}
// System.Void Facebook.Unity.Example.ConsoleBase::set_MenuStack(System.Collections.Generic.Stack`1<System.String>)
extern Il2CppClass* ConsoleBase_t4290192428_il2cpp_TypeInfo_var;
extern const uint32_t ConsoleBase_set_MenuStack_m820012914_MetadataUsageId;
extern "C"  void ConsoleBase_set_MenuStack_m820012914 (Il2CppObject * __this /* static, unused */, Stack_1_t3116948387 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConsoleBase_set_MenuStack_m820012914_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Stack_1_t3116948387 * L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(ConsoleBase_t4290192428_il2cpp_TypeInfo_var);
		((ConsoleBase_t4290192428_StaticFields*)ConsoleBase_t4290192428_il2cpp_TypeInfo_var->static_fields)->set_menuStack_3(L_0);
		return;
	}
}
// System.String Facebook.Unity.Example.ConsoleBase::get_Status()
extern "C"  String_t* ConsoleBase_get_Status_m15166664 (ConsoleBase_t4290192428 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_status_4();
		return L_0;
	}
}
// System.Void Facebook.Unity.Example.ConsoleBase::set_Status(System.String)
extern "C"  void ConsoleBase_set_Status_m2578665855 (ConsoleBase_t4290192428 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_status_4(L_0);
		return;
	}
}
// UnityEngine.Texture2D Facebook.Unity.Example.ConsoleBase::get_LastResponseTexture()
extern "C"  Texture2D_t3542995729 * ConsoleBase_get_LastResponseTexture_m4086112206 (ConsoleBase_t4290192428 * __this, const MethodInfo* method)
{
	{
		Texture2D_t3542995729 * L_0 = __this->get_U3CLastResponseTextureU3Ek__BackingField_12();
		return L_0;
	}
}
// System.Void Facebook.Unity.Example.ConsoleBase::set_LastResponseTexture(UnityEngine.Texture2D)
extern "C"  void ConsoleBase_set_LastResponseTexture_m704700253 (ConsoleBase_t4290192428 * __this, Texture2D_t3542995729 * ___value0, const MethodInfo* method)
{
	{
		Texture2D_t3542995729 * L_0 = ___value0;
		__this->set_U3CLastResponseTextureU3Ek__BackingField_12(L_0);
		return;
	}
}
// System.String Facebook.Unity.Example.ConsoleBase::get_LastResponse()
extern "C"  String_t* ConsoleBase_get_LastResponse_m1599769685 (ConsoleBase_t4290192428 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_lastResponse_5();
		return L_0;
	}
}
// System.Void Facebook.Unity.Example.ConsoleBase::set_LastResponse(System.String)
extern "C"  void ConsoleBase_set_LastResponse_m510948058 (ConsoleBase_t4290192428 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_lastResponse_5(L_0);
		return;
	}
}
// UnityEngine.Vector2 Facebook.Unity.Example.ConsoleBase::get_ScrollPosition()
extern "C"  Vector2_t2243707579  ConsoleBase_get_ScrollPosition_m3651860150 (ConsoleBase_t4290192428 * __this, const MethodInfo* method)
{
	{
		Vector2_t2243707579  L_0 = __this->get_scrollPosition_6();
		return L_0;
	}
}
// System.Void Facebook.Unity.Example.ConsoleBase::set_ScrollPosition(UnityEngine.Vector2)
extern "C"  void ConsoleBase_set_ScrollPosition_m2281592185 (ConsoleBase_t4290192428 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	{
		Vector2_t2243707579  L_0 = ___value0;
		__this->set_scrollPosition_6(L_0);
		return;
	}
}
// System.Single Facebook.Unity.Example.ConsoleBase::get_ScaleFactor()
extern const MethodInfo* Nullable_1_get_HasValue_m3526318906_MethodInfo_var;
extern const MethodInfo* Nullable_1__ctor_m1978009879_MethodInfo_var;
extern const MethodInfo* Nullable_1_get_Value_m672124572_MethodInfo_var;
extern const uint32_t ConsoleBase_get_ScaleFactor_m2222574120_MetadataUsageId;
extern "C"  float ConsoleBase_get_ScaleFactor_m2222574120 (ConsoleBase_t4290192428 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConsoleBase_get_ScaleFactor_m2222574120_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Nullable_1_t339576247 * L_0 = __this->get_address_of_scaleFactor_7();
		bool L_1 = Nullable_1_get_HasValue_m3526318906(L_0, /*hidden argument*/Nullable_1_get_HasValue_m3526318906_MethodInfo_var);
		if (L_1)
		{
			goto IL_0026;
		}
	}
	{
		float L_2 = Screen_get_dpi_m3345126327(NULL /*static, unused*/, /*hidden argument*/NULL);
		Nullable_1_t339576247  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Nullable_1__ctor_m1978009879(&L_3, ((float)((float)L_2/(float)(160.0f))), /*hidden argument*/Nullable_1__ctor_m1978009879_MethodInfo_var);
		__this->set_scaleFactor_7(L_3);
	}

IL_0026:
	{
		Nullable_1_t339576247 * L_4 = __this->get_address_of_scaleFactor_7();
		float L_5 = Nullable_1_get_Value_m672124572(L_4, /*hidden argument*/Nullable_1_get_Value_m672124572_MethodInfo_var);
		return L_5;
	}
}
// System.Int32 Facebook.Unity.Example.ConsoleBase::get_FontSize()
extern "C"  int32_t ConsoleBase_get_FontSize_m100912945 (ConsoleBase_t4290192428 * __this, const MethodInfo* method)
{
	{
		float L_0 = ConsoleBase_get_ScaleFactor_m2222574120(__this, /*hidden argument*/NULL);
		double L_1 = bankers_round((((double)((double)((float)((float)L_0*(float)(16.0f)))))));
		return (((int32_t)((int32_t)L_1)));
	}
}
// UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::get_TextStyle()
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIStyle_t1799908754_il2cpp_TypeInfo_var;
extern Il2CppClass* RectOffset_t3387826427_il2cpp_TypeInfo_var;
extern const uint32_t ConsoleBase_get_TextStyle_m1497590499_MetadataUsageId;
extern "C"  GUIStyle_t1799908754 * ConsoleBase_get_TextStyle_m1497590499 (ConsoleBase_t4290192428 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConsoleBase_get_TextStyle_m1497590499_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GUIStyle_t1799908754 * L_0 = __this->get_textStyle_8();
		if (L_0)
		{
			goto IL_0079;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUISkin_t1436893342 * L_1 = GUI_get_skin_m2309570990(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		GUIStyle_t1799908754 * L_2 = GUISkin_get_textArea_m2761984156(L_1, /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_3 = (GUIStyle_t1799908754 *)il2cpp_codegen_object_new(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m2210993436(L_3, L_2, /*hidden argument*/NULL);
		__this->set_textStyle_8(L_3);
		GUIStyle_t1799908754 * L_4 = __this->get_textStyle_8();
		NullCheck(L_4);
		GUIStyle_set_alignment_m1024943876(L_4, 0, /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_5 = __this->get_textStyle_8();
		NullCheck(L_5);
		GUIStyle_set_wordWrap_m2043927261(L_5, (bool)1, /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_6 = __this->get_textStyle_8();
		RectOffset_t3387826427 * L_7 = (RectOffset_t3387826427 *)il2cpp_codegen_object_new(RectOffset_t3387826427_il2cpp_TypeInfo_var);
		RectOffset__ctor_m4133355596(L_7, ((int32_t)10), ((int32_t)10), ((int32_t)10), ((int32_t)10), /*hidden argument*/NULL);
		NullCheck(L_6);
		GUIStyle_set_padding_m3722809255(L_6, L_7, /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_8 = __this->get_textStyle_8();
		NullCheck(L_8);
		GUIStyle_set_stretchHeight_m421727883(L_8, (bool)1, /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_9 = __this->get_textStyle_8();
		NullCheck(L_9);
		GUIStyle_set_stretchWidth_m1198647818(L_9, (bool)0, /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_10 = __this->get_textStyle_8();
		int32_t L_11 = ConsoleBase_get_FontSize_m100912945(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		GUIStyle_set_fontSize_m4015341543(L_10, L_11, /*hidden argument*/NULL);
	}

IL_0079:
	{
		GUIStyle_t1799908754 * L_12 = __this->get_textStyle_8();
		return L_12;
	}
}
// UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::get_ButtonStyle()
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIStyle_t1799908754_il2cpp_TypeInfo_var;
extern const uint32_t ConsoleBase_get_ButtonStyle_m304276404_MetadataUsageId;
extern "C"  GUIStyle_t1799908754 * ConsoleBase_get_ButtonStyle_m304276404 (ConsoleBase_t4290192428 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConsoleBase_get_ButtonStyle_m304276404_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GUIStyle_t1799908754 * L_0 = __this->get_buttonStyle_9();
		if (L_0)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUISkin_t1436893342 * L_1 = GUI_get_skin_m2309570990(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		GUIStyle_t1799908754 * L_2 = GUISkin_get_button_m797402546(L_1, /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_3 = (GUIStyle_t1799908754 *)il2cpp_codegen_object_new(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m2210993436(L_3, L_2, /*hidden argument*/NULL);
		__this->set_buttonStyle_9(L_3);
		GUIStyle_t1799908754 * L_4 = __this->get_buttonStyle_9();
		int32_t L_5 = ConsoleBase_get_FontSize_m100912945(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIStyle_set_fontSize_m4015341543(L_4, L_5, /*hidden argument*/NULL);
	}

IL_0031:
	{
		GUIStyle_t1799908754 * L_6 = __this->get_buttonStyle_9();
		return L_6;
	}
}
// UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::get_TextInputStyle()
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIStyle_t1799908754_il2cpp_TypeInfo_var;
extern const uint32_t ConsoleBase_get_TextInputStyle_m2745986965_MetadataUsageId;
extern "C"  GUIStyle_t1799908754 * ConsoleBase_get_TextInputStyle_m2745986965 (ConsoleBase_t4290192428 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConsoleBase_get_TextInputStyle_m2745986965_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GUIStyle_t1799908754 * L_0 = __this->get_textInputStyle_10();
		if (L_0)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUISkin_t1436893342 * L_1 = GUI_get_skin_m2309570990(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		GUIStyle_t1799908754 * L_2 = GUISkin_get_textField_m757680403(L_1, /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_3 = (GUIStyle_t1799908754 *)il2cpp_codegen_object_new(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m2210993436(L_3, L_2, /*hidden argument*/NULL);
		__this->set_textInputStyle_10(L_3);
		GUIStyle_t1799908754 * L_4 = __this->get_textInputStyle_10();
		int32_t L_5 = ConsoleBase_get_FontSize_m100912945(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIStyle_set_fontSize_m4015341543(L_4, L_5, /*hidden argument*/NULL);
	}

IL_0031:
	{
		GUIStyle_t1799908754 * L_6 = __this->get_textInputStyle_10();
		return L_6;
	}
}
// UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::get_LabelStyle()
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIStyle_t1799908754_il2cpp_TypeInfo_var;
extern const uint32_t ConsoleBase_get_LabelStyle_m3739078378_MetadataUsageId;
extern "C"  GUIStyle_t1799908754 * ConsoleBase_get_LabelStyle_m3739078378 (ConsoleBase_t4290192428 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConsoleBase_get_LabelStyle_m3739078378_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GUIStyle_t1799908754 * L_0 = __this->get_labelStyle_11();
		if (L_0)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUISkin_t1436893342 * L_1 = GUI_get_skin_m2309570990(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		GUIStyle_t1799908754 * L_2 = GUISkin_get_label_m2703078986(L_1, /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_3 = (GUIStyle_t1799908754 *)il2cpp_codegen_object_new(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m2210993436(L_3, L_2, /*hidden argument*/NULL);
		__this->set_labelStyle_11(L_3);
		GUIStyle_t1799908754 * L_4 = __this->get_labelStyle_11();
		int32_t L_5 = ConsoleBase_get_FontSize_m100912945(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIStyle_set_fontSize_m4015341543(L_4, L_5, /*hidden argument*/NULL);
	}

IL_0031:
	{
		GUIStyle_t1799908754 * L_6 = __this->get_labelStyle_11();
		return L_6;
	}
}
// System.Void Facebook.Unity.Example.ConsoleBase::Awake()
extern "C"  void ConsoleBase_Awake_m354532085 (ConsoleBase_t4290192428 * __this, const MethodInfo* method)
{
	{
		Application_set_targetFrameRate_m2941880625(NULL /*static, unused*/, ((int32_t)60), /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Facebook.Unity.Example.ConsoleBase::Button(System.String)
extern Il2CppClass* GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var;
extern Il2CppClass* ConsoleBase_t4290192428_il2cpp_TypeInfo_var;
extern const uint32_t ConsoleBase_Button_m1033918234_MetadataUsageId;
extern "C"  bool ConsoleBase_Button_m1033918234 (ConsoleBase_t4290192428 * __this, String_t* ___label0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConsoleBase_Button_m1033918234_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___label0;
		GUIStyle_t1799908754 * L_1 = ConsoleBase_get_ButtonStyle_m304276404(__this, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2108882777* L_2 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		IL2CPP_RUNTIME_CLASS_INIT(ConsoleBase_t4290192428_il2cpp_TypeInfo_var);
		int32_t L_3 = ConsoleBase_get_ButtonHeight_m504918286(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_4 = ConsoleBase_get_ScaleFactor_m2222574120(__this, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_5 = GUILayout_MinHeight_m1829895296(NULL /*static, unused*/, ((float)((float)(((float)((float)L_3)))*(float)L_4)), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_5);
		GUILayoutOptionU5BU5D_t2108882777* L_6 = L_2;
		int32_t L_7 = ConsoleBase_get_MainWindowWidth_m4208023734(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_8 = GUILayout_MaxWidth_m3473061269(NULL /*static, unused*/, (((float)((float)L_7))), /*hidden argument*/NULL);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_8);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_8);
		bool L_9 = GUILayout_Button_m2061694122(NULL /*static, unused*/, L_0, L_1, L_6, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Void Facebook.Unity.Example.ConsoleBase::LabelAndTextField(System.String,System.String&)
extern Il2CppClass* GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var;
extern Il2CppClass* ConsoleBase_t4290192428_il2cpp_TypeInfo_var;
extern const uint32_t ConsoleBase_LabelAndTextField_m425453358_MetadataUsageId;
extern "C"  void ConsoleBase_LabelAndTextField_m425453358 (ConsoleBase_t4290192428 * __this, String_t* ___label0, String_t** ___text1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConsoleBase_LabelAndTextField_m425453358_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GUILayout_BeginHorizontal_m212592284(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		String_t* L_0 = ___label0;
		GUIStyle_t1799908754 * L_1 = ConsoleBase_get_LabelStyle_m3739078378(__this, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2108882777* L_2 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)1));
		float L_3 = ConsoleBase_get_ScaleFactor_m2222574120(__this, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_4 = GUILayout_MaxWidth_m3473061269(NULL /*static, unused*/, ((float)((float)(200.0f)*(float)L_3)), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_4);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_4);
		GUILayout_Label_m616748838(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		String_t** L_5 = ___text1;
		String_t** L_6 = ___text1;
		GUIStyle_t1799908754 * L_7 = ConsoleBase_get_TextInputStyle_m2745986965(__this, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2108882777* L_8 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(ConsoleBase_t4290192428_il2cpp_TypeInfo_var);
		int32_t L_9 = ConsoleBase_get_MainWindowWidth_m4208023734(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_10 = GUILayout_MaxWidth_m3473061269(NULL /*static, unused*/, (((float)((float)((int32_t)((int32_t)L_9-(int32_t)((int32_t)150)))))), /*hidden argument*/NULL);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_10);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_10);
		String_t* L_11 = GUILayout_TextField_m1577258936(NULL /*static, unused*/, (*((String_t**)L_6)), L_7, L_8, /*hidden argument*/NULL);
		*((Il2CppObject **)(L_5)) = (Il2CppObject *)L_11;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_5), (Il2CppObject *)L_11);
		GUILayout_EndHorizontal_m4258536965(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Facebook.Unity.Example.ConsoleBase::IsHorizontalLayout()
extern "C"  bool ConsoleBase_IsHorizontalLayout_m2006263654 (ConsoleBase_t4290192428 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Screen_get_orientation_m879255848(NULL /*static, unused*/, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_0) == ((int32_t)3))? 1 : 0);
	}
}
// System.Void Facebook.Unity.Example.ConsoleBase::SwitchMenu(System.Type)
extern Il2CppClass* ConsoleBase_t4290192428_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1_Push_m585566454_MethodInfo_var;
extern const uint32_t ConsoleBase_SwitchMenu_m2043949162_MetadataUsageId;
extern "C"  void ConsoleBase_SwitchMenu_m2043949162 (ConsoleBase_t4290192428 * __this, Type_t * ___menuClass0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConsoleBase_SwitchMenu_m2043949162_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ConsoleBase_t4290192428_il2cpp_TypeInfo_var);
		Stack_1_t3116948387 * L_0 = ((ConsoleBase_t4290192428_StaticFields*)ConsoleBase_t4290192428_il2cpp_TypeInfo_var->static_fields)->get_menuStack_3();
		Type_t * L_1 = Object_GetType_m191970594(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_1);
		NullCheck(L_0);
		Stack_1_Push_m585566454(L_0, L_2, /*hidden argument*/Stack_1_Push_m585566454_MethodInfo_var);
		Type_t * L_3 = ___menuClass0;
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_3);
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.ConsoleBase::GoBack()
extern Il2CppClass* ConsoleBase_t4290192428_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_Any_TisString_t_m3014604563_MethodInfo_var;
extern const MethodInfo* Stack_1_Pop_m116238678_MethodInfo_var;
extern const uint32_t ConsoleBase_GoBack_m2150591355_MetadataUsageId;
extern "C"  void ConsoleBase_GoBack_m2150591355 (ConsoleBase_t4290192428 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConsoleBase_GoBack_m2150591355_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ConsoleBase_t4290192428_il2cpp_TypeInfo_var);
		Stack_1_t3116948387 * L_0 = ((ConsoleBase_t4290192428_StaticFields*)ConsoleBase_t4290192428_il2cpp_TypeInfo_var->static_fields)->get_menuStack_3();
		bool L_1 = Enumerable_Any_TisString_t_m3014604563(NULL /*static, unused*/, L_0, /*hidden argument*/Enumerable_Any_TisString_t_m3014604563_MethodInfo_var);
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ConsoleBase_t4290192428_il2cpp_TypeInfo_var);
		Stack_1_t3116948387 * L_2 = ((ConsoleBase_t4290192428_StaticFields*)ConsoleBase_t4290192428_il2cpp_TypeInfo_var->static_fields)->get_menuStack_3();
		NullCheck(L_2);
		String_t* L_3 = Stack_1_Pop_m116238678(L_2, /*hidden argument*/Stack_1_Pop_m116238678_MethodInfo_var);
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_001e:
	{
		return;
	}
}
// System.Void Facebook.Unity.Example.ConsoleBase::.cctor()
extern Il2CppClass* Stack_1_t3116948387_il2cpp_TypeInfo_var;
extern Il2CppClass* ConsoleBase_t4290192428_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1__ctor_m2674940999_MethodInfo_var;
extern const uint32_t ConsoleBase__cctor_m810911295_MetadataUsageId;
extern "C"  void ConsoleBase__cctor_m810911295 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConsoleBase__cctor_m810911295_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Stack_1_t3116948387 * L_0 = (Stack_1_t3116948387 *)il2cpp_codegen_object_new(Stack_1_t3116948387_il2cpp_TypeInfo_var);
		Stack_1__ctor_m2674940999(L_0, /*hidden argument*/Stack_1__ctor_m2674940999_MethodInfo_var);
		((ConsoleBase_t4290192428_StaticFields*)ConsoleBase_t4290192428_il2cpp_TypeInfo_var->static_fields)->set_menuStack_3(L_0);
		return;
	}
}
// System.Void Facebook.Unity.Example.DialogShare::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3625392560;
extern Il2CppCodeGenString* _stringLiteral989947916;
extern Il2CppCodeGenString* _stringLiteral1175083772;
extern Il2CppCodeGenString* _stringLiteral415143208;
extern Il2CppCodeGenString* _stringLiteral2409011836;
extern Il2CppCodeGenString* _stringLiteral4248546168;
extern Il2CppCodeGenString* _stringLiteral214186452;
extern Il2CppCodeGenString* _stringLiteral174124874;
extern const uint32_t DialogShare__ctor_m3852229291_MetadataUsageId;
extern "C"  void DialogShare__ctor_m3852229291 (DialogShare_t329767461 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DialogShare__ctor_m3852229291_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_shareLink_14(_stringLiteral3625392560);
		__this->set_shareTitle_15(_stringLiteral989947916);
		__this->set_shareDescription_16(_stringLiteral1175083772);
		__this->set_shareImage_17(_stringLiteral415143208);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_feedTo_18(L_0);
		__this->set_feedLink_19(_stringLiteral3625392560);
		__this->set_feedTitle_20(_stringLiteral2409011836);
		__this->set_feedCaption_21(_stringLiteral4248546168);
		__this->set_feedDescription_22(_stringLiteral214186452);
		__this->set_feedImage_23(_stringLiteral174124874);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_feedMediaSource_24(L_1);
		MenuBase__ctor_m2359507012(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Facebook.Unity.Example.DialogShare::ShowDialogModeSelector()
extern "C"  bool DialogShare_ShowDialogModeSelector_m2524942464 (DialogShare_t329767461 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void Facebook.Unity.Example.DialogShare::GetGui()
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppClass* Uri_t19570940_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* FacebookDelegate_1_t4160439974_il2cpp_TypeInfo_var;
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern const MethodInfo* MenuBase_HandleResult_m2705601712_MethodInfo_var;
extern const MethodInfo* FacebookDelegate_1__ctor_m303745986_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral803265134;
extern Il2CppCodeGenString* _stringLiteral3625392560;
extern Il2CppCodeGenString* _stringLiteral193740354;
extern Il2CppCodeGenString* _stringLiteral1298866029;
extern Il2CppCodeGenString* _stringLiteral313736313;
extern Il2CppCodeGenString* _stringLiteral415143208;
extern Il2CppCodeGenString* _stringLiteral325352138;
extern Il2CppCodeGenString* _stringLiteral2435266816;
extern Il2CppCodeGenString* _stringLiteral873740972;
extern Il2CppCodeGenString* _stringLiteral1561429203;
extern Il2CppCodeGenString* _stringLiteral2440032479;
extern Il2CppCodeGenString* _stringLiteral2608463746;
extern Il2CppCodeGenString* _stringLiteral2409011836;
extern Il2CppCodeGenString* _stringLiteral1243415576;
extern Il2CppCodeGenString* _stringLiteral214186452;
extern Il2CppCodeGenString* _stringLiteral174124874;
extern Il2CppCodeGenString* _stringLiteral1496915107;
extern Il2CppCodeGenString* _stringLiteral4126198828;
extern Il2CppCodeGenString* _stringLiteral1416265597;
extern Il2CppCodeGenString* _stringLiteral2415121717;
extern const uint32_t DialogShare_GetGui_m3986781346_MetadataUsageId;
extern "C"  void DialogShare_GetGui_m3986781346 (DialogShare_t329767461 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DialogShare_GetGui_m3986781346_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B11_0 = 0;
	int32_t G_B13_0 = 0;
	int32_t G_B15_0 = 0;
	String_t* G_B20_0 = NULL;
	String_t* G_B19_0 = NULL;
	Uri_t19570940 * G_B21_0 = NULL;
	String_t* G_B21_1 = NULL;
	String_t* G_B23_0 = NULL;
	String_t* G_B23_1 = NULL;
	String_t* G_B23_2 = NULL;
	Uri_t19570940 * G_B23_3 = NULL;
	String_t* G_B23_4 = NULL;
	String_t* G_B22_0 = NULL;
	String_t* G_B22_1 = NULL;
	String_t* G_B22_2 = NULL;
	Uri_t19570940 * G_B22_3 = NULL;
	String_t* G_B22_4 = NULL;
	Uri_t19570940 * G_B24_0 = NULL;
	String_t* G_B24_1 = NULL;
	String_t* G_B24_2 = NULL;
	String_t* G_B24_3 = NULL;
	Uri_t19570940 * G_B24_4 = NULL;
	String_t* G_B24_5 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_0 = GUI_get_enabled_m3504357730(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = ConsoleBase_Button_m1033918234(__this, _stringLiteral803265134, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003c;
		}
	}
	{
		Uri_t19570940 * L_2 = (Uri_t19570940 *)il2cpp_codegen_object_new(Uri_t19570940_il2cpp_TypeInfo_var);
		Uri__ctor_m3927533881(L_2, _stringLiteral3625392560, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)MenuBase_HandleResult_m2705601712_MethodInfo_var);
		FacebookDelegate_1_t4160439974 * L_6 = (FacebookDelegate_1_t4160439974 *)il2cpp_codegen_object_new(FacebookDelegate_1_t4160439974_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m303745986(L_6, __this, L_5, /*hidden argument*/FacebookDelegate_1__ctor_m303745986_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_ShareLink_m472842069(NULL /*static, unused*/, L_2, L_3, L_4, (Uri_t19570940 *)NULL, L_6, /*hidden argument*/NULL);
	}

IL_003c:
	{
		bool L_7 = ConsoleBase_Button_m1033918234(__this, _stringLiteral193740354, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_007b;
		}
	}
	{
		Uri_t19570940 * L_8 = (Uri_t19570940 *)il2cpp_codegen_object_new(Uri_t19570940_il2cpp_TypeInfo_var);
		Uri__ctor_m3927533881(L_8, _stringLiteral3625392560, /*hidden argument*/NULL);
		Uri_t19570940 * L_9 = (Uri_t19570940 *)il2cpp_codegen_object_new(Uri_t19570940_il2cpp_TypeInfo_var);
		Uri__ctor_m3927533881(L_9, _stringLiteral415143208, /*hidden argument*/NULL);
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)MenuBase_HandleResult_m2705601712_MethodInfo_var);
		FacebookDelegate_1_t4160439974 * L_11 = (FacebookDelegate_1_t4160439974 *)il2cpp_codegen_object_new(FacebookDelegate_1_t4160439974_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m303745986(L_11, __this, L_10, /*hidden argument*/FacebookDelegate_1__ctor_m303745986_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_ShareLink_m472842069(NULL /*static, unused*/, L_8, _stringLiteral1298866029, _stringLiteral313736313, L_9, L_11, /*hidden argument*/NULL);
	}

IL_007b:
	{
		String_t** L_12 = __this->get_address_of_shareLink_14();
		ConsoleBase_LabelAndTextField_m425453358(__this, _stringLiteral325352138, L_12, /*hidden argument*/NULL);
		String_t** L_13 = __this->get_address_of_shareTitle_15();
		ConsoleBase_LabelAndTextField_m425453358(__this, _stringLiteral2435266816, L_13, /*hidden argument*/NULL);
		String_t** L_14 = __this->get_address_of_shareDescription_16();
		ConsoleBase_LabelAndTextField_m425453358(__this, _stringLiteral873740972, L_14, /*hidden argument*/NULL);
		String_t** L_15 = __this->get_address_of_shareImage_17();
		ConsoleBase_LabelAndTextField_m425453358(__this, _stringLiteral1561429203, L_15, /*hidden argument*/NULL);
		bool L_16 = ConsoleBase_Button_m1033918234(__this, _stringLiteral2440032479, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0102;
		}
	}
	{
		String_t* L_17 = __this->get_shareLink_14();
		Uri_t19570940 * L_18 = (Uri_t19570940 *)il2cpp_codegen_object_new(Uri_t19570940_il2cpp_TypeInfo_var);
		Uri__ctor_m3927533881(L_18, L_17, /*hidden argument*/NULL);
		String_t* L_19 = __this->get_shareTitle_15();
		String_t* L_20 = __this->get_shareDescription_16();
		String_t* L_21 = __this->get_shareImage_17();
		Uri_t19570940 * L_22 = (Uri_t19570940 *)il2cpp_codegen_object_new(Uri_t19570940_il2cpp_TypeInfo_var);
		Uri__ctor_m3927533881(L_22, L_21, /*hidden argument*/NULL);
		IntPtr_t L_23;
		L_23.set_m_value_0((void*)(void*)MenuBase_HandleResult_m2705601712_MethodInfo_var);
		FacebookDelegate_1_t4160439974 * L_24 = (FacebookDelegate_1_t4160439974 *)il2cpp_codegen_object_new(FacebookDelegate_1_t4160439974_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m303745986(L_24, __this, L_23, /*hidden argument*/FacebookDelegate_1__ctor_m303745986_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_ShareLink_m472842069(NULL /*static, unused*/, L_18, L_19, L_20, L_22, L_24, /*hidden argument*/NULL);
	}

IL_0102:
	{
		bool L_25 = V_0;
		if (!L_25)
		{
			goto IL_0129;
		}
	}
	{
		bool L_26 = Constants_get_IsEditor_m2386263146(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_0126;
		}
	}
	{
		bool L_27 = Constants_get_IsEditor_m2386263146(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_0123;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		bool L_28 = FB_get_IsLoggedIn_m4036830699(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B11_0 = ((int32_t)(L_28));
		goto IL_0124;
	}

IL_0123:
	{
		G_B11_0 = 0;
	}

IL_0124:
	{
		G_B13_0 = G_B11_0;
		goto IL_0127;
	}

IL_0126:
	{
		G_B13_0 = 1;
	}

IL_0127:
	{
		G_B15_0 = G_B13_0;
		goto IL_012a;
	}

IL_0129:
	{
		G_B15_0 = 0;
	}

IL_012a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_enabled_m1897336325(NULL /*static, unused*/, (bool)G_B15_0, /*hidden argument*/NULL);
		bool L_29 = ConsoleBase_Button_m1033918234(__this, _stringLiteral2608463746, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_017d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		Uri_t19570940 * L_31 = (Uri_t19570940 *)il2cpp_codegen_object_new(Uri_t19570940_il2cpp_TypeInfo_var);
		Uri__ctor_m3927533881(L_31, _stringLiteral3625392560, /*hidden argument*/NULL);
		Uri_t19570940 * L_32 = (Uri_t19570940 *)il2cpp_codegen_object_new(Uri_t19570940_il2cpp_TypeInfo_var);
		Uri__ctor_m3927533881(L_32, _stringLiteral174124874, /*hidden argument*/NULL);
		String_t* L_33 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		IntPtr_t L_34;
		L_34.set_m_value_0((void*)(void*)MenuBase_HandleResult_m2705601712_MethodInfo_var);
		FacebookDelegate_1_t4160439974 * L_35 = (FacebookDelegate_1_t4160439974 *)il2cpp_codegen_object_new(FacebookDelegate_1_t4160439974_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m303745986(L_35, __this, L_34, /*hidden argument*/FacebookDelegate_1__ctor_m303745986_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_FeedShare_m1853999185(NULL /*static, unused*/, L_30, L_31, _stringLiteral2409011836, _stringLiteral1243415576, _stringLiteral214186452, L_32, L_33, L_35, /*hidden argument*/NULL);
	}

IL_017d:
	{
		String_t** L_36 = __this->get_address_of_feedTo_18();
		ConsoleBase_LabelAndTextField_m425453358(__this, _stringLiteral1496915107, L_36, /*hidden argument*/NULL);
		String_t** L_37 = __this->get_address_of_feedLink_19();
		ConsoleBase_LabelAndTextField_m425453358(__this, _stringLiteral325352138, L_37, /*hidden argument*/NULL);
		String_t** L_38 = __this->get_address_of_feedTitle_20();
		ConsoleBase_LabelAndTextField_m425453358(__this, _stringLiteral2435266816, L_38, /*hidden argument*/NULL);
		String_t** L_39 = __this->get_address_of_feedCaption_21();
		ConsoleBase_LabelAndTextField_m425453358(__this, _stringLiteral4126198828, L_39, /*hidden argument*/NULL);
		String_t** L_40 = __this->get_address_of_feedDescription_22();
		ConsoleBase_LabelAndTextField_m425453358(__this, _stringLiteral873740972, L_40, /*hidden argument*/NULL);
		String_t** L_41 = __this->get_address_of_feedImage_23();
		ConsoleBase_LabelAndTextField_m425453358(__this, _stringLiteral1561429203, L_41, /*hidden argument*/NULL);
		String_t** L_42 = __this->get_address_of_feedMediaSource_24();
		ConsoleBase_LabelAndTextField_m425453358(__this, _stringLiteral1416265597, L_42, /*hidden argument*/NULL);
		bool L_43 = ConsoleBase_Button_m1033918234(__this, _stringLiteral2415121717, /*hidden argument*/NULL);
		if (!L_43)
		{
			goto IL_0275;
		}
	}
	{
		String_t* L_44 = __this->get_feedTo_18();
		String_t* L_45 = __this->get_feedLink_19();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_46 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		G_B19_0 = L_44;
		if (!L_46)
		{
			G_B20_0 = L_44;
			goto IL_0220;
		}
	}
	{
		G_B21_0 = ((Uri_t19570940 *)(NULL));
		G_B21_1 = G_B19_0;
		goto IL_022b;
	}

IL_0220:
	{
		String_t* L_47 = __this->get_feedLink_19();
		Uri_t19570940 * L_48 = (Uri_t19570940 *)il2cpp_codegen_object_new(Uri_t19570940_il2cpp_TypeInfo_var);
		Uri__ctor_m3927533881(L_48, L_47, /*hidden argument*/NULL);
		G_B21_0 = L_48;
		G_B21_1 = G_B20_0;
	}

IL_022b:
	{
		String_t* L_49 = __this->get_feedTitle_20();
		String_t* L_50 = __this->get_feedCaption_21();
		String_t* L_51 = __this->get_feedDescription_22();
		String_t* L_52 = __this->get_feedImage_23();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_53 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		G_B22_0 = L_51;
		G_B22_1 = L_50;
		G_B22_2 = L_49;
		G_B22_3 = G_B21_0;
		G_B22_4 = G_B21_1;
		if (!L_53)
		{
			G_B23_0 = L_51;
			G_B23_1 = L_50;
			G_B23_2 = L_49;
			G_B23_3 = G_B21_0;
			G_B23_4 = G_B21_1;
			goto IL_0253;
		}
	}
	{
		G_B24_0 = ((Uri_t19570940 *)(NULL));
		G_B24_1 = G_B22_0;
		G_B24_2 = G_B22_1;
		G_B24_3 = G_B22_2;
		G_B24_4 = G_B22_3;
		G_B24_5 = G_B22_4;
		goto IL_025e;
	}

IL_0253:
	{
		String_t* L_54 = __this->get_feedImage_23();
		Uri_t19570940 * L_55 = (Uri_t19570940 *)il2cpp_codegen_object_new(Uri_t19570940_il2cpp_TypeInfo_var);
		Uri__ctor_m3927533881(L_55, L_54, /*hidden argument*/NULL);
		G_B24_0 = L_55;
		G_B24_1 = G_B23_0;
		G_B24_2 = G_B23_1;
		G_B24_3 = G_B23_2;
		G_B24_4 = G_B23_3;
		G_B24_5 = G_B23_4;
	}

IL_025e:
	{
		String_t* L_56 = __this->get_feedMediaSource_24();
		IntPtr_t L_57;
		L_57.set_m_value_0((void*)(void*)MenuBase_HandleResult_m2705601712_MethodInfo_var);
		FacebookDelegate_1_t4160439974 * L_58 = (FacebookDelegate_1_t4160439974 *)il2cpp_codegen_object_new(FacebookDelegate_1_t4160439974_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m303745986(L_58, __this, L_57, /*hidden argument*/FacebookDelegate_1__ctor_m303745986_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_FeedShare_m1853999185(NULL /*static, unused*/, G_B24_5, G_B24_4, G_B24_3, G_B24_2, G_B24_1, G_B24_0, L_56, L_58, /*hidden argument*/NULL);
	}

IL_0275:
	{
		bool L_59 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_enabled_m1897336325(NULL /*static, unused*/, L_59, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.GameGroups::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3836448289;
extern Il2CppCodeGenString* _stringLiteral1575729168;
extern Il2CppCodeGenString* _stringLiteral2816894562;
extern const uint32_t GameGroups__ctor_m2839518528_MetadataUsageId;
extern "C"  void GameGroups__ctor_m2839518528 (GameGroups_t3348180240 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameGroups__ctor_m2839518528_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_gamerGroupName_14(_stringLiteral3836448289);
		__this->set_gamerGroupDesc_15(_stringLiteral1575729168);
		__this->set_gamerGroupPrivacy_16(_stringLiteral2816894562);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_gamerGroupCurrentGroup_17(L_0);
		MenuBase__ctor_m2359507012(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.GameGroups::GetGui()
extern Il2CppClass* FacebookDelegate_1_t1547895262_il2cpp_TypeInfo_var;
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* MenuBase_HandleResult_m2705601712_MethodInfo_var;
extern const MethodInfo* FacebookDelegate_1__ctor_m3830488070_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3294746538;
extern Il2CppCodeGenString* _stringLiteral751157243;
extern Il2CppCodeGenString* _stringLiteral746387316;
extern Il2CppCodeGenString* _stringLiteral2692040674;
extern Il2CppCodeGenString* _stringLiteral3162406914;
extern Il2CppCodeGenString* _stringLiteral1885304534;
extern Il2CppCodeGenString* _stringLiteral2271986460;
extern Il2CppCodeGenString* _stringLiteral1291767325;
extern Il2CppCodeGenString* _stringLiteral3747821717;
extern Il2CppCodeGenString* _stringLiteral694477129;
extern Il2CppCodeGenString* _stringLiteral1198496686;
extern Il2CppCodeGenString* _stringLiteral3162510303;
extern Il2CppCodeGenString* _stringLiteral167301299;
extern Il2CppCodeGenString* _stringLiteral1639140927;
extern Il2CppCodeGenString* _stringLiteral840446492;
extern const uint32_t GameGroups_GetGui_m511950881_MetadataUsageId;
extern "C"  void GameGroups_GetGui_m511950881 (GameGroups_t3348180240 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameGroups_GetGui_m511950881_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B9_0 = 0;
	int32_t G_B14_0 = 0;
	int32_t G_B21_0 = 0;
	{
		bool L_0 = ConsoleBase_Button_m1033918234(__this, _stringLiteral3294746538, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0030;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)MenuBase_HandleResult_m2705601712_MethodInfo_var);
		FacebookDelegate_1_t1547895262 * L_2 = (FacebookDelegate_1_t1547895262 *)il2cpp_codegen_object_new(FacebookDelegate_1_t1547895262_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m3830488070(L_2, __this, L_1, /*hidden argument*/FacebookDelegate_1__ctor_m3830488070_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_GameGroupCreate_m3542859513(NULL /*static, unused*/, _stringLiteral751157243, _stringLiteral746387316, _stringLiteral2692040674, L_2, /*hidden argument*/NULL);
	}

IL_0030:
	{
		bool L_3 = ConsoleBase_Button_m1033918234(__this, _stringLiteral3162406914, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0060;
		}
	}
	{
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)MenuBase_HandleResult_m2705601712_MethodInfo_var);
		FacebookDelegate_1_t1547895262 * L_5 = (FacebookDelegate_1_t1547895262 *)il2cpp_codegen_object_new(FacebookDelegate_1_t1547895262_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m3830488070(L_5, __this, L_4, /*hidden argument*/FacebookDelegate_1__ctor_m3830488070_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_GameGroupCreate_m3542859513(NULL /*static, unused*/, _stringLiteral751157243, _stringLiteral746387316, _stringLiteral1885304534, L_5, /*hidden argument*/NULL);
	}

IL_0060:
	{
		String_t** L_6 = __this->get_address_of_gamerGroupName_14();
		ConsoleBase_LabelAndTextField_m425453358(__this, _stringLiteral2271986460, L_6, /*hidden argument*/NULL);
		String_t** L_7 = __this->get_address_of_gamerGroupDesc_15();
		ConsoleBase_LabelAndTextField_m425453358(__this, _stringLiteral1291767325, L_7, /*hidden argument*/NULL);
		String_t** L_8 = __this->get_address_of_gamerGroupPrivacy_16();
		ConsoleBase_LabelAndTextField_m425453358(__this, _stringLiteral3747821717, L_8, /*hidden argument*/NULL);
		bool L_9 = ConsoleBase_Button_m1033918234(__this, _stringLiteral694477129, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_00a9;
		}
	}
	{
		GameGroups_CallCreateGroupDialog_m2698043461(__this, /*hidden argument*/NULL);
	}

IL_00a9:
	{
		String_t** L_10 = __this->get_address_of_gamerGroupCurrentGroup_17();
		ConsoleBase_LabelAndTextField_m425453358(__this, _stringLiteral1198496686, L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_11 = GUI_get_enabled_m3504357730(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_11;
		bool L_12 = V_0;
		if (!L_12)
		{
			goto IL_00d6;
		}
	}
	{
		String_t* L_13 = __this->get_gamerGroupCurrentGroup_17();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_14 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		G_B9_0 = ((((int32_t)L_14) == ((int32_t)0))? 1 : 0);
		goto IL_00d7;
	}

IL_00d6:
	{
		G_B9_0 = 0;
	}

IL_00d7:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_enabled_m1897336325(NULL /*static, unused*/, (bool)G_B9_0, /*hidden argument*/NULL);
		bool L_15 = ConsoleBase_Button_m1033918234(__this, _stringLiteral3162510303, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_00f2;
		}
	}
	{
		GameGroups_CallJoinGroupDialog_m2365340007(__this, /*hidden argument*/NULL);
	}

IL_00f2:
	{
		bool L_16 = V_0;
		if (!L_16)
		{
			goto IL_00ff;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		bool L_17 = FB_get_IsLoggedIn_m4036830699(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B14_0 = ((int32_t)(L_17));
		goto IL_0100;
	}

IL_00ff:
	{
		G_B14_0 = 0;
	}

IL_0100:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_enabled_m1897336325(NULL /*static, unused*/, (bool)G_B14_0, /*hidden argument*/NULL);
		bool L_18 = ConsoleBase_Button_m1033918234(__this, _stringLiteral167301299, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_011b;
		}
	}
	{
		GameGroups_CallFbGetAllOwnedGroups_m221881952(__this, /*hidden argument*/NULL);
	}

IL_011b:
	{
		bool L_19 = ConsoleBase_Button_m1033918234(__this, _stringLiteral1639140927, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_0131;
		}
	}
	{
		GameGroups_CallFbGetUserGroups_m4084709217(__this, /*hidden argument*/NULL);
	}

IL_0131:
	{
		bool L_20 = V_0;
		if (!L_20)
		{
			goto IL_0147;
		}
	}
	{
		String_t* L_21 = __this->get_gamerGroupCurrentGroup_17();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_22 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		G_B21_0 = ((((int32_t)L_22) == ((int32_t)0))? 1 : 0);
		goto IL_0148;
	}

IL_0147:
	{
		G_B21_0 = 0;
	}

IL_0148:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_enabled_m1897336325(NULL /*static, unused*/, (bool)G_B21_0, /*hidden argument*/NULL);
		bool L_23 = ConsoleBase_Button_m1033918234(__this, _stringLiteral840446492, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_0163;
		}
	}
	{
		GameGroups_CallFbPostToGamerGroup_m4156428270(__this, /*hidden argument*/NULL);
	}

IL_0163:
	{
		bool L_24 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_enabled_m1897336325(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.GameGroups::GroupCreateCB(Facebook.Unity.IGroupCreateResult)
extern Il2CppClass* IGroupCreateResult_t2512549813_il2cpp_TypeInfo_var;
extern const uint32_t GameGroups_GroupCreateCB_m1743336484_MetadataUsageId;
extern "C"  void GameGroups_GroupCreateCB_m1743336484 (GameGroups_t3348180240 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameGroups_GroupCreateCB_m1743336484_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___result0;
		MenuBase_HandleResult_m2705601712(__this, L_0, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___result0;
		NullCheck(L_1);
		String_t* L_2 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String Facebook.Unity.IGroupCreateResult::get_GroupId() */, IGroupCreateResult_t2512549813_il2cpp_TypeInfo_var, L_1);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		Il2CppObject * L_3 = ___result0;
		NullCheck(L_3);
		String_t* L_4 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String Facebook.Unity.IGroupCreateResult::get_GroupId() */, IGroupCreateResult_t2512549813_il2cpp_TypeInfo_var, L_3);
		__this->set_gamerGroupCurrentGroup_17(L_4);
	}

IL_001e:
	{
		return;
	}
}
// System.Void Facebook.Unity.Example.GameGroups::GetAllGroupsCB(Facebook.Unity.IGraphResult)
extern Il2CppClass* IResult_t3447678270_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t2603311978_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t2058570427_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t309261261_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m1012630527_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m1650084162_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m2851989743_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2619694;
extern Il2CppCodeGenString* _stringLiteral287061489;
extern const uint32_t GameGroups_GetAllGroupsCB_m3275190149_MetadataUsageId;
extern "C"  void GameGroups_GetAllGroupsCB_m3275190149 (GameGroups_t3348180240 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameGroups_GetAllGroupsCB_m3275190149_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject* V_0 = NULL;
	List_1_t2058570427 * V_1 = NULL;
	Dictionary_2_t309261261 * V_2 = NULL;
	{
		Il2CppObject * L_0 = ___result0;
		NullCheck(L_0);
		String_t* L_1 = InterfaceFuncInvoker0< String_t* >::Invoke(2 /* System.String Facebook.Unity.IResult::get_RawResult() */, IResult_t3447678270_il2cpp_TypeInfo_var, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0073;
		}
	}
	{
		Il2CppObject * L_3 = ___result0;
		NullCheck(L_3);
		String_t* L_4 = InterfaceFuncInvoker0< String_t* >::Invoke(2 /* System.String Facebook.Unity.IResult::get_RawResult() */, IResult_t3447678270_il2cpp_TypeInfo_var, L_3);
		ConsoleBase_set_LastResponse_m510948058(__this, L_4, /*hidden argument*/NULL);
		Il2CppObject * L_5 = ___result0;
		NullCheck(L_5);
		Il2CppObject* L_6 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(1 /* System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.IResult::get_ResultDictionary() */, IResult_t3447678270_il2cpp_TypeInfo_var, L_5);
		V_0 = L_6;
		Il2CppObject* L_7 = V_0;
		NullCheck(L_7);
		bool L_8 = InterfaceFuncInvoker1< bool, String_t* >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,System.Object>::ContainsKey(!0) */, IDictionary_2_t2603311978_il2cpp_TypeInfo_var, L_7, _stringLiteral2619694);
		if (!L_8)
		{
			goto IL_0073;
		}
	}
	{
		Il2CppObject* L_9 = V_0;
		NullCheck(L_9);
		Il2CppObject * L_10 = InterfaceFuncInvoker1< Il2CppObject *, String_t* >::Invoke(4 /* !1 System.Collections.Generic.IDictionary`2<System.String,System.Object>::get_Item(!0) */, IDictionary_2_t2603311978_il2cpp_TypeInfo_var, L_9, _stringLiteral2619694);
		V_1 = ((List_1_t2058570427 *)CastclassClass(L_10, List_1_t2058570427_il2cpp_TypeInfo_var));
		List_1_t2058570427 * L_11 = V_1;
		NullCheck(L_11);
		int32_t L_12 = List_1_get_Count_m1012630527(L_11, /*hidden argument*/List_1_get_Count_m1012630527_MethodInfo_var);
		if ((((int32_t)L_12) <= ((int32_t)0)))
		{
			goto IL_0073;
		}
	}
	{
		List_1_t2058570427 * L_13 = V_1;
		NullCheck(L_13);
		Il2CppObject * L_14 = List_1_get_Item_m1650084162(L_13, 0, /*hidden argument*/List_1_get_Item_m1650084162_MethodInfo_var);
		V_2 = ((Dictionary_2_t309261261 *)CastclassClass(L_14, Dictionary_2_t309261261_il2cpp_TypeInfo_var));
		Dictionary_2_t309261261 * L_15 = V_2;
		NullCheck(L_15);
		Il2CppObject * L_16 = Dictionary_2_get_Item_m2851989743(L_15, _stringLiteral287061489, /*hidden argument*/Dictionary_2_get_Item_m2851989743_MethodInfo_var);
		__this->set_gamerGroupCurrentGroup_17(((String_t*)CastclassSealed(L_16, String_t_il2cpp_TypeInfo_var)));
	}

IL_0073:
	{
		Il2CppObject * L_17 = ___result0;
		NullCheck(L_17);
		String_t* L_18 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String Facebook.Unity.IResult::get_Error() */, IResult_t3447678270_il2cpp_TypeInfo_var, L_17);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_19 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_008f;
		}
	}
	{
		Il2CppObject * L_20 = ___result0;
		NullCheck(L_20);
		String_t* L_21 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String Facebook.Unity.IResult::get_Error() */, IResult_t3447678270_il2cpp_TypeInfo_var, L_20);
		ConsoleBase_set_LastResponse_m510948058(__this, L_21, /*hidden argument*/NULL);
	}

IL_008f:
	{
		return;
	}
}
// System.Void Facebook.Unity.Example.GameGroups::CallFbGetAllOwnedGroups()
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* FacebookDelegate_1_t3020292135_il2cpp_TypeInfo_var;
extern const MethodInfo* GameGroups_GetAllGroupsCB_m3275190149_MethodInfo_var;
extern const MethodInfo* FacebookDelegate_1__ctor_m2818497626_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2860036975;
extern const uint32_t GameGroups_CallFbGetAllOwnedGroups_m221881952_MetadataUsageId;
extern "C"  void GameGroups_CallFbGetAllOwnedGroups_m221881952 (GameGroups_t3348180240 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameGroups_CallFbGetAllOwnedGroups_m221881952_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		String_t* L_0 = FB_get_AppId_m4024040373(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, L_0, _stringLiteral2860036975, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)GameGroups_GetAllGroupsCB_m3275190149_MethodInfo_var);
		FacebookDelegate_1_t3020292135 * L_3 = (FacebookDelegate_1_t3020292135 *)il2cpp_codegen_object_new(FacebookDelegate_1_t3020292135_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m2818497626(L_3, __this, L_2, /*hidden argument*/FacebookDelegate_1__ctor_m2818497626_MethodInfo_var);
		FB_API_m682532377(NULL /*static, unused*/, L_1, 0, L_3, (Il2CppObject*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.GameGroups::CallFbGetUserGroups()
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* FacebookDelegate_1_t3020292135_il2cpp_TypeInfo_var;
extern const MethodInfo* MenuBase_HandleResult_m2705601712_MethodInfo_var;
extern const MethodInfo* FacebookDelegate_1__ctor_m2818497626_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3624739042;
extern const uint32_t GameGroups_CallFbGetUserGroups_m4084709217_MetadataUsageId;
extern "C"  void GameGroups_CallFbGetUserGroups_m4084709217 (GameGroups_t3348180240 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameGroups_CallFbGetUserGroups_m4084709217_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		String_t* L_0 = FB_get_AppId_m4024040373(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral3624739042, L_0, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)MenuBase_HandleResult_m2705601712_MethodInfo_var);
		FacebookDelegate_1_t3020292135 * L_3 = (FacebookDelegate_1_t3020292135 *)il2cpp_codegen_object_new(FacebookDelegate_1_t3020292135_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m2818497626(L_3, __this, L_2, /*hidden argument*/FacebookDelegate_1__ctor_m2818497626_MethodInfo_var);
		FB_API_m682532377(NULL /*static, unused*/, L_1, 0, L_3, (Il2CppObject*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.GameGroups::CallCreateGroupDialog()
extern Il2CppClass* FacebookDelegate_1_t1547895262_il2cpp_TypeInfo_var;
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern const MethodInfo* GameGroups_GroupCreateCB_m1743336484_MethodInfo_var;
extern const MethodInfo* FacebookDelegate_1__ctor_m3830488070_MethodInfo_var;
extern const uint32_t GameGroups_CallCreateGroupDialog_m2698043461_MetadataUsageId;
extern "C"  void GameGroups_CallCreateGroupDialog_m2698043461 (GameGroups_t3348180240 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameGroups_CallCreateGroupDialog_m2698043461_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = __this->get_gamerGroupName_14();
		String_t* L_1 = __this->get_gamerGroupDesc_15();
		String_t* L_2 = __this->get_gamerGroupPrivacy_16();
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)GameGroups_GroupCreateCB_m1743336484_MethodInfo_var);
		FacebookDelegate_1_t1547895262 * L_4 = (FacebookDelegate_1_t1547895262 *)il2cpp_codegen_object_new(FacebookDelegate_1_t1547895262_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m3830488070(L_4, __this, L_3, /*hidden argument*/FacebookDelegate_1__ctor_m3830488070_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_GameGroupCreate_m3542859513(NULL /*static, unused*/, L_0, L_1, L_2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.GameGroups::CallJoinGroupDialog()
extern Il2CppClass* FacebookDelegate_1_t607253590_il2cpp_TypeInfo_var;
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern const MethodInfo* MenuBase_HandleResult_m2705601712_MethodInfo_var;
extern const MethodInfo* FacebookDelegate_1__ctor_m1654595714_MethodInfo_var;
extern const uint32_t GameGroups_CallJoinGroupDialog_m2365340007_MetadataUsageId;
extern "C"  void GameGroups_CallJoinGroupDialog_m2365340007 (GameGroups_t3348180240 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameGroups_CallJoinGroupDialog_m2365340007_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = __this->get_gamerGroupCurrentGroup_17();
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)MenuBase_HandleResult_m2705601712_MethodInfo_var);
		FacebookDelegate_1_t607253590 * L_2 = (FacebookDelegate_1_t607253590 *)il2cpp_codegen_object_new(FacebookDelegate_1_t607253590_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m1654595714(L_2, __this, L_1, /*hidden argument*/FacebookDelegate_1__ctor_m1654595714_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_GameGroupJoin_m2961721593(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.GameGroups::CallFbPostToGamerGroup()
extern Il2CppClass* Dictionary_2_t3943999495_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* FacebookDelegate_1_t3020292135_il2cpp_TypeInfo_var;
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m760167321_MethodInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m4244870320_MethodInfo_var;
extern const MethodInfo* MenuBase_HandleResult_m2705601712_MethodInfo_var;
extern const MethodInfo* FacebookDelegate_1__ctor_m2818497626_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral359429027;
extern Il2CppCodeGenString* _stringLiteral2914252153;
extern Il2CppCodeGenString* _stringLiteral331268233;
extern const uint32_t GameGroups_CallFbPostToGamerGroup_m4156428270_MetadataUsageId;
extern "C"  void GameGroups_CallFbPostToGamerGroup_m4156428270 (GameGroups_t3348180240 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameGroups_CallFbPostToGamerGroup_m4156428270_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t3943999495 * V_0 = NULL;
	{
		Dictionary_2_t3943999495 * L_0 = (Dictionary_2_t3943999495 *)il2cpp_codegen_object_new(Dictionary_2_t3943999495_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m760167321(L_0, /*hidden argument*/Dictionary_2__ctor_m760167321_MethodInfo_var);
		V_0 = L_0;
		Dictionary_2_t3943999495 * L_1 = V_0;
		NullCheck(L_1);
		Dictionary_2_set_Item_m4244870320(L_1, _stringLiteral359429027, _stringLiteral2914252153, /*hidden argument*/Dictionary_2_set_Item_m4244870320_MethodInfo_var);
		String_t* L_2 = __this->get_gamerGroupCurrentGroup_17();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m2596409543(NULL /*static, unused*/, L_2, _stringLiteral331268233, /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)MenuBase_HandleResult_m2705601712_MethodInfo_var);
		FacebookDelegate_1_t3020292135 * L_5 = (FacebookDelegate_1_t3020292135 *)il2cpp_codegen_object_new(FacebookDelegate_1_t3020292135_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m2818497626(L_5, __this, L_4, /*hidden argument*/FacebookDelegate_1__ctor_m2818497626_MethodInfo_var);
		Dictionary_2_t3943999495 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_API_m682532377(NULL /*static, unused*/, L_3, 1, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.GraphRequest::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GraphRequest__ctor_m4238473039_MetadataUsageId;
extern "C"  void GraphRequest__ctor_m4238473039 (GraphRequest_t3721637485 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GraphRequest__ctor_m4238473039_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_apiQuery_14(L_0);
		MenuBase__ctor_m2359507012(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.GraphRequest::GetGui()
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern Il2CppClass* FacebookDelegate_1_t3020292135_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var;
extern const MethodInfo* MenuBase_HandleResult_m2705601712_MethodInfo_var;
extern const MethodInfo* FacebookDelegate_1__ctor_m2818497626_MethodInfo_var;
extern const MethodInfo* GraphRequest_ProfilePhotoCallback_m4292294696_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3169267644;
extern Il2CppCodeGenString* _stringLiteral2665395863;
extern Il2CppCodeGenString* _stringLiteral1917752877;
extern Il2CppCodeGenString* _stringLiteral953923796;
extern Il2CppCodeGenString* _stringLiteral1094338623;
extern Il2CppCodeGenString* _stringLiteral2760576533;
extern Il2CppCodeGenString* _stringLiteral983527008;
extern const uint32_t GraphRequest_GetGui_m3364648626_MetadataUsageId;
extern "C"  void GraphRequest_GetGui_m3364648626 (GraphRequest_t3721637485 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GraphRequest_GetGui_m3364648626_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_0 = GUI_get_enabled_m3504357730(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		bool L_2 = FB_get_IsLoggedIn_m4036830699(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_0014;
	}

IL_0013:
	{
		G_B3_0 = 0;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_enabled_m1897336325(NULL /*static, unused*/, (bool)G_B3_0, /*hidden argument*/NULL);
		bool L_3 = ConsoleBase_Button_m1033918234(__this, _stringLiteral3169267644, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0041;
		}
	}
	{
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)MenuBase_HandleResult_m2705601712_MethodInfo_var);
		FacebookDelegate_1_t3020292135 * L_5 = (FacebookDelegate_1_t3020292135 *)il2cpp_codegen_object_new(FacebookDelegate_1_t3020292135_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m2818497626(L_5, __this, L_4, /*hidden argument*/FacebookDelegate_1__ctor_m2818497626_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_API_m682532377(NULL /*static, unused*/, _stringLiteral2665395863, 0, L_5, (Il2CppObject*)NULL, /*hidden argument*/NULL);
	}

IL_0041:
	{
		bool L_6 = ConsoleBase_Button_m1033918234(__this, _stringLiteral1917752877, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0069;
		}
	}
	{
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)GraphRequest_ProfilePhotoCallback_m4292294696_MethodInfo_var);
		FacebookDelegate_1_t3020292135 * L_8 = (FacebookDelegate_1_t3020292135 *)il2cpp_codegen_object_new(FacebookDelegate_1_t3020292135_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m2818497626(L_8, __this, L_7, /*hidden argument*/FacebookDelegate_1__ctor_m2818497626_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_API_m682532377(NULL /*static, unused*/, _stringLiteral953923796, 0, L_8, (Il2CppObject*)NULL, /*hidden argument*/NULL);
	}

IL_0069:
	{
		bool L_9 = ConsoleBase_Button_m1033918234(__this, _stringLiteral1094338623, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0086;
		}
	}
	{
		Il2CppObject * L_10 = GraphRequest_TakeScreenshot_m2869343810(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_10, /*hidden argument*/NULL);
	}

IL_0086:
	{
		String_t** L_11 = __this->get_address_of_apiQuery_14();
		ConsoleBase_LabelAndTextField_m425453358(__this, _stringLiteral2760576533, L_11, /*hidden argument*/NULL);
		bool L_12 = ConsoleBase_Button_m1033918234(__this, _stringLiteral983527008, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00c0;
		}
	}
	{
		String_t* L_13 = __this->get_apiQuery_14();
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)(void*)MenuBase_HandleResult_m2705601712_MethodInfo_var);
		FacebookDelegate_1_t3020292135 * L_15 = (FacebookDelegate_1_t3020292135 *)il2cpp_codegen_object_new(FacebookDelegate_1_t3020292135_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m2818497626(L_15, __this, L_14, /*hidden argument*/FacebookDelegate_1__ctor_m2818497626_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_API_m682532377(NULL /*static, unused*/, L_13, 0, L_15, (Il2CppObject*)NULL, /*hidden argument*/NULL);
	}

IL_00c0:
	{
		Texture2D_t3542995729 * L_16 = __this->get_profilePic_15();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_17 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_16, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00e2;
		}
	}
	{
		Texture2D_t3542995729 * L_18 = __this->get_profilePic_15();
		GUILayout_Box_m1903306574(NULL /*static, unused*/, L_18, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
	}

IL_00e2:
	{
		bool L_19 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_enabled_m1897336325(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.GraphRequest::ProfilePhotoCallback(Facebook.Unity.IGraphResult)
extern Il2CppClass* IResult_t3447678270_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IGraphResult_t3984946686_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t GraphRequest_ProfilePhotoCallback_m4292294696_MetadataUsageId;
extern "C"  void GraphRequest_ProfilePhotoCallback_m4292294696 (GraphRequest_t3721637485 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GraphRequest_ProfilePhotoCallback_m4292294696_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___result0;
		NullCheck(L_0);
		String_t* L_1 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String Facebook.Unity.IResult::get_Error() */, IResult_t3447678270_il2cpp_TypeInfo_var, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		Il2CppObject * L_3 = ___result0;
		NullCheck(L_3);
		Texture2D_t3542995729 * L_4 = InterfaceFuncInvoker0< Texture2D_t3542995729 * >::Invoke(0 /* UnityEngine.Texture2D Facebook.Unity.IGraphResult::get_Texture() */, IGraphResult_t3984946686_il2cpp_TypeInfo_var, L_3);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_4, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002d;
		}
	}
	{
		Il2CppObject * L_6 = ___result0;
		NullCheck(L_6);
		Texture2D_t3542995729 * L_7 = InterfaceFuncInvoker0< Texture2D_t3542995729 * >::Invoke(0 /* UnityEngine.Texture2D Facebook.Unity.IGraphResult::get_Texture() */, IGraphResult_t3984946686_il2cpp_TypeInfo_var, L_6);
		__this->set_profilePic_15(L_7);
	}

IL_002d:
	{
		Il2CppObject * L_8 = ___result0;
		MenuBase_HandleResult_m2705601712(__this, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Facebook.Unity.Example.GraphRequest::TakeScreenshot()
extern Il2CppClass* U3CTakeScreenshotU3Ec__Iterator0_t3616411949_il2cpp_TypeInfo_var;
extern const uint32_t GraphRequest_TakeScreenshot_m2869343810_MetadataUsageId;
extern "C"  Il2CppObject * GraphRequest_TakeScreenshot_m2869343810 (GraphRequest_t3721637485 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GraphRequest_TakeScreenshot_m2869343810_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CTakeScreenshotU3Ec__Iterator0_t3616411949 * V_0 = NULL;
	{
		U3CTakeScreenshotU3Ec__Iterator0_t3616411949 * L_0 = (U3CTakeScreenshotU3Ec__Iterator0_t3616411949 *)il2cpp_codegen_object_new(U3CTakeScreenshotU3Ec__Iterator0_t3616411949_il2cpp_TypeInfo_var);
		U3CTakeScreenshotU3Ec__Iterator0__ctor_m933868058(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CTakeScreenshotU3Ec__Iterator0_t3616411949 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_5(__this);
		U3CTakeScreenshotU3Ec__Iterator0_t3616411949 * L_2 = V_0;
		return L_2;
	}
}
// System.Void Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::.ctor()
extern "C"  void U3CTakeScreenshotU3Ec__Iterator0__ctor_m933868058 (U3CTakeScreenshotU3Ec__Iterator0_t3616411949 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::MoveNext()
extern Il2CppClass* WaitForEndOfFrame_t1785723201_il2cpp_TypeInfo_var;
extern Il2CppClass* Texture2D_t3542995729_il2cpp_TypeInfo_var;
extern Il2CppClass* WWWForm_t3950226929_il2cpp_TypeInfo_var;
extern Il2CppClass* FacebookDelegate_1_t3020292135_il2cpp_TypeInfo_var;
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern const MethodInfo* MenuBase_HandleResult_m2705601712_MethodInfo_var;
extern const MethodInfo* FacebookDelegate_1__ctor_m2818497626_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1561396403;
extern Il2CppCodeGenString* _stringLiteral89898502;
extern Il2CppCodeGenString* _stringLiteral359429027;
extern Il2CppCodeGenString* _stringLiteral4249598750;
extern Il2CppCodeGenString* _stringLiteral272956984;
extern const uint32_t U3CTakeScreenshotU3Ec__Iterator0_MoveNext_m690632738_MetadataUsageId;
extern "C"  bool U3CTakeScreenshotU3Ec__Iterator0_MoveNext_m690632738 (U3CTakeScreenshotU3Ec__Iterator0_t3616411949 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CTakeScreenshotU3Ec__Iterator0_MoveNext_m690632738_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_8();
		V_0 = L_0;
		__this->set_U24PC_8((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0040;
		}
	}
	{
		goto IL_0119;
	}

IL_0021:
	{
		WaitForEndOfFrame_t1785723201 * L_2 = (WaitForEndOfFrame_t1785723201 *)il2cpp_codegen_object_new(WaitForEndOfFrame_t1785723201_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m3062480170(L_2, /*hidden argument*/NULL);
		__this->set_U24current_6(L_2);
		bool L_3 = __this->get_U24disposing_7();
		if (L_3)
		{
			goto IL_003b;
		}
	}
	{
		__this->set_U24PC_8(1);
	}

IL_003b:
	{
		goto IL_011b;
	}

IL_0040:
	{
		int32_t L_4 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CwidthU3E__0_0(L_4);
		int32_t L_5 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CheightU3E__1_1(L_5);
		int32_t L_6 = __this->get_U3CwidthU3E__0_0();
		int32_t L_7 = __this->get_U3CheightU3E__1_1();
		Texture2D_t3542995729 * L_8 = (Texture2D_t3542995729 *)il2cpp_codegen_object_new(Texture2D_t3542995729_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1873923924(L_8, L_6, L_7, 3, (bool)0, /*hidden argument*/NULL);
		__this->set_U3CtexU3E__2_2(L_8);
		Texture2D_t3542995729 * L_9 = __this->get_U3CtexU3E__2_2();
		int32_t L_10 = __this->get_U3CwidthU3E__0_0();
		int32_t L_11 = __this->get_U3CheightU3E__1_1();
		Rect_t3681755626  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Rect__ctor_m1220545469(&L_12, (0.0f), (0.0f), (((float)((float)L_10))), (((float)((float)L_11))), /*hidden argument*/NULL);
		NullCheck(L_9);
		Texture2D_ReadPixels_m1120832672(L_9, L_12, 0, 0, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_13 = __this->get_U3CtexU3E__2_2();
		NullCheck(L_13);
		Texture2D_Apply_m3543341930(L_13, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_14 = __this->get_U3CtexU3E__2_2();
		NullCheck(L_14);
		ByteU5BU5D_t3397334013* L_15 = Texture2D_EncodeToPNG_m2680110528(L_14, /*hidden argument*/NULL);
		__this->set_U3CscreenshotU3E__3_3(L_15);
		WWWForm_t3950226929 * L_16 = (WWWForm_t3950226929 *)il2cpp_codegen_object_new(WWWForm_t3950226929_il2cpp_TypeInfo_var);
		WWWForm__ctor_m2129424870(L_16, /*hidden argument*/NULL);
		__this->set_U3CwwwFormU3E__4_4(L_16);
		WWWForm_t3950226929 * L_17 = __this->get_U3CwwwFormU3E__4_4();
		ByteU5BU5D_t3397334013* L_18 = __this->get_U3CscreenshotU3E__3_3();
		NullCheck(L_17);
		WWWForm_AddBinaryData_m1248429305(L_17, _stringLiteral1561396403, L_18, _stringLiteral89898502, /*hidden argument*/NULL);
		WWWForm_t3950226929 * L_19 = __this->get_U3CwwwFormU3E__4_4();
		NullCheck(L_19);
		WWWForm_AddField_m1334606983(L_19, _stringLiteral359429027, _stringLiteral4249598750, /*hidden argument*/NULL);
		GraphRequest_t3721637485 * L_20 = __this->get_U24this_5();
		IntPtr_t L_21;
		L_21.set_m_value_0((void*)(void*)MenuBase_HandleResult_m2705601712_MethodInfo_var);
		FacebookDelegate_1_t3020292135 * L_22 = (FacebookDelegate_1_t3020292135 *)il2cpp_codegen_object_new(FacebookDelegate_1_t3020292135_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m2818497626(L_22, L_20, L_21, /*hidden argument*/FacebookDelegate_1__ctor_m2818497626_MethodInfo_var);
		WWWForm_t3950226929 * L_23 = __this->get_U3CwwwFormU3E__4_4();
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_API_m2469039629(NULL /*static, unused*/, _stringLiteral272956984, 1, L_22, L_23, /*hidden argument*/NULL);
		__this->set_U24PC_8((-1));
	}

IL_0119:
	{
		return (bool)0;
	}

IL_011b:
	{
		return (bool)1;
	}
}
// System.Object Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTakeScreenshotU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2546870454 (U3CTakeScreenshotU3Ec__Iterator0_t3616411949 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_6();
		return L_0;
	}
}
// System.Object Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTakeScreenshotU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m4198689662 (U3CTakeScreenshotU3Ec__Iterator0_t3616411949 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_6();
		return L_0;
	}
}
// System.Void Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::Dispose()
extern "C"  void U3CTakeScreenshotU3Ec__Iterator0_Dispose_m407393447 (U3CTakeScreenshotU3Ec__Iterator0_t3616411949 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_7((bool)1);
		__this->set_U24PC_8((-1));
		return;
	}
}
// System.Void Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CTakeScreenshotU3Ec__Iterator0_Reset_m136064961_MetadataUsageId;
extern "C"  void U3CTakeScreenshotU3Ec__Iterator0_Reset_m136064961 (U3CTakeScreenshotU3Ec__Iterator0_t3616411949 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CTakeScreenshotU3Ec__Iterator0_Reset_m136064961_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Facebook.Unity.Example.LogView::.ctor()
extern Il2CppClass* ConsoleBase_t4290192428_il2cpp_TypeInfo_var;
extern const uint32_t LogView__ctor_m1243717231_MetadataUsageId;
extern "C"  void LogView__ctor_m1243717231 (LogView_t3192394209 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LogView__ctor_m1243717231_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ConsoleBase_t4290192428_il2cpp_TypeInfo_var);
		ConsoleBase__ctor_m2763526896(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.LogView::AddLog(System.String)
extern Il2CppClass* LogView_t3192394209_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_1_t2570160834_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3224552505;
extern const uint32_t LogView_AddLog_m3109927044_MetadataUsageId;
extern "C"  void LogView_AddLog_m3109927044 (Il2CppObject * __this /* static, unused */, String_t* ___log0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LogView_AddLog_m3109927044_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DateTime_t693205669  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(LogView_t3192394209_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = ((LogView_t3192394209_StaticFields*)LogView_t3192394209_il2cpp_TypeInfo_var->static_fields)->get_events_14();
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_1 = DateTime_get_Now_m24136300(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = ((LogView_t3192394209_StaticFields*)LogView_t3192394209_il2cpp_TypeInfo_var->static_fields)->get_datePatt_13();
		String_t* L_3 = DateTime_ToString_m1473013667((&V_0), L_2, /*hidden argument*/NULL);
		String_t* L_4 = ___log0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral3224552505, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker2< int32_t, String_t* >::Invoke(1 /* System.Void System.Collections.Generic.IList`1<System.String>::Insert(System.Int32,!0) */, IList_1_t2570160834_il2cpp_TypeInfo_var, L_0, 0, L_5);
		return;
	}
}
// System.Void Facebook.Unity.Example.LogView::OnGUI()
extern Il2CppClass* GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var;
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppClass* ConsoleBase_t4290192428_il2cpp_TypeInfo_var;
extern Il2CppClass* LogView_t3192394209_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_ToArray_TisString_t_m1652456295_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2778558511;
extern Il2CppCodeGenString* _stringLiteral372029352;
extern const uint32_t LogView_OnGUI_m1023876321_MetadataUsageId;
extern "C"  void LogView_OnGUI_m1023876321 (LogView_t3192394209 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LogView_OnGUI_m1023876321_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Touch_t407273883  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Touch_t407273883  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t2243707579  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		GUILayout_BeginVertical_m3700184690(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		bool L_0 = ConsoleBase_Button_m1033918234(__this, _stringLiteral2778558511, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		ConsoleBase_GoBack_m2150591355(__this, /*hidden argument*/NULL);
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		int32_t L_1 = Input_get_touchCount_m2050827666(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_0072;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Touch_t407273883  L_2 = Input_GetTouch_m1463942798(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = Touch_get_phase_m196706494((&V_0), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)1))))
		{
			goto IL_0072;
		}
	}
	{
		Vector2_t2243707579  L_4 = ConsoleBase_get_ScrollPosition_m3651860150(__this, /*hidden argument*/NULL);
		V_1 = L_4;
		Vector2_t2243707579 * L_5 = (&V_1);
		float L_6 = L_5->get_y_1();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Touch_t407273883  L_7 = Input_GetTouch_m1463942798(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_2 = L_7;
		Vector2_t2243707579  L_8 = Touch_get_deltaPosition_m97688791((&V_2), /*hidden argument*/NULL);
		V_3 = L_8;
		float L_9 = (&V_3)->get_y_1();
		L_5->set_y_1(((float)((float)L_6+(float)L_9)));
		Vector2_t2243707579  L_10 = V_1;
		ConsoleBase_set_ScrollPosition_m2281592185(__this, L_10, /*hidden argument*/NULL);
	}

IL_0072:
	{
		Vector2_t2243707579  L_11 = ConsoleBase_get_ScrollPosition_m3651860150(__this, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2108882777* L_12 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(ConsoleBase_t4290192428_il2cpp_TypeInfo_var);
		int32_t L_13 = ConsoleBase_get_MainWindowFullWidth_m1784023901(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_14 = GUILayout_MinWidth_m2112328859(NULL /*static, unused*/, (((float)((float)L_13))), /*hidden argument*/NULL);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_14);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_14);
		Vector2_t2243707579  L_15 = GUILayout_BeginScrollView_m4280181007(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		ConsoleBase_set_ScrollPosition_m2281592185(__this, L_15, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LogView_t3192394209_il2cpp_TypeInfo_var);
		Il2CppObject* L_16 = ((LogView_t3192394209_StaticFields*)LogView_t3192394209_il2cpp_TypeInfo_var->static_fields)->get_events_14();
		StringU5BU5D_t1642385972* L_17 = Enumerable_ToArray_TisString_t_m1652456295(NULL /*static, unused*/, L_16, /*hidden argument*/Enumerable_ToArray_TisString_t_m1652456295_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Join_m1966872927(NULL /*static, unused*/, _stringLiteral372029352, L_17, /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_19 = ConsoleBase_get_TextStyle_m1497590499(__this, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2108882777* L_20 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)2));
		GUILayoutOption_t4183744904 * L_21 = GUILayout_ExpandHeight_m3157662872(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_21);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_21);
		GUILayoutOptionU5BU5D_t2108882777* L_22 = L_20;
		int32_t L_23 = ConsoleBase_get_MainWindowWidth_m4208023734(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_24 = GUILayout_MaxWidth_m3473061269(NULL /*static, unused*/, (((float)((float)L_23))), /*hidden argument*/NULL);
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_24);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t4183744904 *)L_24);
		GUILayout_TextArea_m2850908253(NULL /*static, unused*/, L_18, L_19, L_22, /*hidden argument*/NULL);
		GUILayout_EndScrollView_m1883730923(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndVertical_m297596185(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.LogView::.cctor()
extern Il2CppClass* LogView_t3192394209_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2246541125;
extern const uint32_t LogView__cctor_m2556219980_MetadataUsageId;
extern "C"  void LogView__cctor_m2556219980 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LogView__cctor_m2556219980_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((LogView_t3192394209_StaticFields*)LogView_t3192394209_il2cpp_TypeInfo_var->static_fields)->set_datePatt_13(_stringLiteral2246541125);
		List_1_t1398341365 * L_0 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_0, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		((LogView_t3192394209_StaticFields*)LogView_t3192394209_il2cpp_TypeInfo_var->static_fields)->set_events_14(L_0);
		return;
	}
}
// System.Void Facebook.Unity.Example.MainMenu::.ctor()
extern "C"  void MainMenu__ctor_m250397532 (MainMenu_t3906043390 * __this, const MethodInfo* method)
{
	{
		MenuBase__ctor_m2359507012(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Facebook.Unity.Example.MainMenu::ShowBackButton()
extern "C"  bool MainMenu_ShowBackButton_m3528638778 (MainMenu_t3906043390 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void Facebook.Unity.Example.MainMenu::GetGui()
extern const Il2CppType* DialogShare_t329767461_0_0_0_var;
extern const Il2CppType* GameGroups_t3348180240_0_0_0_var;
extern const Il2CppType* AppRequests_t1683098915_0_0_0_var;
extern const Il2CppType* GraphRequest_t3721637485_0_0_0_var;
extern const Il2CppType* Pay_t1773509418_0_0_0_var;
extern const Il2CppType* AppEvents_t3685765292_0_0_0_var;
extern const Il2CppType* AppLinks_t3144027900_0_0_0_var;
extern const Il2CppType* AppInvites_t3031115107_0_0_0_var;
extern const Il2CppType* AccessTokenMenu_t400501458_0_0_0_var;
extern Il2CppClass* GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppClass* InitDelegate_t3410465555_il2cpp_TypeInfo_var;
extern Il2CppClass* HideUnityDelegate_t712804158_il2cpp_TypeInfo_var;
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIContent_t4210063000_il2cpp_TypeInfo_var;
extern Il2CppClass* ConsoleBase_t4290192428_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const MethodInfo* MainMenu_OnInitComplete_m1743481226_MethodInfo_var;
extern const MethodInfo* MainMenu_OnHideUnity_m2902777907_MethodInfo_var;
extern const MethodInfo* Enumerable_Contains_TisString_t_m2925925544_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1492737306;
extern Il2CppCodeGenString* _stringLiteral1093747982;
extern Il2CppCodeGenString* _stringLiteral4283667535;
extern Il2CppCodeGenString* _stringLiteral3794283964;
extern Il2CppCodeGenString* _stringLiteral3920883009;
extern Il2CppCodeGenString* _stringLiteral4033035561;
extern Il2CppCodeGenString* _stringLiteral3942167792;
extern Il2CppCodeGenString* _stringLiteral1388907417;
extern Il2CppCodeGenString* _stringLiteral1959458025;
extern Il2CppCodeGenString* _stringLiteral2116175235;
extern Il2CppCodeGenString* _stringLiteral2286180664;
extern Il2CppCodeGenString* _stringLiteral1385705279;
extern Il2CppCodeGenString* _stringLiteral3418376909;
extern Il2CppCodeGenString* _stringLiteral696031040;
extern Il2CppCodeGenString* _stringLiteral3902430326;
extern Il2CppCodeGenString* _stringLiteral3710539768;
extern Il2CppCodeGenString* _stringLiteral592786623;
extern Il2CppCodeGenString* _stringLiteral2868125123;
extern const uint32_t MainMenu_GetGui_m525279159_MetadataUsageId;
extern "C"  void MainMenu_GetGui_m525279159 (MainMenu_t3906043390 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenu_GetGui_m525279159_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	int32_t G_B5_0 = 0;
	int32_t G_B14_0 = 0;
	int32_t G_B20_0 = 0;
	{
		GUILayout_BeginVertical_m3700184690(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_0 = GUI_get_enabled_m3504357730(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = ConsoleBase_Button_m1033918234(__this, _stringLiteral1492737306, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0054;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)MainMenu_OnInitComplete_m1743481226_MethodInfo_var);
		InitDelegate_t3410465555 * L_3 = (InitDelegate_t3410465555 *)il2cpp_codegen_object_new(InitDelegate_t3410465555_il2cpp_TypeInfo_var);
		InitDelegate__ctor_m1337962232(L_3, __this, L_2, /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)MainMenu_OnHideUnity_m2902777907_MethodInfo_var);
		HideUnityDelegate_t712804158 * L_5 = (HideUnityDelegate_t712804158 *)il2cpp_codegen_object_new(HideUnityDelegate_t712804158_il2cpp_TypeInfo_var);
		HideUnityDelegate__ctor_m4212726857(L_5, __this, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_Init_m2482858118(NULL /*static, unused*/, L_3, L_5, (String_t*)NULL, /*hidden argument*/NULL);
		String_t* L_6 = FB_get_AppId_m4024040373(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1093747982, L_6, /*hidden argument*/NULL);
		ConsoleBase_set_Status_m2578665855(__this, L_7, /*hidden argument*/NULL);
	}

IL_0054:
	{
		GUILayout_BeginHorizontal_m212592284(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		bool L_8 = V_0;
		if (!L_8)
		{
			goto IL_006c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		bool L_9 = FB_get_IsInitialized_m1783560910(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_9));
		goto IL_006d;
	}

IL_006c:
	{
		G_B5_0 = 0;
	}

IL_006d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_enabled_m1897336325(NULL /*static, unused*/, (bool)G_B5_0, /*hidden argument*/NULL);
		bool L_10 = ConsoleBase_Button_m1033918234(__this, _stringLiteral4283667535, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0093;
		}
	}
	{
		MainMenu_CallFBLogin_m3934412075(__this, /*hidden argument*/NULL);
		ConsoleBase_set_Status_m2578665855(__this, _stringLiteral3794283964, /*hidden argument*/NULL);
	}

IL_0093:
	{
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		bool L_11 = FB_get_IsLoggedIn_m4036830699(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_enabled_m1897336325(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		bool L_12 = ConsoleBase_Button_m1033918234(__this, _stringLiteral3920883009, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00be;
		}
	}
	{
		MainMenu_CallFBLoginForPublish_m3686831997(__this, /*hidden argument*/NULL);
		ConsoleBase_set_Status_m2578665855(__this, _stringLiteral4033035561, /*hidden argument*/NULL);
	}

IL_00be:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t4210063000_il2cpp_TypeInfo_var);
		GUIContent_t4210063000 * L_13 = ((GUIContent_t4210063000_StaticFields*)GUIContent_t4210063000_il2cpp_TypeInfo_var->static_fields)->get_none_6();
		GUILayoutOptionU5BU5D_t2108882777* L_14 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(ConsoleBase_t4290192428_il2cpp_TypeInfo_var);
		int32_t L_15 = ConsoleBase_get_MarginFix_m1996802084(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_16 = GUILayout_MinWidth_m2112328859(NULL /*static, unused*/, (((float)((float)L_15))), /*hidden argument*/NULL);
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, L_16);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_16);
		GUILayout_Label_m2613589922(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		GUILayout_EndHorizontal_m4258536965(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m212592284(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		GUIContent_t4210063000 * L_17 = ((GUIContent_t4210063000_StaticFields*)GUIContent_t4210063000_il2cpp_TypeInfo_var->static_fields)->get_none_6();
		GUILayoutOptionU5BU5D_t2108882777* L_18 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)1));
		int32_t L_19 = ConsoleBase_get_MarginFix_m1996802084(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_20 = GUILayout_MinWidth_m2112328859(NULL /*static, unused*/, (((float)((float)L_19))), /*hidden argument*/NULL);
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, L_20);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_20);
		GUILayout_Label_m2613589922(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		GUILayout_EndHorizontal_m4258536965(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_21 = ConsoleBase_Button_m1033918234(__this, _stringLiteral3942167792, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_0130;
		}
	}
	{
		MainMenu_CallFBLogout_m3817632860(__this, /*hidden argument*/NULL);
		ConsoleBase_set_Status_m2578665855(__this, _stringLiteral1388907417, /*hidden argument*/NULL);
	}

IL_0130:
	{
		bool L_22 = V_0;
		if (!L_22)
		{
			goto IL_013d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		bool L_23 = FB_get_IsInitialized_m1783560910(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B14_0 = ((int32_t)(L_23));
		goto IL_013e;
	}

IL_013d:
	{
		G_B14_0 = 0;
	}

IL_013e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_enabled_m1897336325(NULL /*static, unused*/, (bool)G_B14_0, /*hidden argument*/NULL);
		bool L_24 = ConsoleBase_Button_m1033918234(__this, _stringLiteral1959458025, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_0163;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_25 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(DialogShare_t329767461_0_0_0_var), /*hidden argument*/NULL);
		ConsoleBase_SwitchMenu_m2043949162(__this, L_25, /*hidden argument*/NULL);
	}

IL_0163:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_26 = GUI_get_enabled_m3504357730(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_26;
		bool L_27 = V_0;
		if (!L_27)
		{
			goto IL_018f;
		}
	}
	{
		AccessToken_t2518141643 * L_28 = AccessToken_get_CurrentAccessToken_m3374355058(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_018f;
		}
	}
	{
		AccessToken_t2518141643 * L_29 = AccessToken_get_CurrentAccessToken_m3374355058(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_29);
		Il2CppObject* L_30 = AccessToken_get_Permissions_m2321258725(L_29, /*hidden argument*/NULL);
		bool L_31 = Enumerable_Contains_TisString_t_m2925925544(NULL /*static, unused*/, L_30, _stringLiteral2116175235, /*hidden argument*/Enumerable_Contains_TisString_t_m2925925544_MethodInfo_var);
		G_B20_0 = ((int32_t)(L_31));
		goto IL_0190;
	}

IL_018f:
	{
		G_B20_0 = 0;
	}

IL_0190:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_enabled_m1897336325(NULL /*static, unused*/, (bool)G_B20_0, /*hidden argument*/NULL);
		bool L_32 = ConsoleBase_Button_m1033918234(__this, _stringLiteral2286180664, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_01b5;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_33 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(GameGroups_t3348180240_0_0_0_var), /*hidden argument*/NULL);
		ConsoleBase_SwitchMenu_m2043949162(__this, L_33, /*hidden argument*/NULL);
	}

IL_01b5:
	{
		bool L_34 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_enabled_m1897336325(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		bool L_35 = ConsoleBase_Button_m1033918234(__this, _stringLiteral1385705279, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_01db;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(AppRequests_t1683098915_0_0_0_var), /*hidden argument*/NULL);
		ConsoleBase_SwitchMenu_m2043949162(__this, L_36, /*hidden argument*/NULL);
	}

IL_01db:
	{
		bool L_37 = ConsoleBase_Button_m1033918234(__this, _stringLiteral3418376909, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_01fb;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_38 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(GraphRequest_t3721637485_0_0_0_var), /*hidden argument*/NULL);
		ConsoleBase_SwitchMenu_m2043949162(__this, L_38, /*hidden argument*/NULL);
	}

IL_01fb:
	{
		bool L_39 = Constants_get_IsWeb_m4203885467(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_0225;
		}
	}
	{
		bool L_40 = ConsoleBase_Button_m1033918234(__this, _stringLiteral696031040, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_0225;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_41 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Pay_t1773509418_0_0_0_var), /*hidden argument*/NULL);
		ConsoleBase_SwitchMenu_m2043949162(__this, L_41, /*hidden argument*/NULL);
	}

IL_0225:
	{
		bool L_42 = ConsoleBase_Button_m1033918234(__this, _stringLiteral3902430326, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_0245;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_43 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(AppEvents_t3685765292_0_0_0_var), /*hidden argument*/NULL);
		ConsoleBase_SwitchMenu_m2043949162(__this, L_43, /*hidden argument*/NULL);
	}

IL_0245:
	{
		bool L_44 = ConsoleBase_Button_m1033918234(__this, _stringLiteral3710539768, /*hidden argument*/NULL);
		if (!L_44)
		{
			goto IL_0265;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_45 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(AppLinks_t3144027900_0_0_0_var), /*hidden argument*/NULL);
		ConsoleBase_SwitchMenu_m2043949162(__this, L_45, /*hidden argument*/NULL);
	}

IL_0265:
	{
		bool L_46 = Constants_get_IsMobile_m868982319(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_46)
		{
			goto IL_028f;
		}
	}
	{
		bool L_47 = ConsoleBase_Button_m1033918234(__this, _stringLiteral592786623, /*hidden argument*/NULL);
		if (!L_47)
		{
			goto IL_028f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_48 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(AppInvites_t3031115107_0_0_0_var), /*hidden argument*/NULL);
		ConsoleBase_SwitchMenu_m2043949162(__this, L_48, /*hidden argument*/NULL);
	}

IL_028f:
	{
		bool L_49 = Constants_get_IsMobile_m868982319(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_49)
		{
			goto IL_02b9;
		}
	}
	{
		bool L_50 = ConsoleBase_Button_m1033918234(__this, _stringLiteral2868125123, /*hidden argument*/NULL);
		if (!L_50)
		{
			goto IL_02b9;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_51 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(AccessTokenMenu_t400501458_0_0_0_var), /*hidden argument*/NULL);
		ConsoleBase_SwitchMenu_m2043949162(__this, L_51, /*hidden argument*/NULL);
	}

IL_02b9:
	{
		GUILayout_EndVertical_m297596185(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_52 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_enabled_m1897336325(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.MainMenu::CallFBLogin()
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern Il2CppClass* FacebookDelegate_1_t3733898188_il2cpp_TypeInfo_var;
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4061286785_MethodInfo_var;
extern const MethodInfo* MenuBase_HandleResult_m2705601712_MethodInfo_var;
extern const MethodInfo* FacebookDelegate_1__ctor_m1054243793_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2346865269;
extern Il2CppCodeGenString* _stringLiteral1111044658;
extern Il2CppCodeGenString* _stringLiteral1552242599;
extern const uint32_t MainMenu_CallFBLogin_m3934412075_MetadataUsageId;
extern "C"  void MainMenu_CallFBLogin_m3934412075 (MainMenu_t3906043390 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenu_CallFBLogin_m3934412075_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t1398341365 * V_0 = NULL;
	{
		List_1_t1398341365 * L_0 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_0, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		V_0 = L_0;
		List_1_t1398341365 * L_1 = V_0;
		NullCheck(L_1);
		List_1_Add_m4061286785(L_1, _stringLiteral2346865269, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
		List_1_t1398341365 * L_2 = V_0;
		NullCheck(L_2);
		List_1_Add_m4061286785(L_2, _stringLiteral1111044658, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
		List_1_t1398341365 * L_3 = V_0;
		NullCheck(L_3);
		List_1_Add_m4061286785(L_3, _stringLiteral1552242599, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
		List_1_t1398341365 * L_4 = V_0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)MenuBase_HandleResult_m2705601712_MethodInfo_var);
		FacebookDelegate_1_t3733898188 * L_6 = (FacebookDelegate_1_t3733898188 *)il2cpp_codegen_object_new(FacebookDelegate_1_t3733898188_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m1054243793(L_6, __this, L_5, /*hidden argument*/FacebookDelegate_1__ctor_m1054243793_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_LogInWithReadPermissions_m450336890(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.MainMenu::CallFBLoginForPublish()
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern Il2CppClass* FacebookDelegate_1_t3733898188_il2cpp_TypeInfo_var;
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4061286785_MethodInfo_var;
extern const MethodInfo* MenuBase_HandleResult_m2705601712_MethodInfo_var;
extern const MethodInfo* FacebookDelegate_1__ctor_m1054243793_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2116175235;
extern const uint32_t MainMenu_CallFBLoginForPublish_m3686831997_MetadataUsageId;
extern "C"  void MainMenu_CallFBLoginForPublish_m3686831997 (MainMenu_t3906043390 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenu_CallFBLoginForPublish_m3686831997_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t1398341365 * V_0 = NULL;
	{
		List_1_t1398341365 * L_0 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_0, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		V_0 = L_0;
		List_1_t1398341365 * L_1 = V_0;
		NullCheck(L_1);
		List_1_Add_m4061286785(L_1, _stringLiteral2116175235, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
		List_1_t1398341365 * L_2 = V_0;
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)MenuBase_HandleResult_m2705601712_MethodInfo_var);
		FacebookDelegate_1_t3733898188 * L_4 = (FacebookDelegate_1_t3733898188 *)il2cpp_codegen_object_new(FacebookDelegate_1_t3733898188_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m1054243793(L_4, __this, L_3, /*hidden argument*/FacebookDelegate_1__ctor_m1054243793_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_LogInWithPublishPermissions_m619042695(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.MainMenu::CallFBLogout()
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern const uint32_t MainMenu_CallFBLogout_m3817632860_MetadataUsageId;
extern "C"  void MainMenu_CallFBLogout_m3817632860 (MainMenu_t3906043390 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenu_CallFBLogout_m3817632860_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_LogOut_m3952695663(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.MainMenu::OnInitComplete()
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* LogView_t3192394209_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2743654117;
extern Il2CppCodeGenString* _stringLiteral3970368125;
extern Il2CppCodeGenString* _stringLiteral146303733;
extern const uint32_t MainMenu_OnInitComplete_m1743481226_MetadataUsageId;
extern "C"  void MainMenu_OnInitComplete_m1743481226 (MainMenu_t3906043390 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenu_OnInitComplete_m1743481226_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ConsoleBase_set_Status_m2578665855(__this, _stringLiteral2743654117, /*hidden argument*/NULL);
		ConsoleBase_set_LastResponse_m510948058(__this, _stringLiteral3970368125, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		bool L_0 = FB_get_IsLoggedIn_m4036830699(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = L_0;
		Il2CppObject * L_2 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_1);
		bool L_3 = FB_get_IsInitialized_m1783560910(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_4 = L_3;
		Il2CppObject * L_5 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral146303733, L_2, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		String_t* L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(LogView_t3192394209_il2cpp_TypeInfo_var);
		LogView_AddLog_m3109927044(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		AccessToken_t2518141643 * L_8 = AccessToken_get_CurrentAccessToken_m3374355058(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0054;
		}
	}
	{
		AccessToken_t2518141643 * L_9 = AccessToken_get_CurrentAccessToken_m3374355058(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_9);
		IL2CPP_RUNTIME_CLASS_INIT(LogView_t3192394209_il2cpp_TypeInfo_var);
		LogView_AddLog_m3109927044(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
	}

IL_0054:
	{
		return;
	}
}
// System.Void Facebook.Unity.Example.MainMenu::OnHideUnity(System.Boolean)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* LogView_t3192394209_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2743654117;
extern Il2CppCodeGenString* _stringLiteral1695412383;
extern Il2CppCodeGenString* _stringLiteral4198907447;
extern const uint32_t MainMenu_OnHideUnity_m2902777907_MetadataUsageId;
extern "C"  void MainMenu_OnHideUnity_m2902777907 (MainMenu_t3906043390 * __this, bool ___isGameShown0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenu_OnHideUnity_m2902777907_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ConsoleBase_set_Status_m2578665855(__this, _stringLiteral2743654117, /*hidden argument*/NULL);
		bool L_0 = ___isGameShown0;
		bool L_1 = L_0;
		Il2CppObject * L_2 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral1695412383, L_2, /*hidden argument*/NULL);
		ConsoleBase_set_LastResponse_m510948058(__this, L_3, /*hidden argument*/NULL);
		bool L_4 = ___isGameShown0;
		bool L_5 = L_4;
		Il2CppObject * L_6 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_5);
		String_t* L_7 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral4198907447, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LogView_t3192394209_il2cpp_TypeInfo_var);
		LogView_AddLog_m3109927044(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.MenuBase::.ctor()
extern Il2CppClass* ConsoleBase_t4290192428_il2cpp_TypeInfo_var;
extern const uint32_t MenuBase__ctor_m2359507012_MetadataUsageId;
extern "C"  void MenuBase__ctor_m2359507012 (MenuBase_t1506935956 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuBase__ctor_m2359507012_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ConsoleBase_t4290192428_il2cpp_TypeInfo_var);
		ConsoleBase__ctor_m2763526896(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Facebook.Unity.Example.MenuBase::ShowDialogModeSelector()
extern "C"  bool MenuBase_ShowDialogModeSelector_m3522050877 (MenuBase_t1506935956 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean Facebook.Unity.Example.MenuBase::ShowBackButton()
extern "C"  bool MenuBase_ShowBackButton_m2153444738 (MenuBase_t1506935956 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void Facebook.Unity.Example.MenuBase::HandleResult(Facebook.Unity.IResult)
extern Il2CppClass* LogView_t3192394209_il2cpp_TypeInfo_var;
extern Il2CppClass* IResult_t3447678270_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1083054768;
extern Il2CppCodeGenString* _stringLiteral3347447868;
extern Il2CppCodeGenString* _stringLiteral2600214883;
extern Il2CppCodeGenString* _stringLiteral2156018129;
extern Il2CppCodeGenString* _stringLiteral4213332260;
extern Il2CppCodeGenString* _stringLiteral2743654117;
extern Il2CppCodeGenString* _stringLiteral1697477896;
extern Il2CppCodeGenString* _stringLiteral2470506088;
extern const uint32_t MenuBase_HandleResult_m2705601712_MetadataUsageId;
extern "C"  void MenuBase_HandleResult_m2705601712 (MenuBase_t1506935956 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuBase_HandleResult_m2705601712_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___result0;
		if (L_0)
		{
			goto IL_001d;
		}
	}
	{
		ConsoleBase_set_LastResponse_m510948058(__this, _stringLiteral1083054768, /*hidden argument*/NULL);
		String_t* L_1 = ConsoleBase_get_LastResponse_m1599769685(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LogView_t3192394209_il2cpp_TypeInfo_var);
		LogView_AddLog_m3109927044(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}

IL_001d:
	{
		ConsoleBase_set_LastResponseTexture_m704700253(__this, (Texture2D_t3542995729 *)NULL, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___result0;
		NullCheck(L_2);
		String_t* L_3 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String Facebook.Unity.IResult::get_Error() */, IResult_t3447678270_il2cpp_TypeInfo_var, L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_005a;
		}
	}
	{
		ConsoleBase_set_Status_m2578665855(__this, _stringLiteral3347447868, /*hidden argument*/NULL);
		Il2CppObject * L_5 = ___result0;
		NullCheck(L_5);
		String_t* L_6 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String Facebook.Unity.IResult::get_Error() */, IResult_t3447678270_il2cpp_TypeInfo_var, L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2600214883, L_6, /*hidden argument*/NULL);
		ConsoleBase_set_LastResponse_m510948058(__this, L_7, /*hidden argument*/NULL);
		goto IL_00cc;
	}

IL_005a:
	{
		Il2CppObject * L_8 = ___result0;
		NullCheck(L_8);
		bool L_9 = InterfaceFuncInvoker0< bool >::Invoke(3 /* System.Boolean Facebook.Unity.IResult::get_Cancelled() */, IResult_t3447678270_il2cpp_TypeInfo_var, L_8);
		if (!L_9)
		{
			goto IL_008b;
		}
	}
	{
		ConsoleBase_set_Status_m2578665855(__this, _stringLiteral2156018129, /*hidden argument*/NULL);
		Il2CppObject * L_10 = ___result0;
		NullCheck(L_10);
		String_t* L_11 = InterfaceFuncInvoker0< String_t* >::Invoke(2 /* System.String Facebook.Unity.IResult::get_RawResult() */, IResult_t3447678270_il2cpp_TypeInfo_var, L_10);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral4213332260, L_11, /*hidden argument*/NULL);
		ConsoleBase_set_LastResponse_m510948058(__this, L_12, /*hidden argument*/NULL);
		goto IL_00cc;
	}

IL_008b:
	{
		Il2CppObject * L_13 = ___result0;
		NullCheck(L_13);
		String_t* L_14 = InterfaceFuncInvoker0< String_t* >::Invoke(2 /* System.String Facebook.Unity.IResult::get_RawResult() */, IResult_t3447678270_il2cpp_TypeInfo_var, L_13);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_15 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_00c1;
		}
	}
	{
		ConsoleBase_set_Status_m2578665855(__this, _stringLiteral2743654117, /*hidden argument*/NULL);
		Il2CppObject * L_16 = ___result0;
		NullCheck(L_16);
		String_t* L_17 = InterfaceFuncInvoker0< String_t* >::Invoke(2 /* System.String Facebook.Unity.IResult::get_RawResult() */, IResult_t3447678270_il2cpp_TypeInfo_var, L_16);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1697477896, L_17, /*hidden argument*/NULL);
		ConsoleBase_set_LastResponse_m510948058(__this, L_18, /*hidden argument*/NULL);
		goto IL_00cc;
	}

IL_00c1:
	{
		ConsoleBase_set_LastResponse_m510948058(__this, _stringLiteral2470506088, /*hidden argument*/NULL);
	}

IL_00cc:
	{
		Il2CppObject * L_19 = ___result0;
		NullCheck(L_19);
		String_t* L_20 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_19);
		IL2CPP_RUNTIME_CLASS_INIT(LogView_t3192394209_il2cpp_TypeInfo_var);
		LogView_AddLog_m3109927044(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.MenuBase::OnGUI()
extern Il2CppClass* GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var;
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppClass* ConsoleBase_t4290192428_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIContent_t4210063000_il2cpp_TypeInfo_var;
extern const uint32_t MenuBase_OnGUI_m446320748_MetadataUsageId;
extern "C"  void MenuBase_OnGUI_m446320748 (MenuBase_t1506935956 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuBase_OnGUI_m446320748_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Touch_t407273883  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Touch_t407273883  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t2243707579  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		bool L_0 = ConsoleBase_IsHorizontalLayout_m2006263654(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		GUILayout_BeginHorizontal_m212592284(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		GUILayout_BeginVertical_m3700184690(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
	}

IL_0021:
	{
		Type_t * L_1 = Object_GetType_m191970594(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_1);
		GUIStyle_t1799908754 * L_3 = ConsoleBase_get_LabelStyle_m3739078378(__this, /*hidden argument*/NULL);
		GUILayout_Label_m616748838(NULL /*static, unused*/, L_2, L_3, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		MenuBase_AddStatus_m2758539425(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		int32_t L_4 = Input_get_touchCount_m2050827666(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0094;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Touch_t407273883  L_5 = Input_GetTouch_m1463942798(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_5;
		int32_t L_6 = Touch_get_phase_m196706494((&V_0), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)1))))
		{
			goto IL_0094;
		}
	}
	{
		Vector2_t2243707579  L_7 = ConsoleBase_get_ScrollPosition_m3651860150(__this, /*hidden argument*/NULL);
		V_1 = L_7;
		Vector2_t2243707579 * L_8 = (&V_1);
		float L_9 = L_8->get_y_1();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Touch_t407273883  L_10 = Input_GetTouch_m1463942798(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_2 = L_10;
		Vector2_t2243707579  L_11 = Touch_get_deltaPosition_m97688791((&V_2), /*hidden argument*/NULL);
		V_3 = L_11;
		float L_12 = (&V_3)->get_y_1();
		L_8->set_y_1(((float)((float)L_9+(float)L_12)));
		Vector2_t2243707579  L_13 = V_1;
		ConsoleBase_set_ScrollPosition_m2281592185(__this, L_13, /*hidden argument*/NULL);
	}

IL_0094:
	{
		Vector2_t2243707579  L_14 = ConsoleBase_get_ScrollPosition_m3651860150(__this, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2108882777* L_15 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(ConsoleBase_t4290192428_il2cpp_TypeInfo_var);
		int32_t L_16 = ConsoleBase_get_MainWindowFullWidth_m1784023901(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_17 = GUILayout_MinWidth_m2112328859(NULL /*static, unused*/, (((float)((float)L_16))), /*hidden argument*/NULL);
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, L_17);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_17);
		Vector2_t2243707579  L_18 = GUILayout_BeginScrollView_m4280181007(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		ConsoleBase_set_ScrollPosition_m2281592185(__this, L_18, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m212592284(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		bool L_19 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Facebook.Unity.Example.MenuBase::ShowBackButton() */, __this);
		if (!L_19)
		{
			goto IL_00d5;
		}
	}
	{
		MenuBase_AddBackButton_m4248190796(__this, /*hidden argument*/NULL);
	}

IL_00d5:
	{
		MenuBase_AddLogButton_m780571607(__this, /*hidden argument*/NULL);
		bool L_20 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Facebook.Unity.Example.MenuBase::ShowBackButton() */, __this);
		if (!L_20)
		{
			goto IL_0104;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t4210063000_il2cpp_TypeInfo_var);
		GUIContent_t4210063000 * L_21 = ((GUIContent_t4210063000_StaticFields*)GUIContent_t4210063000_il2cpp_TypeInfo_var->static_fields)->get_none_6();
		GUILayoutOptionU5BU5D_t2108882777* L_22 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(ConsoleBase_t4290192428_il2cpp_TypeInfo_var);
		int32_t L_23 = ConsoleBase_get_MarginFix_m1996802084(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_24 = GUILayout_MinWidth_m2112328859(NULL /*static, unused*/, (((float)((float)L_23))), /*hidden argument*/NULL);
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_24);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_24);
		GUILayout_Label_m2613589922(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
	}

IL_0104:
	{
		GUILayout_EndHorizontal_m4258536965(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_25 = VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean Facebook.Unity.Example.MenuBase::ShowDialogModeSelector() */, __this);
		if (!L_25)
		{
			goto IL_011a;
		}
	}
	{
		MenuBase_AddDialogModeButtons_m1836654807(__this, /*hidden argument*/NULL);
	}

IL_011a:
	{
		GUILayout_BeginVertical_m3700184690(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(5 /* System.Void Facebook.Unity.Example.MenuBase::GetGui() */, __this);
		GUILayout_Space_m1087863221(NULL /*static, unused*/, (10.0f), /*hidden argument*/NULL);
		GUILayout_EndVertical_m297596185(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndScrollView_m1883730923(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.MenuBase::AddStatus()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var;
extern Il2CppClass* ConsoleBase_t4290192428_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3118048902;
extern const uint32_t MenuBase_AddStatus_m2758539425_MetadataUsageId;
extern "C"  void MenuBase_AddStatus_m2758539425 (MenuBase_t1506935956 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuBase_AddStatus_m2758539425_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GUILayout_Space_m1087863221(NULL /*static, unused*/, (5.0f), /*hidden argument*/NULL);
		String_t* L_0 = ConsoleBase_get_Status_m15166664(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral3118048902, L_0, /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_2 = ConsoleBase_get_TextStyle_m1497590499(__this, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2108882777* L_3 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(ConsoleBase_t4290192428_il2cpp_TypeInfo_var);
		int32_t L_4 = ConsoleBase_get_MainWindowWidth_m4208023734(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t4183744904 * L_5 = GUILayout_MinWidth_m2112328859(NULL /*static, unused*/, (((float)((float)L_4))), /*hidden argument*/NULL);
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_5);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_5);
		GUILayout_Box_m3556262915(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.MenuBase::AddBackButton()
extern Il2CppClass* ConsoleBase_t4290192428_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_Any_TisString_t_m3014604563_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2778558511;
extern const uint32_t MenuBase_AddBackButton_m4248190796_MetadataUsageId;
extern "C"  void MenuBase_AddBackButton_m4248190796 (MenuBase_t1506935956 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuBase_AddBackButton_m4248190796_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ConsoleBase_t4290192428_il2cpp_TypeInfo_var);
		Stack_1_t3116948387 * L_0 = ConsoleBase_get_MenuStack_m2022459395(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = Enumerable_Any_TisString_t_m3014604563(NULL /*static, unused*/, L_0, /*hidden argument*/Enumerable_Any_TisString_t_m3014604563_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_enabled_m1897336325(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		bool L_2 = ConsoleBase_Button_m1033918234(__this, _stringLiteral2778558511, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		ConsoleBase_GoBack_m2150591355(__this, /*hidden argument*/NULL);
	}

IL_0025:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_enabled_m1897336325(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.MenuBase::AddLogButton()
extern const Il2CppType* LogView_t3192394209_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1502599712;
extern const uint32_t MenuBase_AddLogButton_m780571607_MetadataUsageId;
extern "C"  void MenuBase_AddLogButton_m780571607 (MenuBase_t1506935956 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuBase_AddLogButton_m780571607_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ConsoleBase_Button_m1033918234(__this, _stringLiteral1502599712, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(LogView_t3192394209_0_0_0_var), /*hidden argument*/NULL);
		ConsoleBase_SwitchMenu_m2043949162(__this, L_1, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void Facebook.Unity.Example.MenuBase::AddDialogModeButtons()
extern const Il2CppType* ShareDialogMode_t1445392044_0_0_0_var;
extern Il2CppClass* GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2459695545_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* ShareDialogMode_t1445392044_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t MenuBase_AddDialogModeButtons_m1836654807_MetadataUsageId;
extern "C"  void MenuBase_AddDialogModeButtons_m1836654807 (MenuBase_t1506935956 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuBase_AddDialogModeButtons_m1836654807_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		GUILayout_BeginHorizontal_m212592284(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(ShareDialogMode_t1445392044_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Il2CppArray * L_1 = Enum_GetValues_m2107059536(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Il2CppObject * L_2 = Array_GetEnumerator_m2284404958(L_1, /*hidden argument*/NULL);
		V_1 = L_2;
	}

IL_0020:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0038;
		}

IL_0025:
		{
			Il2CppObject * L_3 = V_1;
			NullCheck(L_3);
			Il2CppObject * L_4 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_3);
			V_0 = L_4;
			Il2CppObject * L_5 = V_0;
			MenuBase_AddDialogModeButton_m1973909729(__this, ((*(int32_t*)((int32_t*)UnBox (L_5, ShareDialogMode_t1445392044_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		}

IL_0038:
		{
			Il2CppObject * L_6 = V_1;
			NullCheck(L_6);
			bool L_7 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_6);
			if (L_7)
			{
				goto IL_0025;
			}
		}

IL_0043:
		{
			IL2CPP_LEAVE(0x5C, FINALLY_0048);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0048;
	}

FINALLY_0048:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_8 = V_1;
			Il2CppObject * L_9 = ((Il2CppObject *)IsInst(L_8, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			V_2 = L_9;
			if (!L_9)
			{
				goto IL_005b;
			}
		}

IL_0055:
		{
			Il2CppObject * L_10 = V_2;
			NullCheck(L_10);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_10);
		}

IL_005b:
		{
			IL2CPP_END_FINALLY(72)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(72)
	{
		IL2CPP_JUMP_TBL(0x5C, IL_005c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_005c:
	{
		GUILayout_EndHorizontal_m4258536965(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.MenuBase::AddDialogModeButton(Facebook.Unity.ShareDialogMode)
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppClass* MenuBase_t1506935956_il2cpp_TypeInfo_var;
extern Il2CppClass* ShareDialogMode_t1445392044_il2cpp_TypeInfo_var;
extern const uint32_t MenuBase_AddDialogModeButton_m1973909729_MetadataUsageId;
extern "C"  void MenuBase_AddDialogModeButton_m1973909729 (MenuBase_t1506935956 * __this, int32_t ___mode0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuBase_AddDialogModeButton_m1973909729_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_0 = GUI_get_enabled_m3504357730(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_2 = ___mode0;
		int32_t L_3 = ((MenuBase_t1506935956_StaticFields*)MenuBase_t1506935956_il2cpp_TypeInfo_var->static_fields)->get_shareDialogMode_13();
		G_B3_0 = ((((int32_t)((((int32_t)L_2) == ((int32_t)L_3))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_001a;
	}

IL_0019:
	{
		G_B3_0 = 0;
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_enabled_m1897336325(NULL /*static, unused*/, (bool)G_B3_0, /*hidden argument*/NULL);
		Il2CppObject * L_4 = Box(ShareDialogMode_t1445392044_il2cpp_TypeInfo_var, (&___mode0));
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		bool L_6 = ConsoleBase_Button_m1033918234(__this, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_7 = ___mode0;
		((MenuBase_t1506935956_StaticFields*)MenuBase_t1506935956_il2cpp_TypeInfo_var->static_fields)->set_shareDialogMode_13(L_7);
		int32_t L_8 = ___mode0;
		Mobile_set_ShareDialogMode_m2081995206(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_0043:
	{
		bool L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_enabled_m1897336325(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.Pay::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Pay__ctor_m3165855134_MetadataUsageId;
extern "C"  void Pay__ctor_m3165855134 (Pay_t1773509418 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pay__ctor_m3165855134_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_payProduct_14(L_0);
		MenuBase__ctor_m2359507012(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.Pay::GetGui()
extern Il2CppCodeGenString* _stringLiteral3041380973;
extern Il2CppCodeGenString* _stringLiteral672188016;
extern const uint32_t Pay_GetGui_m1468582959_MetadataUsageId;
extern "C"  void Pay_GetGui_m1468582959 (Pay_t1773509418 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pay_GetGui_m1468582959_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t** L_0 = __this->get_address_of_payProduct_14();
		ConsoleBase_LabelAndTextField_m425453358(__this, _stringLiteral3041380973, L_0, /*hidden argument*/NULL);
		bool L_1 = ConsoleBase_Button_m1033918234(__this, _stringLiteral672188016, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Pay_CallFBPay_m4092762016(__this, /*hidden argument*/NULL);
	}

IL_0027:
	{
		GUILayout_Space_m1087863221(NULL /*static, unused*/, (10.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.Pay::CallFBPay()
extern Il2CppClass* Nullable_1_t334943763_il2cpp_TypeInfo_var;
extern Il2CppClass* FacebookDelegate_1_t1895462303_il2cpp_TypeInfo_var;
extern const MethodInfo* MenuBase_HandleResult_m2705601712_MethodInfo_var;
extern const MethodInfo* FacebookDelegate_1__ctor_m428301059_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2632181174;
extern const uint32_t Pay_CallFBPay_m4092762016_MetadataUsageId;
extern "C"  void Pay_CallFBPay_m4092762016 (Pay_t1773509418 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pay_CallFBPay_m4092762016_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Nullable_1_t334943763  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		String_t* L_0 = __this->get_payProduct_14();
		Initobj (Nullable_1_t334943763_il2cpp_TypeInfo_var, (&V_0));
		Nullable_1_t334943763  L_1 = V_0;
		Initobj (Nullable_1_t334943763_il2cpp_TypeInfo_var, (&V_0));
		Nullable_1_t334943763  L_2 = V_0;
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)MenuBase_HandleResult_m2705601712_MethodInfo_var);
		FacebookDelegate_1_t1895462303 * L_4 = (FacebookDelegate_1_t1895462303 *)il2cpp_codegen_object_new(FacebookDelegate_1_t1895462303_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m428301059(L_4, __this, L_3, /*hidden argument*/FacebookDelegate_1__ctor_m428301059_MethodInfo_var);
		Canvas_Pay_m4050601057(NULL /*static, unused*/, L_0, _stringLiteral2632181174, 1, L_1, L_2, (String_t*)NULL, (String_t*)NULL, (String_t*)NULL, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Game_Manager::.ctor()
extern "C"  void Game_Manager__ctor_m3655742037 (Game_Manager_t4105539090 * __this, const MethodInfo* method)
{
	{
		__this->set_noOfPlayers_2(2);
		__this->set_isSoundOn_5((bool)1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// Game_Manager Game_Manager::get__gminstance()
extern Il2CppClass* Game_Manager_t4105539090_il2cpp_TypeInfo_var;
extern const uint32_t Game_Manager_get__gminstance_m2308798245_MetadataUsageId;
extern "C"  Game_Manager_t4105539090 * Game_Manager_get__gminstance_m2308798245 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Game_Manager_get__gminstance_m2308798245_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Game_Manager_t4105539090 * L_0 = ((Game_Manager_t4105539090_StaticFields*)Game_Manager_t4105539090_il2cpp_TypeInfo_var->static_fields)->get_gmInstance_3();
		return L_0;
	}
}
// System.Void Game_Manager::Start()
extern Il2CppClass* Game_Manager_t4105539090_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Game_Manager_Start_m2210529981_MetadataUsageId;
extern "C"  void Game_Manager_Start_m2210529981 (Game_Manager_t4105539090 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Game_Manager_Start_m2210529981_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Game_Manager_t4105539090 * L_0 = ((Game_Manager_t4105539090_StaticFields*)Game_Manager_t4105539090_il2cpp_TypeInfo_var->static_fields)->get_gmInstance_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0030;
		}
	}
	{
		Game_Manager_t4105539090 * L_2 = ((Game_Manager_t4105539090_StaticFields*)Game_Manager_t4105539090_il2cpp_TypeInfo_var->static_fields)->get_gmInstance_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_2, __this, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		goto IL_0036;
	}

IL_0030:
	{
		((Game_Manager_t4105539090_StaticFields*)Game_Manager_t4105539090_il2cpp_TypeInfo_var->static_fields)->set_gmInstance_3(__this);
	}

IL_0036:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m2330762974(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		int32_t L_6 = Random_Range_m694320887(NULL /*static, unused*/, 0, 2, /*hidden argument*/NULL);
		__this->set_arenaIndex_4(L_6);
		return;
	}
}
// System.Void Game_Manager::Update()
extern "C"  void Game_Manager_Update_m1417075124 (Game_Manager_t4105539090 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_arenaIndex_4();
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0013;
		}
	}
	{
		__this->set_arenaIndex_4(0);
	}

IL_0013:
	{
		return;
	}
}
// System.Void Game_Manager::SetPlayers(System.Int32)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4263387574;
extern Il2CppCodeGenString* _stringLiteral2896826166;
extern const uint32_t Game_Manager_SetPlayers_m277409050_MetadataUsageId;
extern "C"  void Game_Manager_SetPlayers_m277409050 (Game_Manager_t4105539090 * __this, int32_t ___i0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Game_Manager_SetPlayers_m277409050_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___i0;
		__this->set_noOfPlayers_2(L_0);
		int32_t L_1 = ___i0;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m2000667605(NULL /*static, unused*/, _stringLiteral4263387574, L_3, _stringLiteral2896826166, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Game_Manager::GetPlayers()
extern "C"  int32_t Game_Manager_GetPlayers_m971560877 (Game_Manager_t4105539090 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_noOfPlayers_2();
		return L_0;
	}
}
// System.Void Game_Manager::ChangeArena()
extern "C"  void Game_Manager_ChangeArena_m3365242478 (Game_Manager_t4105539090 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_arenaIndex_4();
		__this->set_arenaIndex_4(((int32_t)((int32_t)L_0+(int32_t)1)));
		return;
	}
}
// System.Void Goti_Status::.ctor()
extern "C"  void Goti_Status__ctor_m4251937469 (Goti_Status_t1473870168 * __this, const MethodInfo* method)
{
	{
		__this->set_curIndexPos_8((-1));
		__this->set_runSpeed_10((10.0f));
		__this->set_hieght_11((0.65f));
		__this->set_myWorldPosition_43(((int32_t)70));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Goti_Status::Start()
extern const MethodInfo* Component_GetComponent_TisEnemy_AI_t244483799_m3521222472_MethodInfo_var;
extern const uint32_t Goti_Status_Start_m1169207645_MetadataUsageId;
extern "C"  void Goti_Status_Start_m1169207645 (Goti_Status_t1473870168 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Goti_Status_Start_m1169207645_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_myState_2(1);
		Color_Manager_Script_t4053987423 * L_0 = __this->get_myManger_4();
		NullCheck(L_0);
		TransformU5BU5D_t3764228911* L_1 = L_0->get_myPath_10();
		NullCheck(L_1);
		__this->set_pathL_16(((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))-(int32_t)1)));
		Transform_t3275118058 * L_2 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t2243707580  L_3 = Transform_get_position_m1104419803(L_2, /*hidden argument*/NULL);
		__this->set_myStartPos_13(L_3);
		Vector3_t2243707580  L_4 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_pos_24(L_4);
		Transform_t3275118058 * L_5 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = Transform_GetChild_m3838588184(L_5, 0, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_localPosition_m2533925116(L_6, /*hidden argument*/NULL);
		__this->set_initCPos_23(L_7);
		Color_Manager_Script_t4053987423 * L_8 = __this->get_myManger_4();
		NullCheck(L_8);
		bool L_9 = L_8->get_CPU_13();
		if (!L_9)
		{
			goto IL_0070;
		}
	}
	{
		Color_Manager_Script_t4053987423 * L_10 = __this->get_myManger_4();
		NullCheck(L_10);
		Enemy_AI_t244483799 * L_11 = Component_GetComponent_TisEnemy_AI_t244483799_m3521222472(L_10, /*hidden argument*/Component_GetComponent_TisEnemy_AI_t244483799_m3521222472_MethodInfo_var);
		__this->set_eai_32(L_11);
	}

IL_0070:
	{
		__this->set_myWorldPosition_43(((int32_t)200));
		Camera_t189460977 * L_12 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_myTargetCam_50(L_12);
		return;
	}
}
// System.Void Goti_Status::Update()
extern const MethodInfo* List_1_get_Count_m4027941115_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m2503489122_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1800757231;
extern Il2CppCodeGenString* _stringLiteral1266177044;
extern Il2CppCodeGenString* _stringLiteral721376978;
extern Il2CppCodeGenString* _stringLiteral339800437;
extern Il2CppCodeGenString* _stringLiteral272007032;
extern Il2CppCodeGenString* _stringLiteral2963825781;
extern const uint32_t Goti_Status_Update_m1463045634_MetadataUsageId;
extern "C"  void Goti_Status_Update_m1463045634 (Goti_Status_t1473870168 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Goti_Status_Update_m1463045634_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		int32_t L_0 = __this->get_pathL_16();
		int32_t L_1 = __this->get_curIndexPos_8();
		__this->set_restrictionOutput_15(((int32_t)((int32_t)L_0-(int32_t)L_1)));
		int32_t L_2 = __this->get_myState_2();
		if ((((int32_t)L_2) == ((int32_t)3)))
		{
			goto IL_018d;
		}
	}
	{
		bool L_3 = __this->get_yoICanPlay_6();
		if (!L_3)
		{
			goto IL_0098;
		}
	}
	{
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_set_layer_m2712461877(L_4, 0, /*hidden argument*/NULL);
		Transform_t3275118058 * L_5 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = Transform_GetChild_m3838588184(L_5, 0, /*hidden argument*/NULL);
		Vector3_t2243707580 * L_7 = __this->get_address_of_initCPos_23();
		float L_8 = L_7->get_x_1();
		float L_9 = __this->get_animaOffsetX_25();
		Vector3_t2243707580 * L_10 = __this->get_address_of_initCPos_23();
		float L_11 = L_10->get_y_2();
		float L_12 = __this->get_animaOffsetY_26();
		Vector3_t2243707580 * L_13 = __this->get_address_of_initCPos_23();
		float L_14 = L_13->get_z_3();
		float L_15 = __this->get_animaOffsetZ_27();
		Vector3_t2243707580  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Vector3__ctor_m2638739322(&L_16, ((float)((float)L_8+(float)L_9)), ((float)((float)L_11+(float)L_12)), ((float)((float)L_14+(float)L_15)), /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_set_localPosition_m1026930133(L_6, L_16, /*hidden argument*/NULL);
		Animator_t69676727 * L_17 = __this->get_anime_20();
		NullCheck(L_17);
		Animator_Play_m4142158565(L_17, _stringLiteral1800757231, (-1), /*hidden argument*/NULL);
		goto IL_018d;
	}

IL_0098:
	{
		bool L_18 = __this->get_yoICanPlay_6();
		if (L_18)
		{
			goto IL_018d;
		}
	}
	{
		GameObject_t1756533147 * L_19 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_19);
		GameObject_set_layer_m2712461877(L_19, 2, /*hidden argument*/NULL);
		bool L_20 = __this->get_startRunning_9();
		if (L_20)
		{
			goto IL_00f2;
		}
	}
	{
		bool L_21 = __this->get_attacking_45();
		if (L_21)
		{
			goto IL_00f2;
		}
	}
	{
		Animator_t69676727 * L_22 = __this->get_anime_20();
		NullCheck(L_22);
		Animator_Play_m4142158565(L_22, _stringLiteral1266177044, (-1), /*hidden argument*/NULL);
		Transform_t3275118058 * L_23 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_23);
		Transform_t3275118058 * L_24 = Transform_GetChild_m3838588184(L_23, 0, /*hidden argument*/NULL);
		Vector3_t2243707580  L_25 = __this->get_initCPos_23();
		NullCheck(L_24);
		Transform_set_localPosition_m1026930133(L_24, L_25, /*hidden argument*/NULL);
		goto IL_018d;
	}

IL_00f2:
	{
		bool L_26 = __this->get_startRunning_9();
		if (L_26)
		{
			goto IL_0166;
		}
	}
	{
		bool L_27 = __this->get_attacking_45();
		if (!L_27)
		{
			goto IL_0166;
		}
	}
	{
		Animator_t69676727 * L_28 = __this->get_anime_20();
		NullCheck(L_28);
		Animator_Play_m4142158565(L_28, _stringLiteral721376978, (-1), /*hidden argument*/NULL);
		Vector3_t2243707580 * L_29 = __this->get_address_of_initCPos_23();
		float L_30 = L_29->get_x_1();
		float L_31 = __this->get_attackOffX_30();
		Vector3_t2243707580 * L_32 = __this->get_address_of_initCPos_23();
		float L_33 = L_32->get_y_2();
		Vector3_t2243707580 * L_34 = __this->get_address_of_initCPos_23();
		float L_35 = L_34->get_z_3();
		float L_36 = __this->get_attackOffZ_31();
		Vector3__ctor_m2638739322((&V_0), ((float)((float)L_30+(float)L_31)), L_33, ((float)((float)L_35+(float)L_36)), /*hidden argument*/NULL);
		Transform_t3275118058 * L_37 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_37);
		Transform_t3275118058 * L_38 = Transform_GetChild_m3838588184(L_37, 0, /*hidden argument*/NULL);
		Vector3_t2243707580  L_39 = V_0;
		NullCheck(L_38);
		Transform_set_localPosition_m1026930133(L_38, L_39, /*hidden argument*/NULL);
		goto IL_018d;
	}

IL_0166:
	{
		bool L_40 = __this->get_startRunning_9();
		if (L_40)
		{
			goto IL_018d;
		}
	}
	{
		bool L_41 = __this->get_gettingHit_46();
		if (!L_41)
		{
			goto IL_018d;
		}
	}
	{
		Animator_t69676727 * L_42 = __this->get_anime_20();
		NullCheck(L_42);
		Animator_Play_m4142158565(L_42, _stringLiteral339800437, (-1), /*hidden argument*/NULL);
	}

IL_018d:
	{
		int32_t L_43 = __this->get_myState_2();
		if ((!(((uint32_t)L_43) == ((uint32_t)1))))
		{
			goto IL_01c2;
		}
	}
	{
		Color_Manager_Script_t4053987423 * L_44 = __this->get_myManger_4();
		NullCheck(L_44);
		int32_t L_45 = L_44->get_diceInput_7();
		if ((!(((uint32_t)L_45) == ((uint32_t)6))))
		{
			goto IL_01b6;
		}
	}
	{
		__this->set_canBePlayed_3((bool)1);
		goto IL_01bd;
	}

IL_01b6:
	{
		__this->set_canBePlayed_3((bool)0);
	}

IL_01bd:
	{
		goto IL_0215;
	}

IL_01c2:
	{
		int32_t L_46 = __this->get_myState_2();
		if (L_46)
		{
			goto IL_01d9;
		}
	}
	{
		__this->set_canBePlayed_3((bool)1);
		goto IL_0215;
	}

IL_01d9:
	{
		int32_t L_47 = __this->get_myState_2();
		if ((!(((uint32_t)L_47) == ((uint32_t)2))))
		{
			goto IL_01f1;
		}
	}
	{
		__this->set_canBePlayed_3((bool)0);
		goto IL_0215;
	}

IL_01f1:
	{
		int32_t L_48 = __this->get_myState_2();
		if ((!(((uint32_t)L_48) == ((uint32_t)3))))
		{
			goto IL_0215;
		}
	}
	{
		__this->set_canBePlayed_3((bool)0);
		Animator_t69676727 * L_49 = __this->get_anime_20();
		NullCheck(L_49);
		Animator_Play_m4142158565(L_49, _stringLiteral272007032, (-1), /*hidden argument*/NULL);
	}

IL_0215:
	{
		int32_t L_50 = __this->get_myState_2();
		if ((((int32_t)L_50) == ((int32_t)3)))
		{
			goto IL_026c;
		}
	}
	{
		Color_Manager_Script_t4053987423 * L_51 = __this->get_myManger_4();
		NullCheck(L_51);
		int32_t L_52 = L_51->get_diceInput_7();
		int32_t L_53 = __this->get_restrictionOutput_15();
		if ((((int32_t)L_52) <= ((int32_t)L_53)))
		{
			goto IL_0243;
		}
	}
	{
		__this->set_myState_2(2);
		goto IL_026c;
	}

IL_0243:
	{
		Color_Manager_Script_t4053987423 * L_54 = __this->get_myManger_4();
		NullCheck(L_54);
		int32_t L_55 = L_54->get_diceInput_7();
		int32_t L_56 = __this->get_restrictionOutput_15();
		if ((((int32_t)L_55) > ((int32_t)L_56)))
		{
			goto IL_026c;
		}
	}
	{
		int32_t L_57 = __this->get_myState_2();
		if ((((int32_t)L_57) == ((int32_t)1)))
		{
			goto IL_026c;
		}
	}
	{
		__this->set_myState_2(0);
	}

IL_026c:
	{
		bool L_58 = __this->get_startRunning_9();
		if (!L_58)
		{
			goto IL_03a3;
		}
	}
	{
		List_1_t1612828712 * L_59 = __this->get_pathToFollow_21();
		NullCheck(L_59);
		int32_t L_60 = List_1_get_Count_m4027941115(L_59, /*hidden argument*/List_1_get_Count_m4027941115_MethodInfo_var);
		if ((((int32_t)L_60) <= ((int32_t)0)))
		{
			goto IL_03a3;
		}
	}
	{
		Color_Manager_Script_t4053987423 * L_61 = __this->get_myManger_4();
		NullCheck(L_61);
		Color_Manager_Script_StopShowingAll_m978555850(L_61, /*hidden argument*/NULL);
		Animator_t69676727 * L_62 = __this->get_anime_20();
		NullCheck(L_62);
		Animator_Play_m4142158565(L_62, _stringLiteral2963825781, (-1), /*hidden argument*/NULL);
		List_1_t1612828712 * L_63 = __this->get_pathToFollow_21();
		int32_t L_64 = __this->get_listCounter_22();
		NullCheck(L_63);
		Vector3_t2243707580  L_65 = List_1_get_Item_m2503489122(L_63, L_64, /*hidden argument*/List_1_get_Item_m2503489122_MethodInfo_var);
		V_1 = L_65;
		float L_66 = (&V_1)->get_x_1();
		List_1_t1612828712 * L_67 = __this->get_pathToFollow_21();
		int32_t L_68 = __this->get_listCounter_22();
		NullCheck(L_67);
		Vector3_t2243707580  L_69 = List_1_get_Item_m2503489122(L_67, L_68, /*hidden argument*/List_1_get_Item_m2503489122_MethodInfo_var);
		V_2 = L_69;
		float L_70 = (&V_2)->get_y_2();
		float L_71 = __this->get_hieght_11();
		List_1_t1612828712 * L_72 = __this->get_pathToFollow_21();
		int32_t L_73 = __this->get_listCounter_22();
		NullCheck(L_72);
		Vector3_t2243707580  L_74 = List_1_get_Item_m2503489122(L_72, L_73, /*hidden argument*/List_1_get_Item_m2503489122_MethodInfo_var);
		V_3 = L_74;
		float L_75 = (&V_3)->get_z_3();
		Vector3_t2243707580  L_76;
		memset(&L_76, 0, sizeof(L_76));
		Vector3__ctor_m2638739322(&L_76, L_66, ((float)((float)L_70+(float)L_71)), L_75, /*hidden argument*/NULL);
		__this->set_pos_24(L_76);
		__this->set_calledOnce_12((bool)0);
		Transform_t3275118058 * L_77 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_77);
		Transform_t3275118058 * L_78 = Transform_GetChild_m3838588184(L_77, 0, /*hidden argument*/NULL);
		Vector3_t2243707580 * L_79 = __this->get_address_of_initCPos_23();
		float L_80 = L_79->get_x_1();
		float L_81 = __this->get_walkAnimaX_28();
		Vector3_t2243707580 * L_82 = __this->get_address_of_initCPos_23();
		float L_83 = L_82->get_y_2();
		float L_84 = __this->get_walkAnimaY_29();
		Vector3_t2243707580 * L_85 = __this->get_address_of_initCPos_23();
		float L_86 = L_85->get_z_3();
		Vector3_t2243707580  L_87;
		memset(&L_87, 0, sizeof(L_87));
		Vector3__ctor_m2638739322(&L_87, ((float)((float)L_80+(float)L_81)), ((float)((float)L_83+(float)L_84)), L_86, /*hidden argument*/NULL);
		NullCheck(L_78);
		Transform_set_localPosition_m1026930133(L_78, L_87, /*hidden argument*/NULL);
		Transform_t3275118058 * L_88 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_89 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_89);
		Vector3_t2243707580  L_90 = Transform_get_position_m1104419803(L_89, /*hidden argument*/NULL);
		Vector3_t2243707580  L_91 = __this->get_pos_24();
		float L_92 = __this->get_runSpeed_10();
		float L_93 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_94 = Vector3_MoveTowards_m1358638081(NULL /*static, unused*/, L_90, L_91, ((float)((float)L_92*(float)L_93)), /*hidden argument*/NULL);
		NullCheck(L_88);
		Transform_set_position_m2469242620(L_88, L_94, /*hidden argument*/NULL);
		Transform_t3275118058 * L_95 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_95);
		Vector3_t2243707580  L_96 = Transform_get_position_m1104419803(L_95, /*hidden argument*/NULL);
		Vector3_t2243707580  L_97 = __this->get_pos_24();
		bool L_98 = Vector3_op_Equality_m305888255(NULL /*static, unused*/, L_96, L_97, /*hidden argument*/NULL);
		if (!L_98)
		{
			goto IL_03a3;
		}
	}
	{
		int32_t L_99 = __this->get_listCounter_22();
		__this->set_listCounter_22(((int32_t)((int32_t)L_99+(int32_t)1)));
	}

IL_03a3:
	{
		Transform_t3275118058 * L_100 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_100);
		Vector3_t2243707580  L_101 = Transform_get_position_m1104419803(L_100, /*hidden argument*/NULL);
		Vector3_t2243707580 * L_102 = __this->get_address_of_finalPos_7();
		float L_103 = L_102->get_x_1();
		Vector3_t2243707580 * L_104 = __this->get_address_of_finalPos_7();
		float L_105 = L_104->get_y_2();
		float L_106 = __this->get_hieght_11();
		Vector3_t2243707580 * L_107 = __this->get_address_of_finalPos_7();
		float L_108 = L_107->get_z_3();
		Vector3_t2243707580  L_109;
		memset(&L_109, 0, sizeof(L_109));
		Vector3__ctor_m2638739322(&L_109, L_103, ((float)((float)L_105+(float)L_106)), L_108, /*hidden argument*/NULL);
		bool L_110 = Vector3_op_Equality_m305888255(NULL /*static, unused*/, L_101, L_109, /*hidden argument*/NULL);
		if (!L_110)
		{
			goto IL_0438;
		}
	}
	{
		bool L_111 = __this->get_calledOnce_12();
		if (L_111)
		{
			goto IL_0438;
		}
	}
	{
		Vector3_t2243707580  L_112 = __this->get_finalPos_7();
		Color_Manager_Script_t4053987423 * L_113 = __this->get_myManger_4();
		NullCheck(L_113);
		TransformU5BU5D_t3764228911* L_114 = L_113->get_myPath_10();
		Color_Manager_Script_t4053987423 * L_115 = __this->get_myManger_4();
		NullCheck(L_115);
		TransformU5BU5D_t3764228911* L_116 = L_115->get_myPath_10();
		NullCheck(L_116);
		NullCheck(L_114);
		int32_t L_117 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_116)->max_length))))-(int32_t)1));
		Transform_t3275118058 * L_118 = (L_114)->GetAt(static_cast<il2cpp_array_size_t>(L_117));
		NullCheck(L_118);
		Vector3_t2243707580  L_119 = Transform_get_position_m1104419803(L_118, /*hidden argument*/NULL);
		bool L_120 = Vector3_op_Equality_m305888255(NULL /*static, unused*/, L_112, L_119, /*hidden argument*/NULL);
		if (!L_120)
		{
			goto IL_042b;
		}
	}
	{
		Goti_Status_GotiWin_m1675532522(__this, /*hidden argument*/NULL);
		goto IL_0431;
	}

IL_042b:
	{
		Goti_Status_AfterMovement_m2604936664(__this, /*hidden argument*/NULL);
	}

IL_0431:
	{
		__this->set_calledOnce_12((bool)1);
	}

IL_0438:
	{
		return;
	}
}
// System.Void Goti_Status::ShowICanPlay()
extern "C"  void Goti_Status_ShowICanPlay_m3291303315 (Goti_Status_t1473870168 * __this, const MethodInfo* method)
{
	{
		__this->set_yoICanPlay_6((bool)1);
		__this->set_actionPhase_5((bool)1);
		return;
	}
}
// System.Void Goti_Status::StopShowing()
extern "C"  void Goti_Status_StopShowing_m4087610708 (Goti_Status_t1473870168 * __this, const MethodInfo* method)
{
	{
		__this->set_yoICanPlay_6((bool)0);
		__this->set_actionPhase_5((bool)0);
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = Transform_GetChild_m3838588184(L_0, 0, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = __this->get_initCPos_23();
		NullCheck(L_1);
		Transform_set_localPosition_m1026930133(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Goti_Status::PerformMovement()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m2338641291_MethodInfo_var;
extern const MethodInfo* List_1_set_Item_m3393612627_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral465089729;
extern const uint32_t Goti_Status_PerformMovement_m702764983_MetadataUsageId;
extern "C"  void Goti_Status_PerformMovement_m702764983 (Goti_Status_t1473870168 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Goti_Status_PerformMovement_m702764983_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		Color_Manager_Script_t4053987423 * L_0 = __this->get_myManger_4();
		NullCheck(L_0);
		String_t* L_1 = L_0->get_myName_2();
		String_t* L_2 = Object_get_name_m2079638459(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m612901809(NULL /*static, unused*/, L_1, _stringLiteral465089729, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		int32_t L_4 = __this->get_myWorldPosition_43();
		Color_Manager_Script_t4053987423 * L_5 = __this->get_myManger_4();
		NullCheck(L_5);
		int32_t L_6 = L_5->get_diceInput_7();
		__this->set_worldPosToBeReached_33(((int32_t)((int32_t)L_4+(int32_t)L_6)));
		int32_t L_7 = __this->get_worldPosToBeReached_33();
		V_0 = ((int32_t)((int32_t)L_7-(int32_t)((int32_t)52)));
		int32_t L_8 = V_0;
		if ((((int32_t)L_8) <= ((int32_t)0)))
		{
			goto IL_0050;
		}
	}
	{
		int32_t L_9 = V_0;
		__this->set_worldPosToBeReached_33(L_9);
	}

IL_0050:
	{
		__this->set_performedNext_42((bool)0);
		__this->set_hasCollided_18((bool)0);
		int32_t L_10 = __this->get_myState_2();
		if ((!(((uint32_t)L_10) == ((uint32_t)1))))
		{
			goto IL_00ce;
		}
	}
	{
		AudioSource_t1135106623 * L_11 = __this->get_openingAS_49();
		NullCheck(L_11);
		AudioSource_Play_m353744792(L_11, /*hidden argument*/NULL);
		__this->set_curIndexPos_8(0);
		List_1_t1612828712 * L_12 = __this->get_pathToFollow_21();
		Color_Manager_Script_t4053987423 * L_13 = __this->get_myManger_4();
		NullCheck(L_13);
		TransformU5BU5D_t3764228911* L_14 = L_13->get_myPath_10();
		int32_t L_15 = __this->get_curIndexPos_8();
		NullCheck(L_14);
		int32_t L_16 = L_15;
		Transform_t3275118058 * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		NullCheck(L_17);
		Vector3_t2243707580  L_18 = Transform_get_position_m1104419803(L_17, /*hidden argument*/NULL);
		NullCheck(L_12);
		List_1_Add_m2338641291(L_12, L_18, /*hidden argument*/List_1_Add_m2338641291_MethodInfo_var);
		Color_Manager_Script_t4053987423 * L_19 = __this->get_myManger_4();
		NullCheck(L_19);
		TransformU5BU5D_t3764228911* L_20 = L_19->get_myPath_10();
		int32_t L_21 = __this->get_curIndexPos_8();
		NullCheck(L_20);
		int32_t L_22 = L_21;
		Transform_t3275118058 * L_23 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		NullCheck(L_23);
		Vector3_t2243707580  L_24 = Transform_get_position_m1104419803(L_23, /*hidden argument*/NULL);
		__this->set_finalPos_7(L_24);
		__this->set_startRunning_9((bool)1);
		__this->set_myState_2(0);
		goto IL_0168;
	}

IL_00ce:
	{
		int32_t L_25 = __this->get_myState_2();
		if (L_25)
		{
			goto IL_0168;
		}
	}
	{
		int32_t L_26 = __this->get_curIndexPos_8();
		Color_Manager_Script_t4053987423 * L_27 = __this->get_myManger_4();
		NullCheck(L_27);
		int32_t L_28 = L_27->get_diceInput_7();
		V_1 = ((int32_t)((int32_t)L_26+(int32_t)L_28));
		Color_Manager_Script_t4053987423 * L_29 = __this->get_myManger_4();
		NullCheck(L_29);
		TransformU5BU5D_t3764228911* L_30 = L_29->get_myPath_10();
		int32_t L_31 = V_1;
		NullCheck(L_30);
		int32_t L_32 = L_31;
		Transform_t3275118058 * L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_33);
		Vector3_t2243707580  L_34 = Transform_get_position_m1104419803(L_33, /*hidden argument*/NULL);
		__this->set_finalPos_7(L_34);
		V_2 = 0;
		goto IL_0150;
	}

IL_010b:
	{
		int32_t L_35 = __this->get_curIndexPos_8();
		__this->set_curIndexPos_8(((int32_t)((int32_t)L_35+(int32_t)1)));
		List_1_t1612828712 * L_36 = __this->get_pathToFollow_21();
		Vector3_t2243707580  L_37 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_36);
		List_1_Add_m2338641291(L_36, L_37, /*hidden argument*/List_1_Add_m2338641291_MethodInfo_var);
		List_1_t1612828712 * L_38 = __this->get_pathToFollow_21();
		int32_t L_39 = V_2;
		Color_Manager_Script_t4053987423 * L_40 = __this->get_myManger_4();
		NullCheck(L_40);
		TransformU5BU5D_t3764228911* L_41 = L_40->get_myPath_10();
		int32_t L_42 = __this->get_curIndexPos_8();
		NullCheck(L_41);
		int32_t L_43 = L_42;
		Transform_t3275118058 * L_44 = (L_41)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		NullCheck(L_44);
		Vector3_t2243707580  L_45 = Transform_get_position_m1104419803(L_44, /*hidden argument*/NULL);
		NullCheck(L_38);
		List_1_set_Item_m3393612627(L_38, L_39, L_45, /*hidden argument*/List_1_set_Item_m3393612627_MethodInfo_var);
		int32_t L_46 = V_2;
		V_2 = ((int32_t)((int32_t)L_46+(int32_t)1));
	}

IL_0150:
	{
		int32_t L_47 = V_2;
		Color_Manager_Script_t4053987423 * L_48 = __this->get_myManger_4();
		NullCheck(L_48);
		int32_t L_49 = L_48->get_diceInput_7();
		if ((((int32_t)L_47) < ((int32_t)L_49)))
		{
			goto IL_010b;
		}
	}
	{
		__this->set_startRunning_9((bool)1);
	}

IL_0168:
	{
		return;
	}
}
// System.Void Goti_Status::AfterMovement()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Clear_m576262818_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1266177044;
extern Il2CppCodeGenString* _stringLiteral721376978;
extern Il2CppCodeGenString* _stringLiteral2710706624;
extern Il2CppCodeGenString* _stringLiteral982957810;
extern Il2CppCodeGenString* _stringLiteral2751284617;
extern const uint32_t Goti_Status_AfterMovement_m2604936664_MetadataUsageId;
extern "C"  void Goti_Status_AfterMovement_m2604936664 (Goti_Status_t1473870168 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Goti_Status_AfterMovement_m2604936664_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Transform_t3275118058 * V_1 = NULL;
	TransformU5BU5D_t3764228911* V_2 = NULL;
	int32_t V_3 = 0;
	Vector2_t2243707579  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector2_t2243707579  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t2243707580  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_t2243707580  V_9;
	memset(&V_9, 0, sizeof(V_9));
	{
		bool L_0 = __this->get_attacking_45();
		if (L_0)
		{
			goto IL_0038;
		}
	}
	{
		Animator_t69676727 * L_1 = __this->get_anime_20();
		NullCheck(L_1);
		Animator_Play_m4142158565(L_1, _stringLiteral1266177044, (-1), /*hidden argument*/NULL);
		Transform_t3275118058 * L_2 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = Transform_GetChild_m3838588184(L_2, 0, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = __this->get_initCPos_23();
		NullCheck(L_3);
		Transform_set_localPosition_m1026930133(L_3, L_4, /*hidden argument*/NULL);
		goto IL_0091;
	}

IL_0038:
	{
		Animator_t69676727 * L_5 = __this->get_anime_20();
		NullCheck(L_5);
		Animator_Play_m4142158565(L_5, _stringLiteral721376978, (-1), /*hidden argument*/NULL);
		Vector3_t2243707580 * L_6 = __this->get_address_of_initCPos_23();
		float L_7 = L_6->get_x_1();
		float L_8 = __this->get_attackOffX_30();
		Vector3_t2243707580 * L_9 = __this->get_address_of_initCPos_23();
		float L_10 = L_9->get_y_2();
		Vector3_t2243707580 * L_11 = __this->get_address_of_initCPos_23();
		float L_12 = L_11->get_z_3();
		float L_13 = __this->get_attackOffZ_31();
		Vector3__ctor_m2638739322((&V_0), ((float)((float)L_7+(float)L_8)), L_10, ((float)((float)L_12+(float)L_13)), /*hidden argument*/NULL);
		Transform_t3275118058 * L_14 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		Transform_t3275118058 * L_15 = Transform_GetChild_m3838588184(L_14, 0, /*hidden argument*/NULL);
		Vector3_t2243707580  L_16 = V_0;
		NullCheck(L_15);
		Transform_set_localPosition_m1026930133(L_15, L_16, /*hidden argument*/NULL);
	}

IL_0091:
	{
		List_1_t1612828712 * L_17 = __this->get_pathToFollow_21();
		NullCheck(L_17);
		List_1_Clear_m576262818(L_17, /*hidden argument*/List_1_Clear_m576262818_MethodInfo_var);
		__this->set_listCounter_22(0);
		Color_Manager_Script_t4053987423 * L_18 = __this->get_myManger_4();
		NullCheck(L_18);
		TransformU5BU5D_t3764228911* L_19 = L_18->get_safePoints_11();
		V_2 = L_19;
		V_3 = 0;
		goto IL_0133;
	}

IL_00b6:
	{
		TransformU5BU5D_t3764228911* L_20 = V_2;
		int32_t L_21 = V_3;
		NullCheck(L_20);
		int32_t L_22 = L_21;
		Transform_t3275118058 * L_23 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		V_1 = L_23;
		Transform_t3275118058 * L_24 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_24);
		Vector3_t2243707580  L_25 = Transform_get_position_m1104419803(L_24, /*hidden argument*/NULL);
		V_5 = L_25;
		float L_26 = (&V_5)->get_x_1();
		Transform_t3275118058 * L_27 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		Vector3_t2243707580  L_28 = Transform_get_position_m1104419803(L_27, /*hidden argument*/NULL);
		V_6 = L_28;
		float L_29 = (&V_6)->get_z_3();
		Vector2__ctor_m3067419446((&V_4), L_26, L_29, /*hidden argument*/NULL);
		Transform_t3275118058 * L_30 = V_1;
		NullCheck(L_30);
		Vector3_t2243707580  L_31 = Transform_get_position_m1104419803(L_30, /*hidden argument*/NULL);
		V_8 = L_31;
		float L_32 = (&V_8)->get_x_1();
		Transform_t3275118058 * L_33 = V_1;
		NullCheck(L_33);
		Vector3_t2243707580  L_34 = Transform_get_position_m1104419803(L_33, /*hidden argument*/NULL);
		V_9 = L_34;
		float L_35 = (&V_9)->get_z_3();
		Vector2__ctor_m3067419446((&V_7), L_32, L_35, /*hidden argument*/NULL);
		Vector2_t2243707579  L_36 = V_4;
		Vector2_t2243707579  L_37 = V_7;
		bool L_38 = Vector2_op_Equality_m4168854394(NULL /*static, unused*/, L_36, L_37, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_0128;
		}
	}
	{
		__this->set_isSafe_19((bool)1);
		goto IL_013c;
	}

IL_0128:
	{
		__this->set_isSafe_19((bool)0);
		int32_t L_39 = V_3;
		V_3 = ((int32_t)((int32_t)L_39+(int32_t)1));
	}

IL_0133:
	{
		int32_t L_40 = V_3;
		TransformU5BU5D_t3764228911* L_41 = V_2;
		NullCheck(L_41);
		if ((((int32_t)L_40) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_41)->max_length)))))))
		{
			goto IL_00b6;
		}
	}

IL_013c:
	{
		Color_Manager_Script_t4053987423 * L_42 = __this->get_myManger_4();
		NullCheck(L_42);
		int32_t L_43 = L_42->get_diceInput_7();
		if ((((int32_t)L_43) == ((int32_t)6)))
		{
			goto IL_0169;
		}
	}
	{
		__this->set_startRunning_9((bool)0);
		MonoBehaviour_Invoke_m666563676(__this, _stringLiteral2710706624, (0.5f), /*hidden argument*/NULL);
		goto IL_0180;
	}

IL_0169:
	{
		__this->set_startRunning_9((bool)0);
		MonoBehaviour_Invoke_m666563676(__this, _stringLiteral982957810, (0.5f), /*hidden argument*/NULL);
	}

IL_0180:
	{
		Color_Manager_Script_t4053987423 * L_44 = __this->get_myManger_4();
		NullCheck(L_44);
		String_t* L_45 = Object_get_name_m2079638459(L_44, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_46 = String_Concat_m2596409543(NULL /*static, unused*/, L_45, _stringLiteral2751284617, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		Color_Manager_Script_t4053987423 * L_47 = __this->get_myManger_4();
		NullCheck(L_47);
		Color_Manager_Script_CalculatePlayables_m3860738715(L_47, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Goti_Status::GotiWin()
extern "C"  void Goti_Status_GotiWin_m1675532522 (Goti_Status_t1473870168 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_1 = __this->get_myWinPos_17();
		NullCheck(L_1);
		Vector3_t2243707580  L_2 = Transform_get_position_m1104419803(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_m2469242620(L_0, L_2, /*hidden argument*/NULL);
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t2243707580  L_5 = Transform_get_eulerAngles_m4066505159(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		float L_6 = (&V_0)->get_x_1();
		Transform_t3275118058 * L_7 = __this->get_myWinPos_17();
		NullCheck(L_7);
		Vector3_t2243707580  L_8 = Transform_get_eulerAngles_m4066505159(L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		float L_9 = (&V_1)->get_y_2();
		Transform_t3275118058 * L_10 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t2243707580  L_11 = Transform_get_eulerAngles_m4066505159(L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		float L_12 = (&V_2)->get_z_3();
		Vector3_t2243707580  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Vector3__ctor_m2638739322(&L_13, L_6, L_9, L_12, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_eulerAngles_m2881310872(L_3, L_13, /*hidden argument*/NULL);
		__this->set_startRunning_9((bool)0);
		__this->set_myState_2(3);
		Color_Manager_Script_t4053987423 * L_14 = __this->get_myManger_4();
		NullCheck(L_14);
		Color_Manager_Script_CalculatePlayables_m3860738715(L_14, /*hidden argument*/NULL);
		Color_Manager_Script_t4053987423 * L_15 = __this->get_myManger_4();
		NullCheck(L_15);
		int32_t L_16 = L_15->get_noOfGotiyaWin_14();
		if ((((int32_t)L_16) == ((int32_t)4)))
		{
			goto IL_009e;
		}
	}
	{
		Color_Manager_Script_t4053987423 * L_17 = __this->get_myManger_4();
		NullCheck(L_17);
		Turn_Manager_t2388373579 * L_18 = L_17->get_tm_6();
		NullCheck(L_18);
		Turn_Manager_RepeatTurn_m1002454660(L_18, /*hidden argument*/NULL);
		goto IL_00ae;
	}

IL_009e:
	{
		Color_Manager_Script_t4053987423 * L_19 = __this->get_myManger_4();
		NullCheck(L_19);
		Turn_Manager_t2388373579 * L_20 = L_19->get_tm_6();
		NullCheck(L_20);
		Turn_Manager_PerformNextTurn_m2187029365(L_20, /*hidden argument*/NULL);
	}

IL_00ae:
	{
		return;
	}
}
// System.Collections.IEnumerator Goti_Status::CutGoti(System.Single)
extern Il2CppClass* U3CCutGotiU3Ec__Iterator0_t377100891_il2cpp_TypeInfo_var;
extern const uint32_t Goti_Status_CutGoti_m1406000177_MetadataUsageId;
extern "C"  Il2CppObject * Goti_Status_CutGoti_m1406000177 (Goti_Status_t1473870168 * __this, float ___f0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Goti_Status_CutGoti_m1406000177_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CCutGotiU3Ec__Iterator0_t377100891 * V_0 = NULL;
	{
		U3CCutGotiU3Ec__Iterator0_t377100891 * L_0 = (U3CCutGotiU3Ec__Iterator0_t377100891 *)il2cpp_codegen_object_new(U3CCutGotiU3Ec__Iterator0_t377100891_il2cpp_TypeInfo_var);
		U3CCutGotiU3Ec__Iterator0__ctor_m3139146000(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CCutGotiU3Ec__Iterator0_t377100891 * L_1 = V_0;
		float L_2 = ___f0;
		NullCheck(L_1);
		L_1->set_f_0(L_2);
		U3CCutGotiU3Ec__Iterator0_t377100891 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_1(__this);
		U3CCutGotiU3Ec__Iterator0_t377100891 * L_4 = V_0;
		return L_4;
	}
}
// System.Void Goti_Status::OnTriggerEnter(UnityEngine.Collider)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisGoti_Status_t1473870168_m3287759645_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisWorldPos_t3236901198_m3876800169_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4096075109;
extern Il2CppCodeGenString* _stringLiteral2507853256;
extern Il2CppCodeGenString* _stringLiteral4153955660;
extern Il2CppCodeGenString* _stringLiteral1705900173;
extern Il2CppCodeGenString* _stringLiteral4131547028;
extern Il2CppCodeGenString* _stringLiteral721376978;
extern Il2CppCodeGenString* _stringLiteral530296524;
extern Il2CppCodeGenString* _stringLiteral1539644822;
extern Il2CppCodeGenString* _stringLiteral3885077073;
extern Il2CppCodeGenString* _stringLiteral2296104868;
extern Il2CppCodeGenString* _stringLiteral949617195;
extern Il2CppCodeGenString* _stringLiteral3544109420;
extern Il2CppCodeGenString* _stringLiteral3193648119;
extern const uint32_t Goti_Status_OnTriggerEnter_m3436573705_MetadataUsageId;
extern "C"  void Goti_Status_OnTriggerEnter_m3436573705 (Goti_Status_t1473870168 * __this, Collider_t3497673348 * ___col0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Goti_Status_OnTriggerEnter_m3436573705_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Goti_Status_t1473870168 * V_0 = NULL;
	int32_t V_1 = 0;
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		Collider_t3497673348 * L_0 = ___col0;
		NullCheck(L_0);
		String_t* L_1 = Component_get_tag_m357168014(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_1, _stringLiteral4096075109, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_030d;
		}
	}
	{
		String_t* L_3 = Object_get_name_m2079638459(__this, /*hidden argument*/NULL);
		Collider_t3497673348 * L_4 = ___col0;
		NullCheck(L_4);
		String_t* L_5 = Object_get_name_m2079638459(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m612901809(NULL /*static, unused*/, L_3, _stringLiteral2507853256, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		Collider_t3497673348 * L_7 = ___col0;
		NullCheck(L_7);
		Goti_Status_t1473870168 * L_8 = Component_GetComponent_TisGoti_Status_t1473870168_m3287759645(L_7, /*hidden argument*/Component_GetComponent_TisGoti_Status_t1473870168_m3287759645_MethodInfo_var);
		V_0 = L_8;
		Color_Manager_Script_t4053987423 * L_9 = __this->get_myManger_4();
		NullCheck(L_9);
		Turn_Manager_t2388373579 * L_10 = L_9->get_tm_6();
		NullCheck(L_10);
		Color_Manager_Script_t4053987423 * L_11 = L_10->get_currentTurn_4();
		Color_Manager_Script_t4053987423 * L_12 = __this->get_myManger_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_13 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0308;
		}
	}
	{
		Goti_Status_t1473870168 * L_14 = V_0;
		NullCheck(L_14);
		int32_t L_15 = L_14->get_myWorldPosition_43();
		V_1 = L_15;
		int32_t L_16 = V_1;
		int32_t L_17 = __this->get_worldPosToBeReached_33();
		if ((!(((uint32_t)L_16) == ((uint32_t)L_17))))
		{
			goto IL_02c0;
		}
	}
	{
		Goti_Status_t1473870168 * L_18 = V_0;
		NullCheck(L_18);
		Color_Manager_Script_t4053987423 * L_19 = L_18->get_myManger_4();
		Color_Manager_Script_t4053987423 * L_20 = __this->get_myManger_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_21 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_02a1;
		}
	}
	{
		Goti_Status_t1473870168 * L_22 = V_0;
		NullCheck(L_22);
		bool L_23 = L_22->get_isSafe_19();
		if (L_23)
		{
			goto IL_02a1;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_24 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)7));
		String_t* L_25 = Object_get_name_m2079638459(__this, /*hidden argument*/NULL);
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_25);
		ObjectU5BU5D_t3614634134* L_26 = L_24;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral4153955660);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)_stringLiteral4153955660);
		ObjectU5BU5D_t3614634134* L_27 = L_26;
		int32_t L_28 = __this->get_worldPosToBeReached_33();
		int32_t L_29 = L_28;
		Il2CppObject * L_30 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_29);
		NullCheck(L_27);
		ArrayElementTypeCheck (L_27, L_30);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_30);
		ObjectU5BU5D_t3614634134* L_31 = L_27;
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, _stringLiteral1705900173);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)_stringLiteral1705900173);
		ObjectU5BU5D_t3614634134* L_32 = L_31;
		Collider_t3497673348 * L_33 = ___col0;
		NullCheck(L_33);
		String_t* L_34 = Object_get_name_m2079638459(L_33, /*hidden argument*/NULL);
		NullCheck(L_32);
		ArrayElementTypeCheck (L_32, L_34);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_34);
		ObjectU5BU5D_t3614634134* L_35 = L_32;
		NullCheck(L_35);
		ArrayElementTypeCheck (L_35, _stringLiteral4131547028);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)_stringLiteral4131547028);
		ObjectU5BU5D_t3614634134* L_36 = L_35;
		int32_t L_37 = V_1;
		int32_t L_38 = L_37;
		Il2CppObject * L_39 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_38);
		NullCheck(L_36);
		ArrayElementTypeCheck (L_36, L_39);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)L_39);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_40 = String_Concat_m3881798623(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		__this->set_hasCollided_18((bool)1);
		int32_t L_41 = __this->get_worldPosToBeReached_33();
		if ((((int32_t)L_41) < ((int32_t)((int32_t)45))))
		{
			goto IL_00fd;
		}
	}
	{
		int32_t L_42 = __this->get_worldPosToBeReached_33();
		if ((((int32_t)L_42) <= ((int32_t)((int32_t)52))))
		{
			goto IL_0115;
		}
	}

IL_00fd:
	{
		int32_t L_43 = __this->get_worldPosToBeReached_33();
		if ((((int32_t)L_43) < ((int32_t)1)))
		{
			goto IL_013c;
		}
	}
	{
		int32_t L_44 = __this->get_worldPosToBeReached_33();
		if ((((int32_t)L_44) > ((int32_t)5)))
		{
			goto IL_013c;
		}
	}

IL_0115:
	{
		Color_Manager_Script_t4053987423 * L_45 = __this->get_myManger_4();
		NullCheck(L_45);
		Camera_t189460977 * L_46 = L_45->get_greenLane_19();
		NullCheck(L_46);
		Behaviour_set_enabled_m1796096907(L_46, (bool)1, /*hidden argument*/NULL);
		Color_Manager_Script_t4053987423 * L_47 = __this->get_myManger_4();
		NullCheck(L_47);
		Camera_t189460977 * L_48 = L_47->get_greenLane_19();
		__this->set_myTargetCam_50(L_48);
		goto IL_01f9;
	}

IL_013c:
	{
		int32_t L_49 = __this->get_worldPosToBeReached_33();
		if ((((int32_t)L_49) < ((int32_t)6)))
		{
			goto IL_017c;
		}
	}
	{
		int32_t L_50 = __this->get_worldPosToBeReached_33();
		if ((((int32_t)L_50) > ((int32_t)((int32_t)18))))
		{
			goto IL_017c;
		}
	}
	{
		Color_Manager_Script_t4053987423 * L_51 = __this->get_myManger_4();
		NullCheck(L_51);
		Camera_t189460977 * L_52 = L_51->get_redLane_20();
		NullCheck(L_52);
		Behaviour_set_enabled_m1796096907(L_52, (bool)1, /*hidden argument*/NULL);
		Color_Manager_Script_t4053987423 * L_53 = __this->get_myManger_4();
		NullCheck(L_53);
		Camera_t189460977 * L_54 = L_53->get_redLane_20();
		__this->set_myTargetCam_50(L_54);
		goto IL_01f9;
	}

IL_017c:
	{
		int32_t L_55 = __this->get_worldPosToBeReached_33();
		if ((((int32_t)L_55) < ((int32_t)((int32_t)19))))
		{
			goto IL_01bd;
		}
	}
	{
		int32_t L_56 = __this->get_worldPosToBeReached_33();
		if ((((int32_t)L_56) > ((int32_t)((int32_t)31))))
		{
			goto IL_01bd;
		}
	}
	{
		Color_Manager_Script_t4053987423 * L_57 = __this->get_myManger_4();
		NullCheck(L_57);
		Camera_t189460977 * L_58 = L_57->get_blueLane_22();
		NullCheck(L_58);
		Behaviour_set_enabled_m1796096907(L_58, (bool)1, /*hidden argument*/NULL);
		Color_Manager_Script_t4053987423 * L_59 = __this->get_myManger_4();
		NullCheck(L_59);
		Camera_t189460977 * L_60 = L_59->get_blueLane_22();
		__this->set_myTargetCam_50(L_60);
		goto IL_01f9;
	}

IL_01bd:
	{
		int32_t L_61 = __this->get_worldPosToBeReached_33();
		if ((((int32_t)L_61) < ((int32_t)((int32_t)32))))
		{
			goto IL_01f9;
		}
	}
	{
		int32_t L_62 = __this->get_worldPosToBeReached_33();
		if ((((int32_t)L_62) > ((int32_t)((int32_t)44))))
		{
			goto IL_01f9;
		}
	}
	{
		Color_Manager_Script_t4053987423 * L_63 = __this->get_myManger_4();
		NullCheck(L_63);
		Camera_t189460977 * L_64 = L_63->get_yellowLane_21();
		NullCheck(L_64);
		Behaviour_set_enabled_m1796096907(L_64, (bool)1, /*hidden argument*/NULL);
		Color_Manager_Script_t4053987423 * L_65 = __this->get_myManger_4();
		NullCheck(L_65);
		Camera_t189460977 * L_66 = L_65->get_greenLane_19();
		__this->set_myTargetCam_50(L_66);
	}

IL_01f9:
	{
		Color_Manager_Script_t4053987423 * L_67 = __this->get_myManger_4();
		NullCheck(L_67);
		Color_Manager_Script_EnableMainCamera_m3356488996(L_67, (bool)0, /*hidden argument*/NULL);
		Vector3_t2243707580 * L_68 = __this->get_address_of_initCPos_23();
		float L_69 = L_68->get_x_1();
		float L_70 = __this->get_attackOffX_30();
		Vector3_t2243707580 * L_71 = __this->get_address_of_initCPos_23();
		float L_72 = L_71->get_y_2();
		Vector3_t2243707580 * L_73 = __this->get_address_of_initCPos_23();
		float L_74 = L_73->get_z_3();
		float L_75 = __this->get_attackOffZ_31();
		Vector3__ctor_m2638739322((&V_2), ((float)((float)L_69+(float)L_70)), L_72, ((float)((float)L_74+(float)L_75)), /*hidden argument*/NULL);
		Transform_t3275118058 * L_76 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_76);
		Transform_t3275118058 * L_77 = Transform_GetChild_m3838588184(L_76, 0, /*hidden argument*/NULL);
		Vector3_t2243707580  L_78 = V_2;
		NullCheck(L_77);
		Transform_set_localPosition_m1026930133(L_77, L_78, /*hidden argument*/NULL);
		Animator_t69676727 * L_79 = __this->get_anime_20();
		NullCheck(L_79);
		Animator_Play_m4142158565(L_79, _stringLiteral721376978, (-1), /*hidden argument*/NULL);
		__this->set_attacking_45((bool)1);
		Color_Manager_Script_t4053987423 * L_80 = __this->get_myManger_4();
		NullCheck(L_80);
		AudioSource_t1135106623 * L_81 = L_80->get_mukkeKiAwaaz_18();
		NullCheck(L_81);
		AudioSource_PlayDelayed_m1283429031(L_81, (0.5f), /*hidden argument*/NULL);
		Goti_Status_t1473870168 * L_82 = V_0;
		NullCheck(L_82);
		Il2CppObject * L_83 = Goti_Status_CutGoti_m1406000177(L_82, (1.0f), /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_83, /*hidden argument*/NULL);
		MonoBehaviour_Invoke_m666563676(__this, _stringLiteral530296524, (1.0f), /*hidden argument*/NULL);
		goto IL_02bb;
	}

IL_02a1:
	{
		Goti_Status_t1473870168 * L_84 = V_0;
		NullCheck(L_84);
		String_t* L_85 = Object_get_name_m2079638459(L_84, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_86 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral1539644822, L_85, _stringLiteral3885077073, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_86, /*hidden argument*/NULL);
	}

IL_02bb:
	{
		goto IL_0308;
	}

IL_02c0:
	{
		ObjectU5BU5D_t3614634134* L_87 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_87);
		ArrayElementTypeCheck (L_87, _stringLiteral1539644822);
		(L_87)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1539644822);
		ObjectU5BU5D_t3614634134* L_88 = L_87;
		Goti_Status_t1473870168 * L_89 = V_0;
		NullCheck(L_89);
		String_t* L_90 = Object_get_name_m2079638459(L_89, /*hidden argument*/NULL);
		NullCheck(L_88);
		ArrayElementTypeCheck (L_88, L_90);
		(L_88)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_90);
		ObjectU5BU5D_t3614634134* L_91 = L_88;
		NullCheck(L_91);
		ArrayElementTypeCheck (L_91, _stringLiteral2296104868);
		(L_91)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral2296104868);
		ObjectU5BU5D_t3614634134* L_92 = L_91;
		int32_t L_93 = __this->get_worldPosToBeReached_33();
		int32_t L_94 = L_93;
		Il2CppObject * L_95 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_94);
		NullCheck(L_92);
		ArrayElementTypeCheck (L_92, L_95);
		(L_92)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_95);
		ObjectU5BU5D_t3614634134* L_96 = L_92;
		NullCheck(L_96);
		ArrayElementTypeCheck (L_96, _stringLiteral949617195);
		(L_96)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral949617195);
		ObjectU5BU5D_t3614634134* L_97 = L_96;
		int32_t L_98 = V_1;
		int32_t L_99 = L_98;
		Il2CppObject * L_100 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_99);
		NullCheck(L_97);
		ArrayElementTypeCheck (L_97, L_100);
		(L_97)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_100);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_101 = String_Concat_m3881798623(NULL /*static, unused*/, L_97, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_101, /*hidden argument*/NULL);
	}

IL_0308:
	{
		goto IL_03bd;
	}

IL_030d:
	{
		Collider_t3497673348 * L_102 = ___col0;
		NullCheck(L_102);
		String_t* L_103 = Component_get_tag_m357168014(L_102, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_104 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_103, _stringLiteral3544109420, /*hidden argument*/NULL);
		if (!L_104)
		{
			goto IL_03bd;
		}
	}
	{
		Transform_t3275118058 * L_105 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_106 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_106);
		Vector3_t2243707580  L_107 = Transform_get_eulerAngles_m4066505159(L_106, /*hidden argument*/NULL);
		V_3 = L_107;
		float L_108 = (&V_3)->get_x_1();
		Collider_t3497673348 * L_109 = ___col0;
		NullCheck(L_109);
		Transform_t3275118058 * L_110 = Component_get_transform_m2697483695(L_109, /*hidden argument*/NULL);
		NullCheck(L_110);
		Vector3_t2243707580  L_111 = Transform_get_eulerAngles_m4066505159(L_110, /*hidden argument*/NULL);
		V_4 = L_111;
		float L_112 = (&V_4)->get_y_2();
		Transform_t3275118058 * L_113 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_113);
		Vector3_t2243707580  L_114 = Transform_get_eulerAngles_m4066505159(L_113, /*hidden argument*/NULL);
		V_5 = L_114;
		float L_115 = (&V_5)->get_z_3();
		Vector3_t2243707580  L_116;
		memset(&L_116, 0, sizeof(L_116));
		Vector3__ctor_m2638739322(&L_116, L_108, L_112, L_115, /*hidden argument*/NULL);
		NullCheck(L_105);
		Transform_set_eulerAngles_m2881310872(L_105, L_116, /*hidden argument*/NULL);
		Collider_t3497673348 * L_117 = ___col0;
		NullCheck(L_117);
		WorldPos_t3236901198 * L_118 = Component_GetComponent_TisWorldPos_t3236901198_m3876800169(L_117, /*hidden argument*/Component_GetComponent_TisWorldPos_t3236901198_m3876800169_MethodInfo_var);
		NullCheck(L_118);
		int32_t L_119 = L_118->get_worldPos_2();
		__this->set_myWorldPosition_43(L_119);
		int32_t L_120 = __this->get_myWorldPosition_43();
		int32_t L_121 = L_120;
		Il2CppObject * L_122 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_121);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_123 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral3193648119, L_122, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_123, /*hidden argument*/NULL);
		bool L_124 = __this->get_startRunning_9();
		if (!L_124)
		{
			goto IL_03bd;
		}
	}
	{
		Game_Manager_t4105539090 * L_125 = Game_Manager_get__gminstance_m2308798245(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_125);
		bool L_126 = L_125->get_isSoundOn_5();
		if (!L_126)
		{
			goto IL_03bd;
		}
	}
	{
		AudioSource_t1135106623 * L_127 = __this->get_asWalking_47();
		NullCheck(L_127);
		AudioSource_Play_m353744792(L_127, /*hidden argument*/NULL);
	}

IL_03bd:
	{
		return;
	}
}
// System.Void Goti_Status::AfterAttack()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPow_Script_t2091305652_m2549334947_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1502843828;
extern const uint32_t Goti_Status_AfterAttack_m2009128161_MetadataUsageId;
extern "C"  void Goti_Status_AfterAttack_m2009128161 (Goti_Status_t1473870168 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Goti_Status_AfterAttack_m2009128161_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	GameObject_t1756533147 * V_4 = NULL;
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = Transform_get_position_m1104419803(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		float L_2 = (&V_1)->get_x_1();
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t2243707580  L_4 = Transform_get_position_m1104419803(L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		float L_5 = (&V_2)->get_y_2();
		Transform_t3275118058 * L_6 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		V_3 = L_7;
		float L_8 = (&V_3)->get_z_3();
		Vector3__ctor_m2638739322((&V_0), L_2, ((float)((float)L_5+(float)(2.0f))), L_8, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_9 = __this->get_powObj_48();
		Vector3_t2243707580  L_10 = V_0;
		Quaternion_t4030073918  L_11 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_12 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_9, L_10, L_11, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		V_4 = L_12;
		GameObject_t1756533147 * L_13 = V_4;
		NullCheck(L_13);
		Pow_Script_t2091305652 * L_14 = GameObject_GetComponent_TisPow_Script_t2091305652_m2549334947(L_13, /*hidden argument*/GameObject_GetComponent_TisPow_Script_t2091305652_m2549334947_MethodInfo_var);
		Camera_t189460977 * L_15 = __this->get_myTargetCam_50();
		NullCheck(L_14);
		L_14->set_targetCam_3(L_15);
		MonoBehaviour_Invoke_m666563676(__this, _stringLiteral1502843828, (1.0f), /*hidden argument*/NULL);
		__this->set_startRunning_9((bool)0);
		__this->set_attacking_45((bool)0);
		Transform_t3275118058 * L_16 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580 * L_17 = __this->get_address_of_finalPos_7();
		float L_18 = L_17->get_x_1();
		Vector3_t2243707580 * L_19 = __this->get_address_of_finalPos_7();
		float L_20 = L_19->get_y_2();
		float L_21 = __this->get_hieght_11();
		Vector3_t2243707580 * L_22 = __this->get_address_of_finalPos_7();
		float L_23 = L_22->get_z_3();
		Vector3_t2243707580  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Vector3__ctor_m2638739322(&L_24, L_18, ((float)((float)L_20+(float)L_21)), L_23, /*hidden argument*/NULL);
		NullCheck(L_16);
		Transform_set_position_m2469242620(L_16, L_24, /*hidden argument*/NULL);
		Color_Manager_Script_t4053987423 * L_25 = __this->get_myManger_4();
		NullCheck(L_25);
		Turn_Manager_t2388373579 * L_26 = L_25->get_tm_6();
		NullCheck(L_26);
		Turn_Manager_RepeatTurn_m1002454660(L_26, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Goti_Status::NextTurn()
extern "C"  void Goti_Status_NextTurn_m3875928647 (Goti_Status_t1473870168 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_performedNext_42();
		if (L_0)
		{
			goto IL_002d;
		}
	}
	{
		__this->set_performedNext_42((bool)1);
		bool L_1 = __this->get_hasCollided_18();
		if (L_1)
		{
			goto IL_002d;
		}
	}
	{
		Color_Manager_Script_t4053987423 * L_2 = __this->get_myManger_4();
		NullCheck(L_2);
		Turn_Manager_t2388373579 * L_3 = L_2->get_tm_6();
		NullCheck(L_3);
		Turn_Manager_PerformNextTurn_m2187029365(L_3, /*hidden argument*/NULL);
	}

IL_002d:
	{
		return;
	}
}
// System.Void Goti_Status::RepeatTurn()
extern "C"  void Goti_Status_RepeatTurn_m1200754475 (Goti_Status_t1473870168 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_hasCollided_18();
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		Color_Manager_Script_t4053987423 * L_1 = __this->get_myManger_4();
		NullCheck(L_1);
		Turn_Manager_t2388373579 * L_2 = L_1->get_tm_6();
		NullCheck(L_2);
		Turn_Manager_RepeatTurn_m1002454660(L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// System.Void Goti_Status::FlagLogics()
extern const MethodInfo* Component_GetComponent_TisWorldPos_t3236901198_m3876800169_MethodInfo_var;
extern const uint32_t Goti_Status_FlagLogics_m2683927470_MetadataUsageId;
extern "C"  void Goti_Status_FlagLogics_m2683927470 (Goti_Status_t1473870168 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Goti_Status_FlagLogics_m2683927470_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Goti_Status_t1473870168 * V_2 = NULL;
	Goti_StatusU5BU5D_t3854859849* V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	Transform_t3275118058 * V_6 = NULL;
	TransformU5BU5D_t3764228911* V_7 = NULL;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	Goti_Status_t1473870168 * V_10 = NULL;
	Goti_StatusU5BU5D_t3854859849* V_11 = NULL;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	Vector2_t2243707579  V_14;
	memset(&V_14, 0, sizeof(V_14));
	Vector3_t2243707580  V_15;
	memset(&V_15, 0, sizeof(V_15));
	Vector3_t2243707580  V_16;
	memset(&V_16, 0, sizeof(V_16));
	Vector2_t2243707579  V_17;
	memset(&V_17, 0, sizeof(V_17));
	Vector3_t2243707580  V_18;
	memset(&V_18, 0, sizeof(V_18));
	Vector3_t2243707580  V_19;
	memset(&V_19, 0, sizeof(V_19));
	Goti_Status_t1473870168 * V_20 = NULL;
	Goti_StatusU5BU5D_t3854859849* V_21 = NULL;
	int32_t V_22 = 0;
	int32_t V_23 = 0;
	Goti_Status_t1473870168 * V_24 = NULL;
	Goti_StatusU5BU5D_t3854859849* V_25 = NULL;
	int32_t V_26 = 0;
	int32_t V_27 = 0;
	Goti_Status_t1473870168 * G_B53_0 = NULL;
	Goti_Status_t1473870168 * G_B50_0 = NULL;
	Goti_Status_t1473870168 * G_B51_0 = NULL;
	Goti_Status_t1473870168 * G_B52_0 = NULL;
	int32_t G_B54_0 = 0;
	Goti_Status_t1473870168 * G_B54_1 = NULL;
	{
		int32_t L_0 = __this->get_myWorldPosition_43();
		Color_Manager_Script_t4053987423 * L_1 = __this->get_myManger_4();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_diceInput_7();
		__this->set_worldPosToBeReached_33(((int32_t)((int32_t)L_0+(int32_t)L_2)));
		int32_t L_3 = __this->get_worldPosToBeReached_33();
		V_0 = ((int32_t)((int32_t)L_3-(int32_t)((int32_t)52)));
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0030;
		}
	}
	{
		int32_t L_5 = V_0;
		__this->set_worldPosToBeReached_33(L_5);
	}

IL_0030:
	{
		V_1 = 0;
		goto IL_03b8;
	}

IL_0037:
	{
		int32_t L_6 = __this->get_myState_2();
		if (L_6)
		{
			goto IL_03b4;
		}
	}
	{
		Enemy_AI_t244483799 * L_7 = __this->get_eai_32();
		NullCheck(L_7);
		Goti_StatusU5BU5D_t3854859849* L_8 = L_7->get_enemyPieces_6();
		V_3 = L_8;
		V_4 = 0;
		goto IL_0094;
	}

IL_0056:
	{
		Goti_StatusU5BU5D_t3854859849* L_9 = V_3;
		int32_t L_10 = V_4;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		Goti_Status_t1473870168 * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		V_2 = L_12;
		Goti_Status_t1473870168 * L_13 = V_2;
		NullCheck(L_13);
		bool L_14 = L_13->get_isSafe_19();
		if (L_14)
		{
			goto IL_008e;
		}
	}
	{
		Goti_Status_t1473870168 * L_15 = V_2;
		NullCheck(L_15);
		int32_t L_16 = L_15->get_myWorldPosition_43();
		V_5 = L_16;
		int32_t L_17 = V_5;
		int32_t L_18 = __this->get_worldPosToBeReached_33();
		if ((!(((uint32_t)L_17) == ((uint32_t)L_18))))
		{
			goto IL_0087;
		}
	}
	{
		__this->set_canCutSomeOne_34((bool)1);
		goto IL_009e;
	}

IL_0087:
	{
		__this->set_canCutSomeOne_34((bool)0);
	}

IL_008e:
	{
		int32_t L_19 = V_4;
		V_4 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_0094:
	{
		int32_t L_20 = V_4;
		Goti_StatusU5BU5D_t3854859849* L_21 = V_3;
		NullCheck(L_21);
		if ((((int32_t)L_20) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_21)->max_length)))))))
		{
			goto IL_0056;
		}
	}

IL_009e:
	{
		Color_Manager_Script_t4053987423 * L_22 = __this->get_myManger_4();
		NullCheck(L_22);
		TransformU5BU5D_t3764228911* L_23 = L_22->get_safePoints_11();
		V_7 = L_23;
		V_8 = 0;
		goto IL_011c;
	}

IL_00b3:
	{
		TransformU5BU5D_t3764228911* L_24 = V_7;
		int32_t L_25 = V_8;
		NullCheck(L_24);
		int32_t L_26 = L_25;
		Transform_t3275118058 * L_27 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		V_6 = L_27;
		int32_t L_28 = __this->get_myWorldPosition_43();
		if ((((int32_t)L_28) < ((int32_t)((int32_t)21))))
		{
			goto IL_00e0;
		}
	}
	{
		int32_t L_29 = __this->get_myWorldPosition_43();
		if ((((int32_t)L_29) >= ((int32_t)((int32_t)26))))
		{
			goto IL_00e0;
		}
	}
	{
		__this->set_becomingSafe_35((bool)0);
		goto IL_0116;
	}

IL_00e0:
	{
		Transform_t3275118058 * L_30 = V_6;
		NullCheck(L_30);
		WorldPos_t3236901198 * L_31 = Component_GetComponent_TisWorldPos_t3236901198_m3876800169(L_30, /*hidden argument*/Component_GetComponent_TisWorldPos_t3236901198_m3876800169_MethodInfo_var);
		NullCheck(L_31);
		int32_t L_32 = L_31->get_worldPos_2();
		V_9 = L_32;
		int32_t L_33 = V_9;
		int32_t L_34 = __this->get_worldPosToBeReached_33();
		if ((!(((uint32_t)L_33) == ((uint32_t)L_34))))
		{
			goto IL_010f;
		}
	}
	{
		Transform_t3275118058 * L_35 = V_6;
		__this->set_safePoint_44(L_35);
		__this->set_becomingSafe_35((bool)1);
		goto IL_0127;
	}

IL_010f:
	{
		__this->set_becomingSafe_35((bool)0);
	}

IL_0116:
	{
		int32_t L_36 = V_8;
		V_8 = ((int32_t)((int32_t)L_36+(int32_t)1));
	}

IL_011c:
	{
		int32_t L_37 = V_8;
		TransformU5BU5D_t3764228911* L_38 = V_7;
		NullCheck(L_38);
		if ((((int32_t)L_37) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_38)->max_length)))))))
		{
			goto IL_00b3;
		}
	}

IL_0127:
	{
		Enemy_AI_t244483799 * L_39 = __this->get_eai_32();
		NullCheck(L_39);
		Goti_StatusU5BU5D_t3854859849* L_40 = L_39->get_enemyPieces_6();
		V_11 = L_40;
		V_12 = 0;
		goto IL_01a4;
	}

IL_013c:
	{
		Goti_StatusU5BU5D_t3854859849* L_41 = V_11;
		int32_t L_42 = V_12;
		NullCheck(L_41);
		int32_t L_43 = L_42;
		Goti_Status_t1473870168 * L_44 = (L_41)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		V_10 = L_44;
		Goti_Status_t1473870168 * L_45 = V_10;
		NullCheck(L_45);
		int32_t L_46 = L_45->get_myWorldPosition_43();
		V_13 = L_46;
		int32_t L_47 = V_13;
		int32_t L_48 = __this->get_worldPosToBeReached_33();
		if ((((int32_t)L_47) <= ((int32_t)L_48)))
		{
			goto IL_0197;
		}
	}
	{
		int32_t L_49 = V_13;
		int32_t L_50 = __this->get_worldPosToBeReached_33();
		if ((((int32_t)((int32_t)((int32_t)L_49-(int32_t)L_50))) >= ((int32_t)6)))
		{
			goto IL_0192;
		}
	}
	{
		int32_t L_51 = V_13;
		int32_t L_52 = __this->get_worldPosToBeReached_33();
		if ((((int32_t)((int32_t)((int32_t)L_51-(int32_t)L_52))) < ((int32_t)0)))
		{
			goto IL_0192;
		}
	}
	{
		__this->set_canChase_37((bool)1);
		int32_t L_53 = V_13;
		int32_t L_54 = __this->get_worldPosToBeReached_33();
		__this->set_chaseGap_41(((int32_t)((int32_t)L_53-(int32_t)L_54)));
		goto IL_01af;
	}

IL_0192:
	{
		goto IL_019e;
	}

IL_0197:
	{
		__this->set_canChase_37((bool)0);
	}

IL_019e:
	{
		int32_t L_55 = V_12;
		V_12 = ((int32_t)((int32_t)L_55+(int32_t)1));
	}

IL_01a4:
	{
		int32_t L_56 = V_12;
		Goti_StatusU5BU5D_t3854859849* L_57 = V_11;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_013c;
		}
	}

IL_01af:
	{
		Color_Manager_Script_t4053987423 * L_58 = __this->get_myManger_4();
		NullCheck(L_58);
		TransformU5BU5D_t3764228911* L_59 = L_58->get_myPath_10();
		Color_Manager_Script_t4053987423 * L_60 = __this->get_myManger_4();
		NullCheck(L_60);
		TransformU5BU5D_t3764228911* L_61 = L_60->get_myPath_10();
		NullCheck(L_61);
		NullCheck(L_59);
		int32_t L_62 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_61)->max_length))))-(int32_t)1));
		Transform_t3275118058 * L_63 = (L_59)->GetAt(static_cast<il2cpp_array_size_t>(L_62));
		NullCheck(L_63);
		Vector3_t2243707580  L_64 = Transform_get_position_m1104419803(L_63, /*hidden argument*/NULL);
		V_15 = L_64;
		float L_65 = (&V_15)->get_x_1();
		Color_Manager_Script_t4053987423 * L_66 = __this->get_myManger_4();
		NullCheck(L_66);
		TransformU5BU5D_t3764228911* L_67 = L_66->get_myPath_10();
		Color_Manager_Script_t4053987423 * L_68 = __this->get_myManger_4();
		NullCheck(L_68);
		TransformU5BU5D_t3764228911* L_69 = L_68->get_myPath_10();
		NullCheck(L_69);
		NullCheck(L_67);
		int32_t L_70 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_69)->max_length))))-(int32_t)1));
		Transform_t3275118058 * L_71 = (L_67)->GetAt(static_cast<il2cpp_array_size_t>(L_70));
		NullCheck(L_71);
		Vector3_t2243707580  L_72 = Transform_get_position_m1104419803(L_71, /*hidden argument*/NULL);
		V_16 = L_72;
		float L_73 = (&V_16)->get_z_3();
		Vector2__ctor_m3067419446((&V_14), L_65, L_73, /*hidden argument*/NULL);
		Color_Manager_Script_t4053987423 * L_74 = __this->get_myManger_4();
		NullCheck(L_74);
		TransformU5BU5D_t3764228911* L_75 = L_74->get_myPath_10();
		int32_t L_76 = __this->get_curIndexPos_8();
		Color_Manager_Script_t4053987423 * L_77 = __this->get_myManger_4();
		NullCheck(L_77);
		int32_t L_78 = L_77->get_diceInput_7();
		NullCheck(L_75);
		int32_t L_79 = ((int32_t)((int32_t)L_76+(int32_t)L_78));
		Transform_t3275118058 * L_80 = (L_75)->GetAt(static_cast<il2cpp_array_size_t>(L_79));
		NullCheck(L_80);
		Vector3_t2243707580  L_81 = Transform_get_position_m1104419803(L_80, /*hidden argument*/NULL);
		V_18 = L_81;
		float L_82 = (&V_18)->get_x_1();
		Color_Manager_Script_t4053987423 * L_83 = __this->get_myManger_4();
		NullCheck(L_83);
		TransformU5BU5D_t3764228911* L_84 = L_83->get_myPath_10();
		int32_t L_85 = __this->get_curIndexPos_8();
		Color_Manager_Script_t4053987423 * L_86 = __this->get_myManger_4();
		NullCheck(L_86);
		int32_t L_87 = L_86->get_diceInput_7();
		NullCheck(L_84);
		int32_t L_88 = ((int32_t)((int32_t)L_85+(int32_t)L_87));
		Transform_t3275118058 * L_89 = (L_84)->GetAt(static_cast<il2cpp_array_size_t>(L_88));
		NullCheck(L_89);
		Vector3_t2243707580  L_90 = Transform_get_position_m1104419803(L_89, /*hidden argument*/NULL);
		V_19 = L_90;
		float L_91 = (&V_19)->get_z_3();
		Vector2__ctor_m3067419446((&V_17), L_82, L_91, /*hidden argument*/NULL);
		Vector2_t2243707579  L_92 = V_17;
		Vector2_t2243707579  L_93 = V_14;
		bool L_94 = Vector2_op_Equality_m4168854394(NULL /*static, unused*/, L_92, L_93, /*hidden argument*/NULL);
		if (!L_94)
		{
			goto IL_0281;
		}
	}
	{
		__this->set_winning_36((bool)1);
		goto IL_03bf;
	}

IL_0281:
	{
		__this->set_winning_36((bool)0);
		Enemy_AI_t244483799 * L_95 = __this->get_eai_32();
		NullCheck(L_95);
		Goti_StatusU5BU5D_t3854859849* L_96 = L_95->get_enemyPieces_6();
		V_21 = L_96;
		V_22 = 0;
		goto IL_0308;
	}

IL_029d:
	{
		Goti_StatusU5BU5D_t3854859849* L_97 = V_21;
		int32_t L_98 = V_22;
		NullCheck(L_97);
		int32_t L_99 = L_98;
		Goti_Status_t1473870168 * L_100 = (L_97)->GetAt(static_cast<il2cpp_array_size_t>(L_99));
		V_20 = L_100;
		Goti_Status_t1473870168 * L_101 = V_20;
		NullCheck(L_101);
		int32_t L_102 = L_101->get_myWorldPosition_43();
		V_23 = L_102;
		int32_t L_103 = V_23;
		int32_t L_104 = __this->get_worldPosToBeReached_33();
		if ((((int32_t)L_103) > ((int32_t)L_104)))
		{
			goto IL_02fb;
		}
	}
	{
		int32_t L_105 = __this->get_worldPosToBeReached_33();
		int32_t L_106 = V_23;
		if ((((int32_t)((int32_t)((int32_t)L_105-(int32_t)L_106))) > ((int32_t)5)))
		{
			goto IL_02ef;
		}
	}
	{
		int32_t L_107 = __this->get_worldPosToBeReached_33();
		int32_t L_108 = V_23;
		if ((((int32_t)((int32_t)((int32_t)L_107-(int32_t)L_108))) < ((int32_t)0)))
		{
			goto IL_02ef;
		}
	}
	{
		bool L_109 = __this->get_isSafe_19();
		if (!L_109)
		{
			goto IL_02ef;
		}
	}
	{
		__this->set_isChaser_40((bool)1);
		goto IL_0313;
	}

IL_02ef:
	{
		__this->set_isChaser_40((bool)0);
		goto IL_0302;
	}

IL_02fb:
	{
		__this->set_isChaser_40((bool)0);
	}

IL_0302:
	{
		int32_t L_110 = V_22;
		V_22 = ((int32_t)((int32_t)L_110+(int32_t)1));
	}

IL_0308:
	{
		int32_t L_111 = V_22;
		Goti_StatusU5BU5D_t3854859849* L_112 = V_21;
		NullCheck(L_112);
		if ((((int32_t)L_111) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_112)->max_length)))))))
		{
			goto IL_029d;
		}
	}

IL_0313:
	{
		Enemy_AI_t244483799 * L_113 = __this->get_eai_32();
		NullCheck(L_113);
		Goti_StatusU5BU5D_t3854859849* L_114 = L_113->get_enemyPieces_6();
		V_25 = L_114;
		V_26 = 0;
		goto IL_0376;
	}

IL_0328:
	{
		Goti_StatusU5BU5D_t3854859849* L_115 = V_25;
		int32_t L_116 = V_26;
		NullCheck(L_115);
		int32_t L_117 = L_116;
		Goti_Status_t1473870168 * L_118 = (L_115)->GetAt(static_cast<il2cpp_array_size_t>(L_117));
		V_24 = L_118;
		Goti_Status_t1473870168 * L_119 = V_24;
		NullCheck(L_119);
		int32_t L_120 = L_119->get_myWorldPosition_43();
		V_27 = L_120;
		int32_t L_121 = __this->get_worldPosToBeReached_33();
		int32_t L_122 = V_27;
		if ((((int32_t)L_121) <= ((int32_t)L_122)))
		{
			goto IL_035c;
		}
	}
	{
		bool L_123 = __this->get_becomingSafe_35();
		if (L_123)
		{
			goto IL_035c;
		}
	}
	{
		__this->set_mightGetAttacked_38((bool)1);
		goto IL_0381;
	}

IL_035c:
	{
		int32_t L_124 = __this->get_worldPosToBeReached_33();
		int32_t L_125 = V_27;
		if ((((int32_t)L_124) >= ((int32_t)L_125)))
		{
			goto IL_0370;
		}
	}
	{
		__this->set_mightGetAttacked_38((bool)0);
	}

IL_0370:
	{
		int32_t L_126 = V_26;
		V_26 = ((int32_t)((int32_t)L_126+(int32_t)1));
	}

IL_0376:
	{
		int32_t L_127 = V_26;
		Goti_StatusU5BU5D_t3854859849* L_128 = V_25;
		NullCheck(L_128);
		if ((((int32_t)L_127) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_128)->max_length)))))))
		{
			goto IL_0328;
		}
	}

IL_0381:
	{
		bool L_129 = __this->get_canCutSomeOne_34();
		G_B50_0 = __this;
		if (L_129)
		{
			G_B53_0 = __this;
			goto IL_03ae;
		}
	}
	{
		bool L_130 = __this->get_becomingSafe_35();
		G_B51_0 = G_B50_0;
		if (L_130)
		{
			G_B53_0 = G_B50_0;
			goto IL_03ae;
		}
	}
	{
		bool L_131 = __this->get_canChase_37();
		G_B52_0 = G_B51_0;
		if (L_131)
		{
			G_B53_0 = G_B51_0;
			goto IL_03ae;
		}
	}
	{
		bool L_132 = __this->get_winning_36();
		G_B54_0 = ((((int32_t)L_132) == ((int32_t)0))? 1 : 0);
		G_B54_1 = G_B52_0;
		goto IL_03af;
	}

IL_03ae:
	{
		G_B54_0 = 0;
		G_B54_1 = G_B53_0;
	}

IL_03af:
	{
		NullCheck(G_B54_1);
		G_B54_1->set_doingNothing_39((bool)G_B54_0);
	}

IL_03b4:
	{
		int32_t L_133 = V_1;
		V_1 = ((int32_t)((int32_t)L_133+(int32_t)1));
	}

IL_03b8:
	{
		int32_t L_134 = V_1;
		if ((((int32_t)L_134) <= ((int32_t)3)))
		{
			goto IL_0037;
		}
	}

IL_03bf:
	{
		return;
	}
}
// System.Void Goti_Status::ResetAllCameras()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral749014627;
extern const uint32_t Goti_Status_ResetAllCameras_m1082687783_MetadataUsageId;
extern "C"  void Goti_Status_ResetAllCameras_m1082687783 (Goti_Status_t1473870168 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Goti_Status_ResetAllCameras_m1082687783_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Color_Manager_Script_t4053987423 * L_0 = __this->get_myManger_4();
		NullCheck(L_0);
		Color_Manager_Script_EnableMainCamera_m3356488996(L_0, (bool)1, /*hidden argument*/NULL);
		Camera_t189460977 * L_1 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_myTargetCam_50(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral749014627, /*hidden argument*/NULL);
		Color_Manager_Script_t4053987423 * L_2 = __this->get_myManger_4();
		NullCheck(L_2);
		Color_Manager_Script_DisableAllCameras_m2295878905(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Goti_Status/<CutGoti>c__Iterator0::.ctor()
extern "C"  void U3CCutGotiU3Ec__Iterator0__ctor_m3139146000 (U3CCutGotiU3Ec__Iterator0_t377100891 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Goti_Status/<CutGoti>c__Iterator0::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral339800437;
extern const uint32_t U3CCutGotiU3Ec__Iterator0_MoveNext_m943435336_MetadataUsageId;
extern "C"  bool U3CCutGotiU3Ec__Iterator0_MoveNext_m943435336 (U3CCutGotiU3Ec__Iterator0_t377100891 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCutGotiU3Ec__Iterator0_MoveNext_m943435336_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0068;
		}
	}
	{
		goto IL_00a2;
	}

IL_0021:
	{
		Goti_Status_t1473870168 * L_2 = __this->get_U24this_1();
		NullCheck(L_2);
		Animator_t69676727 * L_3 = L_2->get_anime_20();
		NullCheck(L_3);
		Animator_Play_m4142158565(L_3, _stringLiteral339800437, (-1), /*hidden argument*/NULL);
		Goti_Status_t1473870168 * L_4 = __this->get_U24this_1();
		NullCheck(L_4);
		L_4->set_gettingHit_46((bool)1);
		float L_5 = __this->get_f_0();
		WaitForSeconds_t3839502067 * L_6 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_6, L_5, /*hidden argument*/NULL);
		__this->set_U24current_2(L_6);
		bool L_7 = __this->get_U24disposing_3();
		if (L_7)
		{
			goto IL_0063;
		}
	}
	{
		__this->set_U24PC_4(1);
	}

IL_0063:
	{
		goto IL_00a4;
	}

IL_0068:
	{
		Goti_Status_t1473870168 * L_8 = __this->get_U24this_1();
		NullCheck(L_8);
		L_8->set_gettingHit_46((bool)0);
		Goti_Status_t1473870168 * L_9 = __this->get_U24this_1();
		NullCheck(L_9);
		Transform_t3275118058 * L_10 = Component_get_transform_m2697483695(L_9, /*hidden argument*/NULL);
		Goti_Status_t1473870168 * L_11 = __this->get_U24this_1();
		NullCheck(L_11);
		Vector3_t2243707580  L_12 = L_11->get_myStartPos_13();
		NullCheck(L_10);
		Transform_set_position_m2469242620(L_10, L_12, /*hidden argument*/NULL);
		Goti_Status_t1473870168 * L_13 = __this->get_U24this_1();
		NullCheck(L_13);
		L_13->set_myState_2(1);
		__this->set_U24PC_4((-1));
	}

IL_00a2:
	{
		return (bool)0;
	}

IL_00a4:
	{
		return (bool)1;
	}
}
// System.Object Goti_Status/<CutGoti>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCutGotiU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m889004950 (U3CCutGotiU3Ec__Iterator0_t377100891 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object Goti_Status/<CutGoti>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCutGotiU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m4227727422 (U3CCutGotiU3Ec__Iterator0_t377100891 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Void Goti_Status/<CutGoti>c__Iterator0::Dispose()
extern "C"  void U3CCutGotiU3Ec__Iterator0_Dispose_m1037140661 (U3CCutGotiU3Ec__Iterator0_t377100891 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_3((bool)1);
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void Goti_Status/<CutGoti>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCutGotiU3Ec__Iterator0_Reset_m1475454659_MetadataUsageId;
extern "C"  void U3CCutGotiU3Ec__Iterator0_Reset_m1475454659 (U3CCutGotiU3Ec__Iterator0_t377100891 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCutGotiU3Ec__Iterator0_Reset_m1475454659_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Main_Menu_Manager::.ctor()
extern "C"  void Main_Menu_Manager__ctor_m2204695144 (Main_Menu_Manager_t1163327745 * __this, const MethodInfo* method)
{
	{
		__this->set_isSoundOn_8((bool)1);
		__this->set_splashFadeOutSpeed_11((10.0f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Main_Menu_Manager::Start()
extern Il2CppClass* UnityAction_1_t897193173_il2cpp_TypeInfo_var;
extern const MethodInfo* Main_Menu_Manager_U3CStartU3Em__0_m935656048_MethodInfo_var;
extern const MethodInfo* UnityAction_1__ctor_m1968084291_MethodInfo_var;
extern const MethodInfo* UnityEvent_1_AddListener_m1708363187_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral435008735;
extern const uint32_t Main_Menu_Manager_Start_m177546312_MetadataUsageId;
extern "C"  void Main_Menu_Manager_Start_m177546312 (Main_Menu_Manager_t1163327745 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Main_Menu_Manager_Start_m177546312_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Game_Manager_t4105539090 * L_0 = Game_Manager_get__gminstance_m2308798245(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_hasSplashShown_6();
		if (L_1)
		{
			goto IL_0035;
		}
	}
	{
		RawImage_t2749640213 * L_2 = __this->get_splashScreen_10();
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m3105766835(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)1, /*hidden argument*/NULL);
		MonoBehaviour_Invoke_m666563676(__this, _stringLiteral435008735, (2.0f), /*hidden argument*/NULL);
		goto IL_0046;
	}

IL_0035:
	{
		RawImage_t2749640213 * L_4 = __this->get_splashScreen_10();
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		GameObject_SetActive_m2887581199(L_5, (bool)0, /*hidden argument*/NULL);
	}

IL_0046:
	{
		GameObject_t1756533147 * L_6 = __this->get_localSetupScreen_2();
		NullCheck(L_6);
		GameObject_SetActive_m2887581199(L_6, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_7 = __this->get_creditsScreen_3();
		NullCheck(L_7);
		GameObject_SetActive_m2887581199(L_7, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = __this->get_settingsScreen_4();
		NullCheck(L_8);
		GameObject_SetActive_m2887581199(L_8, (bool)0, /*hidden argument*/NULL);
		Toggle_t3976754468 * L_9 = __this->get_highGraphicsToggle_9();
		NullCheck(L_9);
		ToggleEvent_t1896830814 * L_10 = L_9->get_onValueChanged_19();
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)(void*)Main_Menu_Manager_U3CStartU3Em__0_m935656048_MethodInfo_var);
		UnityAction_1_t897193173 * L_12 = (UnityAction_1_t897193173 *)il2cpp_codegen_object_new(UnityAction_1_t897193173_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m1968084291(L_12, __this, L_11, /*hidden argument*/UnityAction_1__ctor_m1968084291_MethodInfo_var);
		NullCheck(L_10);
		UnityEvent_1_AddListener_m1708363187(L_10, L_12, /*hidden argument*/UnityEvent_1_AddListener_m1708363187_MethodInfo_var);
		Game_Manager_t4105539090 * L_13 = Game_Manager_get__gminstance_m2308798245(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_13);
		bool L_14 = L_13->get_isSoundOn_5();
		if (L_14)
		{
			goto IL_009b;
		}
	}
	{
		Main_Menu_Manager_ToggleSound_m2110619177(__this, /*hidden argument*/NULL);
	}

IL_009b:
	{
		return;
	}
}
// System.Void Main_Menu_Manager::Update()
extern "C"  void Main_Menu_Manager_Update_m2834293253 (Main_Menu_Manager_t1163327745 * __this, const MethodInfo* method)
{
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Color_t2020392075  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		bool L_0 = __this->get_startRemovingSplash_12();
		if (!L_0)
		{
			goto IL_005e;
		}
	}
	{
		RawImage_t2749640213 * L_1 = __this->get_splashScreen_10();
		NullCheck(L_1);
		Color_t2020392075  L_2 = VirtFuncInvoker0< Color_t2020392075  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_1);
		V_0 = L_2;
		float L_3 = (&V_0)->get_a_3();
		if ((!(((float)L_3) >= ((float)(0.0f)))))
		{
			goto IL_005e;
		}
	}
	{
		RawImage_t2749640213 * L_4 = __this->get_splashScreen_10();
		RawImage_t2749640213 * L_5 = L_4;
		NullCheck(L_5);
		Color_t2020392075  L_6 = VirtFuncInvoker0< Color_t2020392075  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_5);
		float L_7 = __this->get_splashFadeOutSpeed_11();
		float L_8 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Color_t2020392075  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Color__ctor_m1909920690(&L_9, (0.0f), (0.0f), (0.0f), ((float)((float)L_7*(float)L_8)), /*hidden argument*/NULL);
		Color_t2020392075  L_10 = Color_op_Subtraction_m3931992385(NULL /*static, unused*/, L_6, L_9, /*hidden argument*/NULL);
		NullCheck(L_5);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_5, L_10);
	}

IL_005e:
	{
		bool L_11 = __this->get_startRemovingSplash_12();
		if (!L_11)
		{
			goto IL_0097;
		}
	}
	{
		RawImage_t2749640213 * L_12 = __this->get_splashScreen_10();
		NullCheck(L_12);
		Color_t2020392075  L_13 = VirtFuncInvoker0< Color_t2020392075  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_12);
		V_1 = L_13;
		float L_14 = (&V_1)->get_a_3();
		if ((!(((float)L_14) <= ((float)(0.0f)))))
		{
			goto IL_0097;
		}
	}
	{
		RawImage_t2749640213 * L_15 = __this->get_splashScreen_10();
		NullCheck(L_15);
		GameObject_t1756533147 * L_16 = Component_get_gameObject_m3105766835(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		GameObject_SetActive_m2887581199(L_16, (bool)0, /*hidden argument*/NULL);
	}

IL_0097:
	{
		return;
	}
}
// System.Void Main_Menu_Manager::NewGame()
extern "C"  void Main_Menu_Manager_NewGame_m3857764328 (Main_Menu_Manager_t1163327745 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_localSetupScreen_2();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Main_Menu_Manager::PACPU()
extern "C"  void Main_Menu_Manager_PACPU_m3742451775 (Main_Menu_Manager_t1163327745 * __this, const MethodInfo* method)
{
	{
		Game_Manager_t4105539090 * L_0 = Game_Manager_get__gminstance_m2308798245(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Game_Manager_SetPlayers_m277409050(L_0, 1, /*hidden argument*/NULL);
		Main_Menu_Manager_StartLocalGame_m3065072519(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Main_Menu_Manager::Credits()
extern "C"  void Main_Menu_Manager_Credits_m3199251246 (Main_Menu_Manager_t1163327745 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_creditsScreen_3();
		GameObject_t1756533147 * L_1 = __this->get_creditsScreen_3();
		NullCheck(L_1);
		bool L_2 = GameObject_get_activeSelf_m313590879(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Main_Menu_Manager::Settings()
extern "C"  void Main_Menu_Manager_Settings_m1217263657 (Main_Menu_Manager_t1163327745 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_settingsScreen_4();
		GameObject_t1756533147 * L_1 = __this->get_settingsScreen_4();
		NullCheck(L_1);
		bool L_2 = GameObject_get_activeSelf_m313590879(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Main_Menu_Manager::Exit()
extern "C"  void Main_Menu_Manager_Exit_m3598658330 (Main_Menu_Manager_t1163327745 * __this, const MethodInfo* method)
{
	{
		Application_Quit_m3885595876(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Main_Menu_Manager::StartLocalGame()
extern "C"  void Main_Menu_Manager_StartLocalGame_m3065072519 (Main_Menu_Manager_t1163327745 * __this, const MethodInfo* method)
{
	{
		Game_Manager_t4105539090 * L_0 = Game_Manager_get__gminstance_m2308798245(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Game_Manager_ChangeArena_m3365242478(L_0, /*hidden argument*/NULL);
		SceneManager_LoadScene_m87258056(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Main_Menu_Manager::BackToMainScreen()
extern "C"  void Main_Menu_Manager_BackToMainScreen_m3865629351 (Main_Menu_Manager_t1163327745 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_localSetupScreen_2();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Main_Menu_Manager::Local2P()
extern "C"  void Main_Menu_Manager_Local2P_m3022977275 (Main_Menu_Manager_t1163327745 * __this, const MethodInfo* method)
{
	{
		Game_Manager_t4105539090 * L_0 = Game_Manager_get__gminstance_m2308798245(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Game_Manager_SetPlayers_m277409050(L_0, 2, /*hidden argument*/NULL);
		Main_Menu_Manager_StartLocalGame_m3065072519(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Main_Menu_Manager::Local3P()
extern "C"  void Main_Menu_Manager_Local3P_m3022977244 (Main_Menu_Manager_t1163327745 * __this, const MethodInfo* method)
{
	{
		Game_Manager_t4105539090 * L_0 = Game_Manager_get__gminstance_m2308798245(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Game_Manager_SetPlayers_m277409050(L_0, 3, /*hidden argument*/NULL);
		Main_Menu_Manager_StartLocalGame_m3065072519(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Main_Menu_Manager::Local4P()
extern "C"  void Main_Menu_Manager_Local4P_m3022977341 (Main_Menu_Manager_t1163327745 * __this, const MethodInfo* method)
{
	{
		Game_Manager_t4105539090 * L_0 = Game_Manager_get__gminstance_m2308798245(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Game_Manager_SetPlayers_m277409050(L_0, 4, /*hidden argument*/NULL);
		Main_Menu_Manager_StartLocalGame_m3065072519(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Main_Menu_Manager::ToggleSound()
extern "C"  void Main_Menu_Manager_ToggleSound_m2110619177 (Main_Menu_Manager_t1163327745 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_isSoundOn_8();
		__this->set_isSoundOn_8((bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0));
		bool L_1 = __this->get_isSoundOn_8();
		if (!L_1)
		{
			goto IL_0040;
		}
	}
	{
		Button_t2872111280 * L_2 = __this->get_soundButton_5();
		NullCheck(L_2);
		Image_t2042527209 * L_3 = Selectable_get_image_m3720077502(L_2, /*hidden argument*/NULL);
		Sprite_t309593783 * L_4 = __this->get_soundOn_6();
		NullCheck(L_3);
		Image_set_sprite_m1800056820(L_3, L_4, /*hidden argument*/NULL);
		Game_Manager_t4105539090 * L_5 = Game_Manager_get__gminstance_m2308798245(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		L_5->set_isSoundOn_5((bool)1);
		goto IL_006c;
	}

IL_0040:
	{
		bool L_6 = __this->get_isSoundOn_8();
		if (L_6)
		{
			goto IL_006c;
		}
	}
	{
		Button_t2872111280 * L_7 = __this->get_soundButton_5();
		NullCheck(L_7);
		Image_t2042527209 * L_8 = Selectable_get_image_m3720077502(L_7, /*hidden argument*/NULL);
		Sprite_t309593783 * L_9 = __this->get_soundOff_7();
		NullCheck(L_8);
		Image_set_sprite_m1800056820(L_8, L_9, /*hidden argument*/NULL);
		Game_Manager_t4105539090 * L_10 = Game_Manager_get__gminstance_m2308798245(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		L_10->set_isSoundOn_5((bool)0);
	}

IL_006c:
	{
		return;
	}
}
// System.Void Main_Menu_Manager::HGToggleListener(System.Boolean)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1190700671;
extern Il2CppCodeGenString* _stringLiteral2752580369;
extern const uint32_t Main_Menu_Manager_HGToggleListener_m2449447290_MetadataUsageId;
extern "C"  void Main_Menu_Manager_HGToggleListener_m2449447290 (Main_Menu_Manager_t1163327745 * __this, bool ___flag0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Main_Menu_Manager_HGToggleListener_m2449447290_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___flag0;
		if (!L_0)
		{
			goto IL_002a;
		}
	}
	{
		QualitySettings_SetQualityLevel_m862593565(NULL /*static, unused*/, 2, /*hidden argument*/NULL);
		int32_t L_1 = QualitySettings_GetQualityLevel_m2349477836(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1190700671, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		goto IL_004f;
	}

IL_002a:
	{
		bool L_5 = ___flag0;
		if (L_5)
		{
			goto IL_004f;
		}
	}
	{
		QualitySettings_SetQualityLevel_m862593565(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		int32_t L_6 = QualitySettings_GetQualityLevel_m2349477836(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_7 = L_6;
		Il2CppObject * L_8 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral2752580369, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
	}

IL_004f:
	{
		return;
	}
}
// System.Void Main_Menu_Manager::RemoveSplash()
extern "C"  void Main_Menu_Manager_RemoveSplash_m3221060547 (Main_Menu_Manager_t1163327745 * __this, const MethodInfo* method)
{
	{
		__this->set_startRemovingSplash_12((bool)1);
		Game_Manager_t4105539090 * L_0 = Game_Manager_get__gminstance_m2308798245(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		L_0->set_hasSplashShown_6((bool)1);
		return;
	}
}
// System.Void Main_Menu_Manager::<Start>m__0(System.Boolean)
extern "C"  void Main_Menu_Manager_U3CStartU3Em__0_m935656048 (Main_Menu_Manager_t1163327745 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		Main_Menu_Manager_HGToggleListener_m2449447290(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void myFBHolder::.ctor()
extern Il2CppCodeGenString* _stringLiteral1414245136;
extern const uint32_t myFBHolder__ctor_m364838827_MetadataUsageId;
extern "C"  void myFBHolder__ctor_m364838827 (myFBHolder_t218619452 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (myFBHolder__ctor_m364838827_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_userName_7(_stringLiteral1414245136);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void myFBHolder::Awake()
extern Il2CppClass* InitDelegate_t3410465555_il2cpp_TypeInfo_var;
extern Il2CppClass* HideUnityDelegate_t712804158_il2cpp_TypeInfo_var;
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern const MethodInfo* myFBHolder_SetInit_m2170005075_MethodInfo_var;
extern const MethodInfo* myFBHolder_OnHideUnity_m4139554744_MethodInfo_var;
extern const uint32_t myFBHolder_Awake_m2771421800_MetadataUsageId;
extern "C"  void myFBHolder_Awake_m2771421800 (myFBHolder_t218619452 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (myFBHolder_Awake_m2771421800_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)myFBHolder_SetInit_m2170005075_MethodInfo_var);
		InitDelegate_t3410465555 * L_1 = (InitDelegate_t3410465555 *)il2cpp_codegen_object_new(InitDelegate_t3410465555_il2cpp_TypeInfo_var);
		InitDelegate__ctor_m1337962232(L_1, __this, L_0, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)myFBHolder_OnHideUnity_m4139554744_MethodInfo_var);
		HideUnityDelegate_t712804158 * L_3 = (HideUnityDelegate_t712804158 *)il2cpp_codegen_object_new(HideUnityDelegate_t712804158_il2cpp_TypeInfo_var);
		HideUnityDelegate__ctor_m4212726857(L_3, __this, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_Init_m2482858118(NULL /*static, unused*/, L_1, L_3, (String_t*)NULL, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = __this->get_UIFBLoggedIn_2();
		NullCheck(L_4);
		GameObject_SetActive_m2887581199(L_4, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void myFBHolder::SetInit()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4011980788;
extern Il2CppCodeGenString* _stringLiteral1558894193;
extern Il2CppCodeGenString* _stringLiteral3510403336;
extern const uint32_t myFBHolder_SetInit_m2170005075_MetadataUsageId;
extern "C"  void myFBHolder_SetInit_m2170005075 (myFBHolder_t218619452 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (myFBHolder_SetInit_m2170005075_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral4011980788, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		bool L_0 = FB_get_IsLoggedIn_m4036830699(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1558894193, /*hidden argument*/NULL);
		goto IL_002d;
	}

IL_0023:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3510403336, /*hidden argument*/NULL);
	}

IL_002d:
	{
		return;
	}
}
// System.Void myFBHolder::OnHideUnity(System.Boolean)
extern "C"  void myFBHolder_OnHideUnity_m4139554744 (myFBHolder_t218619452 * __this, bool ___isGameShown0, const MethodInfo* method)
{
	{
		bool L_0 = ___isGameShown0;
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		Time_set_timeScale_m2194722837(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		goto IL_001f;
	}

IL_0015:
	{
		Time_set_timeScale_m2194722837(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void myFBHolder::FBLogin()
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern Il2CppClass* FacebookDelegate_1_t3733898188_il2cpp_TypeInfo_var;
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4061286785_MethodInfo_var;
extern const MethodInfo* myFBHolder_AuthCallback_m1876299558_MethodInfo_var;
extern const MethodInfo* FacebookDelegate_1__ctor_m1054243793_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1111044658;
extern const uint32_t myFBHolder_FBLogin_m251664878_MetadataUsageId;
extern "C"  void myFBHolder_FBLogin_m251664878 (myFBHolder_t218619452 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (myFBHolder_FBLogin_m251664878_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t1398341365 * V_0 = NULL;
	List_1_t1398341365 * V_1 = NULL;
	{
		List_1_t1398341365 * L_0 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_0, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		V_1 = L_0;
		List_1_t1398341365 * L_1 = V_1;
		NullCheck(L_1);
		List_1_Add_m4061286785(L_1, _stringLiteral1111044658, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
		List_1_t1398341365 * L_2 = V_1;
		V_0 = L_2;
		List_1_t1398341365 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)myFBHolder_AuthCallback_m1876299558_MethodInfo_var);
		FacebookDelegate_1_t3733898188 * L_5 = (FacebookDelegate_1_t3733898188 *)il2cpp_codegen_object_new(FacebookDelegate_1_t3733898188_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m1054243793(L_5, __this, L_4, /*hidden argument*/FacebookDelegate_1__ctor_m1054243793_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_LogInWithReadPermissions_m450336890(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void myFBHolder::AuthCallback(Facebook.Unity.ILoginResult)
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral299883742;
extern Il2CppCodeGenString* _stringLiteral3794248188;
extern const uint32_t myFBHolder_AuthCallback_m1876299558_MetadataUsageId;
extern "C"  void myFBHolder_AuthCallback_m1876299558 (myFBHolder_t218619452 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (myFBHolder_AuthCallback_m1876299558_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		bool L_0 = FB_get_IsLoggedIn_m4036830699(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral299883742, /*hidden argument*/NULL);
		myFBHolder_DealWithFBMenus_m2740628268(__this, (bool)1, /*hidden argument*/NULL);
		goto IL_0031;
	}

IL_0020:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3794248188, /*hidden argument*/NULL);
		myFBHolder_DealWithFBMenus_m2740628268(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void myFBHolder::DealWithFBMenus(System.Boolean)
extern Il2CppClass* FacebookDelegate_1_t3020292135_il2cpp_TypeInfo_var;
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern const MethodInfo* myFBHolder_DealWithProfilePic_m2902463055_MethodInfo_var;
extern const MethodInfo* FacebookDelegate_1__ctor_m2818497626_MethodInfo_var;
extern const MethodInfo* myFBHolder_DealWithUserName_m1764054732_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2403058853;
extern Il2CppCodeGenString* _stringLiteral1249371051;
extern const uint32_t myFBHolder_DealWithFBMenus_m2740628268_MetadataUsageId;
extern "C"  void myFBHolder_DealWithFBMenus_m2740628268 (myFBHolder_t218619452 * __this, bool ___isLoggedIn0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (myFBHolder_DealWithFBMenus_m2740628268_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___isLoggedIn0;
		if (!L_0)
		{
			goto IL_0053;
		}
	}
	{
		GameObject_t1756533147 * L_1 = __this->get_UIFBLoggedIn_2();
		NullCheck(L_1);
		GameObject_SetActive_m2887581199(L_1, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = __this->get_UIFBNotLoggedIn_3();
		NullCheck(L_2);
		GameObject_SetActive_m2887581199(L_2, (bool)0, /*hidden argument*/NULL);
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)myFBHolder_DealWithProfilePic_m2902463055_MethodInfo_var);
		FacebookDelegate_1_t3020292135 * L_4 = (FacebookDelegate_1_t3020292135 *)il2cpp_codegen_object_new(FacebookDelegate_1_t3020292135_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m2818497626(L_4, __this, L_3, /*hidden argument*/FacebookDelegate_1__ctor_m2818497626_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_API_m682532377(NULL /*static, unused*/, _stringLiteral2403058853, 0, L_4, (Il2CppObject*)NULL, /*hidden argument*/NULL);
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)myFBHolder_DealWithUserName_m1764054732_MethodInfo_var);
		FacebookDelegate_1_t3020292135 * L_6 = (FacebookDelegate_1_t3020292135 *)il2cpp_codegen_object_new(FacebookDelegate_1_t3020292135_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m2818497626(L_6, __this, L_5, /*hidden argument*/FacebookDelegate_1__ctor_m2818497626_MethodInfo_var);
		FB_API_m682532377(NULL /*static, unused*/, _stringLiteral1249371051, 0, L_6, (Il2CppObject*)NULL, /*hidden argument*/NULL);
		goto IL_006b;
	}

IL_0053:
	{
		GameObject_t1756533147 * L_7 = __this->get_UIFBLoggedIn_2();
		NullCheck(L_7);
		GameObject_SetActive_m2887581199(L_7, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = __this->get_UIFBNotLoggedIn_3();
		NullCheck(L_8);
		GameObject_SetActive_m2887581199(L_8, (bool)1, /*hidden argument*/NULL);
	}

IL_006b:
	{
		return;
	}
}
// System.String myFBHolder::GetPictureURL(System.String,System.Nullable`1<System.Int32>,System.Nullable`1<System.Int32>,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1_get_HasValue_m2294837272_MethodInfo_var;
extern const MethodInfo* Nullable_1_ToString_m2285560203_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1407553946;
extern Il2CppCodeGenString* _stringLiteral2302031961;
extern Il2CppCodeGenString* _stringLiteral449795626;
extern Il2CppCodeGenString* _stringLiteral2725807435;
extern Il2CppCodeGenString* _stringLiteral1853145380;
extern const uint32_t myFBHolder_GetPictureURL_m1870067007_MetadataUsageId;
extern "C"  String_t* myFBHolder_GetPictureURL_m1870067007 (Il2CppObject * __this /* static, unused */, String_t* ___facebookID0, Nullable_1_t334943763  ___width1, Nullable_1_t334943763  ___height2, String_t* ___type3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (myFBHolder_GetPictureURL_m1870067007_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* G_B3_0 = NULL;
	String_t* G_B5_0 = NULL;
	String_t* G_B4_0 = NULL;
	String_t* G_B6_0 = NULL;
	String_t* G_B6_1 = NULL;
	String_t* G_B8_0 = NULL;
	String_t* G_B7_0 = NULL;
	String_t* G_B9_0 = NULL;
	String_t* G_B9_1 = NULL;
	{
		String_t* L_0 = ___facebookID0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral1407553946, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = Nullable_1_get_HasValue_m2294837272((&___width1), /*hidden argument*/Nullable_1_get_HasValue_m2294837272_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0034;
		}
	}
	{
		String_t* L_3 = Nullable_1_ToString_m2285560203((&___width1), /*hidden argument*/Nullable_1_ToString_m2285560203_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2302031961, L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
		goto IL_0039;
	}

IL_0034:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
	}

IL_0039:
	{
		V_1 = G_B3_0;
		String_t* L_6 = V_1;
		bool L_7 = Nullable_1_get_HasValue_m2294837272((&___height2), /*hidden argument*/Nullable_1_get_HasValue_m2294837272_MethodInfo_var);
		G_B4_0 = L_6;
		if (!L_7)
		{
			G_B5_0 = L_6;
			goto IL_0063;
		}
	}
	{
		String_t* L_8 = Nullable_1_ToString_m2285560203((&___height2), /*hidden argument*/Nullable_1_ToString_m2285560203_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral449795626, L_8, /*hidden argument*/NULL);
		G_B6_0 = L_9;
		G_B6_1 = G_B4_0;
		goto IL_0068;
	}

IL_0063:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_10;
		G_B6_1 = G_B5_0;
	}

IL_0068:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m2596409543(NULL /*static, unused*/, G_B6_1, G_B6_0, /*hidden argument*/NULL);
		V_1 = L_11;
		String_t* L_12 = V_1;
		String_t* L_13 = ___type3;
		G_B7_0 = L_12;
		if (!L_13)
		{
			G_B8_0 = L_12;
			goto IL_0085;
		}
	}
	{
		String_t* L_14 = ___type3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2725807435, L_14, /*hidden argument*/NULL);
		G_B9_0 = L_15;
		G_B9_1 = G_B7_0;
		goto IL_008a;
	}

IL_0085:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B9_0 = L_16;
		G_B9_1 = G_B8_0;
	}

IL_008a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Concat_m2596409543(NULL /*static, unused*/, G_B9_1, G_B9_0, /*hidden argument*/NULL);
		V_1 = L_17;
		String_t* L_18 = V_1;
		String_t* L_19 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_20 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00ad;
		}
	}
	{
		String_t* L_21 = V_0;
		String_t* L_22 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m612901809(NULL /*static, unused*/, L_21, _stringLiteral1853145380, L_22, /*hidden argument*/NULL);
		V_0 = L_23;
	}

IL_00ad:
	{
		String_t* L_24 = V_0;
		return L_24;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> myFBHolder::DeserializeJSONProfile(System.String)
extern Il2CppClass* Json_t151732106_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t309261261_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t3943999495_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m760167321_MethodInfo_var;
extern const MethodInfo* Dictionary_2_TryGetValue_m104737093_MethodInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m4244870320_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2132392826;
extern const uint32_t myFBHolder_DeserializeJSONProfile_m301719529_MetadataUsageId;
extern "C"  Dictionary_2_t3943999495 * myFBHolder_DeserializeJSONProfile_m301719529 (Il2CppObject * __this /* static, unused */, String_t* ___response0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (myFBHolder_DeserializeJSONProfile_m301719529_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t309261261 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Dictionary_2_t3943999495 * V_2 = NULL;
	{
		String_t* L_0 = ___response0;
		IL2CPP_RUNTIME_CLASS_INIT(Json_t151732106_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = Json_Deserialize_m1129260701(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((Dictionary_2_t309261261 *)IsInstClass(L_1, Dictionary_2_t309261261_il2cpp_TypeInfo_var));
		Dictionary_2_t3943999495 * L_2 = (Dictionary_2_t3943999495 *)il2cpp_codegen_object_new(Dictionary_2_t3943999495_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m760167321(L_2, /*hidden argument*/Dictionary_2__ctor_m760167321_MethodInfo_var);
		V_2 = L_2;
		Dictionary_2_t309261261 * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = Dictionary_2_TryGetValue_m104737093(L_3, _stringLiteral2132392826, (&V_1), /*hidden argument*/Dictionary_2_TryGetValue_m104737093_MethodInfo_var);
		if (!L_4)
		{
			goto IL_0035;
		}
	}
	{
		Dictionary_2_t3943999495 * L_5 = V_2;
		Il2CppObject * L_6 = V_1;
		NullCheck(L_5);
		Dictionary_2_set_Item_m4244870320(L_5, _stringLiteral2132392826, ((String_t*)CastclassSealed(L_6, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/Dictionary_2_set_Item_m4244870320_MethodInfo_var);
	}

IL_0035:
	{
		Dictionary_2_t3943999495 * L_7 = V_2;
		return L_7;
	}
}
// System.Void myFBHolder::DealWithProfilePic(Facebook.Unity.IGraphResult)
extern Il2CppClass* IResult_t3447678270_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* FacebookDelegate_1_t3020292135_il2cpp_TypeInfo_var;
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern Il2CppClass* IGraphResult_t3984946686_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1__ctor_m255206972_MethodInfo_var;
extern const MethodInfo* myFBHolder_DealWithProfilePic_m2902463055_MethodInfo_var;
extern const MethodInfo* FacebookDelegate_1__ctor_m2818497626_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2829799868;
extern Il2CppCodeGenString* _stringLiteral3015944840;
extern const uint32_t myFBHolder_DealWithProfilePic_m2902463055_MetadataUsageId;
extern "C"  void myFBHolder_DealWithProfilePic_m2902463055 (myFBHolder_t218619452 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (myFBHolder_DealWithProfilePic_m2902463055_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___result0;
		NullCheck(L_0);
		String_t* L_1 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String Facebook.Unity.IResult::get_Error() */, IResult_t3447678270_il2cpp_TypeInfo_var, L_0);
		if (!L_1)
		{
			goto IL_0048;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2829799868, /*hidden argument*/NULL);
		Nullable_1_t334943763  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Nullable_1__ctor_m255206972(&L_2, ((int32_t)128), /*hidden argument*/Nullable_1__ctor_m255206972_MethodInfo_var);
		Nullable_1_t334943763  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Nullable_1__ctor_m255206972(&L_3, ((int32_t)128), /*hidden argument*/Nullable_1__ctor_m255206972_MethodInfo_var);
		String_t* L_4 = myFBHolder_GetPictureURL_m1870067007(NULL /*static, unused*/, _stringLiteral3015944840, L_2, L_3, (String_t*)NULL, /*hidden argument*/NULL);
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)myFBHolder_DealWithProfilePic_m2902463055_MethodInfo_var);
		FacebookDelegate_1_t3020292135 * L_6 = (FacebookDelegate_1_t3020292135 *)il2cpp_codegen_object_new(FacebookDelegate_1_t3020292135_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m2818497626(L_6, __this, L_5, /*hidden argument*/FacebookDelegate_1__ctor_m2818497626_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_API_m682532377(NULL /*static, unused*/, L_4, 0, L_6, (Il2CppObject*)NULL, /*hidden argument*/NULL);
		return;
	}

IL_0048:
	{
		Image_t2042527209 * L_7 = __this->get_profilePic_5();
		Il2CppObject * L_8 = ___result0;
		NullCheck(L_8);
		Texture2D_t3542995729 * L_9 = InterfaceFuncInvoker0< Texture2D_t3542995729 * >::Invoke(0 /* UnityEngine.Texture2D Facebook.Unity.IGraphResult::get_Texture() */, IGraphResult_t3984946686_il2cpp_TypeInfo_var, L_8);
		Rect_t3681755626  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Rect__ctor_m1220545469(&L_10, (0.0f), (0.0f), (128.0f), (128.0f), /*hidden argument*/NULL);
		Vector2_t2243707579  L_11 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		Sprite_t309593783 * L_12 = Sprite_Create_m3262956430(NULL /*static, unused*/, L_9, L_10, L_11, /*hidden argument*/NULL);
		NullCheck(L_7);
		Image_set_sprite_m1800056820(L_7, L_12, /*hidden argument*/NULL);
		Image_t2042527209 * L_13 = __this->get_profilePic_5();
		Color_t2020392075  L_14 = Color_get_white_m3987539815(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_13);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_13, L_14);
		return;
	}
}
// System.Void myFBHolder::DealWithUserName(Facebook.Unity.IGraphResult)
extern Il2CppClass* IResult_t3447678270_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* FacebookDelegate_1_t3020292135_il2cpp_TypeInfo_var;
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern const MethodInfo* myFBHolder_DealWithUserName_m1764054732_MethodInfo_var;
extern const MethodInfo* FacebookDelegate_1__ctor_m2818497626_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m499153720_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2829799868;
extern Il2CppCodeGenString* _stringLiteral1249371051;
extern Il2CppCodeGenString* _stringLiteral2132392826;
extern const uint32_t myFBHolder_DealWithUserName_m1764054732_MetadataUsageId;
extern "C"  void myFBHolder_DealWithUserName_m1764054732 (myFBHolder_t218619452 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (myFBHolder_DealWithUserName_m1764054732_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___result0;
		NullCheck(L_0);
		String_t* L_1 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String Facebook.Unity.IResult::get_Error() */, IResult_t3447678270_il2cpp_TypeInfo_var, L_0);
		if (!L_1)
		{
			goto IL_002e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2829799868, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)myFBHolder_DealWithUserName_m1764054732_MethodInfo_var);
		FacebookDelegate_1_t3020292135 * L_3 = (FacebookDelegate_1_t3020292135 *)il2cpp_codegen_object_new(FacebookDelegate_1_t3020292135_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m2818497626(L_3, __this, L_2, /*hidden argument*/FacebookDelegate_1__ctor_m2818497626_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_API_m682532377(NULL /*static, unused*/, _stringLiteral1249371051, 0, L_3, (Il2CppObject*)NULL, /*hidden argument*/NULL);
		return;
	}

IL_002e:
	{
		Il2CppObject * L_4 = ___result0;
		NullCheck(L_4);
		String_t* L_5 = InterfaceFuncInvoker0< String_t* >::Invoke(2 /* System.String Facebook.Unity.IResult::get_RawResult() */, IResult_t3447678270_il2cpp_TypeInfo_var, L_4);
		Dictionary_2_t3943999495 * L_6 = myFBHolder_DeserializeJSONProfile_m301719529(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		__this->set_profile_6(L_6);
		Dictionary_2_t3943999495 * L_7 = __this->get_profile_6();
		NullCheck(L_7);
		String_t* L_8 = Dictionary_2_get_Item_m499153720(L_7, _stringLiteral2132392826, /*hidden argument*/Dictionary_2_get_Item_m499153720_MethodInfo_var);
		__this->set_userName_7(L_8);
		Text_t356221433 * L_9 = __this->get_UIFBName_4();
		String_t* L_10 = __this->get_userName_7();
		NullCheck(L_9);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_9, L_10);
		return;
	}
}
// System.Void myFBHolder::Share()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Uri_t19570940_il2cpp_TypeInfo_var;
extern Il2CppClass* FacebookDelegate_1_t4160439974_il2cpp_TypeInfo_var;
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern const MethodInfo* myFBHolder_ShareCallBack_m794070025_MethodInfo_var;
extern const MethodInfo* FacebookDelegate_1__ctor_m303745986_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2541224323;
extern Il2CppCodeGenString* _stringLiteral3685953078;
extern Il2CppCodeGenString* _stringLiteral1071829180;
extern Il2CppCodeGenString* _stringLiteral2906628715;
extern Il2CppCodeGenString* _stringLiteral4131232247;
extern const uint32_t myFBHolder_Share_m2553693052_MetadataUsageId;
extern "C"  void myFBHolder_Share_m2553693052 (myFBHolder_t218619452 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (myFBHolder_Share_m2553693052_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		Uri_t19570940 * L_1 = (Uri_t19570940 *)il2cpp_codegen_object_new(Uri_t19570940_il2cpp_TypeInfo_var);
		Uri__ctor_m3927533881(L_1, _stringLiteral2541224323, /*hidden argument*/NULL);
		Uri_t19570940 * L_2 = (Uri_t19570940 *)il2cpp_codegen_object_new(Uri_t19570940_il2cpp_TypeInfo_var);
		Uri__ctor_m3927533881(L_2, _stringLiteral4131232247, /*hidden argument*/NULL);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)myFBHolder_ShareCallBack_m794070025_MethodInfo_var);
		FacebookDelegate_1_t4160439974 * L_5 = (FacebookDelegate_1_t4160439974 *)il2cpp_codegen_object_new(FacebookDelegate_1_t4160439974_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m303745986(L_5, __this, L_4, /*hidden argument*/FacebookDelegate_1__ctor_m303745986_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_FeedShare_m1853999185(NULL /*static, unused*/, L_0, L_1, _stringLiteral3685953078, _stringLiteral1071829180, _stringLiteral2906628715, L_2, L_3, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void myFBHolder::ShareCallBack(Facebook.Unity.IShareResult)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1271729240;
extern const uint32_t myFBHolder_ShareCallBack_m794070025_MetadataUsageId;
extern "C"  void myFBHolder_ShareCallBack_m794070025 (myFBHolder_t218619452 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (myFBHolder_ShareCallBack_m794070025_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___result0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1271729240, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void myFBHolder::InviteFriends()
extern Il2CppClass* Uri_t19570940_il2cpp_TypeInfo_var;
extern Il2CppClass* FacebookDelegate_1_t2564900615_il2cpp_TypeInfo_var;
extern const MethodInfo* myFBHolder_InviteCallBack_m824578806_MethodInfo_var;
extern const MethodInfo* FacebookDelegate_1__ctor_m3796675595_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3053244618;
extern Il2CppCodeGenString* _stringLiteral4131232247;
extern const uint32_t myFBHolder_InviteFriends_m1616796235_MetadataUsageId;
extern "C"  void myFBHolder_InviteFriends_m1616796235 (myFBHolder_t218619452 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (myFBHolder_InviteFriends_m1616796235_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Uri_t19570940 * L_0 = (Uri_t19570940 *)il2cpp_codegen_object_new(Uri_t19570940_il2cpp_TypeInfo_var);
		Uri__ctor_m3927533881(L_0, _stringLiteral3053244618, /*hidden argument*/NULL);
		Uri_t19570940 * L_1 = (Uri_t19570940 *)il2cpp_codegen_object_new(Uri_t19570940_il2cpp_TypeInfo_var);
		Uri__ctor_m3927533881(L_1, _stringLiteral4131232247, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)myFBHolder_InviteCallBack_m824578806_MethodInfo_var);
		FacebookDelegate_1_t2564900615 * L_3 = (FacebookDelegate_1_t2564900615 *)il2cpp_codegen_object_new(FacebookDelegate_1_t2564900615_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m3796675595(L_3, __this, L_2, /*hidden argument*/FacebookDelegate_1__ctor_m3796675595_MethodInfo_var);
		Mobile_AppInvite_m3081691722(NULL /*static, unused*/, L_0, L_1, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void myFBHolder::InviteCallBack(Facebook.Unity.IAppInviteResult)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2776800502;
extern const uint32_t myFBHolder_InviteCallBack_m824578806_MetadataUsageId;
extern "C"  void myFBHolder_InviteCallBack_m824578806 (myFBHolder_t218619452 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (myFBHolder_InviteCallBack_m824578806_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___result0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral2776800502, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void myFBHolder::Challenge()
extern Il2CppClass* List_1_t2058570427_il2cpp_TypeInfo_var;
extern Il2CppClass* Nullable_1_t334943763_il2cpp_TypeInfo_var;
extern Il2CppClass* FacebookDelegate_1_t909463455_il2cpp_TypeInfo_var;
extern Il2CppClass* FB_t1612658294_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2362181390_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2043351842_MethodInfo_var;
extern const MethodInfo* myFBHolder_DealWithChallenge_m197732585_MethodInfo_var;
extern const MethodInfo* FacebookDelegate_1__ctor_m113473383_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4220587732;
extern Il2CppCodeGenString* _stringLiteral3703024654;
extern Il2CppCodeGenString* _stringLiteral3685953078;
extern const uint32_t myFBHolder_Challenge_m2613916774_MetadataUsageId;
extern "C"  void myFBHolder_Challenge_m2613916774 (myFBHolder_t218619452 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (myFBHolder_Challenge_m2613916774_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t2058570427 * V_0 = NULL;
	Nullable_1_t334943763  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)il2cpp_codegen_object_new(List_1_t2058570427_il2cpp_TypeInfo_var);
		List_1__ctor_m2362181390(L_0, /*hidden argument*/List_1__ctor_m2362181390_MethodInfo_var);
		V_0 = L_0;
		List_1_t2058570427 * L_1 = V_0;
		NullCheck(L_1);
		List_1_Add_m2043351842(L_1, _stringLiteral3703024654, /*hidden argument*/List_1_Add_m2043351842_MethodInfo_var);
		List_1_t2058570427 * L_2 = V_0;
		Initobj (Nullable_1_t334943763_il2cpp_TypeInfo_var, (&V_1));
		Nullable_1_t334943763  L_3 = V_1;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)myFBHolder_DealWithChallenge_m197732585_MethodInfo_var);
		FacebookDelegate_1_t909463455 * L_5 = (FacebookDelegate_1_t909463455 *)il2cpp_codegen_object_new(FacebookDelegate_1_t909463455_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m113473383(L_5, __this, L_4, /*hidden argument*/FacebookDelegate_1__ctor_m113473383_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t1612658294_il2cpp_TypeInfo_var);
		FB_AppRequest_m1975988946(NULL /*static, unused*/, _stringLiteral4220587732, (Il2CppObject*)NULL, L_2, (Il2CppObject*)NULL, L_3, (String_t*)NULL, _stringLiteral3685953078, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void myFBHolder::DealWithChallenge(Facebook.Unity.IAppRequestResult)
extern Il2CppClass* IResult_t3447678270_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const uint32_t myFBHolder_DealWithChallenge_m197732585_MetadataUsageId;
extern "C"  void myFBHolder_DealWithChallenge_m197732585 (myFBHolder_t218619452 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (myFBHolder_DealWithChallenge_m197732585_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___result0;
		NullCheck(L_0);
		String_t* L_1 = InterfaceFuncInvoker0< String_t* >::Invoke(2 /* System.String Facebook.Unity.IResult::get_RawResult() */, IResult_t3447678270_il2cpp_TypeInfo_var, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void myFBHolder::OpenFBPage()
extern "C"  void myFBHolder_OpenFBPage_m1300852820 (myFBHolder_t218619452 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_FBUrl_8();
		Application_OpenURL_m3882634228(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pow_Script::.ctor()
extern "C"  void Pow_Script__ctor_m487179051 (Pow_Script_t2091305652 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pow_Script::Start()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Pow_Script_Start_m3582558687_MetadataUsageId;
extern "C"  void Pow_Script_Start_m3582558687 (Pow_Script_t2091305652 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pow_Script_Start_m3582558687_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		float L_1 = __this->get_destroyTime_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4279412553(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pow_Script::Update()
extern "C"  void Pow_Script_Update_m787413554 (Pow_Script_t2091305652 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Camera_t189460977 * L_1 = __this->get_targetCam_3();
		NullCheck(L_1);
		Transform_t3275118058 * L_2 = Component_get_transform_m2697483695(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_LookAt_m2514033256(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Raycast_Manager::.ctor()
extern "C"  void Raycast_Manager__ctor_m127067030 (Raycast_Manager_t4268834365 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Raycast_Manager::Update()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisGoti_Status_t1473870168_m3287759645_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4096075109;
extern const uint32_t Raycast_Manager_Update_m1479873385_MetadataUsageId;
extern "C"  void Raycast_Manager_Update_m1479873385 (Raycast_Manager_t4268834365 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Raycast_Manager_Update_m1479873385_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RaycastHit_t87180320  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Goti_Status_t1473870168 * V_1 = NULL;
	{
		Camera_t189460977 * L_0 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0087;
		}
	}
	{
		Camera_t189460977 * L_2 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_3 = Input_get_mousePosition_m146923508(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Ray_t2469606224  L_4 = Camera_ScreenPointToRay_m614889538(L_2, L_3, /*hidden argument*/NULL);
		__this->set_mouseToScreen_2(L_4);
		Ray_t2469606224  L_5 = __this->get_mouseToScreen_2();
		bool L_6 = Physics_Raycast_m2736931691(NULL /*static, unused*/, L_5, (&V_0), /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0087;
		}
	}
	{
		Collider_t3497673348 * L_7 = RaycastHit_get_collider_m301198172((&V_0), /*hidden argument*/NULL);
		NullCheck(L_7);
		String_t* L_8 = Component_get_tag_m357168014(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_8, _stringLiteral4096075109, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0087;
		}
	}
	{
		Collider_t3497673348 * L_10 = RaycastHit_get_collider_m301198172((&V_0), /*hidden argument*/NULL);
		NullCheck(L_10);
		Goti_Status_t1473870168 * L_11 = Component_GetComponent_TisGoti_Status_t1473870168_m3287759645(L_10, /*hidden argument*/Component_GetComponent_TisGoti_Status_t1473870168_m3287759645_MethodInfo_var);
		V_1 = L_11;
		Goti_Status_t1473870168 * L_12 = V_1;
		NullCheck(L_12);
		bool L_13 = L_12->get_actionPhase_5();
		if (!L_13)
		{
			goto IL_0087;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_14 = Input_GetMouseButtonDown_m47917805(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0087;
		}
	}
	{
		Goti_Status_t1473870168 * L_15 = V_1;
		NullCheck(L_15);
		Goti_Status_PerformMovement_m702764983(L_15, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_16 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		GameObject_SetActive_m2887581199(L_16, (bool)0, /*hidden argument*/NULL);
	}

IL_0087:
	{
		return;
	}
}
// System.Void Turn_Manager::.ctor()
extern "C"  void Turn_Manager__ctor_m1619170102 (Turn_Manager_t2388373579 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Turn_Manager::Start()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_FindObjectsOfType_TisColor_Manager_Script_t4053987423_m79499997_MethodInfo_var;
extern const MethodInfo* List_1_Add_m966968824_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m926561219_MethodInfo_var;
extern const uint32_t Turn_Manager_Start_m1946345482_MetadataUsageId;
extern "C"  void Turn_Manager_Start_m1946345482 (Turn_Manager_t2388373579 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Turn_Manager_Start_m1946345482_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_Manager_ScriptU5BU5D_t4081434438* V_0 = NULL;
	Color_Manager_Script_t4053987423 * V_1 = NULL;
	Color_Manager_ScriptU5BU5D_t4081434438* V_2 = NULL;
	int32_t V_3 = 0;
	{
		Game_Manager_t4105539090 * L_0 = Game_Manager_get__gminstance_m2308798245(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Game_Manager_GetPlayers_m971560877(L_0, /*hidden argument*/NULL);
		__this->set_noOfPlayers_19(L_1);
		int32_t L_2 = __this->get_noOfPlayers_19();
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_0028;
		}
	}
	{
		__this->set_isCPUGame_16((bool)1);
		goto IL_002f;
	}

IL_0028:
	{
		__this->set_isCPUGame_16((bool)0);
	}

IL_002f:
	{
		GameObject_t1756533147 * L_3 = __this->get_diceRollButt_9();
		NullCheck(L_3);
		GameObject_t1756533147 * L_4 = GameObject_get_gameObject_m3662236595(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_SetActive_m2887581199(L_4, (bool)1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Color_Manager_ScriptU5BU5D_t4081434438* L_5 = Object_FindObjectsOfType_TisColor_Manager_Script_t4053987423_m79499997(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectsOfType_TisColor_Manager_Script_t4053987423_m79499997_MethodInfo_var);
		V_0 = L_5;
		Color_Manager_ScriptU5BU5D_t4081434438* L_6 = V_0;
		V_2 = L_6;
		V_3 = 0;
		goto IL_006e;
	}

IL_004f:
	{
		Color_Manager_ScriptU5BU5D_t4081434438* L_7 = V_2;
		int32_t L_8 = V_3;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Color_Manager_Script_t4053987423 * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_1 = L_10;
		Color_Manager_Script_t4053987423 * L_11 = V_1;
		NullCheck(L_11);
		bool L_12 = L_11->get_amIPlayin_3();
		if (!L_12)
		{
			goto IL_006a;
		}
	}
	{
		List_1_t3423108555 * L_13 = __this->get_listOfPlayers_2();
		Color_Manager_Script_t4053987423 * L_14 = V_1;
		NullCheck(L_13);
		List_1_Add_m966968824(L_13, L_14, /*hidden argument*/List_1_Add_m966968824_MethodInfo_var);
	}

IL_006a:
	{
		int32_t L_15 = V_3;
		V_3 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_006e:
	{
		int32_t L_16 = V_3;
		Color_Manager_ScriptU5BU5D_t4081434438* L_17 = V_2;
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_17)->max_length)))))))
		{
			goto IL_004f;
		}
	}
	{
		List_1_t3423108555 * L_18 = __this->get_listOfPlayers_2();
		NullCheck(L_18);
		Color_Manager_Script_t4053987423 * L_19 = List_1_get_Item_m926561219(L_18, 0, /*hidden argument*/List_1_get_Item_m926561219_MethodInfo_var);
		__this->set_currentTurn_4(L_19);
		Text_t356221433 * L_20 = __this->get_notificationText_3();
		NullCheck(L_20);
		GameObject_t1756533147 * L_21 = Component_get_gameObject_m3105766835(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		GameObject_SetActive_m2887581199(L_21, (bool)0, /*hidden argument*/NULL);
		List_1_t3423108555 * L_22 = __this->get_listOfPlayers_2();
		NullCheck(L_22);
		Color_Manager_Script_t4053987423 * L_23 = List_1_get_Item_m926561219(L_22, 0, /*hidden argument*/List_1_get_Item_m926561219_MethodInfo_var);
		Turn_Manager_SetCurrentTurnTo_m2407535811(__this, L_23, 0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_24 = __this->get_gameCompletionObj_17();
		NullCheck(L_24);
		GameObject_SetActive_m2887581199(L_24, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Turn_Manager::Update()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m1616771018_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m926561219_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisEnemy_AI_t244483799_m3521222472_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3510846499;
extern Il2CppCodeGenString* _stringLiteral3021629811;
extern Il2CppCodeGenString* _stringLiteral2395476974;
extern Il2CppCodeGenString* _stringLiteral777220966;
extern const uint32_t Turn_Manager_Update_m1635222775_MetadataUsageId;
extern "C"  void Turn_Manager_Update_m1635222775 (Turn_Manager_t2388373579 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Turn_Manager_Update_m1635222775_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Text_t356221433 * L_0 = __this->get_notificationText_3();
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = GameObject_get_activeSelf_m313590879(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0044;
		}
	}
	{
		Text_t356221433 * L_3 = __this->get_notificationText_3();
		Text_t356221433 * L_4 = L_3;
		NullCheck(L_4);
		Color_t2020392075  L_5 = VirtFuncInvoker0< Color_t2020392075  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_4);
		Color_t2020392075  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Color__ctor_m1909920690(&L_6, (0.0f), (0.0f), (0.0f), (0.01f), /*hidden argument*/NULL);
		Color_t2020392075  L_7 = Color_op_Subtraction_m3931992385(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_4, L_7);
	}

IL_0044:
	{
		Color_Manager_Script_t4053987423 * L_8 = __this->get_currentTurn_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_8, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_009d;
		}
	}
	{
		int32_t L_10 = __this->get_indexTurn_6();
		List_1_t3423108555 * L_11 = __this->get_listOfPlayers_2();
		NullCheck(L_11);
		int32_t L_12 = List_1_get_Count_m1616771018(L_11, /*hidden argument*/List_1_get_Count_m1616771018_MethodInfo_var);
		if ((!(((uint32_t)L_10) == ((uint32_t)((int32_t)((int32_t)L_12-(int32_t)1))))))
		{
			goto IL_0084;
		}
	}
	{
		List_1_t3423108555 * L_13 = __this->get_listOfPlayers_2();
		NullCheck(L_13);
		Color_Manager_Script_t4053987423 * L_14 = List_1_get_Item_m926561219(L_13, 0, /*hidden argument*/List_1_get_Item_m926561219_MethodInfo_var);
		__this->set_nextTurn_5(L_14);
		goto IL_009d;
	}

IL_0084:
	{
		List_1_t3423108555 * L_15 = __this->get_listOfPlayers_2();
		int32_t L_16 = __this->get_indexTurn_6();
		NullCheck(L_15);
		Color_Manager_Script_t4053987423 * L_17 = List_1_get_Item_m926561219(L_15, ((int32_t)((int32_t)L_16+(int32_t)1)), /*hidden argument*/List_1_get_Item_m926561219_MethodInfo_var);
		__this->set_nextTurn_5(L_17);
	}

IL_009d:
	{
		bool L_18 = __this->get_startRolling_10();
		if (!L_18)
		{
			goto IL_00b3;
		}
	}
	{
		Dice_Script_t640145349 * L_19 = __this->get_dc_11();
		NullCheck(L_19);
		Dice_Script_RollDice_m839356228(L_19, /*hidden argument*/NULL);
	}

IL_00b3:
	{
		Color_Manager_Script_t4053987423 * L_20 = __this->get_currentTurn_4();
		NullCheck(L_20);
		String_t* L_21 = L_20->get_myName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_22 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_21, _stringLiteral3510846499, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00ec;
		}
	}
	{
		Dice_Script_t640145349 * L_23 = __this->get_dc_11();
		NullCheck(L_23);
		Renderer_t257310565 * L_24 = L_23->get_diceR_6();
		NullCheck(L_24);
		Material_t193706927 * L_25 = Renderer_get_material_m2553789785(L_24, /*hidden argument*/NULL);
		Color_t2020392075  L_26 = Color_get_green_m2671273823(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_25);
		Material_set_color_m577844242(L_25, L_26, /*hidden argument*/NULL);
		goto IL_0192;
	}

IL_00ec:
	{
		Color_Manager_Script_t4053987423 * L_27 = __this->get_currentTurn_4();
		NullCheck(L_27);
		String_t* L_28 = L_27->get_myName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_29 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_28, _stringLiteral3021629811, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_0125;
		}
	}
	{
		Dice_Script_t640145349 * L_30 = __this->get_dc_11();
		NullCheck(L_30);
		Renderer_t257310565 * L_31 = L_30->get_diceR_6();
		NullCheck(L_31);
		Material_t193706927 * L_32 = Renderer_get_material_m2553789785(L_31, /*hidden argument*/NULL);
		Color_t2020392075  L_33 = Color_get_red_m2410286591(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_32);
		Material_set_color_m577844242(L_32, L_33, /*hidden argument*/NULL);
		goto IL_0192;
	}

IL_0125:
	{
		Color_Manager_Script_t4053987423 * L_34 = __this->get_currentTurn_4();
		NullCheck(L_34);
		String_t* L_35 = L_34->get_myName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_36 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_35, _stringLiteral2395476974, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_015e;
		}
	}
	{
		Dice_Script_t640145349 * L_37 = __this->get_dc_11();
		NullCheck(L_37);
		Renderer_t257310565 * L_38 = L_37->get_diceR_6();
		NullCheck(L_38);
		Material_t193706927 * L_39 = Renderer_get_material_m2553789785(L_38, /*hidden argument*/NULL);
		Color_t2020392075  L_40 = Color_get_blue_m4180825090(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_39);
		Material_set_color_m577844242(L_39, L_40, /*hidden argument*/NULL);
		goto IL_0192;
	}

IL_015e:
	{
		Color_Manager_Script_t4053987423 * L_41 = __this->get_currentTurn_4();
		NullCheck(L_41);
		String_t* L_42 = L_41->get_myName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_43 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_42, _stringLiteral777220966, /*hidden argument*/NULL);
		if (!L_43)
		{
			goto IL_0192;
		}
	}
	{
		Dice_Script_t640145349 * L_44 = __this->get_dc_11();
		NullCheck(L_44);
		Renderer_t257310565 * L_45 = L_44->get_diceR_6();
		NullCheck(L_45);
		Material_t193706927 * L_46 = Renderer_get_material_m2553789785(L_45, /*hidden argument*/NULL);
		Color_t2020392075  L_47 = Color_get_yellow_m3741935494(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_46);
		Material_set_color_m577844242(L_46, L_47, /*hidden argument*/NULL);
	}

IL_0192:
	{
		Color_Manager_Script_t4053987423 * L_48 = __this->get_currentTurn_4();
		NullCheck(L_48);
		bool L_49 = L_48->get_CPU_13();
		if (!L_49)
		{
			goto IL_01c5;
		}
	}
	{
		bool L_50 = __this->get_calledOnce_12();
		if (L_50)
		{
			goto IL_01c5;
		}
	}
	{
		Color_Manager_Script_t4053987423 * L_51 = __this->get_currentTurn_4();
		NullCheck(L_51);
		Enemy_AI_t244483799 * L_52 = Component_GetComponent_TisEnemy_AI_t244483799_m3521222472(L_51, /*hidden argument*/Component_GetComponent_TisEnemy_AI_t244483799_m3521222472_MethodInfo_var);
		NullCheck(L_52);
		L_52->set_rollDice_2((bool)1);
		__this->set_calledOnce_12((bool)1);
	}

IL_01c5:
	{
		GameObject_t1756533147 * L_53 = __this->get_pauseUI_14();
		bool L_54 = __this->get_isPaused_13();
		NullCheck(L_53);
		GameObject_SetActive_m2887581199(L_53, L_54, /*hidden argument*/NULL);
		bool L_55 = __this->get_isPaused_13();
		if (!L_55)
		{
			goto IL_01f0;
		}
	}
	{
		Time_set_timeScale_m2194722837(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		goto IL_01fa;
	}

IL_01f0:
	{
		Time_set_timeScale_m2194722837(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
	}

IL_01fa:
	{
		return;
	}
}
// System.Void Turn_Manager::SetCurrentTurnTo(Color_Manager_Script,System.Int32)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m1616771018_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m926561219_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral771935255;
extern Il2CppCodeGenString* _stringLiteral2773143782;
extern Il2CppCodeGenString* _stringLiteral1575759254;
extern Il2CppCodeGenString* _stringLiteral1116650511;
extern Il2CppCodeGenString* _stringLiteral2487991118;
extern const uint32_t Turn_Manager_SetCurrentTurnTo_m2407535811_MetadataUsageId;
extern "C"  void Turn_Manager_SetCurrentTurnTo_m2407535811 (Turn_Manager_t2388373579 * __this, Color_Manager_Script_t4053987423 * ___cms0, int32_t ___i1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Turn_Manager_SetCurrentTurnTo_m2407535811_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Color_Manager_Script_t4053987423 * L_0 = ___cms0;
		NullCheck(L_0);
		String_t* L_1 = L_0->get_myName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral771935255, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		int32_t L_3 = ___i1;
		__this->set_indexTurn_6(L_3);
		Dice_Script_t640145349 * L_4 = __this->get_dc_11();
		NullCheck(L_4);
		Dice_Script_EnableArrow_m2954576550(L_4, /*hidden argument*/NULL);
		Color_Manager_Script_t4053987423 * L_5 = ___cms0;
		__this->set_currentTurn_4(L_5);
		Color_Manager_Script_t4053987423 * L_6 = __this->get_currentTurn_4();
		NullCheck(L_6);
		bool L_7 = L_6->get_haveWon_16();
		if (!L_7)
		{
			goto IL_00b2;
		}
	}
	{
		int32_t L_8 = __this->get_indexTurn_6();
		List_1_t3423108555 * L_9 = __this->get_listOfPlayers_2();
		NullCheck(L_9);
		int32_t L_10 = List_1_get_Count_m1616771018(L_9, /*hidden argument*/List_1_get_Count_m1616771018_MethodInfo_var);
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)((int32_t)L_10-(int32_t)1))))))
		{
			goto IL_0062;
		}
	}
	{
		__this->set_indexTurn_6(0);
		goto IL_0070;
	}

IL_0062:
	{
		int32_t L_11 = __this->get_indexTurn_6();
		__this->set_indexTurn_6(((int32_t)((int32_t)L_11+(int32_t)1)));
	}

IL_0070:
	{
		List_1_t3423108555 * L_12 = __this->get_listOfPlayers_2();
		int32_t L_13 = __this->get_indexTurn_6();
		NullCheck(L_12);
		Color_Manager_Script_t4053987423 * L_14 = List_1_get_Item_m926561219(L_12, L_13, /*hidden argument*/List_1_get_Item_m926561219_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral2773143782, L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		List_1_t3423108555 * L_16 = __this->get_listOfPlayers_2();
		int32_t L_17 = __this->get_indexTurn_6();
		NullCheck(L_16);
		Color_Manager_Script_t4053987423 * L_18 = List_1_get_Item_m926561219(L_16, L_17, /*hidden argument*/List_1_get_Item_m926561219_MethodInfo_var);
		int32_t L_19 = __this->get_indexTurn_6();
		Turn_Manager_SetCurrentTurnTo_m2407535811(__this, L_18, L_19, /*hidden argument*/NULL);
		goto IL_00ec;
	}

IL_00b2:
	{
		Color_Manager_Script_t4053987423 * L_20 = ___cms0;
		NullCheck(L_20);
		String_t* L_21 = L_20->get_myName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral1575759254, L_21, _stringLiteral1116650511, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		String_t* L_23 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		Color_Manager_Script_t4053987423 * L_24 = __this->get_currentTurn_4();
		NullCheck(L_24);
		String_t* L_25 = L_24->get_myName_2();
		String_t* L_26 = String_Concat_m612901809(NULL /*static, unused*/, L_23, L_25, _stringLiteral2487991118, /*hidden argument*/NULL);
		Turn_Manager_GiveNotification_m239101948(__this, L_26, /*hidden argument*/NULL);
	}

IL_00ec:
	{
		return;
	}
}
// System.Void Turn_Manager::GiveNotification(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3510846499;
extern Il2CppCodeGenString* _stringLiteral3021629811;
extern Il2CppCodeGenString* _stringLiteral2395476974;
extern Il2CppCodeGenString* _stringLiteral777220966;
extern const uint32_t Turn_Manager_GiveNotification_m239101948_MetadataUsageId;
extern "C"  void Turn_Manager_GiveNotification_m239101948 (Turn_Manager_t2388373579 * __this, String_t* ___s0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Turn_Manager_GiveNotification_m239101948_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Text_t356221433 * L_0 = __this->get_notificationText_3();
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m2887581199(L_1, (bool)1, /*hidden argument*/NULL);
		Color_Manager_Script_t4053987423 * L_2 = __this->get_currentTurn_4();
		NullCheck(L_2);
		String_t* L_3 = L_2->get_myName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_3, _stringLiteral3510846499, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0040;
		}
	}
	{
		Text_t356221433 * L_5 = __this->get_notificationText_3();
		Color_t2020392075  L_6 = Color_get_green_m2671273823(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_5, L_6);
		goto IL_00c8;
	}

IL_0040:
	{
		Color_Manager_Script_t4053987423 * L_7 = __this->get_currentTurn_4();
		NullCheck(L_7);
		String_t* L_8 = L_7->get_myName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_8, _stringLiteral3021629811, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_006f;
		}
	}
	{
		Text_t356221433 * L_10 = __this->get_notificationText_3();
		Color_t2020392075  L_11 = Color_get_red_m2410286591(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_10, L_11);
		goto IL_00c8;
	}

IL_006f:
	{
		Color_Manager_Script_t4053987423 * L_12 = __this->get_currentTurn_4();
		NullCheck(L_12);
		String_t* L_13 = L_12->get_myName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_14 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_13, _stringLiteral2395476974, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_009e;
		}
	}
	{
		Text_t356221433 * L_15 = __this->get_notificationText_3();
		Color_t2020392075  L_16 = Color_get_blue_m4180825090(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_15);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_15, L_16);
		goto IL_00c8;
	}

IL_009e:
	{
		Color_Manager_Script_t4053987423 * L_17 = __this->get_currentTurn_4();
		NullCheck(L_17);
		String_t* L_18 = L_17->get_myName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_19 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_18, _stringLiteral777220966, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00c8;
		}
	}
	{
		Text_t356221433 * L_20 = __this->get_notificationText_3();
		Color_t2020392075  L_21 = Color_get_yellow_m3741935494(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_20);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_20, L_21);
	}

IL_00c8:
	{
		Text_t356221433 * L_22 = __this->get_notificationText_3();
		String_t* L_23 = ___s0;
		NullCheck(L_22);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_22, L_23);
		return;
	}
}
// System.Void Turn_Manager::PerformNextTurn()
extern const MethodInfo* List_1_get_Count_m1616771018_MethodInfo_var;
extern const uint32_t Turn_Manager_PerformNextTurn_m2187029365_MetadataUsageId;
extern "C"  void Turn_Manager_PerformNextTurn_m2187029365 (Turn_Manager_t2388373579 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Turn_Manager_PerformNextTurn_m2187029365_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_diceRollButt_9();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)1, /*hidden argument*/NULL);
		Raycast_Manager_t4268834365 * L_1 = __this->get_rm_7();
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_SetActive_m2887581199(L_2, (bool)0, /*hidden argument*/NULL);
		int32_t L_3 = __this->get_indexTurn_6();
		List_1_t3423108555 * L_4 = __this->get_listOfPlayers_2();
		NullCheck(L_4);
		int32_t L_5 = List_1_get_Count_m1616771018(L_4, /*hidden argument*/List_1_get_Count_m1616771018_MethodInfo_var);
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)((int32_t)L_5-(int32_t)1))))))
		{
			goto IL_0047;
		}
	}
	{
		Color_Manager_Script_t4053987423 * L_6 = __this->get_nextTurn_5();
		Turn_Manager_SetCurrentTurnTo_m2407535811(__this, L_6, 0, /*hidden argument*/NULL);
		goto IL_005b;
	}

IL_0047:
	{
		Color_Manager_Script_t4053987423 * L_7 = __this->get_nextTurn_5();
		int32_t L_8 = __this->get_indexTurn_6();
		Turn_Manager_SetCurrentTurnTo_m2407535811(__this, L_7, ((int32_t)((int32_t)L_8+(int32_t)1)), /*hidden argument*/NULL);
	}

IL_005b:
	{
		bool L_9 = __this->get_isCPUGame_16();
		if (!L_9)
		{
			goto IL_007d;
		}
	}
	{
		int32_t L_10 = __this->get_wonPos_15();
		if ((!(((uint32_t)L_10) == ((uint32_t)1))))
		{
			goto IL_0078;
		}
	}
	{
		Turn_Manager_ShowGameCompletion_m1622668167(__this, /*hidden argument*/NULL);
	}

IL_0078:
	{
		goto IL_0096;
	}

IL_007d:
	{
		int32_t L_11 = __this->get_wonPos_15();
		int32_t L_12 = __this->get_noOfPlayers_19();
		if ((!(((uint32_t)L_11) == ((uint32_t)((int32_t)((int32_t)L_12-(int32_t)1))))))
		{
			goto IL_0096;
		}
	}
	{
		Turn_Manager_ShowGameCompletion_m1622668167(__this, /*hidden argument*/NULL);
	}

IL_0096:
	{
		return;
	}
}
// System.Void Turn_Manager::RepeatTurn()
extern "C"  void Turn_Manager_RepeatTurn_m1002454660 (Turn_Manager_t2388373579 * __this, const MethodInfo* method)
{
	{
		__this->set_calledOnce_12((bool)0);
		Color_Manager_Script_t4053987423 * L_0 = __this->get_currentTurn_4();
		NullCheck(L_0);
		bool L_1 = L_0->get_CPU_13();
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		GameObject_t1756533147 * L_2 = __this->get_diceRollButt_9();
		NullCheck(L_2);
		GameObject_SetActive_m2887581199(L_2, (bool)0, /*hidden argument*/NULL);
		goto IL_0034;
	}

IL_0028:
	{
		GameObject_t1756533147 * L_3 = __this->get_diceRollButt_9();
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)1, /*hidden argument*/NULL);
	}

IL_0034:
	{
		Raycast_Manager_t4268834365 * L_4 = __this->get_rm_7();
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		GameObject_SetActive_m2887581199(L_5, (bool)0, /*hidden argument*/NULL);
		Color_Manager_Script_t4053987423 * L_6 = __this->get_currentTurn_4();
		int32_t L_7 = __this->get_indexTurn_6();
		Turn_Manager_SetCurrentTurnTo_m2407535811(__this, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Turn_Manager::DeactivateButton()
extern "C"  void Turn_Manager_DeactivateButton_m2793329282 (Turn_Manager_t2388373579 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_diceRollButt_9();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Turn_Manager::StopDice()
extern "C"  void Turn_Manager_StopDice_m2156974429 (Turn_Manager_t2388373579 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_diceRollButt_9();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_1 = __this->get_asDice_24();
		NullCheck(L_1);
		AudioSource_Stop_m3452679614(L_1, /*hidden argument*/NULL);
		__this->set_startRolling_10((bool)0);
		Dice_Script_t640145349 * L_2 = __this->get_dc_11();
		NullCheck(L_2);
		Dice_Script_StopDice_m3232255983(L_2, /*hidden argument*/NULL);
		Color_Manager_Script_t4053987423 * L_3 = __this->get_currentTurn_4();
		NullCheck(L_3);
		Color_Manager_Script_StopTheDice_m250861038(L_3, /*hidden argument*/NULL);
		Color_Manager_Script_t4053987423 * L_4 = __this->get_currentTurn_4();
		NullCheck(L_4);
		bool L_5 = L_4->get_CPU_13();
		if (L_5)
		{
			goto IL_004b;
		}
	}
	{
		__this->set_calledOnce_12((bool)0);
	}

IL_004b:
	{
		return;
	}
}
// System.Void Turn_Manager::RollTheDice()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral502884169;
extern Il2CppCodeGenString* _stringLiteral2343170687;
extern const uint32_t Turn_Manager_RollTheDice_m3432382429_MetadataUsageId;
extern "C"  void Turn_Manager_RollTheDice_m3432382429 (Turn_Manager_t2388373579 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Turn_Manager_RollTheDice_m3432382429_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		Game_Manager_t4105539090 * L_0 = Game_Manager_get__gminstance_m2308798245(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_isSoundOn_5();
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		AudioSource_t1135106623 * L_2 = __this->get_asDice_24();
		NullCheck(L_2);
		AudioSource_Play_m353744792(L_2, /*hidden argument*/NULL);
	}

IL_001a:
	{
		__this->set_startRolling_10((bool)1);
		GameObject_t1756533147 * L_3 = __this->get_diceRollButt_9();
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)0, /*hidden argument*/NULL);
		Color_Manager_Script_t4053987423 * L_4 = __this->get_currentTurn_4();
		NullCheck(L_4);
		String_t* L_5 = L_4->get_myName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m2596409543(NULL /*static, unused*/, L_5, _stringLiteral502884169, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		float L_7 = Random_Range_m2884721203(NULL /*static, unused*/, (0.8f), (1.4f), /*hidden argument*/NULL);
		V_0 = L_7;
		float L_8 = V_0;
		MonoBehaviour_Invoke_m666563676(__this, _stringLiteral2343170687, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Turn_Manager::AIRoll()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral502884169;
extern const uint32_t Turn_Manager_AIRoll_m382572419_MetadataUsageId;
extern "C"  void Turn_Manager_AIRoll_m382572419 (Turn_Manager_t2388373579 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Turn_Manager_AIRoll_m382572419_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Game_Manager_t4105539090 * L_0 = Game_Manager_get__gminstance_m2308798245(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_isSoundOn_5();
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		AudioSource_t1135106623 * L_2 = __this->get_asDice_24();
		NullCheck(L_2);
		AudioSource_Play_m353744792(L_2, /*hidden argument*/NULL);
	}

IL_001a:
	{
		__this->set_startRolling_10((bool)1);
		GameObject_t1756533147 * L_3 = __this->get_diceRollButt_9();
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)0, /*hidden argument*/NULL);
		Color_Manager_Script_t4053987423 * L_4 = __this->get_currentTurn_4();
		NullCheck(L_4);
		String_t* L_5 = L_4->get_myName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m2596409543(NULL /*static, unused*/, L_5, _stringLiteral502884169, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Turn_Manager::PauseGame()
extern "C"  void Turn_Manager_PauseGame_m3660233854 (Turn_Manager_t2388373579 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get_isPaused_13();
		__this->set_isPaused_13((bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0));
		int32_t L_1 = Random_Range_m694320887(NULL /*static, unused*/, 0, 5, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_0027;
		}
	}
	{
		AdMob_Manager_t3979489515 * L_3 = AdMob_Manager_get_Instance_m2706670290(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		AdMob_Manager_ShowImageAd_m789447395(L_3, /*hidden argument*/NULL);
	}

IL_0027:
	{
		return;
	}
}
// System.Void Turn_Manager::BTM()
extern "C"  void Turn_Manager_BTM_m4220530601 (Turn_Manager_t2388373579 * __this, const MethodInfo* method)
{
	{
		SceneManager_LoadScene_m87258056(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		AdMob_Manager_t3979489515 * L_0 = AdMob_Manager_get_Instance_m2706670290(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		AdMob_Manager_ShowImageAd_m789447395(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Turn_Manager::Replay()
extern "C"  void Turn_Manager_Replay_m1145465645 (Turn_Manager_t2388373579 * __this, const MethodInfo* method)
{
	{
		SceneManager_LoadScene_m87258056(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Turn_Manager::ShowGameCompletion()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisAnimator_t69676727_m475627522_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m527309189_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1406163925_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1721688489_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m2781297319_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3021629811;
extern Il2CppCodeGenString* _stringLiteral272007032;
extern Il2CppCodeGenString* _stringLiteral2395476974;
extern Il2CppCodeGenString* _stringLiteral777220966;
extern Il2CppCodeGenString* _stringLiteral3510846499;
extern Il2CppCodeGenString* _stringLiteral189104851;
extern Il2CppCodeGenString* _stringLiteral702718481;
extern Il2CppCodeGenString* _stringLiteral1601026680;
extern Il2CppCodeGenString* _stringLiteral2274690421;
extern const uint32_t Turn_Manager_ShowGameCompletion_m1622668167_MetadataUsageId;
extern "C"  void Turn_Manager_ShowGameCompletion_m1622668167 (Turn_Manager_t2388373579 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Turn_Manager_ShowGameCompletion_m1622668167_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Animator_t69676727 * V_0 = NULL;
	Animator_t69676727 * V_1 = NULL;
	Animator_t69676727 * V_2 = NULL;
	Animator_t69676727 * V_3 = NULL;
	Color_Manager_Script_t4053987423 * V_4 = NULL;
	Enumerator_t2957838229  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		AudioSource_t1135106623 * L_0 = __this->get_victoryAS_25();
		NullCheck(L_0);
		AudioSource_Play_m353744792(L_0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = __this->get_gameCompletionObj_17();
		NullCheck(L_1);
		GameObject_SetActive_m2887581199(L_1, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = __this->get_ListObj_18();
		NullCheck(L_2);
		GameObject_SetActive_m2887581199(L_2, (bool)1, /*hidden argument*/NULL);
		Camera_t189460977 * L_3 = __this->get_myMain_26();
		NullCheck(L_3);
		Behaviour_set_enabled_m1796096907(L_3, (bool)0, /*hidden argument*/NULL);
		Camera_t189460977 * L_4 = __this->get_victoryCam_27();
		NullCheck(L_4);
		Behaviour_set_enabled_m1796096907(L_4, (bool)1, /*hidden argument*/NULL);
		String_t* L_5 = __this->get_first_20();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_5, _stringLiteral3021629811, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_009a;
		}
	}
	{
		GameObject_t1756533147 * L_7 = __this->get_victoryObj_28();
		NullCheck(L_7);
		Transform_t3275118058 * L_8 = GameObject_get_transform_m909382139(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = Transform_GetChild_m3838588184(L_8, 0, /*hidden argument*/NULL);
		NullCheck(L_9);
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_SetActive_m2887581199(L_10, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = __this->get_victoryObj_28();
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_t3275118058 * L_13 = Transform_GetChild_m3838588184(L_12, 0, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_t3275118058 * L_14 = Transform_GetChild_m3838588184(L_13, 0, /*hidden argument*/NULL);
		NullCheck(L_14);
		Animator_t69676727 * L_15 = Component_GetComponent_TisAnimator_t69676727_m475627522(L_14, /*hidden argument*/Component_GetComponent_TisAnimator_t69676727_m475627522_MethodInfo_var);
		V_0 = L_15;
		Animator_t69676727 * L_16 = V_0;
		NullCheck(L_16);
		Animator_Play_m4142158565(L_16, _stringLiteral272007032, (-1), /*hidden argument*/NULL);
		goto IL_01b2;
	}

IL_009a:
	{
		String_t* L_17 = __this->get_first_20();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_18 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_17, _stringLiteral2395476974, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00f9;
		}
	}
	{
		GameObject_t1756533147 * L_19 = __this->get_victoryObj_28();
		NullCheck(L_19);
		Transform_t3275118058 * L_20 = GameObject_get_transform_m909382139(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_t3275118058 * L_21 = Transform_GetChild_m3838588184(L_20, 1, /*hidden argument*/NULL);
		NullCheck(L_21);
		GameObject_t1756533147 * L_22 = Component_get_gameObject_m3105766835(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		GameObject_SetActive_m2887581199(L_22, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_23 = __this->get_victoryObj_28();
		NullCheck(L_23);
		Transform_t3275118058 * L_24 = GameObject_get_transform_m909382139(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		Transform_t3275118058 * L_25 = Transform_GetChild_m3838588184(L_24, 0, /*hidden argument*/NULL);
		NullCheck(L_25);
		Transform_t3275118058 * L_26 = Transform_GetChild_m3838588184(L_25, 1, /*hidden argument*/NULL);
		NullCheck(L_26);
		Animator_t69676727 * L_27 = Component_GetComponent_TisAnimator_t69676727_m475627522(L_26, /*hidden argument*/Component_GetComponent_TisAnimator_t69676727_m475627522_MethodInfo_var);
		V_1 = L_27;
		Animator_t69676727 * L_28 = V_1;
		NullCheck(L_28);
		Animator_Play_m4142158565(L_28, _stringLiteral272007032, (-1), /*hidden argument*/NULL);
		goto IL_01b2;
	}

IL_00f9:
	{
		String_t* L_29 = __this->get_first_20();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_30 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_29, _stringLiteral777220966, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_0158;
		}
	}
	{
		GameObject_t1756533147 * L_31 = __this->get_victoryObj_28();
		NullCheck(L_31);
		Transform_t3275118058 * L_32 = GameObject_get_transform_m909382139(L_31, /*hidden argument*/NULL);
		NullCheck(L_32);
		Transform_t3275118058 * L_33 = Transform_GetChild_m3838588184(L_32, 2, /*hidden argument*/NULL);
		NullCheck(L_33);
		GameObject_t1756533147 * L_34 = Component_get_gameObject_m3105766835(L_33, /*hidden argument*/NULL);
		NullCheck(L_34);
		GameObject_SetActive_m2887581199(L_34, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_35 = __this->get_victoryObj_28();
		NullCheck(L_35);
		Transform_t3275118058 * L_36 = GameObject_get_transform_m909382139(L_35, /*hidden argument*/NULL);
		NullCheck(L_36);
		Transform_t3275118058 * L_37 = Transform_GetChild_m3838588184(L_36, 2, /*hidden argument*/NULL);
		NullCheck(L_37);
		Transform_t3275118058 * L_38 = Transform_GetChild_m3838588184(L_37, 0, /*hidden argument*/NULL);
		NullCheck(L_38);
		Animator_t69676727 * L_39 = Component_GetComponent_TisAnimator_t69676727_m475627522(L_38, /*hidden argument*/Component_GetComponent_TisAnimator_t69676727_m475627522_MethodInfo_var);
		V_2 = L_39;
		Animator_t69676727 * L_40 = V_2;
		NullCheck(L_40);
		Animator_Play_m4142158565(L_40, _stringLiteral272007032, (-1), /*hidden argument*/NULL);
		goto IL_01b2;
	}

IL_0158:
	{
		String_t* L_41 = __this->get_first_20();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_42 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_41, _stringLiteral3510846499, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_01b2;
		}
	}
	{
		GameObject_t1756533147 * L_43 = __this->get_victoryObj_28();
		NullCheck(L_43);
		Transform_t3275118058 * L_44 = GameObject_get_transform_m909382139(L_43, /*hidden argument*/NULL);
		NullCheck(L_44);
		Transform_t3275118058 * L_45 = Transform_GetChild_m3838588184(L_44, 3, /*hidden argument*/NULL);
		NullCheck(L_45);
		GameObject_t1756533147 * L_46 = Component_get_gameObject_m3105766835(L_45, /*hidden argument*/NULL);
		NullCheck(L_46);
		GameObject_SetActive_m2887581199(L_46, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_47 = __this->get_victoryObj_28();
		NullCheck(L_47);
		Transform_t3275118058 * L_48 = GameObject_get_transform_m909382139(L_47, /*hidden argument*/NULL);
		NullCheck(L_48);
		Transform_t3275118058 * L_49 = Transform_GetChild_m3838588184(L_48, 3, /*hidden argument*/NULL);
		NullCheck(L_49);
		Transform_t3275118058 * L_50 = Transform_GetChild_m3838588184(L_49, 0, /*hidden argument*/NULL);
		NullCheck(L_50);
		Animator_t69676727 * L_51 = Component_GetComponent_TisAnimator_t69676727_m475627522(L_50, /*hidden argument*/Component_GetComponent_TisAnimator_t69676727_m475627522_MethodInfo_var);
		V_3 = L_51;
		Animator_t69676727 * L_52 = V_3;
		NullCheck(L_52);
		Animator_Play_m4142158565(L_52, _stringLiteral272007032, (-1), /*hidden argument*/NULL);
	}

IL_01b2:
	{
		List_1_t3423108555 * L_53 = __this->get_listOfPlayers_2();
		NullCheck(L_53);
		Enumerator_t2957838229  L_54 = List_1_GetEnumerator_m527309189(L_53, /*hidden argument*/List_1_GetEnumerator_m527309189_MethodInfo_var);
		V_5 = L_54;
	}

IL_01bf:
	try
	{ // begin try (depth: 1)
		{
			goto IL_01e0;
		}

IL_01c4:
		{
			Color_Manager_Script_t4053987423 * L_55 = Enumerator_get_Current_m1406163925((&V_5), /*hidden argument*/Enumerator_get_Current_m1406163925_MethodInfo_var);
			V_4 = L_55;
			Color_Manager_Script_t4053987423 * L_56 = V_4;
			NullCheck(L_56);
			bool L_57 = L_56->get_haveWon_16();
			if (L_57)
			{
				goto IL_01e0;
			}
		}

IL_01d9:
		{
			Color_Manager_Script_t4053987423 * L_58 = V_4;
			NullCheck(L_58);
			Color_Manager_Script_ILOST_m326932739(L_58, /*hidden argument*/NULL);
		}

IL_01e0:
		{
			bool L_59 = Enumerator_MoveNext_m1721688489((&V_5), /*hidden argument*/Enumerator_MoveNext_m1721688489_MethodInfo_var);
			if (L_59)
			{
				goto IL_01c4;
			}
		}

IL_01ec:
		{
			IL2CPP_LEAVE(0x1FF, FINALLY_01f1);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_01f1;
	}

FINALLY_01f1:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2781297319((&V_5), /*hidden argument*/Enumerator_Dispose_m2781297319_MethodInfo_var);
		IL2CPP_END_FINALLY(497)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(497)
	{
		IL2CPP_JUMP_TBL(0x1FF, IL_01ff)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_01ff:
	{
		int32_t L_60 = __this->get_wonPos_15();
		if ((!(((uint32_t)L_60) == ((uint32_t)1))))
		{
			goto IL_029e;
		}
	}
	{
		GameObject_t1756533147 * L_61 = __this->get_ListObj_18();
		NullCheck(L_61);
		Transform_t3275118058 * L_62 = GameObject_get_transform_m909382139(L_61, /*hidden argument*/NULL);
		NullCheck(L_62);
		Transform_t3275118058 * L_63 = Transform_GetChild_m3838588184(L_62, 0, /*hidden argument*/NULL);
		NullCheck(L_63);
		GameObject_t1756533147 * L_64 = Component_get_gameObject_m3105766835(L_63, /*hidden argument*/NULL);
		NullCheck(L_64);
		GameObject_SetActive_m2887581199(L_64, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_65 = __this->get_ListObj_18();
		NullCheck(L_65);
		Transform_t3275118058 * L_66 = GameObject_get_transform_m909382139(L_65, /*hidden argument*/NULL);
		NullCheck(L_66);
		Transform_t3275118058 * L_67 = Transform_GetChild_m3838588184(L_66, 0, /*hidden argument*/NULL);
		NullCheck(L_67);
		Text_t356221433 * L_68 = Component_GetComponent_TisText_t356221433_m1342661039(L_67, /*hidden argument*/Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var);
		String_t* L_69 = __this->get_first_20();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_70 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral189104851, L_69, /*hidden argument*/NULL);
		NullCheck(L_68);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_68, L_70);
		GameObject_t1756533147 * L_71 = __this->get_ListObj_18();
		NullCheck(L_71);
		Transform_t3275118058 * L_72 = GameObject_get_transform_m909382139(L_71, /*hidden argument*/NULL);
		NullCheck(L_72);
		Transform_t3275118058 * L_73 = Transform_GetChild_m3838588184(L_72, 1, /*hidden argument*/NULL);
		NullCheck(L_73);
		GameObject_t1756533147 * L_74 = Component_get_gameObject_m3105766835(L_73, /*hidden argument*/NULL);
		NullCheck(L_74);
		GameObject_SetActive_m2887581199(L_74, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_75 = __this->get_ListObj_18();
		NullCheck(L_75);
		Transform_t3275118058 * L_76 = GameObject_get_transform_m909382139(L_75, /*hidden argument*/NULL);
		NullCheck(L_76);
		Transform_t3275118058 * L_77 = Transform_GetChild_m3838588184(L_76, 1, /*hidden argument*/NULL);
		NullCheck(L_77);
		Text_t356221433 * L_78 = Component_GetComponent_TisText_t356221433_m1342661039(L_77, /*hidden argument*/Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var);
		String_t* L_79 = __this->get_second_21();
		String_t* L_80 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral702718481, L_79, /*hidden argument*/NULL);
		NullCheck(L_78);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_78, L_80);
		goto IL_04ac;
	}

IL_029e:
	{
		int32_t L_81 = __this->get_wonPos_15();
		if ((!(((uint32_t)L_81) == ((uint32_t)2))))
		{
			goto IL_0384;
		}
	}
	{
		GameObject_t1756533147 * L_82 = __this->get_ListObj_18();
		NullCheck(L_82);
		Transform_t3275118058 * L_83 = GameObject_get_transform_m909382139(L_82, /*hidden argument*/NULL);
		NullCheck(L_83);
		Transform_t3275118058 * L_84 = Transform_GetChild_m3838588184(L_83, 0, /*hidden argument*/NULL);
		NullCheck(L_84);
		GameObject_t1756533147 * L_85 = Component_get_gameObject_m3105766835(L_84, /*hidden argument*/NULL);
		NullCheck(L_85);
		GameObject_SetActive_m2887581199(L_85, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_86 = __this->get_ListObj_18();
		NullCheck(L_86);
		Transform_t3275118058 * L_87 = GameObject_get_transform_m909382139(L_86, /*hidden argument*/NULL);
		NullCheck(L_87);
		Transform_t3275118058 * L_88 = Transform_GetChild_m3838588184(L_87, 0, /*hidden argument*/NULL);
		NullCheck(L_88);
		Text_t356221433 * L_89 = Component_GetComponent_TisText_t356221433_m1342661039(L_88, /*hidden argument*/Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var);
		String_t* L_90 = __this->get_first_20();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_91 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral189104851, L_90, /*hidden argument*/NULL);
		NullCheck(L_89);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_89, L_91);
		GameObject_t1756533147 * L_92 = __this->get_ListObj_18();
		NullCheck(L_92);
		Transform_t3275118058 * L_93 = GameObject_get_transform_m909382139(L_92, /*hidden argument*/NULL);
		NullCheck(L_93);
		Transform_t3275118058 * L_94 = Transform_GetChild_m3838588184(L_93, 1, /*hidden argument*/NULL);
		NullCheck(L_94);
		GameObject_t1756533147 * L_95 = Component_get_gameObject_m3105766835(L_94, /*hidden argument*/NULL);
		NullCheck(L_95);
		GameObject_SetActive_m2887581199(L_95, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_96 = __this->get_ListObj_18();
		NullCheck(L_96);
		Transform_t3275118058 * L_97 = GameObject_get_transform_m909382139(L_96, /*hidden argument*/NULL);
		NullCheck(L_97);
		Transform_t3275118058 * L_98 = Transform_GetChild_m3838588184(L_97, 1, /*hidden argument*/NULL);
		NullCheck(L_98);
		Text_t356221433 * L_99 = Component_GetComponent_TisText_t356221433_m1342661039(L_98, /*hidden argument*/Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var);
		String_t* L_100 = __this->get_second_21();
		String_t* L_101 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1601026680, L_100, /*hidden argument*/NULL);
		NullCheck(L_99);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_99, L_101);
		GameObject_t1756533147 * L_102 = __this->get_ListObj_18();
		NullCheck(L_102);
		Transform_t3275118058 * L_103 = GameObject_get_transform_m909382139(L_102, /*hidden argument*/NULL);
		NullCheck(L_103);
		Transform_t3275118058 * L_104 = Transform_GetChild_m3838588184(L_103, 2, /*hidden argument*/NULL);
		NullCheck(L_104);
		GameObject_t1756533147 * L_105 = Component_get_gameObject_m3105766835(L_104, /*hidden argument*/NULL);
		NullCheck(L_105);
		GameObject_SetActive_m2887581199(L_105, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_106 = __this->get_ListObj_18();
		NullCheck(L_106);
		Transform_t3275118058 * L_107 = GameObject_get_transform_m909382139(L_106, /*hidden argument*/NULL);
		NullCheck(L_107);
		Transform_t3275118058 * L_108 = Transform_GetChild_m3838588184(L_107, 2, /*hidden argument*/NULL);
		NullCheck(L_108);
		Text_t356221433 * L_109 = Component_GetComponent_TisText_t356221433_m1342661039(L_108, /*hidden argument*/Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var);
		String_t* L_110 = __this->get_third_22();
		String_t* L_111 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral702718481, L_110, /*hidden argument*/NULL);
		NullCheck(L_109);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_109, L_111);
		goto IL_04ac;
	}

IL_0384:
	{
		int32_t L_112 = __this->get_wonPos_15();
		if ((!(((uint32_t)L_112) == ((uint32_t)3))))
		{
			goto IL_04ac;
		}
	}
	{
		GameObject_t1756533147 * L_113 = __this->get_ListObj_18();
		NullCheck(L_113);
		Transform_t3275118058 * L_114 = GameObject_get_transform_m909382139(L_113, /*hidden argument*/NULL);
		NullCheck(L_114);
		Transform_t3275118058 * L_115 = Transform_GetChild_m3838588184(L_114, 0, /*hidden argument*/NULL);
		NullCheck(L_115);
		GameObject_t1756533147 * L_116 = Component_get_gameObject_m3105766835(L_115, /*hidden argument*/NULL);
		NullCheck(L_116);
		GameObject_SetActive_m2887581199(L_116, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_117 = __this->get_ListObj_18();
		NullCheck(L_117);
		Transform_t3275118058 * L_118 = GameObject_get_transform_m909382139(L_117, /*hidden argument*/NULL);
		NullCheck(L_118);
		Transform_t3275118058 * L_119 = Transform_GetChild_m3838588184(L_118, 0, /*hidden argument*/NULL);
		NullCheck(L_119);
		Text_t356221433 * L_120 = Component_GetComponent_TisText_t356221433_m1342661039(L_119, /*hidden argument*/Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var);
		String_t* L_121 = __this->get_first_20();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_122 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral189104851, L_121, /*hidden argument*/NULL);
		NullCheck(L_120);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_120, L_122);
		GameObject_t1756533147 * L_123 = __this->get_ListObj_18();
		NullCheck(L_123);
		Transform_t3275118058 * L_124 = GameObject_get_transform_m909382139(L_123, /*hidden argument*/NULL);
		NullCheck(L_124);
		Transform_t3275118058 * L_125 = Transform_GetChild_m3838588184(L_124, 1, /*hidden argument*/NULL);
		NullCheck(L_125);
		GameObject_t1756533147 * L_126 = Component_get_gameObject_m3105766835(L_125, /*hidden argument*/NULL);
		NullCheck(L_126);
		GameObject_SetActive_m2887581199(L_126, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_127 = __this->get_ListObj_18();
		NullCheck(L_127);
		Transform_t3275118058 * L_128 = GameObject_get_transform_m909382139(L_127, /*hidden argument*/NULL);
		NullCheck(L_128);
		Transform_t3275118058 * L_129 = Transform_GetChild_m3838588184(L_128, 1, /*hidden argument*/NULL);
		NullCheck(L_129);
		Text_t356221433 * L_130 = Component_GetComponent_TisText_t356221433_m1342661039(L_129, /*hidden argument*/Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var);
		String_t* L_131 = __this->get_second_21();
		String_t* L_132 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1601026680, L_131, /*hidden argument*/NULL);
		NullCheck(L_130);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_130, L_132);
		GameObject_t1756533147 * L_133 = __this->get_ListObj_18();
		NullCheck(L_133);
		Transform_t3275118058 * L_134 = GameObject_get_transform_m909382139(L_133, /*hidden argument*/NULL);
		NullCheck(L_134);
		Transform_t3275118058 * L_135 = Transform_GetChild_m3838588184(L_134, 2, /*hidden argument*/NULL);
		NullCheck(L_135);
		GameObject_t1756533147 * L_136 = Component_get_gameObject_m3105766835(L_135, /*hidden argument*/NULL);
		NullCheck(L_136);
		GameObject_SetActive_m2887581199(L_136, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_137 = __this->get_ListObj_18();
		NullCheck(L_137);
		Transform_t3275118058 * L_138 = GameObject_get_transform_m909382139(L_137, /*hidden argument*/NULL);
		NullCheck(L_138);
		Transform_t3275118058 * L_139 = Transform_GetChild_m3838588184(L_138, 2, /*hidden argument*/NULL);
		NullCheck(L_139);
		Text_t356221433 * L_140 = Component_GetComponent_TisText_t356221433_m1342661039(L_139, /*hidden argument*/Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var);
		String_t* L_141 = __this->get_third_22();
		String_t* L_142 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2274690421, L_141, /*hidden argument*/NULL);
		NullCheck(L_140);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_140, L_142);
		GameObject_t1756533147 * L_143 = __this->get_ListObj_18();
		NullCheck(L_143);
		Transform_t3275118058 * L_144 = GameObject_get_transform_m909382139(L_143, /*hidden argument*/NULL);
		NullCheck(L_144);
		Transform_t3275118058 * L_145 = Transform_GetChild_m3838588184(L_144, 3, /*hidden argument*/NULL);
		NullCheck(L_145);
		GameObject_t1756533147 * L_146 = Component_get_gameObject_m3105766835(L_145, /*hidden argument*/NULL);
		NullCheck(L_146);
		GameObject_SetActive_m2887581199(L_146, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_147 = __this->get_ListObj_18();
		NullCheck(L_147);
		Transform_t3275118058 * L_148 = GameObject_get_transform_m909382139(L_147, /*hidden argument*/NULL);
		NullCheck(L_148);
		Transform_t3275118058 * L_149 = Transform_GetChild_m3838588184(L_148, 3, /*hidden argument*/NULL);
		NullCheck(L_149);
		Text_t356221433 * L_150 = Component_GetComponent_TisText_t356221433_m1342661039(L_149, /*hidden argument*/Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var);
		String_t* L_151 = __this->get_fourth_23();
		String_t* L_152 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral702718481, L_151, /*hidden argument*/NULL);
		NullCheck(L_150);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_150, L_152);
	}

IL_04ac:
	{
		return;
	}
}
// System.Void WorldPos::.ctor()
extern "C"  void WorldPos__ctor_m2110560133 (WorldPos_t3236901198 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
