﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// myFBHolder
struct myFBHolder_t218619452;
// Facebook.Unity.ILoginResult
struct ILoginResult_t403585443;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// Facebook.Unity.IGraphResult
struct IGraphResult_t3984946686;
// Facebook.Unity.IShareResult
struct IShareResult_t830127229;
// Facebook.Unity.IAppInviteResult
struct IAppInviteResult_t3529555166;
// Facebook.Unity.IAppRequestResult
struct IAppRequestResult_t1874118006;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"

// System.Void myFBHolder::.ctor()
extern "C"  void myFBHolder__ctor_m364838827 (myFBHolder_t218619452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void myFBHolder::Awake()
extern "C"  void myFBHolder_Awake_m2771421800 (myFBHolder_t218619452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void myFBHolder::SetInit()
extern "C"  void myFBHolder_SetInit_m2170005075 (myFBHolder_t218619452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void myFBHolder::OnHideUnity(System.Boolean)
extern "C"  void myFBHolder_OnHideUnity_m4139554744 (myFBHolder_t218619452 * __this, bool ___isGameShown0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void myFBHolder::FBLogin()
extern "C"  void myFBHolder_FBLogin_m251664878 (myFBHolder_t218619452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void myFBHolder::AuthCallback(Facebook.Unity.ILoginResult)
extern "C"  void myFBHolder_AuthCallback_m1876299558 (myFBHolder_t218619452 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void myFBHolder::DealWithFBMenus(System.Boolean)
extern "C"  void myFBHolder_DealWithFBMenus_m2740628268 (myFBHolder_t218619452 * __this, bool ___isLoggedIn0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String myFBHolder::GetPictureURL(System.String,System.Nullable`1<System.Int32>,System.Nullable`1<System.Int32>,System.String)
extern "C"  String_t* myFBHolder_GetPictureURL_m1870067007 (Il2CppObject * __this /* static, unused */, String_t* ___facebookID0, Nullable_1_t334943763  ___width1, Nullable_1_t334943763  ___height2, String_t* ___type3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> myFBHolder::DeserializeJSONProfile(System.String)
extern "C"  Dictionary_2_t3943999495 * myFBHolder_DeserializeJSONProfile_m301719529 (Il2CppObject * __this /* static, unused */, String_t* ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void myFBHolder::DealWithProfilePic(Facebook.Unity.IGraphResult)
extern "C"  void myFBHolder_DealWithProfilePic_m2902463055 (myFBHolder_t218619452 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void myFBHolder::DealWithUserName(Facebook.Unity.IGraphResult)
extern "C"  void myFBHolder_DealWithUserName_m1764054732 (myFBHolder_t218619452 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void myFBHolder::Share()
extern "C"  void myFBHolder_Share_m2553693052 (myFBHolder_t218619452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void myFBHolder::ShareCallBack(Facebook.Unity.IShareResult)
extern "C"  void myFBHolder_ShareCallBack_m794070025 (myFBHolder_t218619452 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void myFBHolder::InviteFriends()
extern "C"  void myFBHolder_InviteFriends_m1616796235 (myFBHolder_t218619452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void myFBHolder::InviteCallBack(Facebook.Unity.IAppInviteResult)
extern "C"  void myFBHolder_InviteCallBack_m824578806 (myFBHolder_t218619452 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void myFBHolder::Challenge()
extern "C"  void myFBHolder_Challenge_m2613916774 (myFBHolder_t218619452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void myFBHolder::DealWithChallenge(Facebook.Unity.IAppRequestResult)
extern "C"  void myFBHolder_DealWithChallenge_m197732585 (myFBHolder_t218619452 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void myFBHolder::OpenFBPage()
extern "C"  void myFBHolder_OpenFBPage_m1300852820 (myFBHolder_t218619452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
