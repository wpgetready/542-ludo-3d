﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Turn_Manager
struct Turn_Manager_t2388373579;
// Color_Manager_Script
struct Color_Manager_Script_t4053987423;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Color_Manager_Script4053987423.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Turn_Manager::.ctor()
extern "C"  void Turn_Manager__ctor_m1619170102 (Turn_Manager_t2388373579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Turn_Manager::Start()
extern "C"  void Turn_Manager_Start_m1946345482 (Turn_Manager_t2388373579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Turn_Manager::Update()
extern "C"  void Turn_Manager_Update_m1635222775 (Turn_Manager_t2388373579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Turn_Manager::SetCurrentTurnTo(Color_Manager_Script,System.Int32)
extern "C"  void Turn_Manager_SetCurrentTurnTo_m2407535811 (Turn_Manager_t2388373579 * __this, Color_Manager_Script_t4053987423 * ___cms0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Turn_Manager::GiveNotification(System.String)
extern "C"  void Turn_Manager_GiveNotification_m239101948 (Turn_Manager_t2388373579 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Turn_Manager::PerformNextTurn()
extern "C"  void Turn_Manager_PerformNextTurn_m2187029365 (Turn_Manager_t2388373579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Turn_Manager::RepeatTurn()
extern "C"  void Turn_Manager_RepeatTurn_m1002454660 (Turn_Manager_t2388373579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Turn_Manager::DeactivateButton()
extern "C"  void Turn_Manager_DeactivateButton_m2793329282 (Turn_Manager_t2388373579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Turn_Manager::StopDice()
extern "C"  void Turn_Manager_StopDice_m2156974429 (Turn_Manager_t2388373579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Turn_Manager::RollTheDice()
extern "C"  void Turn_Manager_RollTheDice_m3432382429 (Turn_Manager_t2388373579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Turn_Manager::AIRoll()
extern "C"  void Turn_Manager_AIRoll_m382572419 (Turn_Manager_t2388373579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Turn_Manager::PauseGame()
extern "C"  void Turn_Manager_PauseGame_m3660233854 (Turn_Manager_t2388373579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Turn_Manager::BTM()
extern "C"  void Turn_Manager_BTM_m4220530601 (Turn_Manager_t2388373579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Turn_Manager::Replay()
extern "C"  void Turn_Manager_Replay_m1145465645 (Turn_Manager_t2388373579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Turn_Manager::ShowGameCompletion()
extern "C"  void Turn_Manager_ShowGameCompletion_m1622668167 (Turn_Manager_t2388373579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
