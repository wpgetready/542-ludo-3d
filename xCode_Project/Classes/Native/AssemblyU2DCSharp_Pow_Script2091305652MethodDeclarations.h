﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pow_Script
struct Pow_Script_t2091305652;

#include "codegen/il2cpp-codegen.h"

// System.Void Pow_Script::.ctor()
extern "C"  void Pow_Script__ctor_m487179051 (Pow_Script_t2091305652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pow_Script::Start()
extern "C"  void Pow_Script_Start_m3582558687 (Pow_Script_t2091305652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pow_Script::Update()
extern "C"  void Pow_Script_Update_m787413554 (Pow_Script_t2091305652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
