﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// admob.AdSize
struct AdSize_t3770813302;

#include "codegen/il2cpp-codegen.h"

// System.Void admob.AdSize::.ctor(System.Int32,System.Int32)
extern "C"  void AdSize__ctor_m3068073238 (AdSize_t3770813302 * __this, int32_t ___width0, int32_t ___height1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 admob.AdSize::get_Width()
extern "C"  int32_t AdSize_get_Width_m317946431 (AdSize_t3770813302 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 admob.AdSize::get_Height()
extern "C"  int32_t AdSize_get_Height_m955561220 (AdSize_t3770813302 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void admob.AdSize::.cctor()
extern "C"  void AdSize__cctor_m3201987233 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
